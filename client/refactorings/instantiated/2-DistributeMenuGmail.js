﻿var metadata = {"author":"Sergio Firmenich", "name":"Distribute Menu for Gmail's Inbox", "description":"This refactoring distribute the mark as spam, archive and delete operations in all the listed emails","id":"distributeMenu-mainGmail-sfirmenich"};

function getAccessibilityAugmenter(){
	try{
		var ref = new GMailDistributeMenu();
	}catch(e){
		alert(e);
	}
	return ref
};

function GMailDistributeMenu(){

};

GMailDistributeMenu.prototype = new AbstractInstanceRefactoring();

GMailDistributeMenu.prototype.setTargetURLs = function(){
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?&?(st=)?[0-9]*$/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?&?(st=)?[0-9]*$/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?f=[\w\W]*&?(st=)?[0-9]*/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?gausr=[\w\W]*&?(st=)?[0-9]*/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?auth=[\w\W]*&?(st=)?[0-9]*/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?shva=[\w\W]*&?(st=)?[0-9]*/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/&?(st=)?[0-9]*$/);		
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?pli=[\w\W]*&?(st=)?[0-9]*/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?#&?(st=)?[0-9]*$/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?st=[\w\W]*&?(st=)?[0-9]*/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?zy=[\w\W]*&?(st=)?[0-9]*/);
};			

GMailDistributeMenu.prototype.initialize = function(language){		
		var refactoring = "Asd";
		var refactoring = new DistributeMenu.DistributeMenu("GMAIL WebMail");

		refactoring.setItemXpath(".//table[@class='th']/tbody/tr");
		refactoring.setCheckBoxRelativePath("./td[4]/input");
											  
		var archive_operation = new DistributeMenu.DistributedOperation("Archivar",refactoring);
		archive_operation.setAction(".//input[@name='nvp_a_arch']");
		
		var spam_operation = new DistributeMenu.DistributedOperation("Spam",refactoring);
		spam_operation.setAction(".//input[@name='nvp_a_sp']");		
		
		var delete_operation = new DistributeMenu.DistributedOperation("Eliminar",refactoring);
		delete_operation.setAction(".//input[@name='nvp_a_tr']");

		this.abstract_refactoring = refactoring;
};
