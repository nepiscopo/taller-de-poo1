/*
 * ApplicationFamily abstracts a set of applications sharing the same concepts.
 */
function ApplicationFamily(){
/*
 * Es el constructor de la clase ApplicationFamily.
 * Representa a una familia de aplicacioes web.
 */	
	this.description = "";
	this.name = "";
	this.applications = [];
};
ApplicationFamily.prototype.addApplication = function(application){
	this.applications.push(application);
};

/**************************************
 * Application
 */
function Application(modding_interface,uri){
/*
 * Es el constructor de la clase Application.
 * Representa a una aplicacion web en particular.
 */	
	this.URI = uri;
	this.modding_interface = modding_interface;
	this.adaptationsAsSource = [];
	this.adaptationsAsTarget = [];
};

/*
 * Agrega un modding interface a la aplicacion.
 */
Application.prototype.addModdingInterface = function(modding_interface){
	this.modding_interface = (modding_interface);
	modding_interface.setApplication(this);
};

Application.prototype.getURIPattern = function(){
	return  this.URI
};

/*
 * Devuelve el documento mostrado en el browser si es el correspondiente al de la aplicacion.
 * Devuelve null si el documento mostrado en el browser no corresponde al de la aplicacion.
 */
Application.prototype.getDocument = function(){	
	return (content.document.location.host == this.URI)? content.document:null;
};

/*****
 * Deprecated: based on old version for GreaseMonkey CSN
 * Agrega una instancia de adaptation a la aplicación.
 */
Application.prototype.addSourceAdaptation = function(adaptation){
	this.adaptationsAsSource[this.adaptationsAsSource.length] = adaptation;	
};

/*****
 * Deprecated: based on old version for GreaseMonkey CSN
 * Devuelve las adaptaciones CSN cuya navegacion tiene como nodo source la aplicacion.
 */
Application.prototype.getSourceAdaptations = function(){
	return this.adaptationsAsSource;
};

/*****
 * Deprecated: based on old version for GreaseMonkey CSN
 * Devuelve las adaptaciones CSN cuya navegacion tiene como nodo target la aplicacion.
 */
Application.prototype.getTargetAdaptations = function(){
	return this.adaptationsAsTarget;
};
