/***********************************************************
 * DOMElementWrapper
 * Es la superclase de la que heredan que ConceptDOMWrapper y PropertytDOMWrapper 
 **********************************************************/
function DOMElementWrapper(){
	this.application = null;	
};

DOMElementWrapper.prototype.getDocument = function(){
	return this.getApplication().getDocument();
};

DOMElementWrapper.prototype.getApplication = function(){
	return this.application;
};

DOMElementWrapper.prototype.setApplication = function(application_node){
	this.application = application_node;
};

/***********************************************************
 * ConceptDOMWrapper
 * Es la clase que wrappea un concepto abstracto y lo relaciona para una
 * aplicacion determinada *
 **********************************************************/
function ConceptDOMWrapper(concept,application_node,dom_data){	
	this.class_name = dom_data;
	this.conceptWrapped = concept;
	this.setApplication(application_node);
};

//Concept es subclase de ModelElement
ConceptDOMWrapper.prototype = new DOMElementWrapper();

ConceptDOMWrapper.prototype.getWrappedElement = function(){	
	return this.conceptWrapped;
};

ConceptDOMWrapper.prototype.getWrapperDOMInfo = function(){	
	return this.class_name;
};

ConceptDOMWrapper.prototype.getDOMInstances = function(){	
	var doc = this.getDocument();
	return doc.getElementsByClassName(this.getWrapperDOMInfo());
};

/***********************************************************
 * PropertyDOMWrapper
 * Es la clase que wrappea una propiedad abstracta y lo relaciona para una
 * aplicacion determinada
 **********************************************************/
function PropertyDOMWrapper(property,application,dom_data){	
	this.relativeXPath = dom_data;
	this.propertyWrapped = property;
	this.setApplication(application);
};

//Property es subclase de ModelElement
PropertyDOMWrapper.prototype = new DOMElementWrapper();

PropertyDOMWrapper.prototype.getWrapperDOMInfo = function(){	
	return this.relativeXPath;
};
