function ModdingInterface(){
	this.concepts = {};
	this.application = null;
};

ModdingInterface.prototype.setApplication = function(application){	
	this.application = application;
};

ModdingInterface.prototype.getApplication = function(){
	return this.application;
};

ModdingInterface.prototype.getDocument = function(element){
	return this.application.getDocument();
};

ModdingInterface.prototype.createConcept = function(xpath){	
	var returnedValue = window.openDialog("chrome://cs-adaptation/content/moddingInterfaceMaker/createConcept.xul","window-add-element","",xpath,this,CSA,SuitCaseManager);
};

ModdingInterface.prototype.basicAddConcept = function(concept,dom_info){	
	this.concepts[dom_info] = concept;
};

ModdingInterface.prototype.addConcept = function(concept,dom_info){
	if((typeof(this.concepts[dom_info]) == "undefined") || ((typeof(this.concepts[dom_info]) != "undefined") && (concept.getConceptName() == this.concepts[dom_info].getConceptName()))){
		this.concepts[dom_info] = concept;
		concept.setModdingInterface(this,dom_info);
	}
	else{
		var response = confirm("Ya existe este concepto. ¿Desea reemplazarlo?");
		if (response){
			this.concepts[dom_info] = concept;
			concept.setModdingInterface(this,dom_info);
		}
	}
};

ModdingInterface.prototype.addWrappedConcept = function(wrappedConcept){
	var concept = wrappedConcept.getWrappedElement();
	concept.basicAddDOMElement(wrappedConcept);
	this.concepts[wrappedConcept.getWrapperDOMInfo()] = concept;
	concept.addToModel(this);
};

ModdingInterface.prototype.getConcepts = function(){
	return this.concepts;
};

ModdingInterface.prototype.getCantConcepts = function(){
	var length = 0;
	for (var object in this.concepts)
		length++;
	return length;
};

ModdingInterface.prototype.getConceptByName = function(concept_name){
	for (concept in this.concepts){
		if (this.concepts[concept].concept_name == concept_name)
			return this.concepts[concept]; 
	}
	return null;
};

ModdingInterface.prototype.serialize = function(){
	var json = {};
	for (concept in this.concepts)
		json[this.concepts[concept].getConceptName()] = this.concepts[concept].serialize(this.getApplication());
	return json;
}
/***********************************************************
 * ModelElement
 * Es la superclase de Concept y Property.
 * Es necesario porque es la que maneja que un concepto puede aparecer
 * en varios nodos distintos.
 **********************************************************/
function ModelElement(){
	this.wrappers = [];
};

ModelElement.prototype.basicAddDOMElement = function(wrapper,app){	
	this.wrappers[this.wrappers.length] = wrapper;	
	if(typeof(app) != "undefined")
		wrapper.setApplication(app);
};

ModelElement.prototype.addDOMElement = function(application_node,dom_data){	
	var newWrapper = this.createWrapper(application_node,dom_data);		
	this.wrappers[this.wrappers.length] = newWrapper;	
};


ModelElement.prototype.getDOMWrappers = function(){
	return this.wrappers;
};

ModelElement.prototype.getDOMInfo = function(application){
	var wrapper = this.getDOMWrapperForApplication(application);
	return wrapper.getWrapperDOMInfo();
};

ModelElement.prototype.getDOMWrapperForApplication = function(app){
	for (var i=0;i < this.wrappers.length;i++)
		if (this.wrappers[i].getApplication() == app)
			return this.wrappers[i];
	return false;		
};


/***********************************************************
 * Concept
 **********************************************************/
function Concept(conceptname){
	this.concept_name = conceptname;
	//DeprecatedProperty: class_name, ya no tiene que ir aca, va en el cada wrapper del concepto para cada 
	//aplicacion que tenga la misma modding interface o que en su modding interface incluya el concepto.
	this.class_name = null;
	
	//DeprecatedProperty: Debe analizarse la continuidad de eventos conceptuales
	this.conceptual_events = [];
	this.properties = [];
	this.modding_interface = [];
};

//Concept es subclase de ModelElement
Concept.prototype = new ModelElement();

Concept.prototype.createWrapper = function(application_node,dom_data){
	var wrapper = new ConceptDOMWrapper(this,application_node,dom_data);
	return wrapper;
};

Concept.prototype.addToModel = function(modding_interface){	
	this.modding_interface[this.modding_interface.length] = modding_interface;
};

Concept.prototype.setModdingInterface = function(modding_interface,dom_info){
	//En dom_info recive la informacion referida al concepto para la aplicacion correspondiente
	//a este modding interface.
	this.modding_interface[this.modding_interface.length] = modding_interface;	
	var application = modding_interface.getApplication();
	this.addDOMElement(application,dom_info);
};

Concept.prototype.getModdingInterface = function(){
	//RFC: el indice [0] esta a causa del refactoring, de hecho el metodo getModdingInterface
	//posiblemente no sea necesario de aca en mas.
	//Esto deberia devolver el modding interface de la aplicacion actual. 
	return this.modding_interface[0];
};

Concept.prototype.getCurrentModdingInterface = function(){
	//Esto es un getter de getModdingInterface.
	return this.getModdingInterface();
};

Concept.prototype.getApplication = function(){
	return this.getCurrentModdingInterface().getApplication();
};

Concept.prototype.getConceptName = function(){
	return this.concept_name;
};

//DEPRECATED
Concept.prototype.getClassName = function(){
	return this.class_name;
};

Concept.prototype.getDocument = function(element){
	//Busca el documento de la interfaz de modding.
	return this.modding_interface.getDocument();
};
Concept.prototype.getInstances = function(element){
	//Busca las instancias del DOM a partir de un elemento.
	return element.getElementsByClassName(this.class_name);
};

Concept.prototype.getDOMInstancesForApp = function(application){
	//Busca las instancias del DOM a partir del documento principal del modding interface.
	var currentWrapper = this.getDOMWrapperForApplication(application);
	return currentWrapper.getDOMInstances();
};

Concept.prototype.basicAddProperty = function(property,xpath,application){	
	property.setConcept(this);
	property.addDOMElement(application,xpath);
	this.properties.push(p);
};

Concept.prototype.addProperty = function(name,type,xpath,application){
	var p = new Property(name,type,xpath);
	p.setConcept(this);
	p.addDOMElement(application,xpath);
	this.properties.push(p);
};

Concept.prototype.addConceptualEvent = function(name,event){
	var ce = {'name':name,'event':event};
	this.conceptual_events.push(ce);
};

Concept.prototype.getPropertyByName = function(property_name){
	for (var i = 0; i < this.properties.length; i++){
		if (this.properties[i].getName() == property_name)
			return this.properties[i];
	}
	return null;
};

//DEPRECATED
Concept.prototype.getXpathPropertyByName = function(aaa){
	alert("WARNING: Concept.prototype.getXpathPropertyByName  es utilizada.\nCandidata a deprecated al 19/05/2010.");
	for (var i = 0; i < this.properties.length; i++){
		if (this.properties[i].getName() == property_name)
			return this.properties[i].getXPath();
	}
	return null;
};

Concept.prototype.getProperties = function(){
	return this.properties;
};

Concept.prototype.getConceptualEvents = function(){
	return this.conceptual_events;
};

/***********************************************************
 * Property
 **********************************************************/
function Property(propertyName,propertyType,xpath){
	this.name = propertyName;
	this.type = propertyName;
	this.xpath = xpath;
	this.concept = null;	
};

//Property es subclase de ModelElement
Property.prototype = new ModelElement();

Property.prototype.createWrapper = function(application,dom_data){
	var wrapper = new PropertyDOMWrapper(this,application,dom_data);
	return wrapper;
};

Property.prototype.setConcept = function(concept){
	this.concept = concept;
};

Property.prototype.getApplication = function(){
	return this.concept.getApplication();
};

Property.prototype.getName = function(){
	return this.name;
};

Property.prototype.getType = function(){
	return this.type;
};

Property.prototype.getXPath = function(){	
	return this.getDOMInfo(this.getApplication());
	//return this.xpath;
};


//Deprecated: usando en version vieja basado en GreaseMonkey CSN
Property.prototype.getWindowForAnchorAdition = function(create_anchor){
	//create_anchor indica si se debe crear un anchor, o transformar el DOM element en un anchor.
	//TMP: Debiera ser un Strategy
	if (create_anchor)
		return "chrome://xolar/content/pluginMaker/createSourceAnchor.xul";
	else
		return "chrome://xolar/content/pluginMaker/transformToSourceAnchor.xul";
};

Property.prototype.getDocument = function(){
	return this.concept.getDocument();
};

//DEPRECATED
Property.prototype.getDOMInstances = function(function_){
	//Busca las instancias del DOM a partir del documento principal del modding interface.
	var doc = this.getDocument();
	var concepts_instances = this.concept.getInstances(doc);
	var property_instances = [];
	for (var i=0; i < concepts_instances.length ;i++){
		var instance = function_(concepts_instances[i],this.xpath);
		property_instances[property_instances.length] = instance;
	}
	return (doc != null)? {'doc':doc,'instances':property_instances}: {'doc':null,'instances':[]};
};

//{'name':'target','node':'anchor','value':'href','type':'string'}
Property.prototype.serialize = function(app){
	var json = {};
	json["name"] = this.getName();
	json["type"] = this.getType();
	json["node"] = this.getDOMInfo(app);
	json["value"] = "tempValue";
	return json;
};

Property.prototype.getValue = function(function_,dom_concept_instance){
	var instance = function_(dom_concept_instance,this.xpath);
	//var value = instance.getAttribute(this.getNameForAttributeHTML());	
	return value;
};

