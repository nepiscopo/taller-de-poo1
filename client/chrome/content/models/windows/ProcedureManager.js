
function ProcedureManager(){
	this.procedures = [];
	this.initProcedures();
};

ProcedureManager.prototype.getTestProcedure = function(){
	if (CSA.getProcedureInAction() != null)
		return CSA.getProcedureInAction();
	var aProcedure = new Procedure("Trip Planning Procedure");
	//aProcedure.addTask(new PrimitiveTask("Provide a URL","provideURL",[{"name":"URL","type":"Regular Expression","value":null,"example":"http://www.google.com*"}]));
	aProcedure.addTask(new PrimitiveTask("Load Interact2013 Web Site","loadWebSite",[{"name":"URL","type":"Regular Expression","value":"http://www.interact2013.org/Home","example":"http://www.google.com*"}]));
	aProcedure.addTask(new AugmentationTask("Collect conference place","typedPlainTextDataCollector",[{"name":"Concept","type":"String","value":"ConferencePlace","example":"City"},{"name":"Instance","type":"String","value":null,"example":"París"}]));
	aProcedure.addTask(new AugmentationTask("Collect conference city","typedPlainTextDataCollector",[{"name":"Concept","type":"String","value":"Destination","example":"City"},{"name":"Instance","type":"String","value":null,"example":"París"}]));
	aProcedure.addTask(new AugmentationTask("Collect conference start date","typedPlainTextDataCollector",[{"name":"Concept","type":"String","value":"ConferenceStartDate","example":"City"},{"name":"Instance","type":"String","value":null,"example":"París"}]));
	aProcedure.addTask(new AugmentationTask("Collect conference end date","typedPlainTextDataCollector",[{"name":"Concept","type":"String","value":"ConferenceEndDate","example":"City"},{"name":"Instance","type":"String","value":null,"example":"París"}]));
	return aProcedure;
};

ProcedureManager.prototype.renderProceduresInList = function(){
	var procedureList = document.getElementById("procedureList");
	var current_children = procedureList.children;
	for (var i=0;i < current_children.length;i++)
		procedureList.removeChild(current_children[i]);

	for (var i=0;i < this.procedures.length;i++){
		var aProcedure = this.procedures[i];		
		var procedureListItem = document.createElement("listitem");
		procedureListItem.setAttribute("id","procedure-id");		
		procedureListItem.procedureInstance = aProcedure;
		var procedureListCell = document.createElement("listcell");
		procedureListCell.setAttribute("label", aProcedure.getName());
		procedureListItem.appendChild(procedureListCell);
		procedureList.appendChild(procedureListItem);
	}
};

ProcedureManager.prototype.initProcedures = function(){
	//Esta hecho solo para el del ejemplo,
	// pero por cada procedimiento existente debe crearse el listitem y el listcell
	var aProcedure = this.getTestProcedure();
	//this.procedures.push(aProcedure);
	this.renderProceduresInList();
};

ProcedureManager.prototype.execute = function(){
	var procedureList = document.getElementById("procedureList");
	var procedureInstance = procedureList.selectedItem.procedureInstance;
	CSA.startScenarioPlayer(procedureInstance);
	window.close();
};

ProcedureManager.prototype.edit = function(){
	var procedureList = document.getElementById("procedureList");
	var procedureInstance = procedureList.selectedItem.procedureInstance;
	CSA.startScenarioRecorder(procedureInstance);
	window.close();
};

ProcedureManager.prototype.createNewProcedure = function(){
	var procedureName = prompt("Enter procedure's name");
	procedureName = (procedureName != "")?procedureName:"Undefined Procedure Name";
	var procedureInstance = new Procedure(procedureName);
	this.procedures.push(procedureInstance);
	CSA.startScenarioRecorder(procedureInstance);
	window.close();
};

ProcedureManager.prototype.importProcedureFromFile = function(){
	const nsIFilePicker = Components.interfaces.nsIFilePicker;
	var filePicker = Components.classes["@mozilla.org/filepicker;1"]
                 .createInstance(nsIFilePicker);
    filePicker.init(CSA.mainWindow,"Choose a text editor",nsIFilePicker.modeOpen);
    filePicker.appendFilters(nsIFilePicker.filterXML);
	var stop_asking_exec = true;
	var selectedFile = filePicker.show();
	var file = filePicker.file;

	var strxml = "";
	var fstream = Components.classes["@mozilla.org/network/file-input-stream;1"].
	              createInstance(Components.interfaces.nsIFileInputStream);
	var cstream = Components.classes["@mozilla.org/intl/converter-input-stream;1"].
	              createInstance(Components.interfaces.nsIConverterInputStream);
	fstream.init(file, -1, 0, 0);
	cstream.init(fstream, "UTF-8", 0, 0); // you can use another encoding here if you wish
	 
	let (str = {}) {
	  let read = 0;
	  do { 
	    read = cstream.readString(0xffffffff, str); // read as much as we can and put it in str.value
	    strxml += str.value;
	  } while (read != 0);
	}
	cstream.close(); // this closes fstream
    
    var parser = new CSA.mainWindow.DOMParser();
    var doc = parser.parseFromString(strxml, "application/xml");
    this.createProcedureFromXML(doc);
    this.renderProceduresInList();
};

ProcedureManager.prototype.createProcedureFromXML = function(XMLDOM){
	if (XMLDOM.getElementsByTagName("procedure").length == 0){
		CSA.mainWindow.alert("This XML file is not defined with the correct structure");
		return;
	};
	var procedureDOM = XMLDOM.getElementsByTagName("procedure")[0];
	var newProcedure = new Procedure(procedureDOM.getAttribute("name"));
	newProcedure.buildFromXMLDOM(XMLDOM);
	this.procedures.push(newProcedure);
};

ProcedureManager.prototype.viewExecutions = function(){
	alert("View Procedure Executions");
};
