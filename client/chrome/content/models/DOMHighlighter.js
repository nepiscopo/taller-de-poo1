/*
 * CSNDOM Inspector
 */

function DOMHighlighter(doc, strategy){
	this.doc = (typeof(doc) == "undefined")?content.document:doc;
	//this.strategy = (typeof(strategy) == "undefined")?new AllDOMHighlightedStrategy():doc;	 
}; 	

DOMHighlighter.prototype.start = function(){
	this.enableDomHighlighting();
};

DOMHighlighter.prototype.stop = function(){
	this.disableDomHighlighting();
};

DOMHighlighter.prototype.setStyleHighlighting = function(event){
	event.target.normal_style = event.target.getAttribute("style");
	event.target.setAttribute("style",event.target.getAttribute("style") + " border:1px solid #336699;background-color:#FFFFCC;");
};

DOMHighlighter.prototype.backStyleHighlighting = function(event){
	var style = event.target.normal_style;
	event.target.setAttribute("style",style);
	event.target.setAttribute("style",event.target.getAttribute("style").split(" border:1px solid #336699;background-color:#FFFFCC;")[1]);
};

DOMHighlighter.prototype.selectedElement = function(event){
	
};

DOMHighlighter.prototype.recursiveEnableDomHighlighting = function(element){
	element.addEventListener("mousedown", this.selectElement,true);
	element.addEventListener("mouseover", this.setStyleHighlighting,true);		
	element.addEventListener("mouseout", this.backStyleHighlighting,true);
	if (!((element.getAttribute("class") == "") || (element.getAttribute("class") == null))){
		element.addEventListener("mousedown", this.selectElement,true);
		element.addEventListener("mouseover", this.setStyleHighlighting,true);		
		element.addEventListener("mouseout", this.backStyleHighlighting,true);		
	}
	for (var i = 0; i < element.childNodes.length; i++){
		var child = element.childNodes[i];
		if (!((child.getAttribute("class") == "") || (child.getAttribute("class") == null))){
			child.addEventListener("mousedown", this.selectElement,true);			
			child.addEventListener("mouseover", this.setStyleHighlighting,true);			
			child.addEventListener("mouseout", this.backStyleHighlighting,true);			
		}
		//child.parentNode.insertBefore(a,child);
		child.addEventListener("mousedown", this.selectElement,true);		
		child.addEventListener("mouseover", this.setStyleHighlighting,true);			
		child.addEventListener("mouseout", this.backStyleHighlighting,true);		
		this.recursiveEnableDomHighlighting(child);
	}
};

DOMHighlighter.prototype.enableDomHighlighting = function(){
	var body = this.doc.documentElement.getElementsByTagName("body")[0];
	this.recursiveEnableDomHighlighting(body);	
};

DOMHighlighter.prototype.recursiveDisableDomHighlighting = function(element){	
	element.removeEventListener("mouseover",this.setStyleHighlighting,true);	
	element.removeEventListener("mouseout",this.backStyleHighlighting,true);
	element.removeEventListener("mousedown", this.selectElement,true);
	for (var i = 0; i < element.childNodes.length; i++){
		var child = element.childNodes[i];
		child.removeEventListener("mousedown", this.selectElement,true);
		child.removeEventListener("mouseover",this.setStyleHighlighting,true);
		child.removeEventListener("mouseout",this.backStyleHighlighting,true);
		this.recursiveDisableDomHighlighting(child);
	}
};

DOMHighlighter.prototype.disableDomHighlighting = function(){
	var body = this.doc.documentElement.getElementsByTagName("body")[0];	
	this.recursiveDisableDomHighlighting(body);
};
