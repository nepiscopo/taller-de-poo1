//CSNH ICWE2011
function PlainTextDataCollector(pocket){
	this.setName("PlainTextDataCollector");
	this.setPocket(pocket);
	this.setMenuItem(null);
	this.load();
	var __metadata__ = {"name":"Untyped Collector","id":"untypedPlainTextDataCollector","attributesList":[{"name":"Text value","type":"String","value":null,"example":"París"}],"event":"UntypedInstanceDefined"};
	this.setMetadata(__metadata__);
};

PlainTextDataCollector.prototype = new AbstractDataCollector(); 

PlainTextDataCollector.prototype.onShowingPopUpShowMyItem = function(was_collector_used){
	return this.targetElements["text"] != "";
};

PlainTextDataCollector.prototype.load = function(){
	//PlainTextDataCollector is interested in the contextual menu, but others could be interested in the main CSA menu.
	//Two methods are provided by AbstractDataCollector: #registerContextMenuItem and #registerMainMenuItem and each concrete DataCollector
	//have to use one of them.
	this.registerContextMenuItem();
};

PlainTextDataCollector.prototype.createInstance = function(){
	var concept = this.pocket.getConceptByName("untyped");
	var instance = new TextPlainInstance(this.targetElements["text"],concept);
	this.triggerEventForTaskExecution(this.targetElements["text"], "untyped");
	return instance;
};

PlainTextDataCollector.prototype.setDataForDispatchingEvent = function (menuitem,instance,concept){
	menuitem.dispatcherId = this.getId();
	menuitem.scriptAttributes = '[{"name":"Text value","value":"' + instance + '"}]';
};

PlainTextDataCollector.prototype.automaticExecution = function(attributes){
	var textValue = attributes[0]["value"];
	var concept = this.pocket.getConceptByName("untyped");
	var instance = new TextPlainInstance(textValue,concept);
	this.triggerEventForTaskExecution(textValue,"undefined");
	this.pocket.renderSuitCase();
};


PlainTextDataCollector.prototype.getLabelForMenuItem = function(){
	return "Put it into Pocket";
};

