//CSNH ICWE2011
function AbstractDataCollector(){
	this.name;
	this.pocket;
	this.menuitem;
	this.targetElements = {"text":null,"DOM":null};
	this.metadata = {}
};

AbstractDataCollector.prototype.setMetadata = function(aDictionary){
	this.metadata = aDictionary;
};

AbstractDataCollector.prototype.getName = function(){
	return this.metadata["name"];
};

AbstractDataCollector.prototype.getId = function(){
	return this.metadata["id"];
};

AbstractDataCollector.prototype.getEventName = function(){
	return this.metadata["event"];
};

AbstractDataCollector.prototype.getAttributesList = function(){
	return this.metadata["attributesList"];
};

AbstractDataCollector.prototype.setName = function(name){
	this.name = name;
};

AbstractDataCollector.prototype.setPocket = function(pocket){
	this.pocket = pocket;
	pocket.addDataCollector(this);
};

AbstractDataCollector.prototype.setMenuItem = function(menuitem){
	this.menuitem = menuitem; 
};

AbstractDataCollector.prototype.showingPopUpMenu = function(event){
	var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"]
         .getService(Components.interfaces.nsIWindowMediator);
	var mainWindow = wm.getMostRecentWindow("navigator:browser");
	var tabBrowser = mainWindow.getBrowser();
	var selectedText = tabBrowser.contentWindow.getSelection();
	var selectedString = selectedText.toString();
	var domElement = document.popupNode;
	this.targetElements = {"text":selectedString,"dom":domElement};
	try{
		if(this.onShowingPopUpShowMyItem(false))
			this.showMyMenuItem();
		else
			this.hideMyMenuItem();
	}
	catch(e){}
};

AbstractDataCollector.prototype.hidingPopUpMenu = function(event){
	this.hideMyMenuItem();
	//this.targetElements = {"text":null,"DOM":null};
};

AbstractDataCollector.prototype.showMyMenuItem = function(){	
	this.menuitem.hidden = false;
};

AbstractDataCollector.prototype.hideMyMenuItem = function(){
	try{
		this.menuitem.hidden = true;
	}catch(e){return;}
};

AbstractDataCollector.prototype.addInstanceToPocket = function(){
	var instance = this.createInstance();
	if (instance == null)
		return;
	this.pocket.instanceAdded(instance);
};

AbstractDataCollector.prototype.createAndRegisterMenuItem = function(menu_container){
	if (this.menuitem != null) return;//No deben generarse menuitems de mas.

	var self = this;
	window.addEventListener("popupshowing", function(e) {self.showingPopUpMenu(e); }, false);
	window.addEventListener("popuphidden", function(e) {self.hidingPopUpMenu(e); }, false);	
	
  	var newOption = document.createElement("menuitem");
  	newOption.setAttribute("id","item-data-collector" + this.name);
  	newOption.setAttribute("label",this.getLabelForMenuItem());
	newOption.hidden = true;
  	newOption.collector = this;
  	newOption.setAttribute("oncommand","this.collector.addInstanceToPocket();");
  	menu_container.appendChild(newOption);
  	this.setMenuItem(newOption);
};

AbstractDataCollector.prototype.registerContextMenuItem = function(){
  	var mainPopup = document.getElementById("data-collectors-popupmenu-contextualmenu");
  	this.createAndRegisterMenuItem(mainPopup);
};

AbstractDataCollector.prototype.registerMainMenuItem = function(){
	var mainPopup = document.getElementById("data-collectors-popupmenu-mainmenu");
	this.createAndRegisterMenuItem(mainPopup);  	
};

AbstractDataCollector.prototype.initDialog = function(){	
	return new DataCollectorDialog();
};

AbstractDataCollector.prototype.setDataForDispatchingEvent = function (menuitem,instance,concept){
	menuitem.dispatcherId = this.getId();
	menuitem.scriptAttributes = '[]'// '[{"name":"Concept","value":"' + concept + '"},{"name":"Instance","value":"' + instance + '"}]';
};

AbstractDataCollector.prototype.triggerEventForTaskExecution = function(instance, concept){
	var conceptDefined = document.createEvent("Events");
	conceptDefined.initEvent(this.getEventName(), true, true);
	this.setDataForDispatchingEvent(this.menuitem,instance,concept);
	this.menuitem.dispatchEvent(conceptDefined);
};

/*
 * DataCollectorDialog
 */

function DataCollectorDialog(){
	this.window_path = "chrome://cs-adaptation/content/models/windows/createConceptFromDataCollector.xul"; 
	this.in_params = [];
};

DataCollectorDialog.prototype.showDialog = function(){
	var params = {inn:this.in_params, out:null};
	window.openDialog(this.window_path, "","chrome, dialog, modal, resizable=yes",params).focus();
	return params.out;
};

DataCollectorDialog.prototype.addTextboxParam = function(id,label,value){
	var textbox = document.createElement("textbox");
	textbox.setAttribute("id",id);
	textbox.setAttribute("label",label);
	textbox.setAttribute("value",value);
	textbox.getValue = function(){return this.value};
	this.addParam(textbox,label);
};

DataCollectorDialog.prototype.addCheckboxParam = function(id,label,value){
	var checkbox = document.createElement("checkbox");
	checkbox.setAttribute("id",id);
	checkbox.setAttribute("label",label);
	checkbox.setAttribute("checked",value);
	checkbox.getValue = function(){return this.checked};
	this.addParam(checkbox,"");
};

DataCollectorDialog.prototype.addParam = function(param,label){
	var param_container = document.createElement("hbox");	
	if(label != ""){
		var label_element = document.createElement("label");
		label_element.setAttribute("value",label);
		param_container.appendChild(label_element);		
	}
	param_container.appendChild(param);
	param_container.valuedElement = param;
	this.in_params.push(param_container);
};
