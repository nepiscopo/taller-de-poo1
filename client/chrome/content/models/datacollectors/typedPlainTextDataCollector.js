//CSNH ICWE2011
function TypedPlainTextDataCollector(pocket){
	this.setName("TypePlainTextDataCollector");
	this.setPocket(pocket);
	this.setMenuItem(null);
	this.load();
	var __metadata__ = {"name":"Concept Data Collector","id":"typedPlainTextDataCollector","attributesList":[{"name":"Concept","type":"String","value":null,"example":"City"},{"name":"Instance","type":"String","value":null,"example":"París"}],"event":"ConceptDefined"};
	this.setMetadata(__metadata__);
};

TypedPlainTextDataCollector.prototype = new AbstractDataCollector(); 

TypedPlainTextDataCollector.prototype.onShowingPopUpShowMyItem = function(was_collector_used){
	return this.targetElements["text"] != "";
};

TypedPlainTextDataCollector.prototype.load = function(){
	//PlainTextDataCollector is interested in the contextual menu, but others could be interested in the main CSA menu.
	//Two methods are provided by AbstractDataCollector: #registerContextMenuItem and #registerMainMenuItem and easc concrete DataCollector
	//have to use one of them.
	this.registerContextMenuItem();
};

TypedPlainTextDataCollector.prototype.setDataForDispatchingEvent = function (menuitem,instance,concept){
	menuitem.dispatcherId = this.getId();
	menuitem.scriptAttributes = '[{"name":"Concept","value":"' + concept + '"},{"name":"Instance","value":"' + instance +'"}]';
};

TypedPlainTextDataCollector.prototype.createInstance = function(){
	var dialog = this.initDialog();
	dialog.addTextboxParam("instance_value","Instance Value",this.targetElements["text"]);
	dialog.addTextboxParam("concept_name","Concept Name","untyped");	
	dialog.addCheckboxParam("instance_persistent","Make it persistent","true");
	var out = dialog.showDialog();
	if (out){
		var concept_name = (out["concept_name"] == "")?"untyped":out["concept_name"];
		var instance_value = (out["instance_value"] == "")?"untyped":out["instance_value"];
		var persistent = out["instance_persistent"];	
		var concept = this.pocket.getConceptByName(concept_name);
		var instance = new TextPlainInstance(instance_value,concept);
		this.triggerEventForTaskExecution(instance_value, concept_name);
		return instance;
	}
	return null;
};

TypedPlainTextDataCollector.prototype.automaticExecution = function(attributes){
	var concept = this.pocket.getConceptByName(attributes[0]["value"]);
	var instance = new TextPlainInstance(attributes[1]["value"],concept);
	this.triggerEventForTaskExecution(attributes[1]["value"],attributes[0]["value"]);
	this.pocket.renderSuitCase();
};

TypedPlainTextDataCollector.prototype.getLabelForMenuItem = function(){
	return "Create instance into Pocket...";
};

