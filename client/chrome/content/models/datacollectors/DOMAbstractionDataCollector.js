//CSNH ICWE2011
function DOMAbstractionDataCollector(pocket){
	this.setName("DOMAbstractionDataCollector");
	this.setPocket(pocket);
	this.setMenuItem(null);
	this.load();
	var __metadata__ = {"name":"DOM Data Collector","id":"domDataCollector","attributesList":[{"name":"Concept","type":"String","value":null,"example":"City"},{"name":"xPath","type":"xPath","value":null,"example":"an xPath Expression]"}],"event":"DOMConceptDefined"};
	this.setMetadata(__metadata__);
};

DOMAbstractionDataCollector.prototype = new AbstractDataCollector(); 

DOMAbstractionDataCollector.prototype.onShowingPopUpShowMyItem = function(was_collector_used){
	return this.targetElements["dom"] != null;
};

DOMAbstractionDataCollector.prototype.load = function(){
	//PlainTextDataCollector is interested in the contextual menu, but others could be interested in the main CSA menu.
	//Two methods are provided by AbstractDataCollector: #registerContextMenuItem and #registerMainMenuItem and easc concrete DataCollector
	//have to use one of them.
	this.registerContextMenuItem();
};

DOMAbstractionDataCollector.prototype.createInstance = function(){
	var dialog = this.initDialog();
	dialog.addTextboxParam("concept_name","Concept Name","untyped");
	dialog.addCheckboxParam("instance_persistent","Add it to app model","true");
	var out = dialog.showDialog();
	if (out){
		var persistent = out["instance_persistent"];
		var concept_name = out["concept_name"];
		var concept = this.pocket.getConceptByName(concept_name);
		var instance = new DOMAbstractionInstance(this.targetElements["dom"],concept);
		this.triggerEventForTaskExecution(this.targetElements["dom"],concept_name);
	}
};

DOMAbstractionDataCollector.prototype.setDataForDispatchingEvent = function (menuitem,instance,concept){
	menuitem.dispatcherId = this.getId();
	menuitem.scriptAttributes = '[{"name":"Concept","value":"' + concept + '"},{"name":"xPath","value":"' + instance +'"}]';
};

DOMAbstractionDataCollector.prototype.automaticExecution = function(attributes){
	//var concept = this.pocket.getConceptByName(attributes[0]["value"]);
	//var dom_element = XPATH --> attributes[1]["value"]
	//var instance = new DOMAbstractionInstance(dom_element,concept);
	//this.triggerEventForTaskExecution(attributes[1]["value"],attributes[0]["value"]);
	this.pocket.renderSuitCase();
};

DOMAbstractionDataCollector.prototype.getLabelForMenuItem = function(){
	return "Put UI Widgets into Pocket...";
};

