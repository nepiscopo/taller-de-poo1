function AdaptableElement(){};
 
AdaptableElement.prototype.isCollection = function(){
	return false;
};

AdaptableElement.prototype.getNameForPocket = function(){
	return this.getValue();
};
 
//Concept
function PocketConcept(name){
	this.name = name;
	this.instances = [];
};
 
PocketConcept.prototype = new AdaptableElement();
 
PocketConcept.prototype.getName = function(){
	return this.name;
};

PocketConcept.prototype.setName = function(aValue){
	this.name = aValue;
};

PocketConcept.prototype.getValue = function(){
	return this.getName();
};

PocketConcept.prototype.setValue = function(aValue){
	this.setName(aValue);
};

PocketConcept.prototype.isCollection = function(){
	return true;
};
 
PocketConcept.prototype.getInstances = function(){
	return this.instances;
};

PocketConcept.prototype.getLastInstance = function(){
	return this.instances[this.instances.length-1];
};

PocketConcept.prototype.addInstance = function(instance){
	this.instances.push(instance);
};

PocketConcept.prototype.removeInstance = function(instance){
	var tmp = [];
	for(var i=0;i < this.instances.length;i++){
		if (this.instances[i]==instance)
			continue;
		tmp.push(this.instances[i]);
	}
	this.instances = tmp;
}

PocketConcept.prototype.editFromPocket = function(pocket, newName){
	pocket.changeConceptName(this,newName);
};

//Textplain Instance
function TextPlainInstance(value,concept){
	this.value = value;
	this.concept = concept;
	concept.addInstance(this);  
};
 
TextPlainInstance.prototype = new AdaptableElement();
 
TextPlainInstance.prototype.setConcept = function(concept){	
	this.concept = concept;
};
 
TextPlainInstance.prototype.getConcept = function(){
	return this.concept;
};

TextPlainInstance.prototype.getValue= function(){  
	return this.value;
};

TextPlainInstance.prototype.setValue= function(aValue){  
	this.value = aValue;
};

TextPlainInstance.prototype.adapt = function(anAgumenter){
	anAgumenter.adaptTextPlainInstance(this);
};

TextPlainInstance.prototype.undo = function(anAgumenter){
	anAgumenter.undoTextPlainInstance(this);
};

TextPlainInstance.prototype.editFromPocket = function(pocket, newValue){
	pocket.changePlainTextInstanceValue(this,newValue);
};

function DOMAbstractionInstance(value,concept){
	this.value = value;
	this.concept = this.setConcept(concept);
};
 
//DOMAbstractionInstance son las instancias de conceptos que abstraen elementos del DOM.
 
function DOMAbstractionInstance(value,concept){
	this.value = value;
	this.concept = this.setConcept(concept);
};
 
DOMAbstractionInstance.prototype = new AdaptableElement();

DOMAbstractionInstance.prototype.getNameForPocket = function(){	
	try{
		return "DOM Instance ("+ this.getValue().nodeName +")";
	}catch(e){
		return "";
	}
};

DOMAbstractionInstance.prototype.setConcept = function(concept){
	concept.addInstance(this);
	return concept;
};
 
DOMAbstractionInstance.prototype.getValue = function(){
	return this.value;
	var app = CSA.getCurrentApplication();
	var wrapper = this.value.getDOMWrapperForApplication(app);
	return wrapper.getWrapperDOMInfo();
};
 
DOMAbstractionInstance.prototype.adapt =  function(anAgumenter){
 	anAgumenter.adaptDOMElementInstance(this); 
};

DOMAbstractionInstance.prototype.undo =  function(anAgumenter){
 	anAgumenter.undoDOMElementInstance(this); 
};
