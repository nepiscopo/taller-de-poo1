function AbstractScenario(name){
	this.name = name;
	this.augmenters = {};
	this.components = {};
	this.concepts = {};
};

AbstractScenario.prototype.init = function(){
	this.registerListeners();
	this.registerUseOfAugmenters();
	this.registerUseOfComponents();
};

AbstractScenario.prototype.expressInterestIn = function(event,callback){
	window.addEventListener(event, callback, false);
};

AbstractScenario.prototype.retractInterestIn = function(event,callback){
	window.removeEventListener(event, callback);
};

AbstractScenario.prototype.setComponents = function(){
	//Abstract method. Has to be redefined in concrete scenarios.
	return;
};

AbstractScenario.prototype.setAugmenters = function(){
	//Abstract method. Has to be redefined in concrete scenarios.
	return;
};

AbstractScenario.prototype.registerListeners = function(){
	//Abstract method. Has to be redefined in concrete scenarios.
	//Scenarios have to use #expressInterestIn.
	return;
};

AbstractScenario.prototype.performAugmentation = function(augmenter_name,doc,target,params){
	var augmenter = this.augmenters[augmenter_name];
	augmenter.execute(doc,target,params);
};

AbstractScenario.prototype.getComponent = function(component_name){
	return this.components[component_name];
};

AbstractScenario.prototype.registerUseOfAugmenter = function(augmenter_name){
	var augmenter = CSA.getAugmenter(augmetner_name);
	this.augmenters[augmenter_name] = augmenter;
};

AbstractScenario.prototype.registerUseOfComponent = function(component_name){
	var component = CSA.getComponent(component_name);
	this.components[component_name] = component;
};

AbstractScenario.prototype.registerUseOfAugmenter = function(augmenter_name){
	//Abstract method.
	return;
};

AbstractScenario.prototype.registerUseOfComponent = function(component_name){
	//Abstract method.
	return;
};
