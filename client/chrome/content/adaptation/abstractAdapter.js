//CSNH ICWE2011
function AbstractAdapter(metadata){
	this.name = "";
	this.label = "";
	this.args = null;
	this.doc = null;
	this.metadata = metadata;
	this.quickMenusDescription = [];
	this.quickMenusItems = [];
	this.externalScriptsURIs = {};
};

AbstractAdapter.prototype.initialize = function(){
	if (this.isQuickAugmenter()){
		this.addQuickMenus();
	}
};

AbstractAdapter.prototype.getName = function(){
	return this.name;	
};

AbstractAdapter.prototype.getLabel = function(){
	return this.label;	
};


AbstractAdapter.prototype.makeQuickOptionsVisible = function(){
	for (var i = this.quickMenusItems.length - 1; i >= 0; i--) {
		this.quickMenusItems[i].hidden = false;
	}
	var menu = document.getElementById("quick-augmenter-menu");
	menu.hidden = false;
	popmenu = document.getElementById("quick-augmenter-popupmenu");
	popmenu.hidden = false;
};

AbstractAdapter.prototype.showingPopUpMenu = function(event){
	var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"]
         .getService(Components.interfaces.nsIWindowMediator);
	var mainWindow = wm.getMostRecentWindow("navigator:browser");
	var tabBrowser = mainWindow.getBrowser();
	var selectedText = tabBrowser.contentWindow.getSelection();
	var selectedString = selectedText.toString();
	var domElement = document.popupNode;
	var targetElements = {"text":selectedString,"dom":domElement};
	for (var i = this.quickMenusItems.length - 1; i >= 0; i--) {
		if((this.quickMenusItems[i].descriptor["type"] == "TEXT" && (selectedString != ""))){
			this.quickMenusItems[i].hidden = false;
			this.quickMenusItems[i].targetValue = selectedString;
		}
		else{
			if((event.target.getAttribute("usetoadaptation") == "true") || (event.target.parentNode.getAttribute("id") == "quick-augmenter-popupmenu")){
				this.quickMenusItems[i].hidden = false;
				return;
			}
			//this.quickMenusItems[i].hidden = true;
		}
	};
};

AbstractAdapter.prototype.hidingPopUpMenu = function(e){
	for (var i = this.quickMenusItems.length - 1; i >= 0; i--) {
		;//this.quickMenusItems[i].hidden = true;
	};
};

AbstractAdapter.prototype.executeQuickOption = function(id){
	var targetMenuItem = document.getElementById(id);
	eval("(this." + targetMenuItem.descriptor["callback"] + ")");
	var instance = null;
	var deleteInstance = false;
	if (this.isQuickAugmenter()){
		instance = CSA.getPocket().target_data_for_volatile_adaptation;
		CSA.getPocket().target_data_for_volatile_adaptation = null;
	}
	if(instance == null){
		instance = CSA.getPocket().addDataToSuitcase(targetMenuItem.targetValue,"untyped",false);
		deleteInstance = true;
	}
	targetMenuItem.augmenter.execute(content.document, instance, {});
	if (deleteInstance)
		CSA.getPocket().removeInstance(instance);
};

AbstractAdapter.prototype.createAndRegisterMenuItem = function(menu_container, aMenuDescriptor){
	if (this.menuitem != null)
		return;//No deben generarse menuitems de mas.
	var self = this;
	window.addEventListener("popupshowing", function(e) {self.showingPopUpMenu(e); }, false);
	window.addEventListener("popuphidden", function(e) {self.hidingPopUpMenu(e); }, false);	
  	
  	var newOption = document.createElement("menuitem");
  	var id = "quickMenuOption-" + this.name + "-" + aMenuDescriptor["label"]
  	newOption.setAttribute("id",id);
  	newOption.setAttribute("label",aMenuDescriptor["label"]);
	//newOption.hidden = true;
  	newOption.augmenter = this;
  	newOption.descriptor = aMenuDescriptor;
  	newOption.setAttribute("oncommand","this.augmenter.executeQuickOption('" + id + "')");
  	menu_container.appendChild(newOption);
  	this.quickMenusItems.push(newOption);
};

AbstractAdapter.prototype.addQuickMenus = function(){
	this.__addQuickMenus__();
  	var container;
  	var mainContextMenu = document.getElementById("quick-augmenter-popupmenu");
  	if (this.quickMenusDescription.length <= 1)
  		container = mainContextMenu;
  	else{
    	var mainMenu = document.createElement("menu");
    	mainMenu.setAttribute("id","main-menu-quickaugmenter-" + this.name);
    	mainMenu.setAttribute("label",this.name);
    	var mainPopup = document.createElement("menupopup");
		mainPopup.setAttribute("id","main-popupmenu-quickaugmenter-" + this.name);
		mainMenu.appendChild(mainPopup);
		mainContextMenu.appendChild(mainMenu);
  		container = mainPopup;
  	}
  	for (var i = this.quickMenusDescription.length - 1; i >= 0; i--){
  		this.createAndRegisterMenuItem(mainPopup,this.quickMenusDescription[i]);
  	};
};

AbstractAdapter.prototype.__addQuickMenus__ = function(){
	return;
};

AbstractAdapter.prototype.addQuickMenu = function(label, typeOfInstance, callback){
	this.quickMenusDescription.push({"label":label,"type":typeOfInstance,"callback":callback});
};

AbstractAdapter.prototype.startAutomaticExecution = function(dispatcher, doc, scriptAttributes){
	var pocketStringValue = scriptAttributes[0]["value"];
	var isConcept = scriptAttributes[1]["value"].toLowerCase() == "true";
	dispatcher.automaticExecution(this, doc, pocketStringValue, isConcept);
};

AbstractAdapter.prototype.execute = function(doc, target, args){	
	this.args = args;
	this.doc = doc;
	if(this.isApplicableToConcept() && target.isCollection()){
		var instances = target.getInstances();		
		for (var i = 0;i < instances.length;i++){
			this.applyToInstance(instances[i]);
		}
	}
	else
		this.applyToInstance(target);
};

AbstractAdapter.prototype.undo = function(doc,target,args){	
	this.args = args;
	this.doc = doc;
	if(this.isApplicableToConcept() && target.isCollection()){
		var instances = target.getInstances();		
		for (var i = 0;i < instances.length;i++){
			this.undoForInstance(instances[i]);
		}
	}
	else
		this.undoForInstance(target);		
};

AbstractAdapter.prototype.isQuickAugmenter = function(){
	return false;
};

AbstractAdapter.prototype.isApplicableToConcept = function(){
	throw("isApplicableToConcept() method must be defined in concrete adapters");
};

AbstractAdapter.prototype.applyToInstance = function(instance){
	instance.adapt(this);
};

AbstractAdapter.prototype.undoForInstance = function(instance){
	instance.undo(this);
};

//Hook Method
AbstractAdapter.prototype.adaptTextPlainInstance = function(instance){
	return;
};

//Hook Method
AbstractAdapter.prototype.adaptDOMElementInstance = function(instance){
	return;
};

//Hook Method
AbstractAdapter.prototype.undoTextPlainInstance = function(instance){
	return;
};

//Hook Method
AbstractAdapter.prototype.undoDOMElementInstance = function(instance){
	return;
};


var genericListenerInformation = {"callback":[]};
genericListener = function(event){
	try{		
		genericListenerInformation["callback"](event);
		genericListenerInformation["document"].removeEventListener("mousedown", genericListener, false);		
		genericListenerInformation = {};
	}
	catch (e){;}
};

//Modificar para que el expressInterstIn registre el receptor del mensaje y entonces lo que le llegue no es una funcion cualquiera sino que sea el metodo especifico de ese objeto especifico
AbstractAdapter.prototype.expressInterestIn = function(event, document, callback, only_once){
	genericListenerInformation["document"] = document;
	genericListenerInformation["callback"] = callback;
	if(only_once)
		document.addEventListener(event, genericListener, false);
	else
		document.addEventListener(event, callback, false);
};

AbstractAdapter.prototype.getAttributesAsString = function(attributesList){
	var attributesString = "[";
	for (var i=0;i < attributesList.length;i++){
		var attribute = attributesList[i];
		attributesString += "{'name':'" + attribute["name"] + "', 'value':'" + attribute["value"] + "'}";
		if (i < attributesList.length -1)
			attributesString += ",";
	}
	attributesString += "]";
	return attributesString;
};

AbstractAdapter.prototype.triggerEventForTaskExecution = function(attributes){
	var taskExecuted = document.createEvent("Events");
	taskExecuted.initEvent(this.metadata["id"], true, true);
	var doc = CSA.getCurrentDocument();
	CSA.mainWindow.dispatcherId = this.metadata["id"];
	CSA.mainWindow.scriptAttributes = this.getAttributesAsString(attributes);	
	CSA.mainWindow.dispatchEvent(taskExecuted);
};

AbstractAdapter.prototype.getExternalScriptAs = function(uri,aScriptName){
	try{
		var file = Components.classes["@mozilla.org/file/directory_service;1"].
			           getService(Components.interfaces.nsIProperties).
			           get("TmpD", Components.interfaces.nsIFile);
		file.append("tmpAugmenterScript.js");
		file.createUnique(Components.interfaces.nsIFile.NORMAL_FILE_TYPE, 0777);
	}
	catch(e){;}
	
	try{
		var obj_URI = Components.classes["@mozilla.org/network/io-service;1"].getService(Components.interfaces.nsIIOService).newURI(uri, null, null);
		var persist = Components.classes["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"]
		              .createInstance(Components.interfaces.nsIWebBrowserPersist);
		const nsIWBP = Components.interfaces.nsIWebBrowserPersist;
		const flags = nsIWBP.PERSIST_FLAGS_REPLACE_EXISTING_FILES;
		persist.persistFlags = flags | nsIWBP.PERSIST_FLAGS_FROM_CACHE;
		persist.saveURI(obj_URI, null, null, null, "", file, null);
	}
	catch(e){;}
	this.externalScriptsURIs[aScriptName] = "file://" + file.path;
};

AbstractAdapter.prototype.loadExternalScript = function(aScriptName, aContext_){
	var uri = this.externalScriptsURIs[aScriptName];
	if (typeof(uri) != "undefined"){
		try{
			var service = Components.classes["@mozilla.org/moz/jssubscript-loader;1"].getService(Components.interfaces.mozIJSSubScriptLoader);
			service.loadSubScript(uri, aContext_, "UTF-8");
		}
		catch(e){;}
	}	
};
