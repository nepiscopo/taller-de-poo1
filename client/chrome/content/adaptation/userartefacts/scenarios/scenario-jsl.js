TripPlanning = new Scenario();
TripPlanning = {
	pageLoaded:function{
		preconditions:function(){
		  pageLoaded("www.expedia.com");
		  dataCollected.hasInstanceOf("StartDate");
		  dataCollected.hasInstanceOf("EndDate");
		  dataCollected.hasInstanceOf("Destination");
		  components.date.isValidDate(dataCollected.getLastInstanceOf("StartDate"));
		  components.date.isValidDate(dataCollected.getLastInstanceOf("EndDate"));	  
		},
		actions:function(){
		  TripPlanning.destination = dataCollected.getLastInstanceOf("Destination");
		  TripPlanning.dest_input = abstractModel.getDomElement(document,"Destination");
		  TripPlanning.startDate = dataCollected.getLastInstanceOf("StartDate");
		  TripPlanning.sd_input = abstractModel.getDomElement(document,"StartDate");
		  TripPlanning.endDate = dataCollected.getLastInstanceOf("EndDate");
		  TripPlanning.ed_input = abstractModel.getDomElement(document,"EndDate");
		},
		adaptation:function(augmenters){
		  augmenters.copyIntoInput.execute(document,TripPlanning.destination,TripPlanning.input);	  
		  augmenters.copyIntoInput.execute(document,TripPlanning.startDate,TripPlanning.input);
		  augmenters.copyIntoInput.execute(document,TripPlanning.endDate,TripPlanning.input);
		}
	}
}