var instance = null;

function getArtefact(){
	if (instance == null){
		instance = new BookingScenario();
		instance.registerListeners();
	}
	return instance;
}

function BookingScenario(){
	this.name = "BookingScenario";
}

BookingScenario.prototype = new AbstractScenario();


//PODRÍA SER UN METODO DE AbstractScenario
BookingScenario.prototype.thereIsDataNeeded = function(event){
	return event.manager.isConceptInSuitCase("dates") && event.manager.isConceptInSuitCase("destination");
};

BookingScenario.prototype.showMessage = function(event){
	//if(getArtefact().thereIsDataNeeded(event)){
	if(event.manager.isConceptInSuitCase("dates") && event.manager.isConceptInSuitCase("destination")){
		var msg = "Two dates and a destination were created.<br/>Do you want to use it to booking?<br/><a href='http://www.xpedia.com/'>Open Expedia.com</a><br/>";
		event.manager.renderMessage(msg);
		gBrowser.addTab("http://www.expedia.com/");
	}
};	

//ESTO VA A UN COMPONENT:
BookingScenario.prototype.getDatesFromPocketElement = function(datesInstance){
	var months = {"January":"01","Febrary":"02","March":"03","April":"04","May":"05","June":"06","Jule":"07","August":"08","September":"09","October":"10","November":"11","December":"12"};
	var dates = datesInstance.getValue().split(" ");
	var day1 = dates[1].split("-")[0];
	var day2 = dates[1].split("-")[1];
	var month_number = months[dates[0]];
	var year_number = dates[2][2] + dates[2][3];
	var date_from = month_number + "/" + day1 + "/" + year_number;
	var date_to = month_number + "/" + day2 + "/" + year_number;
	var dict = {};
	dict.from = date_from;
	dict.to = date_to;
	return dict;
};

BookingScenario.prototype.pageLoaded = function(event){
	if (event.url == "http://www.expedia.com/"){
		//if(getArtefact().thereIsDataNeeded(event)){
		if(event.manager.isConceptInSuitCase("dates") && event.manager.isConceptInSuitCase("destination")){
			event.document.getElementById("uw_flight_origin_input").value = "La Plata";
			
			var destinations = event.manager.getConceptByName("destination");
			var dest = destinations.getLastInstance();
			event.document.getElementById("uw_flight_destination_input").value = dest.getValue();

			var datesConcept = event.manager.getConceptByName("dates");
			var datesInstance = datesConcept.getLastInstance();
			var months = {"January":"01","Febrary":"02","March":"03","April":"04","May":"05","June":"06","Jule":"07","August":"08","September":"09","October":"10","November":"11","December":"12"};
			var dates = datesInstance.getValue().split(" ");
			var day1 = dates[1].split("-")[0];
			var day2 = dates[1].split("-")[1];
			var month_number = months[dates[0]];
			var year_number = dates[2][2] + dates[2][3];
			var date_from = month_number + "/" + day1 + "/" + year_number;
			var date_to = month_number + "/" + day2 + "/" + year_number;
			var dict = {};
			dict.from = date_from;
			dict.to = date_to;			

			//var dates = getArtefact().getDatesFromPocketElement(datesConcept.getLastInstance());
			event.document.getElementById("uw_flight_dep_date_input").value = dict.from; // --> FROM DATE		
			event.document.getElementById("uw_flight_return_date_input").value = dict.to; // --> TO DATE
			event.document.getElementById("uw_flight_destination_input").value = dest.getValue();
			dest_g = dest.getValue();
			doc_g = event.document;
			setTimeout('setDest()',2000);
		}
	}
};

var dest_g;
var doc_g;  
function setDest(){
	doc_g.getElementById("uw_flight_destination_input").value = dest_g;
};

BookingScenario.prototype.registerListeners = function(){
	this.expressInterestIn("destinationInstantiated", this.showMessage);
	this.expressInterestIn("datesInstantiated", this.showMessage);
	this.expressInterestIn("PageLoaded", this.pageLoaded);
};

getArtefact();
