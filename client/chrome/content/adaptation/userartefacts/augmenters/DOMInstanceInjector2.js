var attributes = [{"name":"Text value","type":"String","value":"","description":"Is the concept name of the DOM instance","example":"Search Result"},{"name":"Target","type":"xPath","value":"","description":"Is the target element in which the element will be injected","example":"/html/body"}];
var metadata = {"name":"DOMInstanceInjecter","id":"DOMInstanceInjecter","scriptAttributes":attributes, "author":"Sergio Firmenich", "description": "This augmenter injects a DOM Instance collected into the Pocket in the selected DOM element"};
/*
 * This displays a dialog box that allows a user to enter their own
 * search terms to highlight on the page, and then passes the search
 * text or phrase to the highlightSearchTerms function. All parameters
 * are optional.
 */
function getAugmenterInstance(){
	return new DOMInstanceInjecter();
};

cloned_node = null;
function DOMInstanceInjecter(){
	this.name = "DOMInstanceInjecter";
	this.label = "Inject";
};

DOMInstanceInjecter.prototype = new AbstractAdapter(metadata);

DOMInstanceInjecter.prototype.isApplicableToConcept = function(){
	return true;
};

DOMInstanceInjecter.prototype.adaptTextPlainInstance = function(instance){	
	return;
};

DOMInstanceInjecter.prototype.injectDOMInstance = function(event){
	if (cloned_node != null){
		var container = event.target
		container.appendChild(cloned_node);
		cloned_node = null;
	}
	getAugmenterInstance().triggerEventForTaskExecution(metadata["scriptAttributes"]);	
};

DOMInstanceInjecter.prototype.adaptDOMElementInstance = function(instance){
	var dom_element = instance.getValue();
  	cloned_node = dom_element.cloneNode();
	this.expressInterestIn("mousedown", content.document, this.injectDOMInstance, true);
  	this.triggerEventForTaskExecution(metadata["scriptAttributes"]);
};

DOMInstanceInjecter.prototype.automaticExecution = function(attributes){
  var textValue = attributes[0]["value"];
  var isConcept = (attributes[1]["value"] == "true") || (attributes[1]["value"] == "True");
  var doc = CSA.getCurrentDocument();
};


