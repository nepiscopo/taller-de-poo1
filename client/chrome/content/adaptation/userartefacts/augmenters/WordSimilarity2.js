function getAugmenterInstance() {
    return new SimilarityAdapter();
}
;

function SimilarityAdapter() {
    this.name = "MSimilarityAdapter";
    this.label = "Word Similarity";
};


SimilarityAdapter.prototype = new AbstractAdapter();

SimilarityAdapter.prototype.isApplicableToConcept = function() {
    return true;
};

function replaceAll(text, source, target) {
    i = 0;
    while (text.toString().indexOf(source) != -1) {
        text = text.toString().replace(source, target);
    }
    return text;
}

SimilarityAdapter.prototype.doHighlight = function(bodyText, searchTerm, highlightStartTag, highlightEndTag){
    var newText = "";
    var i = 0;
    var lcSearchTerm = searchTerm.toLowerCase();
    var lcBodyText = bodyText.toLowerCase();

    var sTildeArray = ['\xe1', "\xe9    ", "\xed", "\xf3", "\xfa"];
    var tTildeArray = ["a", "e", "i", "o", "u"];
    for (var k = 0; k < 5; k++) {
        lcSearchTerm = replaceAll(lcSearchTerm, sTildeArray[k], tTildeArray[k]);
        lcBodyText = replaceAll(lcBodyText, sTildeArray[k], tTildeArray[k]);
    }
    var cant = 0;
    var seguir = true;
    while (seguir) {
        var j = lcBodyText.indexOf(lcSearchTerm, i);
        if (j == -1) {
            newText += bodyText.substr(i + 1);
            seguir = false;
        } else {
            if (lcBodyText.indexOf(">", j) >= lcBodyText.indexOf("<", j)) {
                cant++;
                if (lcBodyText.lastIndexOf("/script>", j) >= lcBodyText.lastIndexOf("<script", j)) {
                    newText += bodyText.substring(i, j) + highlightStartTag + bodyText.substr(j, searchTerm.length) + highlightEndTag;
                } else {
                    newText += bodyText.substring(i, j + searchTerm.length);
                }
            } else {
                newText += bodyText.substring(i, j + searchTerm.length);
            }
            i = j + searchTerm.length;
        }
    }
    return newText;
};


SimilarityAdapter.prototype.highlightSearchTerms = function(doc, searchText, highlightStartTag, highlightEndTag) {
    if (!doc.body || typeof(doc.body.innerHTML) == "undefined")
        return false;
    var bodyText = doc.body.innerHTML;
    bodyText = this.doHighlight(bodyText, searchText, highlightStartTag, highlightEndTag);
    doc.body.innerHTML = bodyText;
    return true;
};

SimilarityAdapter.prototype.highlightTextInstance = function(aString,anId) {
    var highlightStartTag = "<font onmouseover='var tooltip = document.getElementById(\"" + anId + "\");tooltip.style.display = \"block\"' onmouseout='var tooltip = document.getElementById(\"" + anId + "\");tooltip.style.display = \"none\"' style='color:blue; background-color:orange;'>";
    var highlightEndTag = "</font>";
    this.highlightSearchTerms(this.doc, aString, highlightStartTag, highlightEndTag);
};

SimilarityAdapter.prototype.setSimilarityInformation = function(similarity,aString,anId) {
    var finalTD = "";
    var table = "<table>";
    for (var j = 0; j < similarity.length; j++){
        var td = "<td style='border-width:1px;border-style:solid; border-color:red;'>";
        finalTD +=  "<tr>" + td + similarity[j][0] + "</td>" + td + similarity[j][1] +"</td></tr>";
    }
    table += finalTD + "</table>";
    this.doc.getElementById(anId).innerHTML = table;
    this.highlightTextInstance(aString,anId);
};

SimilarityAdapter.prototype.getSimilarityInformation = function(aString,anId) {
    //aca llamar a la url que obtiene la info y setea el callback, que aca llamamos explicitamente con datos "simulados":
    var nameByName = [[anId, "0.38"],
                    ["José Silva", "1.0"],
                  ["Francisco José Silva Silva", "0.53"],
                  ["José Fernando Silva", "0.52"],
                  ["José Luis Silvan-Cardenas", "0.48"],
                  ["Paulo José Silva Silva", "0.48"],
                  ["José C. Silva", "0.41"],
                  ["José Silva Matos", "0.40"],
                  ["Douglas José Silva", "0.39"],
                  ["Elton José Silva", "0.39"],
                  ["José Augusto Silva", "0.39"],
                  ["José Luís Silva", "0.39"],
                  ["José Machado Silva", "0.39"],
                  ["José Reinaldo Silva", "0.39"],
                  ["José Silvestre Silva", "0.39"],
                  ["José A. Silva", "0.38"],
                  ["José F. Silva", "0.38"], 
                  ["...", "..."]
                  ];
    this.setSimilarityInformation(nameByName,aString,anId);
};

SimilarityAdapter.prototype.adaptTextPlainInstance = function(anInstance) {
    var aString = anInstance.getValue();
    var anId = aString.replace(/\s/g,'', "") + "ToolTip";
    this.renderToolTip(this.doc,anId);
    this.getSimilarityInformation(aString,anId);
};

SimilarityAdapter.getColor = function() {
    var colors = ["orange"];
    return (colors[0]);
};

SimilarityAdapter.prototype.renderToolTip = function(doc,anId){
    var body = doc.body;
    var div = doc.createElement('div');
    div.setAttribute("id",anId);
    div.setAttribute("style","position:absolute;display:none;background-color:#FFEEC7;border:1px solid black;padding:0.0em; font-size:0.8em;");
    body.appendChild(div);    
    body.setAttribute("onmousemove",body.getAttribute("onmousemove") + ";" + 'var tooltip = document.getElementById("'+ anId +'");var tempX = event.pageX + 10;var tempY = event.pageY + 10; if(tempX < 0)tempX = 0;if(tempY < 0)tempY = 0;tooltip.style.top = tempY + "px";tooltip.style.left = tempX + "px";');
    return doc;
};