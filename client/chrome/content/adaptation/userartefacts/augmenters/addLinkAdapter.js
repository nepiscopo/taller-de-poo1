//CSNH ICWE2011
function getAugmenterInstance(){
	return new LinkAdditionAdapter();
};

var data_for_simple_copy = null;

function LinkAdditionAdapter(){
	this.name = "LinkAdditionAdapter";
	this.label = "Add link near";	
};

LinkAdditionAdapter.prototype = new AbstractAdapter();

LinkAdditionAdapter.prototype.isApplicableToConcept = function(){
	return true;
};

LinkAdditionAdapter.prototype.applyToInstance = function (document, instance, args){
	return;
  //var target_function = this.getFunction(args["targetLink"]);
  //try{target_function(document,instance)}
  //catch(e){};
};

LinkAditionAdapter.prototype.addGoogleMapsLink = function (document, instance){
  var anchor = document.createElement("a");
  var href_attribute = "http://www.flickr.com/search/?q="+ instance +"&m=tags";
  anchor.setAttribute("href",href);
  anchor.innerHTML = "See in GoogleMaps";
  this.addLink(document, instance, anchorStartTag, anchorEndTag);
};

LinkAditionAdapter.prototype.do_transformation = function(bodyText, searchTerm, anchorStartTag, anchorEndTag){
  var newText = "";
  var i = -1;
  var lcSearchTerm = searchTerm.toLowerCase();
  var lcBodyText = bodyText.toLowerCase();
    
  while (bodyText.length > 0) {
    i = lcBodyText.indexOf(lcSearchTerm, i+1);
    if (i < 0) {
      newText += bodyText;
      bodyText = "";
    } else {
      // skip anything inside an HTML tag
      if (bodyText.lastIndexOf(">", i) >= bodyText.lastIndexOf("<", i)) {
        // skip anything inside a <script> block
        if (lcBodyText.lastIndexOf("/script>", i) >= lcBodyText.lastIndexOf("<script", i)) {
          newText += bodyText.substring(0, i) + anchorStartTag + bodyText.substr(i, searchTerm.length) + anchorEndTag;
          bodyText = bodyText.substr(i + searchTerm.length);
          lcBodyText = bodyText.toLowerCase();
          i = -1;
        }
      }
    }
  }  
  return newText;
};

LinkAditionAdapter.prototype.transformTerms = function (document, searchText, anchorStartTag, anchorEndTag){
  searchArray = [searchText];
  if (!document.body || typeof(document.body.innerHTML) == "undefined")
    return false;  
  
  var bodyText = document.body.innerHTML;
  for (var i = 0; i < searchArray.length; i++) {
    bodyText = this.do_transformation(bodyText, searchArray[i], anchorStartTag, anchorEndTag);
  }  
  document.body.innerHTML = bodyText;
  return true;
};
