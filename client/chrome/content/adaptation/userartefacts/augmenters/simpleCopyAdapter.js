var attributes = [{"name":"Text value","type":"String","value":"empty :)","example":"Jhon Foo"},{"name":"Input xPath","type":"xPath","value":"/html/body/div/div[2]/div/div/form/table/tbody/tr/td/input","example":"an xPath Expression"},{"name":"Is concept?","type":"Boolean","value":"false","example":"false"}];
var metadata = {"name":"CopyIntoInput","id":"CopyIntoInput","scriptAttributes":attributes,"author":"Sergio Firmenich", "description": "This augmenter allows you to copy a value from the pocket to the selected input"};

function getAugmenterInstance(){
	return new SimpleCopyAdapter();
};

var data_for_simple_copy = null;

function SimpleCopyAdapter(){
	this.name = "SimpleCopyAdapter";
	this.label = "Copy into input ";
};

SimpleCopyAdapter.prototype = new AbstractAdapter(metadata);

SimpleCopyAdapter.prototype.isApplicableToConcept = function(){
	return false;
};

SimpleCopyAdapter.prototype.copyToInput = function(event){
	if(event.target.nodeName.toUpperCase() == "INPUT"){
		event.target.focus();
		event.target.value = data_for_simple_copy;
		getAugmenterInstance().triggerEventForTaskExecution(metadata["scriptAttributes"]);
	}
};

SimpleCopyAdapter.prototype.adaptTextPlainInstance = function(instance){
	data_for_simple_copy = instance.getValue();
	this.expressInterestIn("mousedown", content.document, this.copyToInput, true);
};

SimpleCopyAdapter.prototype.automaticExecution = function(attributes){
	var textValue = attributes[0]["value"];
	var dom_element_xpath = attributes[1]["value"];
	var is_concept = attributes[2]["value"] == "true";
	if (!is_concept){
		try{
			var dom_element = CSA.getElementFromXPath(dom_element_xpath,content.document);
			dom_element.setAttribute("value",textValue);
			this.triggerEventForTaskExecution(attributes);
		}catch(e){;}
	}	
	else{
		var concept = CSA.pocket.getConceptByName(textValue);
		if (concept){
			var instance = concept.getLastInstance();
			var dom_element = CSA.getElementFromXPath(dom_element_xpath,content.document);
			dom_element.setAttribute("value",instance.getValue());
			this.triggerEventForTaskExecution(attributes);
		}
	}
};