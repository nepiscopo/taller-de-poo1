var attributes = [{"name":"Text value","type":"String","value":"empty :)","example":"Jhon Foo"},{"name":"Is a Concept","type":"Boolean","value":"false","example":"true"}];
var metadata = {"name":"DOMInstanceInjecter","id":"DOMInstanceInjecter","scriptAttributes":attributes, "author":"Sergio Firmenich", "description": "This augmenter highlights the ocurrences of the data selected from the Pocket"};
/*
 * This displays a dialog box that allows a user to enter their own
 * search terms to highlight on the page, and then passes the search
 * text or phrase to the highlightSearchTerms function. All parameters
 * are optional.
 */
function getAugmenterInstance(){
	return new DOMInstanceInjecter();
};

function DOMInstanceInjecter(){
	this.name = "DOMInstanceInjecter";
	this.label = "Inject";
};

DOMInstanceInjecter.prototype = new AbstractAdapter(metadata);

DOMInstanceInjecter.prototype.isApplicableToConcept = function(){
	return true;
};

DOMInstanceInjecter.prototype.adaptTextPlainInstance = function(instance){	
	this.highlight_text(this.doc, instance.getValue(),true,false,false);
  this.triggerEventForTaskExecution(metadata["scriptAttributes"]);
};

DOMInstanceInjecter.prototype.adaptDOMElementInstance = function(instance){
	var dom_element = instance.getValue();
  alert(dom_element);
	dom_element.setAttribute("style","background-color:yellow;");
  this.triggerEventForTaskExecution(metadata["scriptAttributes"]);
};

DOMInstanceInjecter.prototype.automaticExecution = function(attributes){
  var textValue = attributes[0]["value"];
  var isConcept = (attributes[1]["value"] == "true") || (attributes[1]["value"] == "True");
  var doc = CSA.getCurrentDocument();
};


