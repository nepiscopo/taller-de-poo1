var attributes = [{"name":"Text value","type":"String","value":"empty :)","example":"Jhon Foo"},{"name":"Is a Concept","type":"Boolean","value":"false","example":"true"}];
var metadata = {"name":"Highlight","id":"SimpleHighlight","scriptAttributes":attributes, "author":"Sergio Firmenich", "description": "This augmenter highlights the ocurrences of the data selected from the Pocket"};
/*
 * This displays a dialog box that allows a user to enter their own
 * search terms to highlight on the page, and then passes the search
 * text or phrase to the highlightSearchTerms function. All parameters
 * are optional.
 */
function getAugmenterInstance(){
	return new HighlightingAdapter();
};

function HighlightingAdapter(){
	this.name = "HighlightingAdapter";
	this.label = "Highlight";
};

HighlightingAdapter.prototype = new AbstractAdapter(metadata);

HighlightingAdapter.prototype.isApplicableToConcept = function(){
	return true;
};

HighlightingAdapter.prototype.adaptTextPlainInstance = function(instance){	
	this.highlight_text(this.doc, instance.getValue(),true,false,false);
  this.triggerEventForTaskExecution(metadata["scriptAttributes"]);
};

HighlightingAdapter.prototype.adaptDOMElementInstance = function(instance){
	var dom_element = instance.getValue();
	dom_element.setAttribute("style","background-color:yellow;");
  this.triggerEventForTaskExecution(metadata["scriptAttributes"]);
};

/*
HighlightingAdapter.prototype.startAutomaticExecution = function(scriptAttributes){
  //Si no quiero que se ejecute automaticamente tomando a los valores de los atributos para obtener un objeto del Pocket
  //tengo que redefinir #startAutomationExecution para que ejecute el método de adaptacion especifico #automaticExecution.
  this.automaticExecution(scriptAttributes)
};*/

HighlightingAdapter.prototype.automaticExecution = function(attributes){
  this.defineAutomaticExecution();
  var textValue = attributes[0]["value"];
  var isConcept = (attributes[1]["value"] == "true") || (attributes[1]["value"] == "True");
  var doc = CSA.getCurrentDocument();
  this.highlight_text(doc, textValue,true,false,false);
  this.triggerEventForTaskExecution(metadata["scriptAttributes"]);
};

HighlightingAdapter.prototype.highlight_text = function(doc, searchText, treatAsPhrase, textColor, bgColor){
  // we can optionally use our own highlight tag values
  if ((!textColor) || (!bgColor)) {
    textColor = "black";
    bgColor = "yellow";
  } 
  highlightStartTag = "<font style='color:" + textColor + "; background-color:" + bgColor + ";'>";
  highlightEndTag = "</font>"; 
  this.highlightSearchTerms(doc, searchText, treatAsPhrase, true, highlightStartTag, highlightEndTag);
};

/*
 * This is the function that actually highlights a text string by
 * adding HTML tags before and after all occurrences of the search
 * term. You can pass your own tags if you'd like, or if the
 * highlightStartTag or highlightEndTag parameters are omitted or
 * are empty strings then the default <font> tags will be used.
 */
HighlightingAdapter.prototype.doHighlight = function(bodyText, searchTerm, highlightStartTag, highlightEndTag) 
{
  // the highlightStartTag and highlightEndTag parameters are optional
  if ((!highlightStartTag) || (!highlightEndTag)) {
    highlightStartTag = "<font style='color:blue; background-color:yellow;'>";
    highlightEndTag = "</font>";
  }
  
  // find all occurences of the search term in the given text,
  // and add some "highlight" tags to them (we're not using a
  // regular expression search, because we want to filter out
  // matches that occur within HTML tags and script blocks, so
  // we have to do a little extra validation)
  var newText = "";
  var i = -1;
  var lcSearchTerm = searchTerm.toLowerCase();
  var lcBodyText = bodyText.toLowerCase();
    
  while (bodyText.length > 0) {
    i = lcBodyText.indexOf(lcSearchTerm, i+1);
    if (i < 0) {
      newText += bodyText;
      bodyText = "";
    } else {
      // skip anything inside an HTML tag
      if (bodyText.lastIndexOf(">", i) >= bodyText.lastIndexOf("<", i)) {
        // skip anything inside a <script> block
        if (lcBodyText.lastIndexOf("/script>", i) >= lcBodyText.lastIndexOf("<script", i)) {
          newText += bodyText.substring(0, i) + highlightStartTag + bodyText.substr(i, searchTerm.length) + highlightEndTag;
          bodyText = bodyText.substr(i + searchTerm.length);
          lcBodyText = bodyText.toLowerCase();
          i = -1;
        }
      }
    }
  }
  
  return newText;
};


/*
 * This is sort of a wrapper function to the doHighlight function.
 * It takes the searchText that you pass, optionally splits it into
 * separate words, and transforms the text on the current web page.
 * Only the "searchText" parameter is required; all other parameters
 * are optional and can be omitted.
 */
HighlightingAdapter.prototype.highlightSearchTerms = function(doc, searchText, treatAsPhrase, warnOnFailure, highlightStartTag, highlightEndTag){
  // if the treatAsPhrase parameter is true, then we should search for 
  // the entire phrase that was entered; otherwise, we will split the
  // search string so that each word is searched for and highlighted
  // individually
  if (treatAsPhrase) {
    searchArray = [searchText];
  } else {
    searchArray = searchText.split(" ");
  }
  if (!doc.body || typeof(doc.body.innerHTML) == "undefined") {
    if (warnOnFailure) {
      alert("Sorry, for some reason the text of this page is unavailable. Searching will not work.");
    }
    return false;
  }  
  var bodyText = doc.body.innerHTML;
  for (var i = 0; i < searchArray.length; i++) {
    bodyText = this.doHighlight(bodyText, searchArray[i], highlightStartTag, highlightEndTag);
  }
  
  doc.body.innerHTML = bodyText;
  return true;
};

