var attributes = [{"name":"Concept Name","type":"String","value":"","description":"Is the concept name of the DOM instance","example":"Search Result"},{"name":"Target","type":"xPath","value":"","description":"Is the target element in which the element will be injected","example":"/html/body"}];
var metadata = {"name":"DOMFloatAddition","id":"DOMFloatAddition","scriptAttributes":attributes, "author":"Sergio Firmenich", "description": "This augmenter adds a DOM Instance collected into the Pocket as a floating element in the current Web page"};
/*
 * This displays a dialog box that allows a user to enter their own
 * search terms to highlight on the page, and then passes the search
 * text or phrase to the highlightSearchTerms function. All parameters
 * are optional.
 */
function getAugmenterInstance(){
	return new DOMFloatAddition();
};

function DOMFloatAddition(){
	this.name = "DOMFloatAddition";
	this.label = "Add as a float element";
};

DOMFloatAddition.prototype = new AbstractAdapter(metadata);

DOMFloatAddition.prototype.isApplicableToConcept = function(){
	return true;
};

DOMFloatAddition.prototype.adaptTextPlainInstance = function(instance){	
	return;
};

function getStyles(aNode){ 
    return aNode.currentStyle || aNode.ownerDocument.defaultView.getComputedStyle(aNode, null);
};

function setComputedStyle(aNode, styles){ 
    var css = "";
    properties = ["background-color","color","font","width","height","border","padding","align","size","accelerator","azimuth","background","background-attachment","background-color","background-image","background-position","background-position-x","background-position-y","background-repeat","behavior","border","border-bottom","border-bottom-color","border-bottom-style","border-bottom-width","border-collapse","border-color","border-left","border-left-color","border-left-style","border-left-width","border-right","border-right-color","border-right-style","border-right-width","border-spacing","border-style","border-top","border-top-color","border-top-style","border-top-width","border-width","bottom","padding","padding-bottom","padding-left","padding-right","padding-top","page","page-break-after","page-break-before","page-break-inside","pause","pause-after","pause-before","pitch","pitch-range","play-during","position","margin","margin-bottom","margin-left","margin-right","margin-top","marker-offset","marks","max-height","max-width","min-height","min-width","-moz-binding","-moz-border-radius","-moz-border-radius-topleft","-moz-border-radius-topright","-moz-border-radius-bottomright","-moz-border-radius-bottomleft","-moz-border-top-colors","-moz-border-right-colors","-moz-border-bottom-colors","-moz-border-left-colors","-moz-opacity","-moz-outline","-moz-outline-color","-moz-outline-style","-moz-outline-width","-moz-user-focus","-moz-user-input","-moz-user-modify","-moz-user-select","height","ime-mode","include-source","-set-link-source","size","speak","speak-header","speak-numeral","speak-punctuation","speech-rate","stress","scrollbar-arrow-color","scrollbar-base-color","scrollbar-dark-shadow-color","scrollbar-face-color","scrollbar-highlight-color","scrollbar-shadow-color","scrollbar-3d-light-color","scrollbar-track-color","table-layout","text-align","text-align-last","text-decoration","text-indent","text-justify","text-overflow","text-shadow","text-transform","text-autospace","text-kashida-space","text-underline-position","top","white-space","widows","width","word-break","word-spacing","word-wrap","writing-mode","z-index","zoom"];

    for (var i = properties.length - 1; i >= 0; i--) {
      if (styles.getPropertyValue(properties[i]) != "")
        css += properties[i] + ":" + styles.getPropertyValue(properties[i]) + ";"
    };
    aNode.setAttribute("style",css);
};


DOMFloatAddition.prototype.adaptDOMElementInstance = function(instance){
  var dom_element = instance.getValue();
  var cloned_node = dom_element.cloneNode(true);
  links = cloned_node.getElementsByTagName("A");
  for (var i = 0; i < links.length; i++){
    links[i].setAttribute("href",links[i].href);
  }
  if (cloned_node.nodeName)
    cloned_node.setAttribute("href",cloned_node.href);
  var style_cloned = getStyles(dom_element);
  setComputedStyle(cloned_node, style_cloned);
  alert(cloned_node.getAttribute("style"));
  var style = cloned_node.getAttribute("style") + "position: fixed; top: 50px; left: 50px; z-index: 2147483647; font-weight: normal; opacity: 1; display: block;"; 
  alert(style);
  cloned_node.setAttribute("style",style);
  content.document.body.appendChild(cloned_element);
  this.triggerEventForTaskExecution(metadata["scriptAttributes"]);
};

DOMFloatAddition.prototype.automaticExecution = function(attributes){
  var textValue = attributes[0]["value"];
  var isConcept = (attributes[1]["value"] == "true") || (attributes[1]["value"] == "True");
  var concept = attributes[0]["value"];
  var dom_element_xpath = attributes[1]["value"];
  var doc = CSA.getCurrentDocument();
  var dom_element = CSA.getElementFromXPath(dom_element_xpath,doc);
};


