var attributes = [{"name":"Text value","type":"String","value":"ConferenceData","example":"Jhon Foo"},{"name":"Is a Concept","type":"Boolean","value":"true","example":"true"}];
var metadata = {"name":"Bouncer","id":"Bouncer","scriptAttributes":attributes, "author":"Sergio Firmenich", "description": "This augmenter highlights the ocurrences of the data selected from the Pocket"};
/*
 * This displays a dialog box that allows a user to enter their own
 * search terms to highlight on the page, and then passes the search
 * text or phrase to the highlightSearchTerms function. All parameters
 * are optional.
 */
function getAugmenterInstance(){
    return new Bouncer();
};


//var animation_block = 'animate = function(eid){ document.getElementById(eid).style = "background-color:yellow;width:200px;height:200px;-moz-transform:rotate(360deg);setTimeout("var elem = document.getElementById(\'" + eid + "\'); var evt = document.createEvent("MouseEvents");evt.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);elem.dispatchEvent(evt);", 5);"';
//var fadeout_script_block = 'fadeout = function(eid){ document.getElementById(eid).style.border = (document.getElementById(eid).style.border == "")? "red solid 1px":parseFloat(document.getElementById(eid).style.opacity) - 0.05; (document.getElementById(eid).style.opacity >= "0")?setTimeout("fadeout(\'" + eid + "\')", 70):document.getElementById(eid).style.opacity;};fadein = function(eid){ document.getElementById(eid).style.opacity = (document.getElementById(eid).style.opacity == "")? 0:parseFloat(document.getElementById(eid).style.opacity) + 0.05; (document.getElementById(eid).style.opacity <= "1")?setTimeout("fadein(\'" + eid + "\')", 30):document.getElementById(eid).style.opacity;};';


function Bouncer(){
    this.name = "Bouncer";
    this.label = "Bounce";
};

Bouncer.prototype = new AbstractAdapter(metadata);

Bouncer.prototype.isApplicableToConcept = function(){
    return true;
};

Bouncer.prototype.adaptTextPlainInstance = function(instance){    
    this.highlight_text(this.doc, instance.getValue(),true,false,false);
    this.triggerEventForTaskExecution(metadata["scriptAttributes"]);
};

Bouncer.prototype.adaptDOMElementInstance = function(instance){
    var css_animation = CSA.getCurrentDocument().createElement("style");
    css_animation.setAttribute("type","text/css");
//  css_animation.innerHTML = ".animationBouncer { -moz-animation: 800ms ease-out 0s normal none 1 bounce; animation: 800ms ease-out 0s normal none 1 bounce;box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.3);}";
//  css_animation.innerHTML = ".animationBouncer {-moz-animation-delay: 0.5s;-moz-animation-duration: 2s;-moz-animation-fill-mode: backwards;-moz-animation-iteration-count:infinite;-moz-animation-name: bounce;}@-moz-keyframes bounce {0%   { top:000px; -moz-animation-timing-function:ease-in;}37%  { top:500px; -moz-animation-timing-function:ease-out;}55%  { top:375px; -moz-animation-timing-function:ease-in;}73%  { top:500px; -moz-animation-timing-function:ease-out;}82%  { top:465px; -moz-animation-timing-function:ease-in;}91%  { top:500px; -moz-animation-timing-function:ease-out;}96%  { top:490px; -moz-animation-timing-function:ease-in;}100% { top:500px;}}";
    css_animation.innerHTML = ".animationBouncer {-moz-animation: 800ms ease-out 0s bounce infinite;animation: 800ms ease-out 0s bounce infinite;} @-moz-keyframes bounce {0%, 20%, 50%, 80%, 100% {-moz-transform: translateY(0);}40% {-moz-transform: translateY(-30px);}60% {-moz-transform: translateY(-15px);}}";

    var dom_element = instance.getValue();
    var doc = CSA.getCurrentDocument();
    metadata["scriptAttributes"][0]["value"] = CSA.getXPathElement(dom_element);
    var container = doc.createElement("div");
    dom_element.parentNode.replaceChild(container,dom_element);
    container.appendChild(dom_element);
    container.parentNode.appendChild(css_animation);
    container.setAttribute("class","animationBouncer");
    metadata["scriptAttributes"][1]["value"] = "false";
    this.triggerEventForTaskExecution(metadata["scriptAttributes"]);
};

Bouncer.prototype.automaticExecution = function(attributes){
    var xPath = attributes[0]["value"];

    var css_animation = CSA.getCurrentDocument().createElement("style");
    css_animation.setAttribute("type","text/css");
    css_animation.innerHTML = ".animationBouncer {-moz-animation: 800ms ease-out 0s bounce infinite;animation: 800ms ease-out 0s bounce infinite;} @-moz-keyframes bounce {0%, 20%, 50%, 80%, 100% {-moz-transform: translateY(0);}40% {-moz-transform: translateY(-30px);}60% {-moz-transform: translateY(-15px);}}";

    var doc = CSA.getCurrentDocument();
    var dom_element = CSA.getElementFromXPath(xPath, doc);
    var container = doc.createElement("div");
    dom_element.parentNode.replaceChild(container,dom_element);
    container.appendChild(dom_element);
    container.parentNode.appendChild(css_animation);
    container.setAttribute("class","animationBouncer");
    this.triggerEventForTaskExecution(metadata["scriptAttributes"]);
};


Bouncer.prototype.startAutomaticExecution = function(dispatcher, doc, scriptAttributes){
    var isConcept = scriptAttributes[1]["value"].toLowerCase() == "true";
    if (isConcept)
        dispatcher.automaticExecution(this, doc, pocketStringValue, isConcept);
    else{
        this.automaticExecution(scriptAttributes, doc);
    }
};
