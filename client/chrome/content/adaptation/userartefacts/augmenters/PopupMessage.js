var attributes = [{"name":"Message","type":"String","value":"Do you want to go to <a href='http://www..com'></a>?","description":"Is the inner HTML used for rendering the message","example":"Search Result"}];
var metadata = {"name":"PopUpMessage","id":"PopUpMessage","scriptAttributes":attributes, "author":"Sergio Firmenich", "description": "This augmenter shows a message"};
/*
 * This displays a dialog box that allows a user to enter their own
 * search terms to highlight on the page, and then passes the search
 * text or phrase to the highlightSearchTerms function. All parameters
 * are optional.
 */
function getAugmenterInstance(){
	return new PopUpMessage();
};

function PopUpMessage(){
	this.name = "PopUpMessage";
	this.label = "Use for PopUpMessage";
};

PopUpMessage.prototype = new AbstractAdapter(metadata);

PopUpMessage.prototype.isApplicableToConcept = function(){
	return false;
};

PopUpMessage.prototype.adaptDOMElementInstance = function(instance){	
	return;
};

var fadein_script_block = 'fadein = function(eid){ document.getElementById(eid).style.opacity = (document.getElementById(eid).style.opacity == "")? 0:parseFloat(document.getElementById(eid).style.opacity) + 0.05; (document.getElementById(eid).style.opacity <= "1")?setTimeout("fadein(\'" + eid + "\')", 30):document.getElementById(eid).style.opacity;};';
var fade_script_block = 'fadeout = function(eid){ document.getElementById(eid).style.opacity = (document.getElementById(eid).style.opacity == "")? 1:parseFloat(document.getElementById(eid).style.opacity) - 0.05; (document.getElementById(eid).style.opacity >= "0")?setTimeout("fadeout(\'" + eid + "\')", 70):document.getElementById(eid).style.opacity;};fadein = function(eid){ document.getElementById(eid).style.opacity = (document.getElementById(eid).style.opacity == "")? 0:parseFloat(document.getElementById(eid).style.opacity) + 0.05; (document.getElementById(eid).style.opacity <= "1")?setTimeout("fadein(\'" + eid + "\')", 30):document.getElementById(eid).style.opacity;};';

PopUpMessage.prototype.adaptTextPlainInstance = function(instance){
  var doc = CSA.getCurrentDocument();

  var adaptation_message = null;
  var style = "padding:20px 20px 20px 20px; font-size: 10pt; color: #333333;font-family:'arial';background-color:#CCDDEE;width: auto; height: auto; position: fixed; top: 50px; right: 50px;  border: 1px solid #336699; display: block;opacity:0;-moz-border-radius: 10px;"; 
  if(adaptation_message==null)
    var adaptation_message = doc.createElement("div");
  else{
    style = adaptation_message.getAttribute("style"); 
    adaptation_message.innerHTML = "";
  }
  var fade_content = doc.createElement("script");
  fade_content.setAttribute("type","text/javascript");
  fade_content.innerHTML = fade_script_block;
  var msg = null;
  adaptation_message.setAttribute("id","lifia-csnh-message-csa-id");
  adaptation_message.setAttribute("style",style);
  adaptation_message.setAttribute("onclick","fadein('lifia-csnh-message-csa-id')");
  adaptation_message.innerHTML = (msg != null)? msg:"Si desea disparar la adaptación haga click <a href='#'>aquí</a>";

  doc.body.appendChild(adaptation_message);
  adaptation_message.appendChild(fade_content);

  var evt = doc.createEvent("MouseEvents");
  evt.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
  adaptation_message.dispatchEvent(evt);
  adaptation_message.setAttribute("onclick","setTimeout('fadeout(" + '"lifia-csnh-message-csa-id"' + ")',1);");
  //adaptation_message.setAttribute("onclick",adaptation_message.getAttribute("onclick") + ";setTimeout('document.getElementById(" + '"lifia-csnh-message-csa-id"' + ").parentNode.removeChild(document.getElementById(" + '"lifia-csnh-message-csa-id"' + "))',7000);");
};

PopUpMessage.prototype.automaticExecution = function(attributes){
  var textValue = attributes[0]["value"];
  var doc = CSA.getCurrentDocument();

  var style = "padding:20px 20px 20px 20px; font-size: 10pt; color: #333333;font-family:'arial';background-color:#CCDDEE;width: auto; height: auto; position: fixed; top: 50px; right: 50px;  border: 1px solid #336699; display: block;opacity:0;-moz-border-radius: 10px;"; 
  if(adaptation_message==null)
    var adaptation_message = doc.createElement("div");
  else{
    style = adaptation_message.getAttribute("style"); 
    adaptation_message.innerHTML = "";
  }

  var fade_content = doc.createElement("script");
  fade_content.setAttribute("type","text/javascript");
  fade_content.innerHTML = fade_script_block;

  adaptation_message.setAttribute("id","lifia-csnh-message-csa-id");
  adaptation_message.setAttribute("style",style);
  adaptation_message.setAttribute("onclick","fadein('lifia-csnh-message-csa-id')");
  adaptation_message.innerHTML = (textValue != null)?textValue:"Si desea disparar la adaptación haga click <a href='#'>aquí</a>";

  doc.body.appendChild(adaptation_message);
  adaptation_message.appendChild(fade_content);

  var evt = doc.createEvent("MouseEvents");
  evt.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
  adaptation_message.dispatchEvent(evt);

  adaptation_message.setAttribute("onclick","setTimeout('fadeout(" + '"lifia-csnh-message-csa-id"' + ")',0);");
  //adaptation_message.setAttribute("onclick",adaptation_message.getAttribute("onclick") + ";setTimeout('document.getElementById(" + '"lifia-csnh-message-csa-id"' + ").parentNode.removeChild(document.getElementById(" + '"lifia-csnh-message-csa-id"' + "))',7000);");
  //var dom_element = CSA.getElementFromXPath(dom_element_xpath,doc);
  this.triggerEventForTaskExecution(metadata["scriptAttributes"]);
};

PopUpMessage.prototype.startAutomaticExecution = function(dispatcher, doc, scriptAttributes){
  //var pocketStringValue = scriptAttributes[0]["value"];
  //var isConcept = scriptAttributes[1]["value"].toLowerCase() == "true";
  //dispatcher.automaticExecution(this, doc, pocketStringValue, isConcept);
  this.automaticExecution(scriptAttributes);
};

