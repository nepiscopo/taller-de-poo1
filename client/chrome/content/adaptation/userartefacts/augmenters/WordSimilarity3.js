var metadata = {"name":"Similarity","id":"SimpleSimilarity","scriptAttributes":[], "author":"Sergio Firmenich", "description": "This augmenter consumes similarity services for adapt the current Web page."};

function getAugmenterInstance() {
    return new SimilarityAdapter();
};

function SimilarityAdapter() {
    this.name = "MSimilarityAdapter";
    this.label = "Word Similarity";
    this.similarityFunction = "TF-IDF";
    this.manager = null;
    this.getExternalScriptAs("https://dl.dropboxusercontent.com/u/15619201/SimilarityManager.user.js", "FunctionalityManager");
};

SimilarityAdapter.prototype = new AbstractAdapter();
SimilarityAdapter.prototype.constructor = SimilarityAdapter;

SimilarityAdapter.prototype.isQuickAugmenter = function(){
    return true;
};

SimilarityAdapter.prototype.__addQuickMenus__ = function(){
    if(this.manager == null){
        this.manager = {};
        this.loadExternalScript("FunctionalityManager",this.manager);
    }
    var manager = new this.manager.SimilarityFunctionManager();
    var similarityFunctions = manager.getSimilarityFunctions();
    for (sfunction in similarityFunctions){
        this.addQuickMenu(sfunction,"TEXT","setSimilarityFunction('" + sfunction + "')");    
    }
};

SimilarityAdapter.prototype.setSimilarityFunction = function(aFunctionName) {
    this.similarityFunction = aFunctionName;
};

SimilarityAdapter.prototype.isApplicableToConcept = function() {
    return true;
};

function replaceAll(text, source, target) {
    i = 0;
    while (text.toString().indexOf(source) != -1) {
        text = text.toString().replace(source, target);
    }
    return text;
}

SimilarityAdapter.prototype.doHighlight = function(bodyText, searchTerm, highlightStartTag, highlightEndTag){
  if ((!highlightStartTag) || (!highlightEndTag)) {
    highlightStartTag = "<font style='color:blue; background-color:yellow;'>";
    highlightEndTag = "</font>";
  }
  
  var newText = "";
  var i = -1;
  var lcSearchTerm = searchTerm.toLowerCase();
  var lcBodyText = bodyText.toLowerCase();
    
  while (bodyText.length > 0) {
    i = lcBodyText.indexOf(lcSearchTerm, i+1);
    if (i < 0) {
      newText += bodyText;
      bodyText = "";
    } else {
      // skip anything inside an HTML tag
      if (bodyText.lastIndexOf(">", i) >= bodyText.lastIndexOf("<", i)) {
        // skip anything inside a <script> block
        if (lcBodyText.lastIndexOf("/script>", i) >= lcBodyText.lastIndexOf("<script", i)) {
          newText += bodyText.substring(0, i) + highlightStartTag + bodyText.substr(i, searchTerm.length) + highlightEndTag;
          bodyText = bodyText.substr(i + searchTerm.length);
          lcBodyText = bodyText.toLowerCase();
          i = -1;
        }
      }
    }
  } 
  return newText;
};


SimilarityAdapter.prototype.highlightSearchTerms = function(doc, searchText, highlightStartTag, highlightEndTag) {
    if (!doc.body || typeof(doc.body.innerHTML) == "undefined")
        return false;
    var bodyText = doc.body.innerHTML;
    bodyText = this.doHighlight(bodyText, searchText, highlightStartTag, highlightEndTag);
    doc.body.innerHTML = bodyText;
    return true;
};

SimilarityAdapter.prototype.highlightTextInstance = function(aString,anId){
    var highlightStartTag = "<font onmouseover='var tooltip = document.getElementById(\"" + anId + "\");tooltip.style.display = \"block\"' onmouseout='var tooltip = document.getElementById(\"" + anId + "\");tooltip.style.display = \"none\"' style='color:blue; background-color:orange;'>";
    var highlightEndTag = "</font>";
    this.highlightSearchTerms(this.doc, aString, highlightStartTag, highlightEndTag);
};

SimilarityAdapter.prototype.setSimilarityInformation = function(similarity,aString,anId) {
    var heads = similarity["heads"];
    var values = similarity["results"];
    var finalTH = "";
    var finalTD = "";
    var table = "<table>";
    var td = "";
    for (var j = 0; j < heads.length; j++){
        td += "<td style='border-width:2px;border-style:solid;border-color:red;'>" + heads[j] + "</td>";
    }
    finalTH +=  "<tr>" + td + "</tr>";
    table += finalTH;
    for (var j = 0; j < values.length; j++){
        var td = "";
        for (var k = 0; k < values[j].length; k++)
            td += "<td style='border-width:1px;border-style:solid; border-color:red;'>" + values[j][k] + "</td>";
        table +=  "<tr>" + td + "</tr>";
    }    
    table += "</table>";
    this.highlightTextInstance(aString,anId);
    this.renderToolTip(this.doc,anId);
    this.doc.getElementById(anId).innerHTML = table;
};

SimilarityAdapter.prototype.getSimilarityInformation = function(aString,anId) {
    var manager = new this.manager.SimilarityFunctionManager();
    var result = manager.calculateSim(aString,"",this.similarityFunction);
    this.setSimilarityInformation(result,aString,anId);
};

SimilarityAdapter.prototype.adaptTextPlainInstance = function(anInstance) {
    if(this.manager == null){
        this.manager = {};
        this.loadExternalScript("FunctionalityManager",this.manager);
    }
    var aString = anInstance.getValue();
    var anId = aString.replace(/\s/g,'', "") + "ToolTip";
    this.getSimilarityInformation(aString,anId);
};

SimilarityAdapter.getColor = function() {
    var colors = ["orange"];
    return (colors[0]);
};
 
SimilarityAdapter.prototype.renderToolTip = function(doc,anId){
    var body = doc.body;
    var div = doc.createElement('div');
    div.setAttribute("style","position:absolute;display:none;background-color:#FFEEC7;border:1px solid black;padding:0.0em; font-size:0.8em;");
    body.appendChild(div);
    div.setAttribute("id", anId);
    body.setAttribute("onmousemove",body.getAttribute("onmousemove") + ";" + 'var tooltip = document.getElementById("'+ anId +'");var tempX = event.pageX + 10;var tempY = event.pageY + 10; if(tempX < 0)tempX = 0;if(tempY < 0)tempY = 0;tooltip.style.top = tempY + "px";tooltip.style.left = tempX + "px";');
    return doc;
};
