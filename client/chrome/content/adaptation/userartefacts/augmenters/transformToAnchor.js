var metadata = {"name":"TransformIntoWikiLink", "author":"Sergio Firmenich", "description": "This augmenter convert any occurrence of the Pocket data into a corresponding Wikipedia link"};
function getAugmenterInstance(){
	return new WikiLinkAdapter();
};

function WikiLinkAdapter(){
	this.label = "Transform into WikiLink";
	this.name = "WikiLinkAdapter";
};

WikiLinkAdapter.prototype = new AbstractAdapter();

WikiLinkAdapter.prototype.isApplicableToConcept = function(){
	return true;
};

WikiLinkAdapter.prototype.adaptTextPlainInstance = function(instance){	
	this.transform_to_anchor(this.doc, instance.getValue());
};

WikiLinkAdapter.prototype.transform_to_anchor = function(doc, data){  
  anchorStartTag = "<a href='http://en.wikipedia.org/wiki/" + data + "'>";
  anchorEndTag = "</a>";
  return transformTerms(doc, data, anchorStartTag, anchorEndTag);
};

/*
 * This is the function that actually highlights a text string by
 * adding HTML tags before and after all occurrences of the search
 * term. You can pass your own tags if you'd like, or if the
 * highlightStartTag or highlightEndTag parameters are omitted or
 * are empty strings then the default <font> tags will be used.
 */
function do_transformation(bodyText, searchTerm, anchorStartTag, anchorEndTag) 
{
  
  // find all occurences of the search term in the given text,
  // and add some "highlight" tags to them (we're not using a
  // regular expression search, because we want to filter out
  // matches that occur within HTML tags and script blocks, so
  // we have to do a little extra validation)
  var newText = "";
  var i = -1;
  var lcSearchTerm = searchTerm.toLowerCase();
  var lcBodyText = bodyText.toLowerCase();
    
  while (bodyText.length > 0) {
    i = lcBodyText.indexOf(lcSearchTerm, i+1);
    if (i < 0) {
      newText += bodyText;
      bodyText = "";
    } else {
      // skip anything inside an HTML tag
      if (bodyText.lastIndexOf(">", i) >= bodyText.lastIndexOf("<", i)) {
        // skip anything inside a <script> block
        if (lcBodyText.lastIndexOf("/script>", i) >= lcBodyText.lastIndexOf("<script", i)) {
          newText += bodyText.substring(0, i) + anchorStartTag + bodyText.substr(i, searchTerm.length) + anchorEndTag;
          bodyText = bodyText.substr(i + searchTerm.length);
          lcBodyText = bodyText.toLowerCase();
          i = -1;
        }
      }
    }
  }
  
  return newText;
}


/*
 * This is sort of a wrapper function to the doHighlight function.
 * It takes the searchText that you pass, optionally splits it into
 * separate words, and transforms the text on the current web page.
 * Only the "searchText" parameter is required; all other parameters
 * are optional and can be omitted.
 */
function transformTerms(doc, searchText, anchorStartTag, anchorEndTag){
  searchArray = [searchText];
  
  var bodyText = doc.body.innerHTML;
  for (var i = 0; i < searchArray.length; i++) {
    bodyText = do_transformation(bodyText, searchArray[i], anchorStartTag, anchorEndTag);
  }

  doc.body.innerHTML = bodyText;
  return true;
};



