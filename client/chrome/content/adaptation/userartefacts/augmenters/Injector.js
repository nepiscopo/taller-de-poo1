var attributes = [{"name":"Concept Name","type":"String","value":"","description":"Is the concept name of the DOM instance","example":"Search Result"},{"name":"Target","type":"xPath","value":"","description":"Is the target element in which the element will be injected","example":"/html/body"}];
var metadata = {"name":"DOMInstanceInjecter","id":"DOMInstanceInjecter","scriptAttributes":attributes, "author":"Sergio Firmenich", "description": "This augmenter injects a DOM Instance collected into the Pocket in the selected DOM element"};
/*
 * This displays a dialog box that allows a user to enter their own
 * search terms to highlight on the page, and then passes the search
 * text or phrase to the highlightSearchTerms function. All parameters
 * are optional.
 */
function getAugmenterInstance(){
	return new DOMInstanceInjecter();
};

var cloned_node = null;
var style_cloned = null;
function DOMInstanceInjecter(){
	this.name = "DOMInstanceInjecter";
	this.label = "Inject";
};

DOMInstanceInjecter.prototype = new AbstractAdapter(metadata);

DOMInstanceInjecter.prototype.isApplicableToConcept = function(){
	return true;
};

DOMInstanceInjecter.prototype.adaptTextPlainInstance = function(instance){	
	return;
};

DOMInstanceInjecter.prototype.injectDOMInstance = function(event){
	if (cloned_node != null){
		var container = event.target;
		container.appendChild(cloned_node);
        setComputedStyle(cloned_node, style_cloned);
        cloned_node = null;
        style_cloned = null;
	}
	getAugmenterInstance().triggerEventForTaskExecution(metadata["scriptAttributes"]);	
};

function getStyles(aNode){ 
    return el.currentStyle || el.ownerDocument.defaultView.getComputedStyle(el, null);
};

function setComputedStyles(aNode, styles){ 
    var css = "";
    for (p in styles) 
        if (styles.getPropertyValue(p) != "")
            css += p + ":" + styles.getPropertyValue(p) + ";"
    alert(css);
    aNode.setAttribute("style",css);
};

DOMInstanceInjecter.prototype.adaptDOMElementInstance = function(instance){
	var dom_element = instance.getValue();
  	cloned_node = dom_element.cloneNode(true);
  	style_cloned = getStyles(dom_element);
	this.expressInterestIn("mousedown", content.document, this.injectDOMInstance, true);
  	this.triggerEventForTaskExecution(metadata["scriptAttributes"]);
};

DOMInstanceInjecter.prototype.automaticExecution = function(attributes){
  var textValue = attributes[0]["value"];
  var isConcept = (attributes[1]["value"] == "true") || (attributes[1]["value"] == "True");
  var concept = attributes[0]["value"];
  var dom_element_xpath = attributes[1]["value"];
  var doc = CSA.getCurrentDocument();
  var dom_element = CSA.getElementFromXPath(dom_element_xpath,doc);

};


