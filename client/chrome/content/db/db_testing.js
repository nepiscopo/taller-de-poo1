
function DBManager(){
	try{
		this.file = Components.classes["@mozilla.org/file/directory_service;1"]
	                    .getService(Components.interfaces.nsIProperties)
	                    .get("ProfD", Components.interfaces.nsIFile);	                    
		this.file.append("csa_db.sqlite");		
		this.storageService = Components.classes["@mozilla.org/storage/service;1"]
	                     .getService(Components.interfaces.mozIStorageService);	
		this.mDBConn = this.storageService.openDatabase(this.file);
	}
	catch(e){
		return false;
	}
}

DBManager.prototype.create_schema = function(){
	/*
	 * Crea la base de datos para la apliación CSA.
	 * Devuelve verdadero si pudo crear, falso si no.
 	 */	
	try{
		if (!this.mDBConn.tableExists("WebApp"))
			this.mDBConn.executeSimpleSQL("CREATE TABLE WebApp (id integer PRIMARY KEY,func_specification varchar(8000),hash_def varchar(100),last_modified varchar(200))");
	}
	catch (e){
		alert('db_not_created');
		//alert(this.msgs.GetStringFromName('db_not_created'));
		return false;
	}
	return true;
}

DBManager.prototype.drop_db_xolar = function(){
	/*
	 * Elimina la base de datos de la apliación Xolar.
	 * Devuelve verdadero si pudo eliminar, falso si no.
 	 */
	
	try{		
		if (this.mDBConn.tableExists("WebApp")){
			//a = confirm(this.msgs.GetStringFromName('db_delete_apps'));
			a = confirm('Desea eliminar todos los modelos?');
			if (a == true){
				this.mDBConn.executeSimpleSQL("DROP TABLE WebApp");
				alert('db_deleted');
				//alert(this.msgs.GetStringFromName('db_deleted'));
			}
		}
	}
	catch (e){
		//alert(this.msgs.GetStringFromName('db_not_deleted'));
		alert('db_not_deleted');
		return false;
	}
	return true;
}
