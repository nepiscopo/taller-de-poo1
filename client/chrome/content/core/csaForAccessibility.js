var __split = false;
var __postpone = false;
var __distribute = false;

function initCSAccessiblityManager(){
	//window.addEventListener("SplitedSectionSelected",function(e) {  loadSplitedSection(e); }, false, true);
	return CSAccessiblityManager.getInstance();
};


function CSAccessiblityManager(){
	this.executionStrategy = this.executeRefactoring;
	//Just for Gmail, and test of March 2012:
	//this.refactorings_set = this.total_refactoring;	
	this.refactorings_set = this.getRefactoringsForURL;
	this.currentRefactorings = [];
	this.lastRefactoringApplied = null;
	this.language = "es";
	this.languages = {"es":{},"en":{}};
	this.initLanguages();
	this.refactoring_manager;
};

//Singleton
CSAccessiblityManager.__instance__ = null;
CSAccessiblityManager.getInstance = function (){
	if (CSAccessiblityManager.__instance__ == null)
			CSAccessiblityManager.__instance__ = new CSAccessiblityManager();
	return CSAccessiblityManager.__instance__;
};

CSAccessiblityManager.prototype.initLanguages = function(){
	this.languages["es"]["accessibility"] = "Accesibilidad";
	this.languages["es"]["contextual"] = "Usar menú contextual";
	this.languages["es"]["withcolulmn"] = "Usar columna de operaciones";
	this.languages["es"]["install"] = "Instalar";
	this.languages["es"]["uninstall"] = "Desinstalar";
	this.languages["es"]["language"] = "Idiomas";
	this.languages["es"]["spanish"] = "Español";
	this.languages["es"]["english"] = "Inglés";
	this.languages["es"]["gmail"] = "Abrir Gmail version HTML";
	this.languages["es"]["total"] = "Refactorización total";
	this.languages["es"]["specific"] = "Refactorización de elementos específicos";
	this.languages["es"]["structural"] = "Refactorización estructural";
	this.languages["es"]["moreoptions"] = "Más opciones";		
	
	this.languages["en"]["accessibility"] = "Accessibility";
	this.languages["en"]["install"] = "Install";
	this.languages["en"]["uninstall"] = "Uninstall";
	this.languages["en"]["language"] = "Language";
	this.languages["en"]["spanish"] = "Spanish";
	this.languages["en"]["english"] = "English";
	this.languages["en"]["gmail"] = "Open HTML Gmail";
	this.languages["en"]["total"] = "Total refactorization";
	this.languages["en"]["specific"] = "Specific elements refactorization";
	this.languages["en"]["structural"] = "Structural Refactorization";
	this.languages["en"]["contextual"] = "Use contextual menu";
	this.languages["en"]["withcolulmn"] = "Use operation column";	
	this.languages["en"]["moreoptions"] = "More options";
};

CSAccessiblityManager.prototype.setRefactoringManager = function(aRefactoringManager){
	this.refactoring_manager = aRefactoringManager;
};

CSAccessiblityManager.prototype.changeLabelNextItemMenu = function(){
	document.getElementById("item-change-accesibility-next").disabled = !document.getElementById("item-change-accesibility-manual").disabled;
	if(document.getElementById("item-change-accesibility-next").disabled) return;
	var cant = (this.lastRefactoringApplied != null)?this.lastRefactoringApplied + 1:0;
	var label = "Aplicar siguiente refactoring (" + cant + "/" + this.currentRefactorings.length +")";
	document.getElementById("item-change-accesibility-next").setAttribute("label",label);
	if(cant == this.currentRefactorings.length)
		document.getElementById("item-change-accesibility-next").disabled = true;
	else
		document.getElementById("item-change-accesibility-next").disabled = false;
};

CSAccessiblityManager.prototype.manageRefactorings = function(){
	
};

CSAccessiblityManager.prototype.executeNextRefactoring = function(){
	var last = this.lastRefactoringApplied;
	if((last == null) &&  (this.currentRefactorings.length > 0)){
		this.lastRefactoringApplied = 0;
	}
	else{
		this.lastRefactoringApplied++;
	}
	this.executeRefactoring();
	this.changeLabelNextItemMenu();
};

CSAccessiblityManager.prototype.changeAvailableSubMenuItems = function(id){
	document.getElementById("total-refactoring").disabled = false;
	document.getElementById("structural-refactoring").disabled = false;
	document.getElementById("specific-refactoring").disabled = false;
	document.getElementById("item-change-accesibility-manual").disabled = false;
	document.getElementById(id).disabled = true;	
};

CSAccessiblityManager.prototype.changeAvailableMenuItems = function(){
	//document.getElementById("item-change-accesibility-automatic").disabled = !document.getElementById("item-change-accesibility-automatic").disabled;	
	document.getElementById("item-change-accesibility-manual").disabled = !document.getElementById("item-change-accesibility-manual").disabled;
	if (document.getElementById("item-change-accesibility-manual").disabled){
		document.getElementById("total-refactoring").disabled = false;
		document.getElementById("structural-refactoring").disabled = false;
		document.getElementById("specific-refactoring").disabled = false;
	}
	this.changeLabelNextItemMenu();
};

CSAccessiblityManager.prototype.changeAvailableLanguageMenuItems = function(){
	document.getElementById("spanish-item").disabled = !document.getElementById("spanish-item").disabled;
	document.getElementById("spanish-item").setAttribute("checked",!document.getElementById("spanish-item").getAttribute("checked"));	
	document.getElementById("english-item").disabled = !document.getElementById("english-item").disabled;
	document.getElementById("english-item").setAttribute("checked",!document.getElementById("english-item").getAttribute("checked"));
	
	document.getElementById("csa-main-menu").setAttribute("label",this.languages[this.language]["accessibility"]);
	document.getElementById("item-change-accesibility-automatic").setAttribute("label",this.languages[this.language]["install"]);
	document.getElementById("item-change-accesibility-manual").setAttribute("label",this.languages[this.language]["uninstall"]);
	document.getElementById("language").setAttribute("label",this.languages[this.language]["language"]);
	document.getElementById("spanish-item").setAttribute("label",this.languages[this.language]["spanish"]);
	document.getElementById("english-item").setAttribute("label",this.languages[this.language]["english"]);
	document.getElementById("item-open-gmail").setAttribute("label",this.languages[this.language]["gmail"]);

	document.getElementById("structural-refactoring").setAttribute("label",this.languages[this.language]["structural"]);
	document.getElementById("specific-refactoring").setAttribute("label",this.languages[this.language]["specific"]);
	document.getElementById("total-refactoring").setAttribute("label",this.languages[this.language]["total"]);

	document.getElementById("with-contextual-menu").setAttribute("label",this.languages[this.language]["contextual"]);
	document.getElementById("with-container-column").setAttribute("label",this.languages[this.language]["withcolulmn"]);
	document.getElementById("more-options").setAttribute("label",this.languages[this.language]["moreoptions"]);
};

CSAccessiblityManager.prototype.setLanguage = function(language){	
	this.language = language;
	this.changeAvailableLanguageMenuItems();	
};

CSAccessiblityManager.prototype.doNothing = function(){
	return
};

CSAccessiblityManager.prototype.setManualRefactorization = function(){
	this.executionStrategy = this.executeRefactoring;
	this.changeAvailableMenuItems();
	this.reloadApp();
};

CSAccessiblityManager.prototype.setAutomaticRefactorization = function(refactorings_set){
	if (typeof(refactorings_set) != "undefined"){
		var function_for_subset = eval(refactorings_set);
		this.refactorings_set = function_for_subset;
	}
	this.executionStrategy = this.executeRefactorings;
	//this.changeAvailableMenuItems();
	this.reloadApp();
};

CSAccessiblityManager.prototype.reloadApp = function(){
	try{
		var tmp = content.document.location;
		content.document.location.href = "about:blank";
		content.document.location.href = tmp;
	}
	catch(e){
		content.document.location.href = "about:blank";
		//content.document.location.href = 'https://mail.google.com/mail/h/';
	}
};

CSAccessiblityManager.prototype.getRefactoringsForURL = function(doc,url){
	var refactorings = this.refactoring_manager.getActivatedInstantiatedRefactorings();
	var target_refactorings = [];
	for (var i=0;i < refactorings.length;i++){	
		if(refactorings[i].isPageATarget(url)){
			try{
				refactorings[i].initRefactoringForPageLoaded(doc,this.language);
				try{
					refactorings[i].getApp().setLanguage(this.language);
				}catch(e){;};
			}
			catch (e){
				refactorings[i].initRefactoringForPageLoaded(doc);
			}
			target_refactorings.push(refactorings[i]);
		}
	}
	return target_refactorings
};

CSAccessiblityManager.prototype.setSetOfRefactorings = function(target,url){
	return this.getRefactoringsForURL(target,url);
};

//TMP: Experimento para IEEE IC, Agosto 2012
CSAccessiblityManager.prototype.changeRefactoringMenuForRefactoring = function(element_id, is_element_enabled){
	if (is_element_enabled){
		document.getElementById(element_id + "_active").setAttribute("disabled","true");
		document.getElementById(element_id + "_deactive").setAttribute("disabled","false");
	}
	else{
		document.getElementById(element_id + "_active").setAttribute("disabled","false");
		document.getElementById(element_id + "_deactive").setAttribute("disabled","true");
	}
};

//TMP: Experimento para IEEE IC, Agosto 2012
CSAccessiblityManager.prototype.changeRefactoringMenus = function(){
	this.changeRefactoringMenuForRefactoring("__split",__split);
	this.changeRefactoringMenuForRefactoring("__distribute",__distribute);
	this.changeRefactoringMenuForRefactoring("__postpone",__postpone);
	if (__split || __distribute || __postpone)
		document.getElementById("item-change-accesibility-manual").disabled = false;
	else
		document.getElementById("item-change-accesibility-manual").disabled = true;
};

//TMP: Experimento para IEEE IC, Agosto 2012
CSAccessiblityManager.prototype.refactoringSetChanged = function(refactoringGetFunction){
	this.changeRefactoringMenus();
	this.setAutomaticRefactorization(refactoringGetFunction);
};

CSAccessiblityManager.prototype.getRefactoringSubSet = function(target,url){
	return this.refactorings_set(target,url);
};

CSAccessiblityManager.prototype.pageLoaded = function(event){
	var self = CSAccessiblityManager.getInstance();
	var num = gBrowser.browsers.length;	
	for (var i = 0; i < num; i++) {
		var b = gBrowser.getBrowserAtIndex(i);
		if (event.target.location == b.currentURI.spec){
			if (event.target == content.document){
				//var refactorings = self.getRefactoringsForURL(event.target,event.target.location.href);
				var refactorings = self.getRefactoringSubSet(event.target,event.target.location.href);
				self.setApplicableRefactorings(refactorings);
				self.lastRefactoringApplied = null;
				self.changeLabelNextItemMenu();
				self.executionStrategy();
			}
		}
	}
};

CSAccessiblityManager.prototype.setApplicableRefactorings = function(refactorings){
	this.currentRefactorings = [];
	//var str_refactorings = "";
	for (var i = 0;i < refactorings.length;i++){
		var refactoring = refactorings[i];
		try{
			this.currentRefactorings.push(refactoring);
			//str_refactorings += refactoring.getApp().constructor + "\n";
		}
		catch (e){return;}
	}
};

CSAccessiblityManager.prototype.executeRefactorings = function(){
	var refactorings = this.currentRefactorings;
	for (var i = 0;i < refactorings.length;i++){
		var refactoring = refactorings[i];
		this.adaptDocumentWith(content.document,refactoring);
	}
	this.playSound();
};

CSAccessiblityManager.prototype.executeRefactoring = function(){
	var refactorings = this.currentRefactorings;
	var last = this.lastRefactoringApplied;
	if(last == null)
		return;
	if (last < refactorings.length){
		var refactoring = refactorings[last];
		this.adaptDocumentWith(content.document,refactoring);
		this.playSound();
	}
};

CSAccessiblityManager.prototype.adaptDocumentWith = function(doc,refactoring){
	try{
		var adapted_doc = refactoring.adaptDocument(doc);
	}
	catch(e){return doc};
	return adapted_doc;
};

CSAccessiblityManager.prototype.playSound = function(){
	var player = Components.classes["@mozilla.org/sound;1"].createInstance(Components.interfaces.nsISound);
	player.beep();
	try{	
		player.play("chrome://cs-adaptation/content/accessibility/sound/receive.wav");
	}
	catch(e){
		return;
	}
};
