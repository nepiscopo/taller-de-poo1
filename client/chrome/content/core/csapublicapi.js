function CSAPublicResponder(){
/*
 * Es el constructor de la clase CSAPublicResponder.
 * Implementa las respuestas a los mensajes que pueden ser enviados
 * desde los scripts de GM.
 */	
};

CSAPublicResponder.prototype.basicRequest = function(event) {
	//Capto la peticion:
	var request = event.target;
	//Determino de que aplicacion viene:
	var application = content.document.location;
	//Veo cual es el método que requiere:
	var methodRequested = request.getAttribute("method");
	var parameters = request.getAttribute("parameters");
	var responseParams = "";
	var responseResult = "undefined";    
	if(typeof(eval('(' + "CSAPublicResponder.prototype." + methodRequested + ')')) != "undefined"){	
		try{
			responseParams = eval('(' + "CSAPublicResponder.prototype." + methodRequested + "('" + parameters + "')" + ')');
			responseResult = "executed";			
		}catch(e){
			responseResult = "failed";
		}
	}

	//Obtengo el objeto para responder:
	var response = content.document.getElementById(request.getAttribute("responseItIn"));
	//Seteo la respuesta:
	response.setAttribute("response",responseResult);
	response.setAttribute("params",responseParams);
	
	//Respondo:
	var respond = document.createEvent("Events");
	respond.initEvent("change", true, false);
	response.dispatchEvent(respond);
};

CSAPublicResponder.prototype.getConcepts = function(params){	
	var model = CSA.getModdingInterface(params);
	if(typeof(model) != "undefined"){
		/*
		 * Aca deberia generar el json con el modding interface serializado
		 */
		var modelStringified = JSON.stringify(model.serialize());		
		return modelStringified;
		/*json de ejemplo:
		 * "{'ResultadoDeBusqueda':{
		 *               'DOMInfo':'g',
		 *               'properties':{
		 *                       'target':{
		 *                            'name':'target',
		 *                            'node':'anchor',
		 *                            'value':'href',
		 *                            'type':'string'},
		 *                       'body':{
		 *                            'name':'body',
		 *                            'node':'div',
		 *                            'value':'innerHTML',
		 *                            'type':'string'}
		 *                		  }
		 *           	  }
		 *     }"
		*/
	}
	else
		return "{}";
};

CSAPublicResponder.prototype.getAdaptations = function(params){	
	var adaptations = {};

	var file = Components.classes["@mozilla.org/file/local;1"].
                     createInstance(Components.interfaces.nsILocalFile);
	file.initWithPath("/home/keco/Escritorio/searchResult.xml");	
	var data = "";
	var fstream = Components.classes["@mozilla.org/network/file-input-stream;1"].
	                        createInstance(Components.interfaces.nsIFileInputStream);
	var cstream = Components.classes["@mozilla.org/intl/converter-input-stream;1"].
	                        createInstance(Components.interfaces.nsIConverterInputStream);
	fstream.init(file, -1, 0, 0);
	cstream.init(fstream, "UTF-8", 0, 0); // you can use another encoding here if you wish
	
	let (str = {}) {
	  let read = 0;
	  do { 
	    read = cstream.readString(0xffffffff, str); // read as much as we can and put it in str.value
	    data += str.value;
	  } while (read != 0);
	}
	cstream.close(); // this closes fstream

    data = data.replace(/\s*[\r\n][\r\n \t]*/g, "");
	adaptations["SearchResultHighlight"] = data;
	return JSON.stringify(adaptations);
};

CSAPublicResponder.prototype.reloadUnshownTabs = function(params){
	var num = gBrowser.browsers.length;
	for (var j = 0; j < num; j++) {
		var tab = gBrowser.browsers[j].contentDocument;
		if (tab.location != content.document.location){
			applications_for_reloading[tab.location] = tab.location;
		}
	}
};
