/*
 * CSNGUIManager:
 * manejador de interfaz grafica del plugin para CSN
 * encargado de abrir, refrescar y ocultar menues, toolbars, windows, etc. 
 */
var global_csn_manager;
function CSAGUIManager(csamanager){
/*
 * Constructor de la clase CSNManager.
 */
	this.notify_toolbar = null;
	this.csa_manager = csamanager;
	global_csa_manager = csamanager;
	this.initialize();	
};

CSAGUIManager.prototype.initialize = function(){
	this.notify_toolbar = document.getElementById("csntoolbar");
};

CSAGUIManager.prototype.showNotifyForNewPlugin = function(){
	instance_saved = this;
	gBrowser.getNotificationBox().appendNotification("Information from another application has been detected. Do you want to use it to create a new CSN plugin?",
                                                      "csa-for-new-plugin-notification",
                                                      "chrome://cs-adaptation/skin/img/application_form_add.png",
                                                       4,Array({label: "Accept", callback: this.openPluginConstructor, popup: null, accessKey:'A'},
                                                               {label: "Not Accept", callback: this.hideNotifyForNewPlugin, popup: null, accessKey:'N'},
                                                               {label: "Not Accept and Delete", callback: this.hideNotifyForNewPluginAndDelete, popup: null, accessKey:'D'}));	
};

CSAGUIManager.prototype.showArtefactDetectionMessage = function(msg,f_callback,artefact){
	//BROWSER DEPENDENCY
	var AcceptCallback = function(){
		if(f_callback == null) return;		
		f_callback(artefact);
	};
	
	gBrowser.getNotificationBox().appendNotification(msg,
                                      msg + "-notification",
                                      "chrome://cs-adaptation/skin/img/application_form_add.png",
                                       4,Array({label: "Accept", callback: AcceptCallback, popup: null, accessKey:'A'},
                                               {label: "Not Accept", callback: this.hideNotifyForNewPlugin, popup: null, accessKey:'N'}
                                               ));

};

CSAGUIManager.prototype.showTemporalMessage = function(msg,miliseconds){
	var AcceptCallback = function(){
		return;
	}
	gBrowser.getNotificationBox().appendNotification(msg,
                                      msg + "-notification",
                                      "chrome://cs-adaptation/skin/img/application_form_add.png",
                                       4,Array({label: "Accept", callback: AcceptCallback, popup: null, accessKey:'A'}                                            
                                               ));

    try{
		var notification = gBrowser.getNotificationBox().getNotificationWithValue(msg + "-notification");
	}catch(e){;}
  	setTimeout(function (){gBrowser.getNotificationBox().removeNotification(notification)},miliseconds);
};


CSAGUIManager.prototype.hideNotifyForNewPlugin = function(){
	return;
}

CSAGUIManager.prototype.hideNotifyForNewPluginAndDelete = function(){
	gBrowser.getNotificationBox().removeCurrentNotification();
	global_csn_manager.deleteInformationAboutSourceApp();
}

CSAGUIManager.prototype.toggleModdingInterfaceViewer = function(){	
	toggleSidebar('CSASidebar');
}

CSAGUIManager.prototype.openProcedureManager = function(){	
	var window_path = "chrome://cs-adaptation/content/models/windows/ProcedureManager.xul"; 
	window.openDialog(window_path, "","chrome, dialog, modal, resizable=yes",this.csa_manager).focus();
}

CSAGUIManager.prototype.startScenarioRecorder = function(){	
	toggleSidebar('CSASidebarScenarioRecorder');
};

CSAGUIManager.prototype.startScenarioPlayer = function(){	
	toggleSidebar('CSASidebarScenarioPlayer');
};

CSAGUIManager.prototype.refreshModdingInterfaceViewer = function(){
	return;
	this.toggleModdingInterfaceViewer();
	this.toggleModdingInterfaceViewer();
}

CSAGUIManager.prototype.toggleHighlightingMenu = function(){
	var enableButton = document.getElementById("item-enable-dom-highlighting");
	var disableButton = document.getElementById("item-disable-dom-highlighting");
	enableButton.hidden = !enableButton.hidden;
	disableButton.hidden = !disableButton.hidden;
}
