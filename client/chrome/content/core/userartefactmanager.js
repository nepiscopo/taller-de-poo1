function listProperties(object){
	var stream = "";
	cant = 0;
	for (k in object){
		try{
			stream += k + " = " + object[k] + "\n";
		}
		catch(e){			
			stream += k + " = EXCEPTION: " + e;
		}
		cant += 1;
	}	
	alert(stream);
};

var ArtefactManager__instance__ = null;

function UserArtefactManager(CSAInstance){
	if (ArtefactManager__instance__ != null)
		return ArtefactManager__instance__;
	this.csa = CSAInstance;
	ArtefactManager__instance__ = this;
	this.augmenters = null;
	this.init();
};

UserArtefactManager.prototype.init = function(){
	var artifacts_path = this.getArtifactsPath();
	this.initializeFolders(artifacts_path);
	this.getAugmenters();	
};

UserArtefactManager.prototype.addAugmenter = function(aWrapper){	
	this.augmenters.push(aWrapper);
};

UserArtefactManager.prototype.getArtifactsPath = function(){
	var csaFolder = "csa-artefacts";
	var file = Components.classes["@mozilla.org/file/directory_service;1"].
           getService(Components.interfaces.nsIProperties).
           get("ProfD", Components.interfaces.nsIFile);
	file.append(csaFolder);
	if( !file.exists() || !file.isDirectory() ) {   // if it doesn't exist, create
   		file.create(Components.interfaces.nsIFile.DIRECTORY_TYPE, 0777);
	}
	return file.path;
};

UserArtefactManager.prototype.initializeFolders = function(artefacts_path){
	try{
		var aFile = Components.classes["@mozilla.org/file/local;1"].
        		    createInstance(Components.interfaces.nsILocalFile);
		aFile.initWithPath( artefacts_path );	
		aFile.append("augmenters");
		if( !aFile.exists() || !aFile.isDirectory() ) {   // if it doesn't exist, create
	   		aFile.create(Components.interfaces.nsIFile.DIRECTORY_TYPE, 0777);
		}
		aFile.initWithPath( artefacts_path );
		aFile.append("scenarios");
		if( !aFile.exists() || !aFile.isDirectory() ) {   // if it doesn't exist, create
	   		aFile.create(Components.interfaces.nsIFile.DIRECTORY_TYPE, 0777);
		}
		aFile.initWithPath( artefacts_path );
		aFile.append("components");
		if( !aFile.exists() || !aFile.isDirectory() ) {  // if it doesn't exist, create
	   		aFile.create(Components.interfaces.nsIFile.DIRECTORY_TYPE, 0777);
		}
	}
	catch(e){return;}
};

UserArtefactManager.getInstance = function(){
	if(ArtefactManager__instance__ == null)
		ArtefactManager__instance__ = new UserArtefactSearcher(CSA);
	return ArtefactManager__instance__;
};

UserArtefactManager.prototype.contentLoaded = function(event){
 	var  doc = event.originalTarget;
 	if (doc.contentType.match("javascript") == null){  		
  		return;
  	}
	try{
	  	var self = UserArtefactManager.getInstance();
	  	var file = Components.classes["@mozilla.org/file/directory_service;1"].
		           getService(Components.interfaces.nsIProperties).
		           get("TmpD", Components.interfaces.nsIFile);
		file.append("jsFileLoaded.js");
		file.createUnique(Components.interfaces.nsIFile.NORMAL_FILE_TYPE, 0777);
	}
	catch(e){;}
	
	try{
		var obj_URI = Components.classes["@mozilla.org/network/io-service;1"].getService(Components.interfaces.nsIIOService).newURI(doc.location.href, null, null);
		var persist = Components.classes["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"]
		              .createInstance(Components.interfaces.nsIWebBrowserPersist);
		
		const nsIWBP = Components.interfaces.nsIWebBrowserPersist;
		const flags = nsIWBP.PERSIST_FLAGS_REPLACE_EXISTING_FILES;
		persist.persistFlags = flags | nsIWBP.PERSIST_FLAGS_FROM_CACHE;
		//persist.saveURI(obj_URI, null, null, null, "", file);
		persist.saveURI(obj_URI, null, null, null, "", file, null);
	}
	catch(e){;}
	try{
		//self.csa.showSimpleMessage("Analizing JavaScript code...");
		setTimeout(function(){UserArtefactManager.getInstance().suggestArtefactInstallation(file)}, 1000 );		
	}
	catch(e){;}
};

UserArtefactManager.prototype.suggestArtefactInstallation = function(file){
	if (this.checkForUserArtefact(file)){
		this.csa.suggestUserArtefactInstallation(file,this.installArtifact);
	}
};

UserArtefactManager.prototype.checkForUserArtefact = function(docLoaded){	
	var artefactLoaded = false;
	artefactLoaded = artefactLoaded || this.checkForAugmenter(docLoaded);
	//artefactLoaded = artefactLoaded || this.checkForScenario(docLoaded);	
	//artefactLoaded = artefactLoaded || this.checkForComponent(docLoaded);
	return artefactLoaded;
};

UserArtefactManager.prototype.checkForAugmenter = function(doc){
	var docPath = "file://" + doc.path;
	var augmenter_script = {};
	var service = Components.classes["@mozilla.org/moz/jssubscript-loader;1"].getService(Components.interfaces.mozIJSSubScriptLoader);
	try{		
		service.loadSubScript(docPath,augmenter_script);
		if(typeof (augmenter_script.getAugmenterInstance) != "undefined")
			return true;
	}
	catch(e){;}	
	return false;
};


UserArtefactManager.prototype.checkForScenario = function(event){
	return false;
};

UserArtefactManager.prototype.checkForComponent = function(event){
	return false;
};

UserArtefactManager.prototype.installArtifact = function(file){
	self = ArtefactManager__instance__;
	try{
		self.installAugmenter(file);
	}catch(e){;}
	return;
};


UserArtefactManager.prototype.installAugmenter = function(file){
	var path = this.getAugmentersFolder(this.getArtefactsPath());
	try{
		var augmenterFolder = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
		augmenterFolder.initWithPath(path);
		file.copyTo(augmenterFolder,"");
		var wrapped_augmenter = new AugmenterWrapper(path + "/" + file.leafName,this);
		this.addAugmenter(wrapped_augmenter);		
	}catch(e){;}
};

UserArtefactManager.prototype.installScenario = function(file){
	return;
};
	
UserArtefactManager.prototype.installComponent = function(file){
	return;
};

UserArtefactManager.prototype.openScriptManagementWindow = function(){
	var params = {"artifactmanager":this};	
	window.openDialog("chrome://cs-adaptation/content/conf/config.xul", "Augmenters Management", "chrome,centerscreen",params)
};

UserArtefactManager.prototype.getArtefactsPath = function(){
	var csaFolder = "csa-artefacts";
	var file = Components.classes["@mozilla.org/file/directory_service;1"].
           getService(Components.interfaces.nsIProperties).
           get("ProfD", Components.interfaces.nsIFile);
	file.append(csaFolder);
	if( !file.exists() || !file.isDirectory() ) {   // if it doesn't exist, create
   		file.create(Components.interfaces.nsIFile.DIRECTORY_TYPE, 0777);
	}
	return file.path;
};

UserArtefactManager.prototype.getAugmentersFolder = function(artefacts_path){
	var augmenters_fs_path = "";
	var osString = Components.classes["@mozilla.org/xre/app-info;1"]
               .getService(Components.interfaces.nsIXULRuntime).OS;
	if(osString == "WINNT"){
		augmenters_fs_path = artefacts_path + "\\augmenters";
	}
	else
		augmenters_fs_path = artefacts_path + "/augmenters";
	return augmenters_fs_path;
};

UserArtefactManager.prototype.getScenarioFolder = function(artefacts_path){
	var scenarios_fs_path = "";
	var osString = Components.classes["@mozilla.org/xre/app-info;1"]
               .getService(Components.interfaces.nsIXULRuntime).OS;
	if(osString == "WINNT"){
		scenarios_fs_path = artefacts_path + "\\scenarios";
	}
	else
		scenarios_fs_path = artefacts_path + "/scenarios";
	return scenarios_fs_path;
};

UserArtefactManager.prototype.loadScenarios = function(artefacts_path){
	var scenarios_fs_path = this.getScenarioFolder(artefacts_path);
	var scenarios_scripts = this.getUserArtefactsScripts(scenarios_fs_path);
	for (var i=0;i < scenarios_scripts.length;i++){
		try{scenarios_scripts[i].getArtefact().init();}
		catch(e){continue;}
	}	
};

UserArtefactManager.prototype.loadComponents = function(artefacts_path){
	return;
};

UserArtefactManager.prototype.getAugmentersWrappers = function(aFileSystemPath){
	//BROWSER_DEPENDENCY: Levantar nombres de scripts del filesystem.
	var root = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
	var artifactsEnum;
	try{
		root.initWithPath(aFileSystemPath);
		artifactsEnum = root.directoryEntries;		
	}catch(e){;}

	var artifacts_filename = [];
	var artifacts = [];
	while (artifactsEnum.hasMoreElements()) {
		try{
			var artifacts_path = artifactsEnum.getNext().QueryInterface(Components.interfaces.nsILocalFile).path;
			var splited_art_path = artifacts_path.split("/");
			var artifact_wrapper;
			artifacts_filename.push(splited_art_path[splited_art_path.length-1]);
			artifact_wrapper = new AugmenterWrapper(artifacts_path,this);
			artifacts.push(artifact_wrapper);
		}
		catch(e){continue;}
	}
	/*
	artifact_wrapper = new AugmenterWrapper("chrome://cs-adaptation/content/adaptation/userartefacts/augmenters/highlightingAdapter.js",this);
	artifacts.push(artifact_wrapper);
	artifact_wrapper = new AugmenterWrapper("chrome://cs-adaptation/content/adaptation/userartefacts/augmenters/simpleAdapter.js",this);
	artifacts.push(artifact_wrapper);
	*/
	return artifacts;
};

UserArtefactManager.prototype.initializeAugmenters = function(){
	for (var i = this.augmenters.length - 1; i >= 0; i--) {
		this.augmenters[i].initialize();
	};
	var quickAugmentersMenu = document.getElementById("quick-augmenter-menu");
	quickAugmentersMenu.disabled = false;
	quickAugmentersMenu.setAttribute("disabled",false);
};

UserArtefactManager.prototype.getAugmenters = function(){
	if (this.augmenters != null){
		try{
			return this.augmenters;
		}catch(e){;}
	}
	var artefacts_path = this.getArtefactsPath();
	var augmenters_fs_path = this.getAugmentersFolder(artefacts_path);
	this.augmenters = this.getAugmentersWrappers(augmenters_fs_path);
	return this.augmenters;
};

UserArtefactManager.prototype.getActivatedAugmenters = function(){
	var all_augmenters = this.getAugmenters();
	var activated_augmenters = [];
	for(var i=0;i < all_augmenters.length;i++)
		if (all_augmenters[i].isActivated())
			activated_augmenters.push(all_augmenters[i])
	return activated_augmenters;
};

UserArtefactManager.prototype.getActivatedQuickAugmenters = function(){
	var all_augmenters = this.getActivedAugmenters();
	var activated_quickAugmenters = [];
	for(var i=0;i < all_augmenters.length;i++)
		if (all_augmenters[i].isQuickAugmenter())
			activated_quickAugmenters.push(all_augmenters[i])
	return activated_quickAugmenters;
};

UserArtefactManager.prototype.unloadAugmenter = function(aWrapper){
	var index = this.augmenters.indexOf(aWrapper);
	if (index > -1) {
	    this.augmenters.splice(index, 1);
	}
};
/**************************************
 ArtifactWrappers
 Represetan a un archivo en el filesystem en pos de obtener metadata del artefacto y el script propiamente dicho
*/ 

function AugmenterWrapper(file_path,manager){
	this.manager = manager;
	this.augmenter_file_path = file_path;
	this.activated = true;
	this.augmenter = null;
	this.metadata = null;
	this.setAugmenterData();
};

AugmenterWrapper.prototype.initialize = function(){
	this.augmenter.initialize();
};

AugmenterWrapper.prototype.activate = function(){
	this.activated = true;
};

AugmenterWrapper.prototype.deactivate = function(){
	this.activated = false;
};

AugmenterWrapper.prototype.toggleActivation = function(){
	if(this.activated)
		this.deactivate();
	else
		this.activate();
};

AugmenterWrapper.prototype.isActivated = function(){
	return this.activated;
};

AugmenterWrapper.prototype.setAugmenterData = function(){
	var script = this.loadScript();
	this.metadata = script.metadata;
	this.augmenter = this.extractAugmenterInstance(script);
};

AugmenterWrapper.prototype.getFilePath = function(){
	return this.augmenter_file_path;
};

AugmenterWrapper.prototype.delete = function(){
	this.manager.unloadAugmenter(this);
	Components.utils.unload(this.absolutePath());
	var file = Components.classes["@mozilla.org/file/local;1"].  
       			createInstance(Components.interfaces.nsILocalFile); 
	file.initWithPath(this.augmenter_file_path);
	file.remove(false);
};

AugmenterWrapper.prototype.loadScriptInContext = function(aContext){
	var service = Components.classes["@mozilla.org/moz/jssubscript-loader;1"].getService(Components.interfaces.mozIJSSubScriptLoader);
	service.loadSubScript(this.absolutePath(),aContext);
	return aContext;
};

AugmenterWrapper.prototype.absolutePath = function(){
 	return "file://" + this.augmenter_file_path;
};

AugmenterWrapper.prototype.loadScript = function(){
	var aContext = {};
	return this.loadScriptInContext(aContext);
};

AugmenterWrapper.prototype.getMetadata = function(){	
	var metadata;
	//script = this.loadScript();
	//metadata = script["metadata"];
	metadata = this.metadata;
	if(typeof(metadata) == "undefined"){
		metadata = {"name":"Augmenter","author":"No author","description":"No description"};	
	}	
	return metadata;
};

AugmenterWrapper.prototype.author = function(){
	return this.getMetadata()["author"];
};

AugmenterWrapper.prototype.name = function(){
	return this.getMetadata()["name"];
};

AugmenterWrapper.prototype.description = function(){
	return this.getMetadata()["description"];
};

AugmenterWrapper.prototype.getId = function(){
	return this.getMetadata()["id"];
};

AugmenterWrapper.prototype.getAttributesList = function(){
	return this.getMetadata()["scriptAttributes"];
};

AugmenterWrapper.prototype.getEventName = function(){
	return this.getMetadata()["id"];
};

AugmenterWrapper.prototype.getAugmenterInstance = function(){
	return this.augmenter;
};

AugmenterWrapper.prototype.extractAugmenterInstance = function(script){
	return script.getAugmenterInstance();
};

AugmenterWrapper.prototype.getName = function(){
	return this.augmenter.getName();
};

AugmenterWrapper.prototype.getLabel = function(){
	return this.augmenter.getLabel();
};

AugmenterWrapper.prototype.isQuickAugmenter = function(){
	return this.augmenter.isQuickAugmenter();
};

AugmenterWrapper.prototype.addQuickMenus = function(){
	return this.augmenter.addQuickMenus();
};

AugmenterWrapper.prototype.makeQuickOptionsVisible = function(){
	return this.augmenter.makeQuickOptionsVisible();
};

AugmenterWrapper.prototype.startAutomaticExecution = function(dispatcher, doc, target){
	return this.augmenter.startAutomaticExecution(dispatcher, doc, target);
};

AugmenterWrapper.prototype.execute = function(doc, target){
	return this.augmenter.execute(doc, target);
};

AugmenterWrapper.prototype.automaticExecution = function(attributes){	
	return this.augmenter.automaticExecution(attributes);
};

AugmenterWrapper.prototype.undo = function(doc,target){	
	return this.augmenter.undo(doc,target);
};

AugmenterWrapper.prototype.reload = function(){
	this.manager.unloadAugmenter(this);
	Components.utils.unload(this.absolutePath());	
	this.augmenter = {};
	this.setAugmenterData();
	this.manager.addAugmenter(this);
};
