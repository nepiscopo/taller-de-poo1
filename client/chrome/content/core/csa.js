var CSA__instance__ = null;

function CSAManager(aWindow, aConsole, aPocket){
	if (CSA__instance__ != null)
		return CSA__instance__;
	this.applications = {};
	this.pocket = aPocket;
	this.modding_interfaces = {};
	this.source_adaptations = {};
	this.as_target_adaptations = {};
	this.app_source_detected = false;//Deprecated	
	this.gui_manager = new CSAGUIManager(this);	
	this.artifact_manager = new UserArtefactManager(this);	
	this.components = {};
	this.yule = yule;
	this.init();
	this.procedure_in_action = null;
	this.mainWindow = aWindow;
	this.console = aConsole;
	CSA__instance__ = this;
	aWindow.__CSA__instance__ = this;
	aWindow.tasksInExecution = {};
};

CSAManager.prototype.openExportWindow = function(){

};

CSAManager.prototype.getPocket = function(){
	return this.pocket;
};

CSAManager.prototype.getDataCollectors = function(){
	return this.pocket.getDataCollectors();
};

CSAManager.prototype.getProcedureInAction = function(){
	return this.procedure_in_action;
};

CSAManager.prototype.getAugmenterDispatcher = function(){
	return this.pocket.getAugmenterDispatcher();	
};

CSAManager.prototype.setProcedureInAction = function(aProcedure){
	this.procedure_in_action = aProcedure;
};

CSAManager.prototype.getArtifactManager = function(){
	return this.artifact_manager;
};

CSAManager.prototype.getAugmenters = function(){
	return this.artifact_manager.getActivatedAugmenters();
};

CSAManager.prototype.init = function(){
	return;
};

CSAManager.prototype.installUserArtefact = function(file){
	var self = new CSAManager();
	self.artifact_manager.installArtifact(file);
};

CSAManager.prototype.suggestUserArtefactInstallation = function(file,callback){
	var msg = "A new adaptation component has been detected, do you want to install it?";
	this.gui_manager.showArtefactDetectionMessage(msg,callback,file);
};

CSAManager.prototype.showSimpleMessage = function(msg){
	this.gui_manager.showTemporalMessage(msg,3000);
};

CSAManager.prototype.getApplications = function(){
	return this.applications;
};

CSAManager.prototype.getElementFromXPath = function(dom_element_xpath,aDocument){
	var dom_elements = aDocument.evaluate(dom_element_xpath, aDocument, null, XPathResult.ANY_TYPE, null);
	var element = dom_elements.iterateNext();
	return element;
};

CSAManager.prototype.getXPathElement = function(dom_element,aDocument){
	try{
		var xPath = getXPathForElement(dom_element);
	}
	catch(e){
		return "error";
	}
	return xPath;
};

CSAManager.prototype.getCurrentDocument = function(){
	return content.document;
};


/*
 * Utilizando GuiManager
 */
CSAManager.prototype.showNotifyForNewPlugin = function(){
	this.gui_manager.showNotifyForNewPlugin();
};

CSAManager.prototype.toggleModdingInterfaceViewer = function(){	
	this.gui_manager.toggleModdingInterfaceViewer();
}

//Deprecated
CSAManager.prototype.startScenarioRecorder = function(aProcedure){
	this.setProcedureInAction(aProcedure);
	this.gui_manager.startScenarioRecorder();
};

CSAManager.prototype.startScenarioPlayer = function(aProcedure){
	this.setProcedureInAction(aProcedure);
	this.gui_manager.startScenarioPlayer();	
}

CSAManager.prototype.onApplicationShowedChange = function(event){	
	for (var app in applications_for_reloading){
		if (app == content.document.location){			
			content.document.location.reload(true);
			applications_for_reloading = {};
		}
	}
	
	return;
	
	var self = CSA;
	self.refreshModdingInterfaceViewer();
	if (self.existAppSourceDetected()){
		self.gui_manager.showNotifyForNewPlugin();
	}
};

CSAManager.prototype.refreshModdingInterfaceViewer = function(){
	this.gui_manager.refreshModdingInterfaceViewer();
};

CSAManager.prototype.toggleHighlightingMenu = function(){
	this.gui_manager.toggleHighlightingMenu();
};

CSAManager.prototype.getGUIManager = function(){
	return this.gui_manager;	
};

CSAManager.prototype.openProcedureManager = function(){
	this.gui_manager.openProcedureManager();
}

/*
 * Creando modding interface
 */
CSAManager.prototype.createModdingInterface = function(){	
	var url = content.document.location.host;	
	if(typeof(this.modding_interfaces[url]) == "undefined"){
		var mi = new ModdingInterface();
		var app = new Application(mi,url);
		this.applications[url] = app;
		mi.setApplication(app);
		this.modding_interfaces[url] = mi;
	}
	else
		;
	return this.modding_interfaces[url];
};

CSAManager.prototype.addConcept = function(xpath){
	var url = content.document.location;
	var modding_interface;	
	if(typeof(this.modding_interfaces[url]) == "undefined")
		modding_interface = this.createModdingInterface();
	else
		modding_interface = this.modding_interfaces[url];
	var concept = modding_interface.createConcept(xpath);
	return concept;
};

CSAManager.prototype.renderSuitCase = function(){
	SuitCaseManager.renderSuitCase();	
};

CSAManager.prototype.getModdingInterfaces = function(){	
	return this.modding_interfaces;
};

CSAManager.prototype.getModdingInterface = function(url){
	if (typeof(url) == "undefined")
		var url = content.document.location.host; 
	return this.modding_interfaces[url];
};

CSAManager.prototype.getCurrentApplication = function(){ 
	var mi = this.getModdingInterface(content.document.location.host);
	if (typeof(mi) != "undefined")
		return mi.getApplication();
	return false
};

CSAManager.prototype.getCantModdingInterfaces = function(){
	var length = 0;
	for (var object in this.modding_interfaces)
		length++;
	return length;
};

CSAManager.prototype.historyAsArray = function(){
	return this.see_history();
};

CSAManager.prototype.appsInUse = function(){
	var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"]
	                 .getService(Components.interfaces.nsIWindowMediator);
	var browserEnumerator = wm.getEnumerator("navigator:browser");

	// Check each browser instance for our URL
	var urls = []
	while (browserEnumerator.hasMoreElements()) {
		var browserWin = browserEnumerator.getNext();
		var tabbrowser = browserWin.gBrowser;
		var numTabs = tabbrowser.browsers.length;
		for (var index = 0; index < numTabs; index++) {
	  		var currentBrowser = tabbrowser.getBrowserAtIndex(index);
	  		urls.push(currentBrowser.currentURI.spec);
	  		//tabbrowser.selectedTab = tabbrowser.tabContainer.childNodes[index];
	    	//browserWin.focus();	  		
		}
	}
	return urls;
};

CSAManager.prototype.see_history = function(){
  	var sessionHistory = getBrowser().mCurrentBrowser.webNavigation.sessionHistory;
	var urls = new Array();
	var string = "";
	for (i = 0; i < sessionHistory.count; i++){
		urls.push(sessionHistory.getEntryAtIndex(i, false).URI.spec);
		string += i + ": " + urls[i] + "\n";
	}
	return urls;	
};


/****
 * Deprecated: based on old version for GreaseMonkey CSN 
 * Creando nuevo plugin
 */
CSAManager.prototype.setAppSourceForNewPlugin = function(url,element,create_anchor){
	/*
	 * Un ejemplo de element:
 	 * (type tiene que eliminarse, no es necesario)
	 * {"type":'concept', 
	 *  "element":una instancia de Concept, 
	 *  "onwer":'http://delicious.com/'};
	 */
	var modding_element = element['element'];
	var dialog_uri = modding_element.getWindowForAnchorAdition(create_anchor);
	var returnedValue = window.openDialog(dialog_uri,"window-add-anchor","",CSN,content.document,modding_element);
	this.app_source_detected = url;
};

CSAManager.prototype.existAppSourceDetected = function(){	
	return (this.app_source_detected != false);
};

CSAManager.prototype.createNewPluginAccepted = function(){
	this.gui_manager.hideNotifyForNewPlugin();
};

CSAManager.prototype.createNewPluginNotAccepted = function(){
	this.gui_manager.hideNotifyForNewPlugin();
};

CSAManager.prototype.deleteInformationAboutSourceApp = function(){
	this.app_source_detected = false;
};

CSAManager.prototype.alert = function(aString){
	alert(aString);
};
