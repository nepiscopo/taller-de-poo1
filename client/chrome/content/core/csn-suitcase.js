//CSNH ICWE2011
function $(eid){
	return content.document.getElementById(eid);
} 

var drag_drop_script_block = 'function evitaEventos(event){	event.preventDefault();}function comienzoMovimiento(event, id){	elMovimiento=content.document.getElementById(id);	cursorComienzoX=event.clientX+window.scrollX;	cursorComienzoY=event.clientY+window.scrollY;	content.document.addEventListener("mousemove", enMovimiento, true);	content.document.addEventListener("mouseup", finMovimiento, true); elComienzoX=parseInt(elMovimiento.style.left);	elComienzoY=parseInt(elMovimiento.style.top);	elMovimiento.style.zIndex=++posicion;	evitaEventos(event);}function enMovimiento(event){	var xActual, yActual;	xActual=event.clientX+window.scrollX;	yActual=event.clientY+window.scrollY;	elMovimiento.style.left=(elComienzoX+xActual-cursorComienzoX)+"px";	elMovimiento.style.top=(elComienzoY+yActual-cursorComienzoY)+"px";	evitaEventos(event);}function finMovimiento(event){if(event.target.getAttribute("parentId") == null && event.target.getAttribute("id") != "csn-close-button" )return;content.document.removeEventListener("mousemove", enMovimiento, true);	content.document.removeEventListener("mouseup", finMovimiento, true); var element_id = event.target.getAttribute("parentId"); element = content.document.getElementById(element_id);offset = getOffset(element);  if (offset.left < 0) element.style.left = "10px";} function getOffset( el ) {    var _x = 0;    var _y = 0;    while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {        _x += el.offsetLeft - el.scrollLeft;        _y += el.offsetTop - el.scrollTop;        el = el.parentNode;    }    return { top: _y, left: _x }; } ;';
//var drag_drop_script_block = 'function evitaEventos(event){	event.preventDefault();}function comienzoMovimiento(event, id){	elMovimiento=content.document.getElementById(id);	cursorComienzoX=event.clientX+window.scrollX;	cursorComienzoY=event.clientY+window.scrollY;	content.document.addEventListener("mousemove", enMovimiento, true);	content.document.addEventListener("mouseup", finMovimiento, true); elComienzoX=parseInt(elMovimiento.style.left);	elComienzoY=parseInt(elMovimiento.style.top);	elMovimiento.style.zIndex=++posicion;	evitaEventos(event);}function enMovimiento(event){	var xActual, yActual;	xActual=event.clientX+window.scrollX;	yActual=event.clientY+window.scrollY;	elMovimiento.style.left=(elComienzoX+xActual-cursorComienzoX)+"px";	elMovimiento.style.top=(elComienzoY+yActual-cursorComienzoY)+"px";	evitaEventos(event);}function finMovimiento(event){content.document.removeEventListener("mousemove", enMovimiento, true);	content.document.removeEventListener("mouseup", finMovimiento, true);} function getOffset( el ) {    var _x = 0;    var _y = 0;    while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {        _x += el.offsetLeft - el.scrollLeft;        _y += el.offsetTop - el.scrollTop;        el = el.parentNode;    }    return { top: _y, left: _x }; } ;';
var fadein_script_block = 'fadein = function(eid){ document.getElementById(eid).style.opacity = (document.getElementById(eid).style.opacity == "")? 0:parseFloat(document.getElementById(eid).style.opacity) + 0.05; (document.getElementById(eid).style.opacity <= "1")?setTimeout("fadein(\'" + eid + "\')", 30):document.getElementById(eid).style.opacity;};';
var fade_script_block = 'fadeout = function(eid){ document.getElementById(eid).style.opacity = (document.getElementById(eid).style.opacity == "")? 1:parseFloat(document.getElementById(eid).style.opacity) - 0.05; (document.getElementById(eid).style.opacity >= "0")?setTimeout("fadeout(\'" + eid + "\')", 70):document.getElementById(eid).style.opacity;};fadein = function(eid){ document.getElementById(eid).style.opacity = (document.getElementById(eid).style.opacity == "")? 0:parseFloat(document.getElementById(eid).style.opacity) + 0.05; (document.getElementById(eid).style.opacity <= "1")?setTimeout("fadein(\'" + eid + "\')", 30):document.getElementById(eid).style.opacity;};';

//var documentBody = content.document.getElementsByTagName('body')[0]; var documentWidth = window.getComputedStyle(documentBody, null).width; var documentHeight = window.getComputedStyle(documentBody, null).height; if (offset.left < 0) 	element.style.left = 10; if (offset.top < 0) 	element.style.top = 10; if (documentWidth < offset.left + element.style.width) 	element.style.left = documentWidth - element.style.width - 10;if (documentHeight < offset.top + element.style.height) 	element.style.top = documentHeight - element.style.height - 10;	


/*
 var documentBody = content.document.getElementsByTagName("body")[0]; var documentWidth = window.getComputedStyle(documentBody, null).width; var documentHeight = window.getComputedStyle(documentBody, null).height;
 var documentBody = content.document.getElementsByTagName('body')[0];
 var documentWidth = window.getComputedStyle(documentBody, null).width;
 var documentHeight = window.getComputedStyle(documentBody, null).height;
 if (offset.left < 0)
 	element.style.left = 10;
 if (offset.top < 0)
 	element.style.top = 10;
 if (documentWidth < offset.left + element.style.width)
 	element.style.left = documentWidth - element.style.width - 10;
 if (documentHeight < offset.top + element.style.height)
 	element.style.top = documentHeight - element.style.height - 10; 	
 */


/***********************
 * SuitaCaseManager (Pocket)
 **********************/

//CSNH ICWE2011
function CSNSuitcaseManager(){
	this.navigation_suitcase = [];	
	this.target_data_for_volatile_adaptation = null;
	this.target_instance_for_adaptation = null;
	this.dispatcher = new AugmenterDispatcher();
	this.addConcept("untyped");
	this.html_id = "lifia-csnh-suitcase-csa-id";
	this.dataCollectors = [];
};

CSNSuitcaseManager.prototype.getAugmenterDispatcher = function(){
	return this.dispatcher;
};

CSNSuitcaseManager.prototype.addDataCollector = function(aDataCollector){
	this.dataCollectors.push(aDataCollector);
};

CSNSuitcaseManager.prototype.getDataCollectors = function(){
	return this.dataCollectors;
};

CSNSuitcaseManager.prototype.getDOMInstance = function(doc){
	var currentDocument = (doc != null)?doc:content.document;	
	suitcase = currentDocument.getElementById(this.getHtmlId());
	return suitcase;
};

CSNSuitcaseManager.prototype.getHtmlId = function(doc){
	return this.html_id;
};

CSNSuitcaseManager.prototype.triggerEventForDataAddition = function(concept,instance){
	try{
		var conceptDefined = document.createEvent("Events");
		conceptDefined.initEvent("ConceptDefined", true, false);
		conceptDefined.concept = concept;
		conceptDefined.instance = instance;
		conceptDefined.manager = this;
		//alert("event to be dispatched: " + conceptDefined.concept.getValue() + " - " + conceptDefined.instance.getValue());
		window.dispatchEvent(conceptDefined);
	}
	catch(e){;}
	try{
		var conceptInstantiated = document.createEvent("Events");
		conceptInstantiated.initEvent(concept.getName() + "Instantiated", true, false);
		conceptInstantiated.concept = concept;
		conceptInstantiated.instance = instance;
		conceptInstantiated.manager = this;
		window.dispatchEvent(conceptInstantiated);
	}
	catch(e){;}
};

CSNSuitcaseManager.prototype.triggerEventForConceptEdited = function(concept){
	try{
		var conceptDefined = document.createEvent("Events");
		conceptDefined.initEvent("ConceptDefined", true, false);
		conceptDefined.concept = concept;
		conceptDefined.instance = concept.getLastInstance();
		conceptDefined.manager = this;
		window.dispatchEvent(conceptDefined);
	}
	catch(e){;}
};

CSNSuitcaseManager.prototype.triggerEventForInstanceEdited = function(instance){
	try{
		var conceptInstantiated = document.createEvent("Events");
		conceptInstantiated.initEvent(instance.getConcept().getName() + "Instantiated", true, false);
		conceptInstantiated.concept = instance.getConcept();
		conceptInstantiated.instance = instance;
		conceptInstantiated.manager = this;
		window.dispatchEvent(conceptInstantiated);
	}
	catch(e){;}
};

CSNSuitcaseManager.prototype.addDataToSuitcase = function(new_data,data_concept,persistent){
	var concept = this.getConceptByName(data_concept);	
	var instance = new TextPlainInstance(new_data,concept);
	this.renderSuitCase();	
	this.triggerEventForDataAddition(concept,instance);
	this.updateSuitCase();
	return instance;
};

CSNSuitcaseManager.prototype.instanceAdded = function(instance,persistent){
	this.triggerEventForDataAddition(instance.getConcept(),instance)
	this.renderSuitCase();
};


CSNSuitcaseManager.prototype.addDOMDataToSuitcase = function(new_data,data_concept,persistent){
	var concept = this.getConceptByName(data_concept);
	var instance = new DOMAbstractionInstance(new_data,concept);	
	this.renderSuitCase();
	this.triggerEventForDataAddition(concept,instance);
	this.updateSuitCase();
};

CSNSuitcaseManager.prototype.isConceptInSuitCase = function(concept_name){
	for (var i=0; i < this.navigation_suitcase.length; i++)
		if (this.navigation_suitcase[i].getName() == concept_name)
			return this.navigation_suitcase[i];
	return false;	
};

CSNSuitcaseManager.prototype.getPocketElementFromText = function(pocketTextValue, isConcept){
	var aConcept = this.isConceptInSuitCase(pocketTextValue);
	if (aConcept && isConcept)
		return aConcept;
	var newInstance = this.addDataToSuitcase(pocketTextValue, "untyped");
	alert(newInstance.getValue());
	return newInstance;
};

CSNSuitcaseManager.prototype.addConcept = function(concept_name){
	var concept = new PocketConcept(concept_name);
	this.navigation_suitcase.push(concept);
	return concept;
};

CSNSuitcaseManager.prototype.getConceptByName = function(concept_name){
	var concept = this.isConceptInSuitCase(concept_name);
	if(!concept)
		concept = this.addConcept(concept_name);
	return concept;
};

CSNSuitcaseManager.prototype.getCantData = function(){
	var cant = 0;
	for (var i=0;i < this.navigation_suitcase.length;i++)
		cant += this.navigation_suitcase[i].getInstances().length;
	return cant;
};

CSNSuitcaseManager.prototype.setTargetElementForAdaptation = function(pocket_node){
	this.target_data_for_volatile_adaptation = this.getDataFromRenderedItem(pocket_node);
};

CSNSuitcaseManager.prototype.getDataFromRenderedItem = function(pocket_node){
	var id = pocket_node.getAttribute("id");
	var is_concept = pocket_node.getAttribute("kind") == "concept";
	var concept_name = id.split("-")[0];
	var instance_index = id.split("-")[1];
	var concept = this.getConceptByName(concept_name);
	if (is_concept)
		element = concept;
	else
		element = concept.getInstances()[instance_index];	
	return element;
};

CSNSuitcaseManager.prototype.pageLoaded = function(event){
	var num = gBrowser.browsers.length;
	for (var i = 0; i < num; i++) {
		var b = gBrowser.getBrowserAtIndex(i);
		if (event.target.location == b.currentURI.spec){
			var pageLoadedEvent = document.createEvent("Events");
			pageLoadedEvent.initEvent("PageLoaded", true, false);
			pageLoadedEvent.url = event.target.location;
			pageLoadedEvent.host = event.target.host;
			pageLoadedEvent.document = event.target;
			pageLoadedEvent.manager = SuitCaseManager;
			window.dispatchEvent(pageLoadedEvent);
		}
	}
	SuitCaseManager.renderSuitCase();
};

CSNSuitcaseManager.prototype.updateSuitCase = function(){
	var self = SuitCaseManager;
	self.renderSuitCase();
};

CSNSuitcaseManager.prototype.removeInstance = function(anInstance){
	var concept = anInstance.getConcept();
	concept.removeInstance(anInstance);
	this.updateSuitCase();
};

CSNSuitcaseManager.prototype.removeDataFromSuitCase = function(){
	if (this.target_data_for_volatile_adaptation == null)
		return;	
	var is_concept = this.target_data_for_volatile_adaptation.isCollection();
	if(is_concept){
		var tmp = [];
		for(var i=0;i < this.navigation_suitcase.length;i++){
			if (this.navigation_suitcase[i]==this.target_data_for_volatile_adaptation)
				continue;
			tmp.push(this.navigation_suitcase[i]);
		}
		this.navigation_suitcase = tmp;
	}
	else{
		var concept = this.target_data_for_volatile_adaptation.getConcept();
		concept.removeInstance(this.target_data_for_volatile_adaptation);
	}
	this.updateSuitCase();
	this.target_data_for_volatile_adaptation = null;
	this.updateSuitCase();
};


CSNSuitcaseManager.prototype.hiddingPopMenu = function(event){	
	var context_menu = document.getElementById("contentAreaContextMenu");
	if (event.target != context_menu)
		return;
	var childNodes = context_menu.childNodes;
	for (var i = 0; i < childNodes.length; i++) {		
  		var child = childNodes[i];
  		if (child.parentNode.getAttribute("id") == "quick-augmenter-popupmenu")
  			continue;
  		if(child.getAttribute("class") == "context-from-pocket")
  			context_menu.removeChild(child);
  		else
  			child.hidden = false;
	}	
};

CSNSuitcaseManager.prototype.showingPopMenu = function(event){
	if(document.popupNode == null) return;
	if(document.popupNode.getAttribute("usetoadaptation") == "true"){	
		SuitCaseManager.setTargetElementForAdaptation(document.popupNode);
		var context_menu = document.getElementById("contentAreaContextMenu");
		var childNodes = context_menu.childNodes;
		for (var i = 0; i < childNodes.length; i++) {
	  		var child = childNodes[i];		    
			child.hidden = true;
		}
		var delete_item = document.createElement("menuitem");
		delete_item.setAttribute("id", "delete-data-from-pocket");
		delete_item.setAttribute("label","Remove from Pocket");
		delete_item.setAttribute("class","context-from-pocket");
		delete_item.setAttribute("onclick", "SuitCaseManager.removeDataFromSuitCase();");
		delete_item.hidden = false;
		context_menu.appendChild(delete_item);
		var separator = document.createElement("menuseparator");
		separator.setAttribute("class","context-from-pocket");
		context_menu.appendChild(separator);
		var available_augmenters = CSA.getAugmenters();
		for (var i = 0;i < available_augmenters.length;i++){
			var augmenter = available_augmenters[i];
			if (augmenter.isQuickAugmenter());
				augmenter.makeQuickOptionsVisible();
			var augmentation_action = document.createElement("menuitem");
			augmentation_action.setAttribute("id", "augmentation-action-"+augmenter.getName());
			augmentation_action.setAttribute("label",augmenter.getLabel());
			augmentation_action.setAttribute("class","context-from-pocket");
			augmentation_action.augmenter = augmenter;
			augmentation_action.setAttribute("onclick", 'event = document.createEvent("Event");event.target_element = document.popupNode;event.initEvent("DispatchAugmenterSelected", true, false);this.dispatchEvent(event);');
			augmentation_action.hidden = false;
			context_menu.appendChild(augmentation_action);
		}
	}
	else{		
		try{document.getElementById("context-item-add-instance-to-suitcase").hidden = false;}catch(e){;}
		try{document.getElementById("context-item-add-data-to-suitcase").hidden = false;}catch(e){;}
		try{document.getElementById("delete-data-from-pocket").hidden = true;}catch(e){;}				
		try{
			for (var i = 0;i < actions.length;i++)			
				document.getElementById("augmentation-action-"+i).hidden = true;
		}catch(e){
			;
		}
	}
};

CSNSuitcaseManager.prototype.setStyleHighlighting = function(event){
	event.target.setAttribute("style","background-color:#FFFF44;text-decoration:underline;");
};

CSNSuitcaseManager.prototype.backStyleHighlighting = function(event){
	event.target.setAttribute("style","background-color:#FFFFCC;text-decoration:none;");
};

CSNSuitcaseManager.prototype.editPocketElement = function(e){
	var element = SuitCaseManager.getDataFromRenderedItem(e.target);
	var new_value = prompt("Entry the new value:", element.getValue());
	element.editFromPocket(SuitCaseManager,new_value);
	SuitCaseManager.updateSuitCase();
}

CSNSuitcaseManager.prototype.changeConceptName = function(concept,newName){
	concept.setName(newName);
	this.triggerEventForConceptEdited(concept);
	this.triggerEventForInstanceEdited(concept.getLastInstance());
};

CSNSuitcaseManager.prototype.changePlainTextInstanceValue = function(instance,newValue){
	instance.setValue(newValue);
	this.triggerEventForConceptEdited(instance.getConcept());	
	this.triggerEventForInstanceEdited(instance);
};

CSNSuitcaseManager.prototype.renderSuitCase = function(){
	//BROWSER DEPENDENCY: content.document --> Must be: Browser.getCurrentDocument()
	var suitcase = content.document.getElementById(this.getHtmlId());
	if ((SuitCaseManager.getCantData() == 0) && (typeof(CSA.getModdingInterface()) == "undefined")){	
		if(suitcase!=null)
			suitcase.innerHTML = "";
		return;
	}	
	suitcase = content.document.getElementById(this.getHtmlId());
	var style = "font-size: 10pt; color: #333333;font-family:'arial';background-color:#FFFFCC;width: auto; height: auto; position: fixed; top: 50px; left: 50px; z-index: 2147483647; font-weight: normal; opacity: 1; display: block;text-align:left";	
	if(suitcase==null){
		var suitcase = content.document.createElement("div");
		suitcase.setAttribute("style",style);
	}
	else{		
		style = suitcase.getAttribute("style");
		suitcase.setAttribute("style",style);		
		suitcase.style.visibility="visible";
		suitcase.innerHTML = "";
	}
	var drag_drop_content = content.document.createElement("script");
	drag_drop_content.setAttribute("type","text/javascript");
	drag_drop_content.innerHTML = drag_drop_script_block;
	suitcase.appendChild(drag_drop_content);
	suitcase.setAttribute("id",this.getHtmlId());
	var suitcase_bar = content.document.createElement("div");
	//var onclick = "document.getElementById('lifia-csnh-suitcase-csa-id').parentNode.removeChild(document.getElementById('lifia-csnh-suitcase-csa-id'))";
	
	var onclick = 'document.getElementById("' + this.getHtmlId() + '").style.visibility="hidden";';	
	suitcase_bar.innerHTML = "<a id='csn-close-button' href='javascript:void(0);' onclick="+onclick+">x</a>";
	suitcase_bar.setAttribute("parentId",this.getHtmlId());
	suitcase_bar.setAttribute("style","text-align:right;background-color:#FFFF99");
	suitcase_bar.setAttribute("onmousedown","comienzoMovimiento(event, '" + this.getHtmlId() + "');");
	suitcase_bar.setAttribute("onmouseover","document.getElementById('" + this.getHtmlId() + "').style.cursor='move'");
	suitcase.appendChild(suitcase_bar);	
	var elements = content.document.createElement("dl");	
	for (var i=0;i < SuitCaseManager.navigation_suitcase.length;i++){
		var concept = SuitCaseManager.navigation_suitcase[i];
		var instances = concept.getInstances();
		if (instances.length == 0) continue;
		var concept_element = content.document.createElement("dt");
		concept_element.innerHTML = concept.getName();
		concept_element.setAttribute("usetoadaptation","true");
		concept_element.setAttribute("id",concept.getName());
		concept_element.setAttribute("kind","concept");
		concept_element.addEventListener("mouseout", SuitCaseManager.backStyleHighlighting,false);
		concept_element.addEventListener("mouseover", SuitCaseManager.setStyleHighlighting,false);
		concept_element.addEventListener("dblclick", SuitCaseManager.editPocketElement,false);
		elements.appendChild(concept_element);
		for (var k=0; k < instances.length;k++){
			var data_element = content.document.createElement("dd");
			data_element.setAttribute("id",concept.getName() + "-" + k);
			data_element.setAttribute("usetoadaptation","true");
			data_element.setAttribute("kind","instance");
			data_element.innerHTML = instances[k].getNameForPocket();
			data_element.addEventListener("mouseout", SuitCaseManager.backStyleHighlighting,false);
			data_element.addEventListener("mouseover", SuitCaseManager.setStyleHighlighting,false);
			data_element.addEventListener("dblclick", SuitCaseManager.editPocketElement,false);
			elements.appendChild(data_element);
		}		
	}	
	suitcase.appendChild(elements);
	content.document.body.appendChild(suitcase);
};

CSNSuitcaseManager.prototype.renderMessage = function(msg){
	adaptation_message = content.document.getElementById("lifia-csnh-message-csa-id");
	var style = "padding:20px 20px 20px 20px; font-size: 10pt; color: #333333;font-family:'arial';background-color:#CCDDEE;width: auto; height: auto; position: fixed; top: 50px; right: 50px;  border: 1px solid #336699; display: block;opacity:0;-moz-border-radius: 10px;";	
	if(adaptation_message==null)
		var adaptation_message = content.document.createElement("div");
	else{
		style = adaptation_message.getAttribute("style"); 
		adaptation_message.innerHTML = "";
	}
	var fade_content = content.document.createElement("script");
	fade_content.setAttribute("type","text/javascript");
	fade_content.innerHTML = fade_script_block;

	adaptation_message.setAttribute("id","lifia-csnh-message-csa-id");
	adaptation_message.setAttribute("style",style);
	adaptation_message.setAttribute("onclick","fadein('lifia-csnh-message-csa-id')");
	adaptation_message.setAttribute("onclick",adaptation_message.getAttribute("onclick") + ";setTimeout('fadeout(" + '"lifia-csnh-message-csa-id"' + ")',5000);");
	adaptation_message.setAttribute("onclick",adaptation_message.getAttribute("onclick") + ";setTimeout('document.getElementById(" + '"lifia-csnh-message-csa-id"' + ").parentNode.removeChild(document.getElementById(" + '"lifia-csnh-message-csa-id"' + "))',7000);");
	adaptation_message.innerHTML = (msg != null)? msg:"Si desea disparar la adaptación haga click <a href='#'>aquí</a>";
	content.document.body.appendChild(adaptation_message);
	adaptation_message.appendChild(fade_content);
	var evt = content.document.createEvent("MouseEvents");
  	evt.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
	adaptation_message.dispatchEvent(evt);
};

CSNSuitcaseManager.prototype.undoAugmenterAdaptation = function(){
	this.dispatcher.undoAugmenterAdaptation(content.document);
};

/*
 * AugmenterDispatcher
 */

function AugmenterDispatcher(){
   	window.addEventListener("DispatchAugmenterSelected", this.dispatchAugmenterListener, false, true);	
   	//this.last_augmenter = null;
   	this.documents_histories = [];
};

AugmenterDispatcher.prototype.getDocumentHistory = function(doc){
	var doc_versions = [];
	for (var i=0;i < this.documents_histories.length;i++){		
		if (this.documents_histories[i]._location_ == doc.location)
			doc_versions.push(this.documents_histories[i]);
	}
	return doc_versions;
};

AugmenterDispatcher.prototype.undoAugmenterAdaptation = function(doc){	
	var versions = this.getDocumentHistory(doc);
	if (versions.length > 0){
		var newest_old_version = versions[versions.length-1];
		doc.body.innerHTML = newest_old_version.body.innerHTML;
		this.documents_histories.pop(newest_old_version);
	}
	else
		return;
	SuitCaseManager.renderSuitCase();
	return;
	//this.last_augmenter["augmenter"].undo(this.last_augmenter["document"],this.last_augmenter["element"]);
	//this.last_augmenter = null;	
};

AugmenterDispatcher.prototype.setLastAugmenter = function(augmenter,element,doc,old_doc){
	this.documents_histories.push(old_doc);
	
	//this.last_augmenter = {};
	//this.last_augmenter["augmenter"] = augmenter;
	//this.last_augmenter["element"] = element;
	//this.last_augmenter["document"] = doc;
	//this.last_augmenter["olddocument"] = old_doc;
	//alert("AugmenterDispatcher.prototype.setLastAugmenter jaaaaa");
};

AugmenterDispatcher.prototype.dispatchAugmenterListener = function(e){
	var augmenter = e.target.augmenter;
	var element = SuitCaseManager.getDataFromRenderedItem(e.target_element);
	var old_doc = content.document.cloneNode(true);
	old_doc._location_ = content.document.location;	
	augmenter.execute(content.document, element);
	SuitCaseManager.renderSuitCase();
	var self = SuitCaseManager.getAugmenterDispatcher();
	self.setLastAugmenter(augmenter,element,content.document,old_doc);
};

AugmenterDispatcher.prototype.automaticExecution = function(augmenter, doc, pocketTextValue, isConcept){
	var pocketElement = SuitCaseManager.getPocketElementFromText(pocketTextValue, isConcept);
	augmenter.execute(doc,pocketElement);
	SuitCaseManager.renderSuitCase();
};

AugmenterDispatcher.prototype.showUndo = function(e){
	var self = SuitCaseManager.getAugmenterDispatcher();
	var option = document.getElementById("context-item-undo-augmenter");
	var doc = content.document;
	var versions = self.getDocumentHistory(doc);	
	option.disabled = versions.length == 0;
};

AugmenterDispatcher.prototype.dispatchAugmenterAutomatically = function(augmenter, scriptAttributes){
	try{
		augmenter.startAutomaticExecution(this, content.document, scriptAttributes);
	}catch(e){
		augmenter.automaticExecution(scriptAttributes);
	}
};

