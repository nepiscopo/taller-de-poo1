var CSA__instance__ = null;

function DOMDocument(doc){
	this.document = doc;
	this.initProperties();
};

DOMDocument.prototype.initProperties = function() {
	var properties_str = "";
	var aProperty;
	for (v in this.document){
		try{
	   		if (typeof(this.document[v].call) == "undefined"){
	   			aProperty = v;
		   		properties_str += v + "\n";
		   		try{
		   			this[v] = this.document[v];
		   		}
		   		catch(e){
		   			alert(e);
		   		}
	   		}
   		}
   		catch(e){
   			continue;
   		}
   	}
};

//DOMDocument.prototype = HTMLDocument;

//@Property
//DOMDocument.prototype.location = http://www.google.com.ar/


DOMDocument.prototype.querySelector = function() {
    return this.document.querySelector.apply(null,arguments);
};

DOMDocument.prototype.querySelectorAll = function() {
    return this.document.querySelectorAll.apply(null,arguments);
};

DOMDocument.prototype.evaluate = function() {
	return this.document.evaluate.apply(null,arguments);
};

DOMDocument.prototype.createExpression = function() {
    return this.document.createExpression.apply(null,arguments);
};

DOMDocument.prototype.createNSResolver = function() {
    return this.document.createNSResolver.apply(null,arguments);
};

DOMDocument.prototype.addEventListener = function() {
    return this.document.addEventListener.apply(null,arguments);
};

DOMDocument.prototype.removeEventListener = function() {
    return this.document.removeEventListener.apply(null,arguments);
};

DOMDocument.prototype.dispatchEvent = function() {
    return this.document.dispatchEvent.apply(null,arguments)
};

//@Property
//DOMDocument.prototype.body = [object HTMLBodyElement]

//@Property
//DOMDocument.prototype.anchors = [object HTMLCollection]

//@Property
//DOMDocument.prototype.links = [object HTMLCollection]

//@Property
//DOMDocument.prototype.URL = http://www.google.com.ar/

//@Property
//DOMDocument.prototype.forms = [object HTMLCollection]

//@Property
//DOMDocument.prototype.cookie = PREF=ID=488e83ae353a8649:U=78a4ad7856342e58:FF=0:TM=1313679382:LM=1329392509:S=83GuiF8k6DhcUSei; PP_TOS_ACK=2

//@Property
//DOMDocument.prototype.images = [object HTMLCollection]

//@Property
//DOMDocument.prototype.domain = www.google.com.ar

//@Property
//DOMDocument.prototype.designMode = off


DOMDocument.prototype.getElementsByName = function() {
    return this.document.getElementsByName.apply(null,arguments);
};

DOMDocument.prototype.write = function() {
    return this.document.write.apply(null,arguments);
};

DOMDocument.prototype.writeln = function() {
    return this.document.writeln.apply(null,arguments);
};

DOMDocument.prototype.getSelection = function() {
   	return this.document.getSelection.apply(null,arguments);
};

//@Property
//DOMDocument.prototype.documentElement = [object HTMLHtmlElement]

//@Property
//DOMDocument.prototype.implementation = [object DOMImplementation]

//@Property
//DOMDocument.prototype.doctype = [object DocumentType]

//@Property
//DOMDocument.prototype.documentURI = http://www.google.com.ar/

//@Property
//DOMDocument.prototype.defaultView = [object Window]

//@Property
//DOMDocument.prototype.title = Google

//@Property
//DOMDocument.prototype.referrer = 

//@Property
//DOMDocument.prototype.activeElement = [object HTMLBodyElement]

//@Property
//DOMDocument.prototype.onreadystatechange = null

//@Property
//DOMDocument.prototype.onmouseenter = null

//@Property
//DOMDocument.prototype.onmouseleave = null

DOMDocument.prototype.getElementsByTagName = function() {
	return this.document.getElementsByTagName.apply(null,arguments);
};

DOMDocument.prototype.getElementsByTagNameNS = function() {
    return this.document.getElementsByTagNameNS.apply(null,arguments);
};

DOMDocument.prototype.getElementById = function() {
    return this.document.getElementById.apply(null,arguments);
};

DOMDocument.prototype.createDocumentFragment = function() {
    return this.document.createDocumentFragment.apply(null,arguments);
};

DOMDocument.prototype.createElement = function() {
    return this.document.createElement.apply(null,arguments);
};

DOMDocument.prototype.createElementNS = function() {
    return this.document.createElementNS.apply(null,arguments);
};

DOMDocument.prototype.importNode = function() {
    return this.document.importNode.apply(null,arguments);
};

DOMDocument.prototype.createTextNode = function() {
    return this.document.createTextNode.apply(null,arguments);
};

DOMDocument.prototype.adoptNode = function() {
    return this.document.adoptNode.apply(null,arguments);
};

DOMDocument.prototype.createNodeIterator = function() {
    return this.document.createNodeIterator.apply(null,arguments);
};

DOMDocument.prototype.createEvent = function() {
    return this.document.createEvent.apply(null,arguments);
};

DOMDocument.prototype.getElementsByClassName = function() {
    return this.document.getElementsByClassName.apply(null,arguments);
};

DOMDocument.prototype.hasFocus = function() {
    return this.document.hasFocus.apply(null,arguments);
};

DOMDocument.prototype.elementFromPoint = function() {
    return this.document.elementFromPoint.apply(null,arguments);
};

//@Property
//DOMDocument.prototype.nodeName = #document

//@Property
//DOMDocument.prototype.nodeValue = null

//@Property
//DOMDocument.prototype.nodeType = 9

//@Property
//DOMDocument.prototype.parentNode = null

//@Property
//DOMDocument.prototype.parentElement = null

//@Property
//DOMDocument.prototype.childNodes = [object NodeList]

//@Property
//DOMDocument.prototype.firstChild = [object DocumentType]

//@Property
//DOMDocument.prototype.lastChild = [object HTMLHtmlElement]

//@Property
//DOMDocument.prototype.previousSibling = null

//@Property
//DOMDocument.prototype.nextSibling = null

//@Property
//DOMDocument.prototype.attributes = null

//@Property
//DOMDocument.prototype.ownerDocument = null

//@Property
//DOMDocument.prototype.namespaceURI = null

//@Property
//DOMDocument.prototype.prefix = null

//@Property
//DOMDocument.prototype.localName = null

//@Property
//DOMDocument.prototype.baseURI = http://www.google.com.ar/

//@Property
//DOMDocument.prototype.textContent = null


DOMDocument.prototype.insertBefore = function () {
    return this.document.insertBefore.apply(null,arguments);
};

DOMDocument.prototype.replaceChild = function () {
    return this.document.replaceChild.apply(null,arguments);
};

DOMDocument.prototype.removeChild = function () {
    return this.document.removeChild.apply(null,arguments);
};

DOMDocument.prototype.appendChild = function () {
    return this.document.appendChild.apply(null,arguments);
};

DOMDocument.prototype.hasChildNodes = function () {
    return this.document.hasChildNodes.apply(null,arguments);
};

DOMDocument.prototype.cloneNode = function () {
    return this.document.cloneNode.apply(null,arguments);
};

DOMDocument.prototype.normalize = function () {
    return this.document.normalize.apply(null,arguments);
};

DOMDocument.prototype.isSupported = function () {
    return this.document.isSupported.apply(null,arguments);
};

DOMDocument.prototype.hasAttributes = function () {
    return this.document.hasAttributes.apply(null,arguments);
};

DOMDocument.prototype.compareDocumentPosition = function () {
    return this.document.compareDocumentPosition.apply(null,arguments);
};

DOMDocument.prototype.lookupPrefix = function () {
    return this.document.lookupPrefix.apply(null,arguments);
};

DOMDocument.prototype.isDefaultNamespace = function () {
    return this.document.isDefaultNamespace.apply(null,arguments);
};

DOMDocument.prototype.lookupNamespaceURI = function () {
    return this.document.lookupNamespaceURI.apply(null,arguments);
};

DOMDocument.prototype.isEqualNode = function () {
    return this.document.isEqualNode.apply(null,arguments);
};

DOMDocument.prototype.setUserData = function () {
    return this.document.setUserData.apply(null,arguments);
};

DOMDocument.prototype.getUserData = function () {
    return this.document.getUserData.apply(null,arguments);
};

DOMDocument.prototype.contains = function () {
    return this.document.contains.apply(null,arguments);
};

DOMDocument.prototype.createComment = function () {
    return this.document.createComment.apply(null,arguments);
};

DOMDocument.prototype.createCDATASection = function () {
    return this.document.createCDATASection.apply(null,arguments);
};

//@Property
//DOMDocument.prototype.createProcessingInstruction = 
 