function listProperties(object){
	var stream = "";
	cant = 0;
	for (k in object){
		try{
			stream += k + " = " + object[k] + "\n";
		}
		catch(e){			
			stream += k + " = EXCEPTION: " + e;
		}
		cant += 1;
	}	
	alert(stream);
};

var RefactoringManager__instance__ = null;

function RefactoringManager(CSAInstance,CSWRInstance){
	if (RefactoringManager__instance__ != null)
		return RefactoringManager__instance__;
	this.csa = CSAInstance;
	this.cswr = CSWRInstance;
	CSWRInstance.setRefactoringManager(this);
	RefactoringManager__instance__ = this;
	this.instantiated_refactorings = null;
	this.generic_refactorings = null;
	this.simpleStorageManager = null;
	this.init();
};

RefactoringManager.prototype.init = function(){
	var artifacts_path = this.getArtifactsPath();
	this.initializeFolders(artifacts_path);
	this.getInstantiatedRefactorings();	
	this.getGenericRefactorings();	
	this.simpleStorageManager = new SimpleStorage();
};

RefactoringManager.prototype.addInstantiatedRefactoring = function(aWrapper){	
	this.instantiated_refactorings.push(aWrapper);
	this.saveOrder();
};

RefactoringManager.prototype.addGenericRefactoring = function(aWrapper){	
	this.generic_refactorings.push(aWrapper);
};

RefactoringManager.prototype.getArtifactsPath = function(){
	var csaFolder = "csa-artefacts";
	var file = Components.classes["@mozilla.org/file/directory_service;1"].
           getService(Components.interfaces.nsIProperties).
           get("ProfD", Components.interfaces.nsIFile);
	file.append(csaFolder);
	if( !file.exists() || !file.isDirectory() ) {   // if it doesn't exist, create
   		file.create(Components.interfaces.nsIFile.DIRECTORY_TYPE, 0777);
	}
	return file.path;
};

RefactoringManager.prototype.saveOrder = function(){
	//this.simpleStorageManager.setValue("__CSWRefactoringManagerOrder__", "siiii, la concha de la loraaaa");
	var value = {}
	for (var i=0;i < this.instantiated_refactorings.length ;i++){
		var instance = this.instantiated_refactorings[i];
		value[instance.getId()] = i;
	}
	var str_value = JSON.stringify(value);
	this.simpleStorageManager.setValue("__CSWRefactoringManagerOrder__",str_value);
};

RefactoringManager.prototype.__restoreOrder__ = function(aValue){
	var self = new RefactoringManager();
	var order = JSON.parse(aValue);
	var tmp_array = [];
	var ordered_refactorings = [];
	var all = self.instantiated_refactorings;
	for(each in order){
		var refactoring = self.getInstantiatedRefactoringById(each);
		if(refactoring){
			tmp_array[order[each]] = refactoring;
		}
	}
	for (var i = 0; i < tmp_array.length; i++) {
		var r = tmp_array[i];
		if(!r || (typeof(r) == "undefined"))
			continue;
		ordered_refactorings.push(r);
	};
	self.instantiated_refactorings = ordered_refactorings;
	if(self.instantiated_refactorings.length != all.length){
		for (var i = 0;i < all.length;i++){
			if(self.instantiated_refactorings.indexOf(all[i]) == -1)
				self.instantiated_refactorings.push(all[i]);
		}
		self.saveOrder();
	}
};

RefactoringManager.prototype.restoreOrder = function(){
	this.simpleStorageManager.getValue("__CSWRefactoringManagerOrder__",this.__restoreOrder__);
};

RefactoringManager.prototype.getInstantiatedRefactoringById = function(anId){
	for (var i=0;i< this.instantiated_refactorings.length;i++){
		var id = this.instantiated_refactorings[i].getId();
		if (id == anId)
			return this.instantiated_refactorings[i];
	}
	return null;
};

RefactoringManager.prototype.initializeFolders = function(artefacts_path){
	try{
		var aFile = Components.classes["@mozilla.org/file/local;1"].
        		    createInstance(Components.interfaces.nsILocalFile);
		aFile.initWithPath( artefacts_path );	
		aFile.append("instantiated_refactorings");
		if( !aFile.exists() || !aFile.isDirectory() ) {   // if it doesn't exist, create
	   		aFile.create(Components.interfaces.nsIFile.DIRECTORY_TYPE, 0777);
		}
		aFile.initWithPath( artefacts_path );
		aFile.append("generic_refactorings");
		if( !aFile.exists() || !aFile.isDirectory() ) {   // if it doesn't exist, create
	   		aFile.create(Components.interfaces.nsIFile.DIRECTORY_TYPE, 0777);
		}
	}
	catch(e){return;}
};

RefactoringManager.getInstance = function(){
	if(RefactoringManager__instance__ == null)
		RefactoringManager__instance__ = new RefactoringManager(CSA);
	return RefactoringManager__instance__;
};

RefactoringManager.prototype.contentLoaded = function(event){
 	var  doc = event.originalTarget;
 	if (doc.contentType.match("javascript") == null){  		
  		return;
  	}
	try{
	  	var self = RefactoringManager.getInstance();
	  	var file = Components.classes["@mozilla.org/file/directory_service;1"].
		           getService(Components.interfaces.nsIProperties).
		           get("TmpD", Components.interfaces.nsIFile);
		file.append("jsFileLoaded.js");
		file.createUnique(Components.interfaces.nsIFile.NORMAL_FILE_TYPE, 0777);
	}
	catch(e){;}
	
	try{
		var obj_URI = Components.classes["@mozilla.org/network/io-service;1"].getService(Components.interfaces.nsIIOService).newURI(doc.location.href, null, null);
		var persist = Components.classes["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"]
		              .createInstance(Components.interfaces.nsIWebBrowserPersist);
		
		const nsIWBP = Components.interfaces.nsIWebBrowserPersist;
		const flags = nsIWBP.PERSIST_FLAGS_REPLACE_EXISTING_FILES;
		persist.persistFlags = flags | nsIWBP.PERSIST_FLAGS_FROM_CACHE;
		//persist.saveURI(obj_URI, null, null, null, "", file);
		persist.saveURI(obj_URI, null, null, null, "", file, null);
	}
	catch(e){;}
	try{
		self.csa.showSimpleMessage("Analizing JavaScript code...");
		setTimeout(function(){RefactoringManager.getInstance().suggestArtefactInstallation(file)}, 5000 );		
	}
	catch(e){;}
};

RefactoringManager.prototype.suggestArtefactInstallation = function(file){
	if (this.checkForUserArtefact(file)){
		this.csa.suggestUserArtefactInstallation(file,this.installArtifact);
	}
};

RefactoringManager.prototype.checkForUserArtefact = function(docLoaded){	
	var artefactLoaded = false;
	artefactLoaded = artefactLoaded || this.checkForInstantiatedRefactoring(docLoaded) || this.checkForGenericRefactoring(docLoaded);
	return artefactLoaded;	
	
};

RefactoringManager.prototype.checkForInstantiatedRefactoring = function(doc){
	var docPath = "file://" + doc.path;
	var refactoring_script = {};
	var service = Components.classes["@mozilla.org/moz/jssubscript-loader;1"].getService(Components.interfaces.mozIJSSubScriptLoader);
	try{		
		service.loadSubScript(docPath,refactoring_script,"US-ASCII");
		if(typeof (refactoring_script.getAccessibilityAugmenter) != "undefined")//Cambiar el nombre por AccessibilityRefactoring
			if(refactoring_script.getAccessibilityAugmenter().isInstantiated())
				return true;
	}
	catch(e){;}	
	return false;
};

RefactoringManager.prototype.checkForGenericRefactoring = function(doc){
	var docPath = "file://" + doc.path;
	var refactoring_script = {};
	var service = Components.classes["@mozilla.org/moz/jssubscript-loader;1"].getService(Components.interfaces.mozIJSSubScriptLoader);
	try{
		service.loadSubScript(docPath,refactoring_script);
		if(typeof (refactoring_script.exportedObjects.GenericRefactoring) != "undefined")//Cambiar el nombre por AccessibilityRefactoring	
			return true;			
	}
	catch(e){
		;
	}	
	return false;
};

RefactoringManager.prototype.installArtifact = function(doc){
	self = RefactoringManager.getInstance();
	if(self.checkForInstantiatedRefactoring(doc)){
		self.installInstantiatedRefactoring(doc);
		return;
	}else{
		if(self.checkForGenericRefactoring(doc)){
			self.installGenericRefactoring(doc);
		};	
	}
};


RefactoringManager.prototype.installInstantiatedRefactoring = function(file){
	var path = this.getInstantiatedRefactoringFolder(this.getArtefactsPath());
	try{
		var refactoringFolder = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
		refactoringFolder.initWithPath(path);
		file.copyTo(refactoringFolder,"");
		var refactoring = new RefactoringInstantiatedWrapper(path + "/" + file.leafName,this);
		if(!refactoring.isAlreadyInstalled()){
			this.addInstantiatedRefactoring(refactoring);
		}
		else{
			refactoring.delete();
			this.csa.alert("Ya existe un refactoring con este id: " + refactoring.getId());
		}
	}catch(e){;}
};

RefactoringManager.prototype.installGenericRefactoring = function(file){
	var path = this.getGenericRefactoringFolder(this.getArtefactsPath());
	try{
		var refactoringFolder = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
		refactoringFolder.initWithPath(path);
		file.copyTo(refactoringFolder,"");
		var refactoring = new RefactoringGenericWrapper(path + "/" + file.leafName,this);
		if(!refactoring.isAlreadyInstalled()){
			this.addGenericRefactoring(refactoring);
		}
		else{
			refactoring.delete();
			this.csa.alert("Ya existe un refactoring con este id: " + refactoring.getId());
		}
	}catch(e){;}
};


RefactoringManager.prototype.installScenario = function(file){
	return;
};

RefactoringManager.prototype.openScriptManagementWindow = function(){
   	var params = {"artifactmanager":this};
	window.openDialog("chrome://cs-adaptation/content/accessibility/conf/config.xul", "Refactoring Management", "chrome,centerscreen",params);
};

RefactoringManager.prototype.getArtefactsPath = function(){
	var csaFolder = "csa-artefacts";
	var file = Components.classes["@mozilla.org/file/directory_service;1"].
           getService(Components.interfaces.nsIProperties).
           get("ProfD", Components.interfaces.nsIFile);
	file.append(csaFolder);
	if( !file.exists() || !file.isDirectory() ) {   // if it doesn't exist, create
   		file.create(Components.interfaces.nsIFile.DIRECTORY_TYPE, 0777);
	}
	return file.path;
};

RefactoringManager.prototype.getInstantiatedRefactoringFolder = function(artefacts_path){
	var instantiated_refactorings_fs_path = "";
	var osString = Components.classes["@mozilla.org/xre/app-info;1"]
               .getService(Components.interfaces.nsIXULRuntime).OS;
	if(osString == "WINNT"){
		instantiated_refactorings_fs_path = artefacts_path + "\\instantiated_refactorings";
	}
	else
		instantiated_refactorings_fs_path = artefacts_path + "/instantiated_refactorings";
	return instantiated_refactorings_fs_path;
};

RefactoringManager.prototype.getGenericRefactoringFolder = function(artefacts_path){
	var generic_refactorings_fs_path = "";
	var osString = Components.classes["@mozilla.org/xre/app-info;1"]
               .getService(Components.interfaces.nsIXULRuntime).OS;
	if(osString == "WINNT"){
		generic_refactorings_fs_path = artefacts_path + "\\generic_refactorings";
	}
	else
		generic_refactorings_fs_path = artefacts_path + "/generic_refactorings";
	return generic_refactorings_fs_path;
};

RefactoringManager.prototype.getInstantiatedRefactoringWrappers = function(aFileSystemPath){
	//BROWSER_DEPENDENCY: Levantar nombres de scripts del filesystem.
	var root = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
	var artifactsEnum;
	try{
		root.initWithPath(aFileSystemPath);
		artifactsEnum = root.directoryEntries;		
	}catch(e){return [];}

	var artifacts_filename = [];
	var artifacts = [];
	while (artifactsEnum.hasMoreElements()) {
		try{
			var artifacts_path = artifactsEnum.getNext().QueryInterface(Components.interfaces.nsILocalFile).path;
			var splited_art_path = artifacts_path.split("/");
			var artifact_wrapper;
			artifacts_filename.push(splited_art_path[splited_art_path.length-1]);
			artifact_wrapper = new RefactoringInstantiatedWrapper(artifacts_path,this);
			artifacts.push(artifact_wrapper);
		}
		catch(e){continue;}
	}
	return artifacts;
};

RefactoringManager.prototype.getGenericRefactoringWrappers = function(aFileSystemPath){
	//BROWSER_DEPENDENCY: Levantar nombres de scripts del filesystem.
	var root = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
	var artifactsEnum;
	try{
		root.initWithPath(aFileSystemPath);
		artifactsEnum = root.directoryEntries;		
	}catch(e){return [];}

	var artifacts_filename = [];
	var artifacts = [];
	while (artifactsEnum.hasMoreElements()) {
		try{
			var artifacts_path = artifactsEnum.getNext().QueryInterface(Components.interfaces.nsILocalFile).path;
			var splited_art_path = artifacts_path.split("/");
			var artifact_wrapper;
			artifacts_filename.push(splited_art_path[splited_art_path.length-1]);
			artifact_wrapper = new RefactoringGenericWrapper(artifacts_path,this);
			artifacts.push(artifact_wrapper);
		}
		catch(e){continue;}
	}
	return artifacts;
};


RefactoringManager.prototype.initializeInstantiatedRefactorings = function(){
	for (var i = this.instantiated_refactorings.length - 1; i >= 0; i--) {
		this.instantiated_refactorings[i].initialize();
	};
};

RefactoringManager.prototype.getInstantiatedRefactorings = function(){
	if (this.instantiated_refactorings != null){
		try{
			return this.instantiated_refactorings;
		}catch(e){;}
	}
	var artefacts_path = this.getArtefactsPath();
	var refactorings_fs_path = this.getInstantiatedRefactoringFolder(artefacts_path);
	this.instantiated_refactorings = this.getInstantiatedRefactoringWrappers(refactorings_fs_path);
	return this.instantiated_refactorings;
};

RefactoringManager.prototype.getGenericRefactorings = function(){
	if (this.generic_refactorings != null){
		try{
			return this.generic_refactorings;
		}catch(e){;}
	}
	var artefacts_path = this.getArtefactsPath();
	var refactorings_fs_path = this.getGenericRefactoringFolder(artefacts_path);
	this.generic_refactorings = this.getGenericRefactoringWrappers(refactorings_fs_path);
	return this.generic_refactorings;
};

RefactoringManager.prototype.getActivatedInstantiatedRefactorings = function(){
	var all_instantiated_refactorings = this.getInstantiatedRefactorings();
	var instantiated_refactorings = [];
	for(var i=0;i < all_instantiated_refactorings.length;i++)
		if (all_instantiated_refactorings[i].isActivated())
			instantiated_refactorings.push(all_instantiated_refactorings[i].getInstantiatedRefactoring())
	return instantiated_refactorings;
};

RefactoringManager.prototype.unloadInstantiatedRefactoring = function(aWrapper){
	var index = this.instantiated_refactorings.indexOf(aWrapper);
	if (index > -1) {
	    this.instantiated_refactorings.splice(index, 1);
	}
	this.saveOrder();
};

RefactoringManager.prototype.unloadGenericRefactoring = function(aWrapper){
	var index = this.generic_refactorings.indexOf(aWrapper);
	if (index > -1) {
	    this.generic_refactorings.splice(index, 1);
	}
};

RefactoringManager.prototype.move = function (old_index, new_index) {
    if (new_index >= this.instantiated_refactorings.length) {
        var k = new_index - this.instantiated_refactorings.length;
        while ((k--) + 1) {
            this.instantiated_refactorings.push(undefined);
        }
    }
    this.instantiated_refactorings.splice(new_index, 0, this.instantiated_refactorings.splice(old_index, 1)[0]);
    this.saveOrder();
    return this.instantiated_refactorings;
};

RefactoringManager.prototype.moveDown = function(aWrapper){
	var position = this.instantiated_refactorings.indexOf(aWrapper);
	if((-1 < position)&& (position <  this.instantiated_refactorings.length-1)){
		this.move(position,position+1);
	}
};

RefactoringManager.prototype.moveUp = function(aWrapper){
	var position = this.instantiated_refactorings.indexOf(aWrapper);
	if(0 < position){
		this.move(position,position-1);
	}
};

RefactoringManager.prototype.checkForUpdates = function(){
	var oldRefactorings = [],
		newScripts = [],
		refactorings = this.getGenericRefactorings().concat(this.getInstantiatedRefactorings());
	for(var i=0,j=refactorings.length;i<j;i++){
		refactorings[i].checkMyUpdate(oldRefactorings, newScripts);
	}
	if(newScripts.length>0 && newScripts.length === oldRefactorings.length){
		var doUpdate = confirm("New versions of installed refactorings available. Do you want to install them?");
		if(doUpdate === true){
			try{
				installUpdates(oldRefactorings,newScripts);
			}catch(error){
				alert(error);
			}
		}
	}
};

RefactoringManager.prototype.installUpdates = function(oldRefactorings,newScripts){
		for(var i=0,j=newScripts.length;i<j;i++){
			var script = newScripts[i];
			this.installArtifact(script);
		}
		for(var i=0,j=oldRefactorings.length;i<j;i++){
			oldWrappers[i].delete();
		}
		alert("Updates succesfully installed. Please restart Firefox to use them.");
};

/**************************************
 ArtifactWrappers
 Represetan a un archivo en el filesystem en pos de obtener metadata del artefacto y el script propiamente dicho
*/ 

function RefactoringInstantiatedWrapper(file_path,manager){
	this.manager = manager;
	this.refactoring_file_path = file_path;
	this.activated = true;
	this.refactoring = null;
	this.metadata = null;
	this.initialize();
};

RefactoringInstantiatedWrapper.prototype.initialize = function(){
	var script = this.loadScript();
	this.metadata = script.metadata;
	this.refactoring = this.extractRefactoringInstance(script);
	try{
		this.refactoring.setTargetURLs();
		this.refactoring.initialize("es");
	}catch(e){
		alert("Hubo un error al intentar inicializar un refactoring instanciado: " + e);
	}
};

RefactoringInstantiatedWrapper.prototype.activate = function(){
	this.activated = true;
};

RefactoringInstantiatedWrapper.prototype.deactivate = function(){
	this.activated = false;
};


RefactoringInstantiatedWrapper.prototype.isAlreadyInstalled = function(){
	for(var i=0;i < this.manager.instantiated_refactorings.length;i++){
		var refactoring = this.manager.instantiated_refactorings[i];
		if ((refactoring.getId() == this.getId()) && (refactoring != this))
			return true;
	}
	return false;
};

RefactoringInstantiatedWrapper.prototype.toggleActivation = function(){
	if(this.activated)
		this.deactivate();
	else
		this.activate();
};

RefactoringInstantiatedWrapper.prototype.isActivated = function(){
	return this.activated;
};

RefactoringInstantiatedWrapper.prototype.getFilePath = function(){
	return this.refactoring_file_path;
};

RefactoringInstantiatedWrapper.prototype.delete = function(){
	this.manager.unloadInstantiatedRefactoring(this);
	Components.utils.unload(this.absolutePath());
	var file = Components.classes["@mozilla.org/file/local;1"].  
       			createInstance(Components.interfaces.nsILocalFile); 
	file.initWithPath(this.refactoring_file_path);
	file.remove(false);
};

RefactoringInstantiatedWrapper.prototype.loadScriptInContext = function(aContext){
	var service = Components.classes["@mozilla.org/moz/jssubscript-loader;1"].getService(Components.interfaces.mozIJSSubScriptLoader);
	service.loadSubScript(this.absolutePath(),aContext,"US-ASCII");
	return aContext;
};

RefactoringInstantiatedWrapper.prototype.absolutePath = function(){
 	return "file://" + this.refactoring_file_path;
};

RefactoringInstantiatedWrapper.prototype.loadScript = function(){
	var aContext = {};
	this.addGenericRefactoringsToContext(aContext);
	return this.loadScriptInContext(aContext);
};

RefactoringInstantiatedWrapper.prototype.addGenericRefactoringsToContext = function(aContext){
	var genericRefactorings = this.manager.getGenericRefactorings();
	for (var i = 0; i < genericRefactorings.length; i++) {
		var refactoring = genericRefactorings[i].getRefactoringName();
		aContext[refactoring] = {};
		genericRefactorings[i].loadNewInstanceInContext(aContext[refactoring]);
		aContext[refactoring][refactoring] = genericRefactorings[i].getRefactoringClass();
		var exportedObjects = genericRefactorings[i].getExportedObjects();
		for (exportedObject in exportedObjects) {
			aContext[refactoring][exportedObject] = exportedObjects[exportedObject];
		}
	};
};
	
RefactoringInstantiatedWrapper.prototype.getMetadata = function(){	
	var metadata;
	metadata = this.metadata;
	if(typeof(metadata) == "undefined"){
		metadata = {"name":"Refactoring","author":"No author","description":"No description"};	
	}	
	return metadata;
};

RefactoringInstantiatedWrapper.prototype.author = function(){
	return this.getMetadata()["author"];
};

RefactoringInstantiatedWrapper.prototype.name = function(){
	return this.getMetadata()["name"];
};

RefactoringInstantiatedWrapper.prototype.description = function(){
	return this.getMetadata()["description"];
};

RefactoringInstantiatedWrapper.prototype.getId = function(){
	return this.getMetadata()["id"];
};

RefactoringInstantiatedWrapper.prototype.extractRefactoringInstance = function(script){
	return script.getAccessibilityAugmenter();
};

RefactoringInstantiatedWrapper.prototype.getName = function(){
	return this.refactoring.getName();
};

RefactoringInstantiatedWrapper.prototype.execute = function(doc, target){
	return this.refactoring.execute(doc, target);
};

RefactoringInstantiatedWrapper.prototype.reload = function(){
	this.manager.unloadInstantiatedRefactoring(this);
	Components.utils.unload(this.absolutePath());
	this.refactoring = {};
	this.initialize();
	this.manager.addInstantiatedRefactoring(this);
};

RefactoringInstantiatedWrapper.prototype.getInstantiatedRefactoring = function(){
	return this.refactoring;
};

RefactoringInstantiatedWrapper.prototype.checkMyUpdate = checkForMyUpdate;

/******************************/
/* GENERIC REFACTORING WRAPPER*/
/******************************/

function RefactoringGenericWrapper(file_path,manager){
	this.manager = manager;
	this.refactoring_file_path = file_path;
	this.refactoring = null;
	this.exportedObjects = null;
	this.metadata = null;
	this.initialize();
};

RefactoringGenericWrapper.prototype.addSupportObjectsToContext = function(aContext){
	aContext["RefactoringPersistenceManager"] = RefactoringPersistenceManager;
	return aContext;
};

RefactoringGenericWrapper.prototype.initialize = function(){
	var script = this.loadScript();
	this.metadata = script.metadata;
	this.exportedObjects = script.exportedObjects;
	this.refactoring = this.extractRefactoringClass(script);
};

RefactoringGenericWrapper.prototype.getFilePath = function(){//Factorizar en superclase AbstractRefactoringWrapper
	return this.refactoring_file_path;
};

RefactoringGenericWrapper.prototype.delete = function(){//Factorizar en superclase AbstractRefactoringWrapper! TemplateMethod para el unload
	this.manager.unloadGenericRefactoring(this);
	Components.utils.unload(this.absolutePath());
	var file = Components.classes["@mozilla.org/file/local;1"].  
       			createInstance(Components.interfaces.nsILocalFile); 
	file.initWithPath(this.refactoring_file_path);
	file.remove(false);
};

RefactoringGenericWrapper.prototype.isAlreadyInstalled = function(){
	for(var i=0;i < this.manager.generic_refactorings.length;i++){
		var refactoring = this.manager.generic_refactorings[i];
		if ((refactoring.getId() == this.getId()) && (refactoring != this))
			return true;
	}
	return false;
};

RefactoringGenericWrapper.prototype.loadScriptInContext = function(aContext){//Factorizar en superclase AbstractRefactoringWrapper
	var service = Components.classes["@mozilla.org/moz/jssubscript-loader;1"].getService(Components.interfaces.mozIJSSubScriptLoader);
	service.loadSubScript(this.absolutePath(),aContext,"US-ASCII");
	return aContext;
};

RefactoringGenericWrapper.prototype.absolutePath = function(){//Factorizar en superclase AbstractRefactoringWrapper
 	return "file://" + this.refactoring_file_path;
};

RefactoringGenericWrapper.prototype.loadScript = function(){
	var aContext = {}
	aContext["instance"] = null;
	aContext["document"] = null;
	this.loadScriptInContext(aContext);
	this.addSupportObjectsToContext(aContext);
	return aContext;
};

RefactoringGenericWrapper.prototype.getMetadata = function(){//Factorizar en superclase AbstractRefactoringWrapper	
	var metadata;
	metadata = this.metadata;
	if(typeof(metadata) == "undefined"){
		metadata = {"name":"Refactoring","author":"No author","description":"No description"};	
	}	
	return metadata;
};

RefactoringGenericWrapper.prototype.author = function(){
	return this.getMetadata()["author"];
};

RefactoringGenericWrapper.prototype.name = function(){
	return this.getMetadata()["name"];
};

RefactoringGenericWrapper.prototype.description = function(){
	return this.getMetadata()["description"];
};

RefactoringGenericWrapper.prototype.getId = function(){
	return this.getMetadata()["id"];
};

RefactoringGenericWrapper.prototype.getRefactoringGeneric = function(){
	return this.refactoring;
};

RefactoringGenericWrapper.prototype.extractRefactoringClass = function(script){
	return script.exportedObjects.GenericRefactoring;
};

RefactoringGenericWrapper.prototype.getName = function(){
	return this.refactoring.getName();
};

RefactoringGenericWrapper.prototype.reload = function(){
	this.manager.unloadGenericRefactoring(this);
	Components.utils.unload(this.absolutePath());
	this.refactoring = {};
	this.initialize();
	this.manager.addGenericRefactoring(this);
};

RefactoringGenericWrapper.prototype.getRefactoringName = function(){
	return this.refactoring.name;
};

RefactoringGenericWrapper.prototype.getRefactoringClass = function(){
	return this.refactoring;
};

RefactoringGenericWrapper.prototype.getExportedObjects = function(){
	return this.exportedObjects;
};

RefactoringGenericWrapper.prototype.loadNewInstanceInContext = function(aContext){
	aContext["instance"] = null;
	aContext["document"] = null;
	this.loadScriptInContext(aContext);
	this.addSupportObjectsToContext(aContext);
	return aContext
};

RefactoringGenericWrapper.prototype.checkMyUpdate = checkForMyUpdate;

function checkForMyUpdate(oldRefactorings,newScripts){
	var metadata = this.getMetadata(),
		xhr,
		id = metadata["id"],
		version = metadata["version"];
	id = window.btoa(id);
	version = window.btoa(version);
	xhr = new XMLHttpRequest();
	xhr.open("GET","http://vps-959090-x.dattaweb.com/refactorings/app.php/api/refactoring/get-update/"+id+"/"+version,true);
	xhr.send();
	xhr.onreadystatechange = function(){
		if (xhr.readyState === 4 && xhr.status === 200){
			response = JSON.parse(xhr["response"])["response"];
			if(response === "Success"){
				script = window.atob(JSON.parse(xhr["response"])["script"]);
				oldRefactorings.push(metadata);
				newScripts.push(script);
			}
		}
	}
};
