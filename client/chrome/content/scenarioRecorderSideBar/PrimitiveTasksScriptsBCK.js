function PrimitiveTaskScriptsManager(){
	this.scripts = {};//{"id":functionName}
	this.initialize();
};

PrimitiveTaskScriptsManager.prototype.initialize = function(){
	var provideURL = new ProvideURL();
	this.scripts[provideURL.getId()] = provideURL;

	var loadWebSite = new LoadWebSite();
	this.scripts[loadWebSite.getId()] = loadWebSite;

	var navigateLink = new NavigateLink();
	this.scripts[navigateLink.getId()] = navigateLink;

	var goBack = new GoBack();
	this.scripts[goBack.getId()] = goBack;

	var clickButton = new ClickButton();
	this.scripts[clickButton.getId()] = clickButton;

	var submitForm = new SubmitForm();
	this.scripts[submitForm.getId()] = submitForm;

	var selectInMenu = new SelectInMenu();
	this.scripts[selectInMenu.getId()] = selectInMenu;


	var enterStringIntoInput = new 	EnterStringIntoInput();
	this.scripts[enterStringIntoInput.getId()] = enterStringIntoInput;

};

PrimitiveTaskScriptsManager.prototype.getSupportedTasks = function(){
	return this.scripts;
};

PrimitiveTaskScriptsManager.prototype.getTaskScriptById = function(anId){
	try{return this.scripts[anId]}
	catch(e){return null;}
};

PrimitiveTaskScriptsManager.prototype.removeDataFromPreviousExecution = function(){
	for (each in this.scripts)
		this.scripts[each].removeManualExecutionData();
};
/***************************************/
function AbstractPrimitiveTaskScript(){
	//Se requiere: nombre, id, lista de attributos donde cada elemento es un objeto ("name","type","value","example")
	this.name;
	this.id;
	this.scriptAttributes;
};
AbstractPrimitiveTaskScript.prototype.intialize = function(aName, anId, aListOfAttributes){
	this.name = aName;
	this.id = anId;
	this.scriptAttributes = aListOfAttributes;
};
AbstractPrimitiveTaskScript.prototype.getName = function(){
	return this.name;
}
AbstractPrimitiveTaskScript.prototype.getScriptsAttributes = function(){
	return this.scriptAttributes;
}
AbstractPrimitiveTaskScript.prototype.getId = function(){
	return this.id;
}
AbstractPrimitiveTaskScript.prototype.tryAutomaticExecution = function(){
	if(this.isDataNeededAvailable()){
		this.automaticExecution();//Lanzar excepciones y cosas para poder reflejar el estado en el escenario.
		return true;
	}
	return false;
};
AbstractPrimitiveTaskScript.prototype.setEventsManagersForManualExecution = function(){
	return;
};
AbstractPrimitiveTaskScript.prototype.isDataNeededAvailable = function(){
	return true;
};
AbstractPrimitiveTaskScript.prototype.getAttribute = function(anAttrinuteName){
	for (var i = 0;i < this.scriptAttributes.length;i++){
		var attribute = this.scriptAttributes[i];
		if (attribute["name"] == anAttrinuteName)
			return attribute;
	}
	return false;
};

AbstractPrimitiveTaskScript.prototype.setAttributeValue = function(aName,aValue){
	for (var i = 0;i < this.scriptAttributes.length;i++){
		var attribute = this.scriptAttributes[i];
		if (attribute["name"] == aName)
			attribute["value"] = aValue;
	}
	return false;
};

AbstractPrimitiveTaskScript.prototype.validateAndUseAttributes = function(aListOfAttributes){
	var eventAttributes = aListOfAttributes;
	var validateValues = CSA.mainWindow.__StateOfTaskInExecution__.task.useSetValuesAsConditionsForManualExecution;
	for (var i=0;i < this.scriptAttributes.length;i++){
		var attrName = this.scriptAttributes[i]["name"];
		var attrValue = this.scriptAttributes[i]["value"];
		for (var j=0;j < eventAttributes.length;j++){
			var eAttrName = eventAttributes[j]["name"];
			var eAttrValue = eventAttributes[j]["value"];
			//CSA.mainWindow.alert(attrValue+ "=" + eAttrValue);
			if (eAttrName == attrName){
				this.scriptAttributes[i]["value"] = eAttrValue;
				if ((attrValue == null) || (attrValue == "") || (attrValue == eAttrValue))
					continue;				
				else
					if (validateValues){
						return false;	
					}			
			}
		}
	}
	return true; 
};
/***************************************/
function ProvideURL(){
	this.intialize("Provide a URL", "provideURL", [{"name":"URL","type":"Regular Expression","value":"http://www.youtube.com","example":"http://www.google.com*"}]);
};
ProvideURL.prototype = new AbstractPrimitiveTaskScript();
ProvideURL.prototype.isDataNeededAvailable = function(){
	var url = this.getAttribute("URL");
	return url["value"] != null;
};
ProvideURL.prototype.automaticExecution = function(){
	CSA.mainWindow.content.document.location.href = this.getAttribute("URL")["value"];
	//CSA.addTab(this.getAttribute("URL")["value"]);
};
ProvideURL.prototype.removeManualExecutionData = function(){
	CSA.mainWindow.removeEventListener("TabSelect",__callback__ProvideURL__, false);
};
function __callback__ProvideURL__(e){
    var task = CSA.mainWindow.__StateOfTaskInExecution__.task;
    CSA.mainWindow.__StateOfTaskInExecution__.removeManualExecutionDataAndContinue();
};
ProvideURL.prototype.setEventsManagersForManualExecution = function(aState){
    CSA.mainWindow.__StateOfTaskInExecution__ = aState;
    CSA.mainWindow.addEventListener("TabSelect",__callback__ProvideURL__, false);	
};

/***************************************/
function LoadWebSite(){
	this.intialize("Load Web Site", "loadWebSite", [{"name":"URL","type":"Regular Expression","value":null,"example":"http://www.google.com*"}]);
};
LoadWebSite.prototype = new AbstractPrimitiveTaskScript();
LoadWebSite.prototype.isDataNeededAvailable = function(){
	var url = this.getAttribute("URL");
	return url["value"] != null;
};
LoadWebSite.prototype.automaticExecution = function(){
	CSA.mainWindow.content.document.location.href = this.getAttribute("URL")["value"];
	//CSA.addTab(this.getAttribute("URL")["value"]);
};
LoadWebSite.prototype.removeManualExecutionData = function(){
	CSA.mainWindow.removeEventListener("DOMContentLoaded",__callback__LoadWebSite__, false);
};
function __callback__LoadWebSite__(e){
    var task = CSA.mainWindow.__StateOfTaskInExecution__.task;
   	var buildedAttributes = [{"name":"URL","type":"","value":e.target.location.href,"example":"an xPath expression"}];
    if (CSA.mainWindow.__ScriptTaskWaiting__.validateAndUseAttributes(buildedAttributes)){
    	task.updateDataFromExecution(CSA.mainWindow.__ScriptTaskWaiting__.scriptAttributes);
        CSA.mainWindow.__StateOfTaskInExecution__.removeManualExecutionDataAndContinue();
	}
};
LoadWebSite.prototype.setEventsManagersForManualExecution = function(aState){
    CSA.mainWindow.__ScriptTaskWaiting__ = this;
    CSA.mainWindow.__StateOfTaskInExecution__ = aState;
	var appcontent = CSA.mainWindow.document.getElementById("appcontent");
    appcontent.addEventListener("DOMContentLoaded", __callback__LoadWebSite__, true);
};

/***************************************/
function NavigateLink(){
	this.intialize("Navigate a Link", "navigateLink", [{"name":"Anchor element","type":"xPath","value":null,"example":"an xPath expression"}]);
};
NavigateLink.prototype = new AbstractPrimitiveTaskScript();
NavigateLink.prototype.isDataNeededAvailable = function(){
	return ((this.getAttribute("Anchor element")["value"] != "")  && (this.getAttribute("Anchor element")["value"] != null));
};
NavigateLink.prototype.automaticExecution = function(){
	var anchor = CSA.getElementFromXPath(this.getAttribute("Anchor element")["value"],CSA.getCurrentDocument());
	anchor.click();
};
NavigateLink.prototype.removeManualExecutionData = function(){
	CSA.mainWindow.removeEventListener("click",__callback__NavigateLink__, false);
};
function __callback__NavigateLink__(e){
    var task = CSA.mainWindow.__StateOfTaskInExecution__.task;
    if (e.target.nodeName == "A"){
    	var theAnchorXPath = CSA.getXPathElement(e.target,CSA.getCurrentDocument());
    	var buildedAttributes = [{"name":"Anchor element","type":"","value":theAnchorXPath,"example":"an xPath expression"}];
    	if (CSA.mainWindow.__ScriptTaskWaiting__.validateAndUseAttributes(buildedAttributes)){
    		CSA.mainWindow.__StateOfTaskInExecution__.task.updateDataFromExecution(CSA.mainWindow.__ScriptTaskWaiting__.scriptAttributes);
    		CSA.mainWindow.__StateOfTaskInExecution__.removeManualExecutionDataAndContinue();
    	}
    }
};
NavigateLink.prototype.setEventsManagersForManualExecution = function(aState){
    CSA.mainWindow.__ScriptTaskWaiting__ = this;
    CSA.mainWindow.__StateOfTaskInExecution__ = aState;
	var appcontent = CSA.mainWindow.document.getElementById("appcontent");
    appcontent.addEventListener("click", __callback__NavigateLink__, true);
};

/***************************************/
function GoBack(){
	this.intialize("Go Back", "goBack", []);
};
GoBack.prototype = new AbstractPrimitiveTaskScript();
GoBack.prototype.isDataNeededAvailable = function(){
	return true;
};

GoBack.prototype.automaticExecution = function(){
	history.back();
};
GoBack.prototype.removeManualExecutionData = function(){
	return;
};
function __callback__GoBack__(e){
    var task = CSA.mainWindow.__StateOfTaskInExecution__.task;
    CSA.mainWindow.__StateOfTaskInExecution__.removeManualExecutionDataAndContinue();    
};
GoBack.prototype.setEventsManagersForManualExecution = function(aState){
    CSA.mainWindow.__StateOfTaskInExecution__ = aState;
	var appcontent = CSA.mainWindow.document.getElementById("appcontent");
    appcontent.addEventListener("click", __callback__GoBack__, true);
};

/***************************************/
function ClickButton(){
	this.intialize("Click a Button", "clickButton", [{"name":"Button element","type":"xPath","value":null,"example":"an xPath expression"}]);
};
ClickButton.prototype = new AbstractPrimitiveTaskScript();
ClickButton.prototype.isDataNeededAvailable = function(){
	return ((this.getAttribute("Button element")["value"] != "")  && (this.getAttribute("Button element")["value"] != null));
};
ClickButton.prototype.automaticExecution = function(){
	var button = CSA.getElementFromXPath(this.getAttribute("Button element")["value"],CSA.getCurrentDocument());
	button.click();
};
ClickButton.prototype.removeManualExecutionData = function(){
	CSA.mainWindow.removeEventListener("click",__callback__clickButton__, false);
};
function __callback__clickButton__(e){
    var task = CSA.mainWindow.__StateOfTaskInExecution__.task;
    if ((e.target.nodeName.toLowerCase() == "button") || ((e.target.nodeName.toLowerCase() == "input") && (e.target.getAttribute("type") == "submit"))){
    	var theButtonXPath = CSA.getXPathElement(e.target,CSA.getCurrentDocument());
    	var buildedAttributes = [{"name":"Button element","type":"","value":theButtonXPath,"example":"an xPath expression"}];
    	if (CSA.mainWindow.__ScriptTaskWaiting__.validateAndUseAttributes(buildedAttributes)){
    		CSA.mainWindow.__StateOfTaskInExecution__.task.updateDataFromExecution(CSA.mainWindow.__ScriptTaskWaiting__.scriptAttributes);
    		CSA.mainWindow.__StateOfTaskInExecution__.removeManualExecutionDataAndContinue();
    	}
	}
};
ClickButton.prototype.setEventsManagersForManualExecution = function(aState){
    CSA.mainWindow.__StateOfTaskInExecution__ = aState;
    CSA.mainWindow.__ScriptTaskWaiting__ = this;
	var appcontent = CSA.mainWindow.document.getElementById("appcontent");
    appcontent.addEventListener("click", __callback__clickButton__, true);
};
/***************************************/
function SubmitForm(){
	this.intialize("Submit a Form", "submitForm", [{"name":"Form element","type":"xPath","value":null,"example":"an xPath expression"}]);
};
SubmitForm.prototype = new AbstractPrimitiveTaskScript();
SubmitForm.prototype.isDataNeededAvailable = function(){
	return ((this.getAttribute("Form element")["value"] != "")  && (this.getAttribute("Form element")["value"] != null));
};
SubmitForm.prototype.automaticExecution = function(){
	var form = CSA.getElementFromXPath(this.getAttribute("Form element")["value"],CSA.getCurrentDocument());
	form.submit();
};
SubmitForm.prototype.removeManualExecutionData = function(){
	CSA.mainWindow.removeEventListener("submit",__callback__submitForm__, false);
};
function __callback__submitForm__(e){
    var task = CSA.mainWindow.__StateOfTaskInExecution__.task;    
    if (e.target.nodeName.toLowerCase() == "form"){
    	var theFormXPath = CSA.getXPathElement(e.target,CSA.getCurrentDocument());
    	var buildedAttributes = [{"name":"Form element","type":"","value":theFormXPath,"example":"an xPath expression"}];
    	if (CSA.mainWindow.__ScriptTaskWaiting__.validateAndUseAttributes(buildedAttributes)){
    		CSA.mainWindow.__StateOfTaskInExecution__.task.updateDataFromExecution(CSA.mainWindow.__ScriptTaskWaiting__.scriptAttributes);
    		CSA.mainWindow.__StateOfTaskInExecution__.removeManualExecutionDataAndContinue();
    	}
	}
};
SubmitForm.prototype.setEventsManagersForManualExecution = function(aState){
    CSA.mainWindow.__StateOfTaskInExecution__ = aState;
    CSA.mainWindow.__ScriptTaskWaiting__ = this;
	var appcontent = CSA.mainWindow.document.getElementById("appcontent");
    appcontent.addEventListener("submit", __callback__submitForm__, true);
};
/***************************************/
function SelectInMenu(){
	this.intialize("Select an option menu", "selectOption", [{"name":"Option element","type":"xPath","value":null,"example":"an xPath expression"}]);
};
SelectInMenu.prototype = new AbstractPrimitiveTaskScript();
SelectInMenu.prototype.isDataNeededAvailable = function(){
	return ((this.getAttribute("Option element")["value"] != "")  && (this.getAttribute("Option element")["value"] != null));
};
SelectInMenu.prototype.automaticExecution = function(){
	var option = CSA.getElementFromXPath(this.getAttribute("Option element")["value"],CSA.getCurrentDocument());
	option.click();
	option.setAttribute("selected","true");
};
SelectInMenu.prototype.removeManualExecutionData = function(){
	CSA.mainWindow.removeEventListener("click",__callback__selectInMenu__, false);
};
function __callback__selectInMenu__(e){
    var task = CSA.mainWindow.__StateOfTaskInExecution__.task;    
    if (e.target.nodeName.toLowerCase() == "option"){
    	var theOptionXPath = CSA.getXPathElement(e.target,CSA.getCurrentDocument());
    	var buildedAttributes = [{"name":"Option element","type":"","value":theOptionXPath,"example":"an xPath expression"}];
    	if (CSA.mainWindow.__ScriptTaskWaiting__.validateAndUseAttributes(buildedAttributes)){
    		CSA.mainWindow.__StateOfTaskInExecution__.task.updateDataFromExecution(CSA.mainWindow.__ScriptTaskWaiting__.scriptAttributes);
    		CSA.mainWindow.__StateOfTaskInExecution__.removeManualExecutionDataAndContinue();
    	}
	}
};
SelectInMenu.prototype.setEventsManagersForManualExecution = function(aState){
    CSA.mainWindow.__StateOfTaskInExecution__ = aState;
    CSA.mainWindow.__ScriptTaskWaiting__ = this;
	var appcontent = CSA.mainWindow.document.getElementById("appcontent");
    appcontent.addEventListener("click", __callback__selectInMenu__, true);
};
/***************************************/
function EnterStringIntoInput(){
	this.intialize("Enter String into Input", "enterStringIntoInput", [{"name":"Input element","type":"xPath","value":null,"example":"an xPath expression"},{"name":"Value","type":"String","value":null,"example":"Hello World!"}]);
};
EnterStringIntoInput.prototype = new AbstractPrimitiveTaskScript();
EnterStringIntoInput.prototype.isDataNeededAvailable = function(){
	var valueInput = this.getAttribute("Input element")["value"];
	var textValue = this.getAttribute("Value")["value"];
	var attributes = ((valueInput != "")  && (valueInput != null));
	return attributes && ((textValue != "")  && (textValue != null));
};
EnterStringIntoInput.prototype.automaticExecution = function(){
	var input = CSA.getElementFromXPath(this.getAttribute("Input element")["value"],CSA.getCurrentDocument());
	input.setAttribute("value",this.getAttribute("Value")["value"]);
};
EnterStringIntoInput.prototype.removeManualExecutionData = function(){
	CSA.mainWindow.removeEventListener("change",__callback__enterStringIntoInput__, false);
};
function __callback__enterStringIntoInput__(e){
	
    var task = CSA.mainWindow.__StateOfTaskInExecution__.task;
    if ((e.target.nodeName.toLowerCase() == "input") || (e.target.nodeName.toLowerCase() == "textarea")){
		var theInputXPath = CSA.getXPathElement(e.target,CSA.getCurrentDocument());
		var theInputValue = e.target.value;
		var buildedAttributes = [{"name":"Input element","type":"xPath","value":theInputXPath,"example":"an xPath expression"},{"name":"Value","type":"String","value":theInputValue,"example":"Hello World!"}];
		if (CSA.mainWindow.__ScriptTaskWaiting__.validateAndUseAttributes(buildedAttributes)){
			CSA.mainWindow.__StateOfTaskInExecution__.task.updateDataFromExecution(CSA.mainWindow.__ScriptTaskWaiting__.scriptAttributes);
			CSA.mainWindow.__StateOfTaskInExecution__.removeManualExecutionDataAndContinue();
		}
	}
};
EnterStringIntoInput.prototype.setEventsManagersForManualExecution = function(aState){
    CSA.mainWindow.__StateOfTaskInExecution__ = aState;
    CSA.mainWindow.__ScriptTaskWaiting__ = this;
	var appcontent = CSA.mainWindow.document.getElementById("appcontent");
    appcontent.addEventListener("change", __callback__enterStringIntoInput__, true);
};
/***************************************/