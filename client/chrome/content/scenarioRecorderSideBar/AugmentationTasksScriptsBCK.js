function AugmentationTaskScriptsManager(){
	this.scripts = {};//{"id":functionName}
	this.artifactScripts = {};//{"id":functionName}
	this.initialize();
};

AugmentationTaskScriptsManager.prototype.initialize = function(){
	var dataCollectors = CSA.getDataCollectors();
	for (var i=0;i < dataCollectors.length;i++){
		var dataCollector = dataCollectors[i];
		var dataCollectorTask = new AugmentationTaskScript(dataCollector.getName(),dataCollector.getId(),dataCollector.getAttributesList(),dataCollector);
		dataCollectorTask.setManager(this);
		this.scripts[dataCollector.getId()] = dataCollectorTask;
		this.artifactScripts[dataCollector.getId()] = dataCollector;
	};

	var augmenters = CSA.getAugmenters();
	for (var i=0;i < augmenters.length;i++){
		var augmenter = augmenters[i];
		var augmenterTask = new AugmentationTaskScript(augmenter.getName(),augmenter.getId(),augmenter.getAttributesList(),augmenter);
		augmenterTask.setManager(this);
		this.scripts[augmenter.getId()] = augmenterTask;
		this.artifactScripts[augmenter.getId()] = augmenter;
	};
};

AugmentationTaskScriptsManager.prototype.getSupportedTasks = function(){
	return this.scripts;
};

AugmentationTaskScriptsManager.prototype.getArtifactScriptById = function(anId){
	try{return this.artifactScripts[anId]}
	catch(e){return null}
};

AugmentationTaskScriptsManager.prototype.getTaskScriptById = function(anId){
	try{return this.scripts[anId]}
	catch(e){return null;}
};

AugmentationTaskScriptsManager.prototype.removeDataFromPreviousExecution = function(){
	for (each in this.scripts)
		this.scripts[each].removeManualExecutionData();
};
/***************************************/
function AugmentationTaskScript(aName, anId, artifactListOfAttributes, anArtifact){
	this.manager = null;
	this.artifact = null;
	this.name = aName;
	this.id = anId;
	this.scriptAttributes = artifactListOfAttributes;
	if (typeof(anArtifact) != "undefined")
		this.artifact = anArtifact;
};
AugmentationTaskScript.prototype.setManager = function(aManager){
	this.manager = aManager;
}
AugmentationTaskScript.prototype.getName = function(){
	return this.name;
}
AugmentationTaskScript.prototype.getScriptsAttributes = function(){
	return this.scriptAttributes;
}
AugmentationTaskScript.prototype.getId = function(){
	return this.id;
}
AugmentationTaskScript.prototype.getAttribute = function(anAttrinuteName){
	for (var i = 0;i < this.scriptAttributes.length;i++){
		var attribute = this.scriptAttributes[i];
		if (attribute["name"] == anAttrinuteName)
			return attribute;
	}
	return false;
};

AugmentationTaskScript.prototype.setAttributeValue = function(aName,aValue){
	for (var i = 0;i < this.scriptAttributes.length;i++){
		var attribute = this.scriptAttributes[i];
		if (attribute["name"] == aName)
			attribute["value"] = aValue;
	}
	return false;
};
AugmentationTaskScript.prototype.isDataNeededAvailable = function(){
	return true;//aca deberiamos prefuntarle al artifact
};
AugmentationTaskScript.prototype.tryAutomaticExecution = function(){
	if(this.isDataNeededAvailable()){
		this.automaticExecution();//Lanzar excepciones y cosas para poder reflejar el estado en el escenario.
		return true;
	}
	return false;
};
AugmentationTaskScript.prototype.getArtifact = function(){
	if (this.artifact == null){
		this.artifact = this.manager.getArtifactScriptById(this.getId());
	}
	return this.artifact;
}
AugmentationTaskScript.prototype.automaticExecution = function(){	
	var dispatcher = CSA.getAugmenterDispatcher();
	dispatcher.dispatchAugmenterAutomatically(this.getArtifact(),this.scriptAttributes);
};
AugmentationTaskScript.prototype.removeManualExecutionData = function(){

   	var eventName = CSA.mainWindow.__eventForDeletingListener__;
   	CSA.mainWindow.__eventAugmentationTaskTarget__.removeEventListener(eventName,AugmentationTaskScript__callback__,false);

   	CSA.mainWindow.__StateOfTaskInExecution__ = null;
	CSA.mainWindow.__AugmentationScriptListener__ = null;
	CSA.mainWindow.__eventForDeletingListener__ = null;	
	CSA.mainWindow.__eventAugmentationTaskTarget__ = null;	
	
};

AugmentationTaskScript.prototype.validateAndUseAttributes = function(aListOfAttributes){
	var eventAttributes = eval ("(" + aListOfAttributes + ")");
	var validateValues = CSA.mainWindow.__StateOfTaskInExecution__.task.useSetValuesAsConditionsForManualExecution;
	for (var i=0;i < this.scriptAttributes.length;i++){
		var attrName = this.scriptAttributes[i]["name"];
		var attrValue = this.scriptAttributes[i]["value"];
		for (var j=0;j < eventAttributes.length;j++){
			var eAttrName = eventAttributes[j]["name"];
			var eAttrValue = eventAttributes[j]["value"];
			if (eAttrName == attrName){
				this.scriptAttributes[i]["value"] = eAttrValue;
				if ((attrValue == null) || (attrValue == "") || (attrValue == eAttrValue))
					continue;				
				else
					if (validateValues)
						return false;				
			}
		}
	}
	return true; 
};

function AugmentationTaskScript__callback__(e){
	var self = CSA.mainWindow.__AugmentationScriptListener__;
	if ((e.target.dispatcherId == self.getId()) && (self.validateAndUseAttributes(e.target.scriptAttributes))){
  		//var task = CSA.mainWindow.__StateOfTaskInExecution__.task;
  		CSA.mainWindow.__eventAugmentationTaskTarget__ = e.target;
		CSA.mainWindow.__StateOfTaskInExecution__.task.updateDataFromExecution(self.scriptAttributes);
    	CSA.mainWindow.__StateOfTaskInExecution__.removeManualExecutionDataAndContinue();
    }
};

AugmentationTaskScript.prototype.setEventsManagersForManualExecution = function(aState){
	var eventName = this.getArtifact().getEventName();
	CSA.mainWindow.__StateOfTaskInExecution__ = aState;
	CSA.mainWindow.__AugmentationScriptListener__ = this;
	CSA.mainWindow.__eventForDeletingListener__ = eventName;
	CSA.mainWindow.addEventListener(eventName,AugmentationTaskScript__callback__, false);
};
