/*
 * CSNDOM Inspector
 */

__DUIElementCollector__INSTANCE__ = null;
function DUIElementCollector(aTask) {
	if (__DUIElementCollector__INSTANCE__ != null){
		return __DUIElementCollector__INSTANCE__;
	}
	this.dom_element_for_concept = null;
	this.task = aTask;
	this.lastDropTarget = null;
	this.DOMDocument = null;
	__DUIElementCollector__INSTANCE__ = this;
}; 	

DUIElementCollector.prototype.addNewElementToModdingInterface = function(){
	var element_xpath = getXPathForElement(this.dom_element_for_concept,content.document);	
};


var COMPUTED_STYLE = "";
function getStyles(aNode){ 
    return aNode.currentStyle || aNode.ownerDocument.defaultView.getComputedStyle(aNode, null);
};

function setComputedStyles(aNode, styles){ 
    var css = "";
    for (p in styles) 
        if (styles.getPropertyValue(p) != "")
            css += p + ":" + styles.getPropertyValue(p) + ";"
    aNode.setAttribute("style",css);
    return css;
};

DUIElementCollector.prototype.setStyleHighlighting = function(event){
	event.stopPropagation();
	event.preventDefault();
	
	COMPUTED_STYLE = getStyles(event.target);
	if (typeof( event.target.setAttribute ) == 'function') {
		event.target.setAttribute("style_bck",COMPUTED_STYLE);
        event.target.setAttribute("style",event.target.style +	" border:1px solid #336699;background-color:#FFFFCC;");
	}
};

DUIElementCollector.prototype.backStyleHighlighting = function(event){
	event.preventDefault();
	event.stopPropagation();
	if (typeof(event.target.setAttribute ) == 'function')  {
	    setComputedStyles(event.target,COMPUTED_STYLE);
	}
};


DUIElementCollector.prototype.recursiveSetAllChildStyles = function(element){
	try{
		var styles = getStyles(element);
		setComputedStyles(element, styles);
	}
	catch(e){;}
	for (var i = 0; i < element.childNodes.length; i++){
		var child = element.childNodes[i];
		this.recursiveSetAllChildStyles(child);
	}
};

DUIElementCollector.prototype.setAllChildStyles = function(element){
	this.recursiveSetAllChildStyles(element);
};

DUIElementCollector.prototype.selectElement = function(event){
	event.preventDefault();
	event.stopPropagation();	

	var __DUIDOLLECTOR__ = new DUIElementCollector();
	__DUIDOLLECTOR__.lastDropTarget = event.target;

	if (typeof( event.target.setAttribute ) == 'function')  {
	    setComputedStyles(event.target,COMPUTED_STYLE);
	}
	
	var xmlString = new CSA.mainWindow.XMLSerializer();
    __DUIDOLLECTOR__.disableDomHighlighting();

	__DUIDOLLECTOR__.setAllChildStyles(event.target);

	
	var duiElement = xmlString.serializeToString(event.target);
	__DUIDOLLECTOR__.task.setDuiElement(duiElement);

};

DUIElementCollector.prototype.recursiveEnableDomHighlighting = function(element){
	element.addEventListener("mouseover", this.setStyleHighlighting,false);
	element.addEventListener("mouseout", this.backStyleHighlighting,false);
	element.addEventListener("click", this.selectElement,false);

	for (var i = 0; i < element.childNodes.length; i++){
		var child = element.childNodes[i];
		this.recursiveEnableDomHighlighting(child);
	}
};

DUIElementCollector.prototype.enableDomHighlighting = function(aDOM){
	this.DOMDocument = aDOM;
	var body = aDOM.body;
	this.recursiveEnableDomHighlighting(body);
};

DUIElementCollector.prototype.recursiveDisableDomHighlighting = function(element){	
	element.removeEventListener("mouseover",this.setStyleHighlighting,false);	
	element.removeEventListener("mouseout",this.backStyleHighlighting,false);
	element.removeEventListener("click", this.selectElement,false);

	for (var i = 0; i < element.childNodes.length; i++){
		var child = element.childNodes[i];
		this.recursiveDisableDomHighlighting(child);
	}
};

DUIElementCollector.prototype.disableDomHighlighting = function(){
	var body = this.DOMDocument.body;
	this.recursiveDisableDomHighlighting(body);
};
