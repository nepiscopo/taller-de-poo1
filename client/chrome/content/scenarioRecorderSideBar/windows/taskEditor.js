function TaskEditor(aTask){
	this.task = aTask;
	this.init();
};

TaskEditor.prototype.setTaskOptions = function(){
	document.getElementById("repetitive-task").setAttribute("checked",this.task.isRepetitive());
	document.getElementById("optional-task").setAttribute("checked",this.task.isOptional());
	document.getElementById("automatic-task").setAttribute("checked",this.task.isAutomatic());
	document.getElementById("use-attributes-for-manual-execution").setAttribute("checked",this.task.useSetValuesAsConditionsForManualExecution);
	document.getElementById("task-edition-dialog").setAttribute("title",this.task.getName());
	document.getElementById("task-name").setAttribute("value",this.task.getName());
};

TaskEditor.prototype.setTaskAttributes = function(){
	var taskAttributes = this.task.getTaskAttributes();	
	if (taskAttributes.length){		
		var attributesContainer = document.getElementById("attributes-container");
		attributesContainer.setAttribute("hidden","false");
		var attributesBox = document.getElementById("attributes");
		for(var i = 0;i < taskAttributes.length;i++){
			var attribute = taskAttributes[i];
			var attributeBox = document.createElement("hbox");
			attributeBox.attribute = attribute;

			var attributeLabel = document.createElement("label");
			attributeLabel.setAttribute("value",attribute.getName() + ":");
			attributeLabel.setAttribute("style","text-align:right;");
			attributeBox.appendChild(attributeLabel);

			var attributeInput = document.createElement("textbox");
			attributeInput.setAttribute("value",attribute.getValue());
			attributeInput.setAttribute("flex","1");
			attributeInput.setAttribute("id","task-attribute-"+i);
			attributeInput.taskAttribute = attribute;
			attributeBox.appendChild(attributeInput);
			attributesBox.appendChild(attributeBox);
		}
	}
	if (this.task.duiElement != null){
		var duiElementTextBox = document.getElementById("dui-element-xpath");
		duiElementTextBox.value = this.task.duiElement;
	}
	var duiElementStatic = document.getElementById("dui-element-static");
	duiElementStatic.setAttribute("checked",this.task.duiStaticElement);
};

TaskEditor.prototype.setPreconditions = function(){
	//Esta hecho solo para el del ejemplo,
	// pero por cada procedimiento existente debe crearse el listitem y el listcell
	for (var i = 0;i < this.task.preconditions.length;i++){
		var precondition = this.task.preconditions[i];
		var preconditionList = document.getElementById("preconditions-list");
		
		var preconditionsListItem = document.createElement("listitem");
		preconditionsListItem.setAttribute("id",precondition.getId());

		preconditionsListItem.preconditionInstance = precondition;
		preconditionsListItem.taskInstance = this.task;
		
		var procedureListCell = document.createElement("listcell");
		procedureListCell.setAttribute("label", precondition.getName());
		preconditionsListItem.appendChild(procedureListCell);
	
		procedureListCell = document.createElement("listcell");
		procedureListCell.setAttribute("label", precondition.getAttributesAsString());
		preconditionsListItem.appendChild(procedureListCell);

		preconditionList.appendChild(preconditionsListItem);
	}
};
TaskEditor.prototype.resetPreconditions = function(){
	var preconditionList = document.getElementById("preconditions-list");
	var children = preconditionList.children;
	for (var i = 0;i < children.length;i++){
		if (typeof(children[i].preconditionInstance) != "undefined"){
			children[i].parentNode.removeChild(children[i]);
		}
	}
	var preconditionList = document.getElementById("preconditions-list");
	var children = preconditionList.children;
	for (var i = 0;i < children.length;i++){
		if (typeof(children[i].preconditionInstance) != "undefined"){
			children[i].parentNode.removeChild(children[i]);
		}
	}
	this.setPreconditions();
};

TaskEditor.prototype.init = function(){
	this.setTaskOptions();
	this.setTaskAttributes();
	this.setPreconditions();
};

TaskEditor.prototype.removePrecondition = function(){
	var preconditionList = document.getElementById("preconditions-list");
	var preconditionInstance = preconditionList.selectedItem.preconditionInstance;
	var taskInstance = preconditionList.selectedItem.taskInstance;
	taskInstance.removePrecondition(preconditionInstance);
	
	this.resetPreconditions();
};

TaskEditor.prototype.editPrecondition = function(){
	var preconditionList = document.getElementById("preconditions-list");
	var preconditionInstance = preconditionList.selectedItem.preconditionInstance;
	var newValues = prompt("Enter new attributes values",preconditionInstance.getAttributesAsString())
	var attrs = newValues.split(",");
	var attributes = {};
	for (var i=0;i < attrs.length;i++){
		var attributeValues = attrs[i].split("=");
		if (attributeValues.length > 1)
			attributes[attributeValues[0]] = attributeValues[1];
	}
	preconditionInstance.setAttributes(attributes);
	this.resetPreconditions();
};

TaskEditor.prototype.addNewPrecondition = function(){
	var aMenuItem = document.getElementById('preconditionsAvailables').selectedItem;
	var preconditionName = aMenuItem.getAttribute("value");
	var newPrecondition = eval ("(new " + preconditionName + "('" + "untyped" + "'))");
	newPrecondition.setCSA(CSA);
	this.task.addPrecondition(newPrecondition);
	this.resetPreconditions();
};

TaskEditor.prototype.saveTaskState = function(){
	this.task.setName(document.getElementById("task-name").value);
	var taskAttributes = this.task.getTaskAttributes();	
	if (taskAttributes.length){
		var attributesBox = document.getElementById("attributes");
		for(var i = 0;i < taskAttributes.length;i++){
			var attributeInput = document.getElementById("task-attribute-"+i);
			attributeInput.taskAttribute.setValue(attributeInput.value);
		}
	}
	this.task.setRepetitive(document.getElementById("repetitive-task").checked);
	this.task.setOptional(document.getElementById("optional-task").checked);
	this.task.setAutomatic(document.getElementById("automatic-task").checked);
	this.task.useSetValuesAsConditionsForManualExecution = document.getElementById("use-attributes-for-manual-execution").checked;
	if (this.task.isAutomatic()){
		this.task.setRepetitive(false);
	}
};


