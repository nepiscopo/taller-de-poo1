function AugmentationTaskScriptsManager(){
	this.scripts = {};
	this.artifactScripts = {};
	this.initialize();
};

AugmentationTaskScriptsManager.prototype.initialize = function(){
	var dataCollectors = CSA.getDataCollectors();
	for (var i=0;i < dataCollectors.length;i++){
		var dataCollector = dataCollectors[i];
		var dataCollectorTask = new AugmentationTaskScript(dataCollector.getName(),dataCollector.getId(),dataCollector.getAttributesList(),dataCollector);
		dataCollectorTask.setManager(this);
		this.scripts[dataCollector.getId()] = dataCollectorTask;
		this.artifactScripts[dataCollector.getId()] = dataCollector;
	};	
	var augmenters = CSA.getAugmenters();
	for (var i=0;i < augmenters.length;i++){
		var augmenter = augmenters[i];
		var augmenterTask = new AugmentationTaskScript(augmenter.getName(),augmenter.getId(),augmenter.getAttributesList(),augmenter);
		augmenterTask.setManager(this);
		this.scripts[augmenter.getId()] = augmenterTask;
		this.artifactScripts[augmenter.getId()] = augmenter;
	};
};

AugmentationTaskScriptsManager.prototype.getSupportedTasks = function(){
	return this.scripts;
};

AugmentationTaskScriptsManager.prototype.getArtifactScriptById = function(anId){
	try{return this.artifactScripts[anId]}
	catch(e){return null}
};

AugmentationTaskScriptsManager.prototype.getTaskScriptById = function(anId){
	try{return this.scripts[anId]}
	catch(e){return null;}
};

AugmentationTaskScriptsManager.prototype.removeDataFromPreviousExecution = function(){
	for (each in this.scripts)
		this.scripts[each].removeManualExecutionData();
};
/***************************************/
function AugmentationTaskScript(aName, anId, artifactListOfAttributes, anArtifact){
	this.manager = null;
	this.artifact = null;
	this.name = aName;
	this.id = anId;
	this.scriptAttributes = artifactListOfAttributes;
	if (typeof(anArtifact) != "undefined")
		this.artifact = anArtifact;
};
AugmentationTaskScript.prototype.setManager = function(aManager){
	this.manager = aManager;
}
AugmentationTaskScript.prototype.getName = function(){
	return this.name;
}
AugmentationTaskScript.prototype.getScriptsAttributes = function(){
	return this.scriptAttributes;
}
AugmentationTaskScript.prototype.getId = function(){
	return this.id;
}
AugmentationTaskScript.prototype.getAttribute = function(anAttrinuteName){
	for (var i = 0;i < this.scriptAttributes.length;i++){
		var attribute = this.scriptAttributes[i];
		if (attribute["name"] == anAttrinuteName)
			return attribute;
	}
	return false;
};

AugmentationTaskScript.prototype.setAttributeValue = function(aName,aValue){
	for (var i = 0;i < this.scriptAttributes.length;i++){
		var attribute = this.scriptAttributes[i];
		if (attribute["name"] == aName)
			attribute["value"] = aValue;
	}
	return false;
};
	
AugmentationTaskScript.prototype.isDataNeededAvailable = function(){
	return true;//aca deberiamos prefuntarle al artifact
};

AugmentationTaskScript.prototype.tryAutomaticExecution = function(){
	if(this.isDataNeededAvailable()){
		this.automaticExecution();//Lanzar excepciones y cosas para poder reflejar el estado en el escenario.
		return true;
	}
	return false;
};

AugmentationTaskScript.prototype.getArtifact = function(){
	if (this.artifact == null){
		this.artifact = this.manager.getArtifactScriptById(this.getId());
	}
	return this.artifact;
}

AugmentationTaskScript.prototype.automaticExecution = function(){	
	var dispatcher = CSA.getAugmenterDispatcher();
	dispatcher.dispatchAugmenterAutomatically(this.getArtifact(),this.scriptAttributes);
};

AugmentationTaskScript.prototype.removeManualExecutionData = function(targetEventName){
	var eventNameForDeleting = (typeof(targetEventName) == "undefined")?this.getArtifact().getEventName():targetEventName;
	var eventName = CSA.mainWindow.tasksInExecution[eventNameForDeleting]["__eventForDeletingListener__"];
	try{
		CSA.mainWindow.tasksInExecution[eventNameForDeleting]["__eventAugmentationTaskTarget__"].removeEventListener(eventName,AugmentationTaskScript__callback__,false);
		delete (CSA.mainWindow.tasksInExecution[eventNameForDeleting]);
	}catch(e){
		CSA.mainWindow.removeEventListener(eventName,AugmentationTaskScript__callback__,false);;
	}
};

AugmentationTaskScript.prototype.validateAndUseAttributes = function(aTask, aListOfAttributes){
	var eventAttributes = null;
	var validateValues = aTask.useSetValuesAsConditionsForManualExecution;
	try{
		eventAttributes = eval ("(" + aListOfAttributes + ")");
	}
	catch(e){
		eventAttributes = [{'name':'Concept Name', 'value':'ConferenceData'},{'name':'Target', 'value':'//p[@class="search-box-title-sub"]'}];
	}
	for (var i=0;i < this.scriptAttributes.length;i++){
		var attrName = this.scriptAttributes[i]["name"];
		var attrValue = this.scriptAttributes[i]["value"];
		for (var j=0;j < eventAttributes.length;j++){
			var eAttrName = eventAttributes[j]["name"];
			var eAttrValue = eventAttributes[j]["value"];
			if (eAttrName == attrName){
				this.scriptAttributes[i]["value"] = eAttrValue;
				if ((attrValue == null) || (attrValue == "") || (attrValue == eAttrValue))
					continue;				
				else
					if (validateValues)
						return false;				
			}
		}
	}
	return true; 
};

function AugmentationTaskScript__callback__(e){
	var eventName = e.type;
	var state = CSA.mainWindow.tasksInExecution[eventName]["__StateOfTaskInExecution__"];
	var self = CSA.mainWindow.tasksInExecution[eventName]["__AugmentationScriptListener__"];
	if ((e.target.dispatcherId == self.getId()) && (self.validateAndUseAttributes(state.task, e.target.scriptAttributes))){
  		CSA.mainWindow.tasksInExecution[eventName]["__eventAugmentationTaskTarget__"] = e.target;
		state.task.updateDataFromExecution(self.scriptAttributes);
    	state.removeManualExecutionDataAndContinue(eventName);
    }
};

AugmentationTaskScript.prototype.setEventsManagersForManualExecution = function(aState){
	var eventName = this.getArtifact().getEventName();
	if (!CSA.mainWindow.tasksInExecution) 
		CSA.mainWindow.tasksInExecution = {};
	CSA.mainWindow.tasksInExecution[eventName] = {"__StateOfTaskInExecution__": aState, "__AugmentationScriptListener__": this, "__eventForDeletingListener__": eventName};
	CSA.mainWindow.addEventListener(eventName,AugmentationTaskScript__callback__, false);
};
