function init(){
    return ;
};

function toggleExecutionState(event){
    if (!event.target.label)
        return;
    var attrs = prompt("Attributes:");
    if (attrs != ""){
        event.target.setAttribute("attrs",attrs);
        event.target.label.setAttribute("value",event.target.label.subtask + " (" + attrs + ")");
    }
    else{
        event.target.setAttribute("attrs",event.target.getAttribute("attrs")); 
    }
    if(event.target.getAttribute("executed") == "false"){
        event.target.style.backgroundColor = "#334455";
        event.target.style.color = "white";
        event.target.setAttribute("executed","true");
        event.target.setAttribute("style","background-color:#334455;color:white; padding:3px;border-right:1px solid #333333;border-top:1px solid #333333;border-bottom:1px solid white;border-left:1px solid white;margin:3px;");
    }
    else{
        event.target.setAttribute("style","background-color:#AABBCC; padding:3px;border-right:1px solid white;border-top:1px solid white;border-bottom:1px solid black;border-left:1px solid black;margin:3px;");
        event.target.setAttribute("executed","false");
    }
};

function targetEventListener(event){
    createNewScenarioSubTask(event.getType(), event.getTarget());
};

function targetEventListener2(event){
    createNewScenarioSubTask("Concept Defined:" + event.concept, "Description: " + event.instance);
    stringg = "";
    for (p in event){
        stringg += p + ": " + event[p] + "\n";
    }
};

function testingYule(){
    return;
    YULE.subscribe(targetEventListener, "load");
    YULE.subscribe(targetEventListener, "click");
    YULE.subscribe(targetEventListener, "keydown");
    YULE.subscribe(targetEventListener, "PageNavigationEvent");
    YULE.subscribe(targetEventListener, "LocationBarChangeEvent");
    YULE.subscribe(targetEventListener, "TabCloseEvent");
    YULE.subscribe(targetEventListener, "TabChangeEvent");
    //YULE.subscribe(targetEventListener, "ConceptDefined");    
};

function toggleTaskSelector(selectedKind){
    var primitive = document.getElementById("primitive-tasks-menu");
    var augmentation = document.getElementById("augmentation-tasks-menu");
    var target_menu = document.getElementById(selectedKind.value + "-tasks-menu");
    if (primitive == target_menu){
        primitive.setAttribute("style", "width:250px;display:inline");
        primitive.setAttribute("disabled","false");
        augmentation.setAttribute("disabled","true");
        augmentation.setAttribute("style", "width:250px;display:none");
    }
    else{
        if (augmentation == target_menu){
            augmentation.setAttribute("style", "width:250px;display:inline");
            augmentation.setAttribute("disabled","false");
            primitive.setAttribute("disabled","true");
            primitive.setAttribute("style", "width:250px;display:none");
        }
        else{
            primitive.setAttribute("disabled","true");
            augmentation.setAttribute("disabled","true");
        }
    }
};


/*
TaskManager -> Tasks

ProcedureEditor -> Procedure
Procedure -> Task(attributes, event)
Task superclassOf: PrimitiveTask
Task superclassOf: AugmentationTask
GroupTask -> Task

TaskInProcedure -> TaskState
TaskInProcedure -> Task

TaskState superclassOf: InEditionState
TaskState superclassOf: UnstartedState
TaskState superclassOf: StartedState
TaskState superclassOf: FinishedState
TaskState superclassOf: RecordingState

ProcedurePlayer -*> ProcedureExecution
ProcedureExecution -*> Procedure

Tasks (state, name, type, kind, event)
*/
/********************************************************/
/*                Preconditions                         */
/********************************************************/

function PreconditionsManager(){
    this.preconditionsAvailable = [];
};

//-----------

function AbstractPrecondition(){
    //Se requiere: nombre, id, lista de attributos donde cada elemento es un objeto ("name","type","value","example")
    this.name;
    this.id;
    this.preconditionAttributes = [];
    this.CSA;
};

AbstractPrecondition.prototype.initialize = function(aName, anId, attributes){
    this.name = aName;
    this.id = anId;
    this.preconditionAttributes = attributes;
    this.CSA;
};

AbstractPrecondition.prototype.getId = function(){
    return this.id;
};

AbstractPrecondition.prototype.getName = function(){
    return this.name;
};

AbstractPrecondition.prototype.getAttributesAsString = function(){
    var asString = "";
    for (attribute in this.preconditionAttributes){
        asString += attribute + "=" + this.preconditionAttributes[attribute] + ",";
    }
    return asString;
};

AbstractPrecondition.prototype.setAttributes = function(newAttributes){
    this.preconditionAttributes = newAttributes;
};

AbstractPrecondition.prototype.setCSA = function(aCSA){
    this.CSA = aCSA;
}

AbstractPrecondition.prototype.isSatisfied = function(){
    alert("subclass responsibility");
};

AbstractPrecondition.prototype.getPreconditionAttributeValue = function(anAttrinuteName){
    try{return this.preconditionAttributes[anAttrinuteName]}
    catch(e){return null;}
};

AbstractPrecondition.prototype.getDescription = function(anAttrinuteName){
    return "Subclass responsibility";
};

AbstractPrecondition.prototype.getXML = function(){
    var xml = '\t\t\t\t' + '<precondition id="' + this.getId() + '">\n';
    xml += '\t\t\t\t\t' + '<attributes>\n';
    for (attr in this.preconditionAttributes){
        xml += '\t\t\t\t\t\t' + '<name>' + attr + '</name>\n';
        xml += '\t\t\t\t\t\t' + '<value>' + this.preconditionAttributes[attr] + '</value>\n';
    }
    xml += '\t\t\t\t\t' + '</attributes>\n';
    xml += '\t\t\t\t' + "</precondition>\n";
    return xml;
};

//-----------
function PocketHasInstanceOf(aConceptName){//ESTA
    this.initialize("PocketHasInstanceOf","pocketHasInstanceOf",{"concept":aConceptName});
};
PocketHasInstanceOf.prototype = new AbstractPrecondition();
PocketHasInstanceOf.prototype.isSatisfied = function(){
    var conceptName = this.getPreconditionAttributeValue("concept");
    return this.CSA.pocket.isConceptInSuitCase(conceptName) != false;
};
PocketHasInstanceOf.prototype.getDescription = function(anAttrinuteName){
    return "Waiting for a '" + this.getPreconditionAttributeValue("concept") + "' instance";
};
//-----------
function PocketIsNotEmpty(){//ESTA
    this.initialize("PocketIsNotEmpty","pocketIsNotEmpty",{});
};
PocketIsNotEmpty.prototype = new AbstractPrecondition();
PocketIsNotEmpty.prototype.isSatisfied = function(){
    return this.CSA.pocket.getCantData() > 0;
};
PocketIsNotEmpty.prototype.getDescription = function(anAttrinuteName){
    return "Waiting for something into the Pocket";
};
//-----------
function LastUsedWebApplicationIs(aRegExpURL){//ESTA
    this.initialize("LastUsedWebApplicationIs","lastUsedWebApplicationIs",{"url":aRegExpURL});
};
LastUsedWebApplicationIs.prototype = new AbstractPrecondition();
LastUsedWebApplicationIs.prototype.isSatisfied = function(){
    var regExpURL = this.getPreconditionAttributeValue("url");
    var webAppsInUse = CSA.historyAsArray();
    if (webAppsInUse.length >=2)
        return (webAppsInUse[webAppsInUse.length-2].match(regExpURL) != null);
    return false;    
};
LastUsedWebApplicationIs.prototype.getDescription = function(anAttrinuteName){
    return "Waiting for a previous Web application";
};
//-----------
function WebApplicationInUse(aRegExpURL){//ESTAA
    this.initialize("WebApplicationInUse","webApplicationInUse",{"url":aRegExpURL});
};
WebApplicationInUse.prototype = new AbstractPrecondition();
WebApplicationInUse.prototype.isSatisfied = function(){
    var regExpURL = this.getPreconditionAttributeValue("url");
    var webAppsInUse = CSA.appsInUse();
    for(var i=0;i < webAppsInUse.length;i++)
        if (webAppsInUse[i].match(regExpURL) != null)
            return true;    
    return false;
};
WebApplicationInUse.prototype.getDescription = function(anAttrinuteName){
    return "Waiting for a Web application";
};
//-----------
function CurrentWebApplication(aRegExpURL){
    this.initialize("CurrentWebApplication","currentWebApplication",{"url":aRegExpURL});
};
CurrentWebApplication.prototype = new AbstractPrecondition();
CurrentWebApplication.prototype.isSatisfied = function(){
    var currentURL = CSA.mainWindow.content.document.location.href;
    var regExpURL = this.getPreconditionAttributeValue("url");
    return currentURL.match(regExpURL) != null;
};
CurrentWebApplication.prototype.getDescription = function(anAttrinuteName){
    return "Waiting for displaying a particular Web site";
};
//-----------
function WebApplicationUsed(aRegExpURL){//ESTA
    this.initialize("WebApplicationUsed","webApplicationUsed",{"url":aRegExpURL});
};
WebApplicationUsed.prototype = new AbstractPrecondition();
WebApplicationUsed.prototype.isSatisfied = function(){
    var regExpURL = this.getPreconditionAttributeValue("url");
    var webAppsUsed = CSA.historyAsArray();
    for(var i=0;i < webAppsUsed.length;i++)
        if (webAppsUsed[i].match(regExpURL) != null)
            return true;
    
    return false;
};
WebApplicationUsed.prototype.getDescription = function(anAttrinuteName){
    return "Waiting for a Web application in the navigational history";
};

/*************************************************/
function Procedure(aName,anEditor){
    this.tasks = [];
    this.editor = (typeof(anEditor) != "undefined")?anEditor:null;
    this.name = (typeof(aName) != "undefined")?aName:"Undefined Procedure Name";
    this.state = new ProcedureStoppedState(this);
};

Procedure.prototype.getName = function(){
    return this.name;
};

Procedure.prototype.addTask = function(aTask){
    this.tasks.push(aTask);
    if(this.editor != null)
        this.editor.renderProcedure();
    aTask.setProcedure(this);
    return aTask;
};

Procedure.prototype.removeTask = function(aTask){
    var indexOf = this.tasks.indexOf(aTask);
    if (indexOf > -1){
        this.tasks.splice(indexOf,1);
        return aTask;
    }
    return null;
};

Procedure.prototype.moveUpTask = function(aTask){
    var indexFrom = this.tasks.indexOf(aTask);
    if (indexFrom > 0){
        var indexTo = indexFrom - 1;
        this.tasks.splice(indexTo, 0, this.tasks.splice(indexFrom, 1)[0]);
        return aTask;
    }
    return null;};

Procedure.prototype.moveDownTask = function(aTask){
    var indexFrom = this.tasks.indexOf(aTask);
    if (indexFrom > -1){
        var indexTo = indexFrom + 1;
        this.tasks.splice(indexTo, 0, this.tasks.splice(indexFrom, 1)[0]);
        return aTask;
    }
    return null;
};

Procedure.prototype.getCantTasks = function(){
    return this.tasks.length;
};

Procedure.prototype.getTasks = function(){    
    return this.tasks;
};

Procedure.prototype.setEditor = function(anEditor){
    this.editor = anEditor;
};

Procedure.prototype.getEditor = function(){
    return this.editor;
};

Procedure.prototype.execute = function(){
    CSA.mainWindow.tasksInExecution = {};
    this.state.execute();
};


Procedure.prototype.getNextTaskForBeExecuted = function(){
    var task = this.state.getNextTaskForBeExecuted();
    return task;
};

Procedure.prototype.setState = function(aProcedureState){
    this.state = aProcedureState;
};

Procedure.prototype.isFinished = function(){
    return this.state.isFinished();
};

Procedure.prototype.skipTask = function(){
    return this.state.skipTask();
};

Procedure.prototype.restartExecution = function(){
    this.state = new ProcedureStoppedState(this);
    for (var i = 0;i < this.tasks.length;i++)
        this.tasks[i].restartExecution();
};

Procedure.prototype.tryNewExecutionOfCurrentTask = function(){
    this.state.tryNewExecutionOfCurrentTask();
};

Procedure.prototype.getXML = function(){
    var xml = '<?xml version="1.0" encoding="UTF-8"?>\n';
    //var xml = '<xml version="1.0" encoding="UTF-8">\n';
    xml += '<procedure name="' + this.getName() + '">\n';
    xml += '\t<tasks>\n';
    for (var i=0;i < this.tasks.length;i++){
        xml += this.tasks[i].getXML();
    }
    xml += '\t</tasks>\n';
    xml += '</procedure>\n';
    return xml;
};

Procedure.prototype.__getTaskFromXMLDOM__ = function(taskDOM){
    var taskType = taskDOM.nodeName;
    var newTask;
    var attributes = [];
    var attributesDOMCollection = taskDOM.getElementsByTagName("attribute");
    var attributesDOM = taskDOM.getElementsByTagName("attributes")[0];
    for (var i=0;i < attributesDOMCollection.length;i++){
        var attributeDOM = attributesDOMCollection[i];

        var nameValue = attributeDOM.getElementsByTagName("name")[0].textContent;
        var typeValue = attributeDOM.getElementsByTagName("type")[0].textContent;
        var valueValue = attributeDOM.getElementsByTagName("value")[0].textContent;
        var exampleValue = attributeDOM.getElementsByTagName("example")[0].textContent;
        attributes.push({"name":nameValue,"type":typeValue,"value":valueValue, "example": exampleValue})
    }
    if (taskType.toLowerCase() == "primitivetask")
        newTask = new PrimitiveTask(this, taskDOM.getAttribute("id"), attributes);
    else
        newTask = new AugmentationTask(this, taskDOM.getAttribute("id"), attributes);
    newTask.setName(taskDOM.getAttribute("name"));
    newTask.setRepetitive(taskDOM.getAttribute("repetitive") == "true");
    newTask.setAutomatic(taskDOM.getAttribute("automatic") == "true");
    newTask.setOptional(taskDOM.getAttribute("optional") == "true");
    newTask.useSetValuesAsConditionsForManualExecution = (attributesDOM.getAttribute("usevaluesascondition") == "true")

    var preconditionDOMCollection = taskDOM.getElementsByTagName("precondition");
    for (var i=0;i < preconditionDOMCollection.length;i++){
        var preconditionDOM = preconditionDOMCollection[i];
        

        var preconditionClasses = {"pocketHasInstanceOf":PocketHasInstanceOf,"pocketIsNotEmpty":PocketIsNotEmpty,"webApplicationUsed":WebApplicationUsed,"webApplicationInUse":WebApplicationInUse,"lastUsedWebApplicationIs":LastUsedWebApplicationIs,"currentWebApplication":CurrentWebApplication};
        var preconditionClass = preconditionClasses[preconditionDOM.getAttribute("id")];

        var preconditionAttributesDOMCollection = preconditionDOM.getElementsByTagName("attributes")[0].children;
        var preconditionAttributes = [];
        var preconditionAttrValue;
        for (var j=0;j < preconditionAttributesDOMCollection.length;j++){
            var preconditionAttributeDOM = preconditionAttributesDOMCollection[j];
            var preconditionAttrName = preconditionAttributeDOM.nodeName.toLowerCase();
            preconditionAttrValue = preconditionAttributeDOM.textContent;
            preconditionAttributes.push({preconditionAttrName:preconditionAttrValue});
        }

        var newPrecondition = new preconditionClass(preconditionAttrValue);
        newTask.addPrecondition(newPrecondition);
        newPrecondition.setCSA(CSA);        
    }

    return newTask;
};

Procedure.prototype.buildFromXMLDOM = function(XMLDOM){
    var tasks =  XMLDOM.getElementsByTagName("tasks")[0];
    for (var i=0;i < tasks.children.length;i++){
        var taskDOM = tasks.children[i];
        var task = this.__getTaskFromXMLDOM__(taskDOM);
        this.addTask(task);
    };
    return this;
};
/*************************************************/
function ProcedureState(){
    this.taskInExecution;
    this.procedure;
};

ProcedureState.prototype.getNextTaskForBeExecuted = function(){
    alert("subclass responsibility");
};

ProcedureState.prototype.execute = function(){
    alert("subclass responsibility");
};

ProcedureState.prototype.saveTaskInExecution = function(aTask){
    this.taskInExecution = aTask;
};

ProcedureState.prototype.getStateName = function(){
    return "Procedure State";
};

ProcedureState.prototype.isFinished = function(){
    return false;
};

ProcedureState.prototype.skipTask = function(){
    this.taskInExecution.forceNextState();
};

ProcedureState.prototype.tryNewExecutionOfCurrentTask = function(){
    this.taskInExecution.state = new UnstartedState(this.taskInExecution);
    this.procedure.execute();
};
//----

function ProcedureStoppedState(aProcedure){
    this.taskInExecution = null;
    this.procedure = aProcedure;
};

ProcedureStoppedState.prototype = new ProcedureState();

ProcedureStoppedState.prototype.getStateName = function(){
    return "Procedure Stop State";
};

ProcedureStoppedState.prototype.getNextTaskForBeExecuted = function(){
    if (this.procedure.tasks.length)
        return this.procedure.tasks[0];
    return null;
};

ProcedureStoppedState.prototype.execute = function(){
    var task = this.getNextTaskForBeExecuted();    
    var newState = new ProcedureInExecutionState(this.procedure, task);
    this.procedure.setState(newState);
    var taskExecutionResult = task.execute();
    this.saveTaskInExecution(task);
    return taskExecutionResult;
};
//----

function ProcedureInExecutionState(aProcedure,aTask){
    this.taskInExecution = aTask;
    this.procedure = aProcedure;
};

ProcedureInExecutionState.prototype = new ProcedureState();

ProcedureInExecutionState.prototype.getStateName = function(){
    return "Procedure In Execution State";
};

ProcedureInExecutionState.prototype.getNextTaskForBeExecuted = function(){
    var newTask = null;
    if (this.taskInExecution.isFinished()){
        var index = this.procedure.tasks.indexOf(this.taskInExecution);
        if ((index != -1) && (index < (this.procedure.tasks.length - 1)))
            newTask = this.procedure.tasks[index+1];
    }else{
        newTask = this.taskInExecution;
    };
    return newTask;
};

ProcedureInExecutionState.prototype.execute = function(){

    var task = this.getNextTaskForBeExecuted();
    this.saveTaskInExecution(task);
    if (task == null){
        this.procedure.setState(new ProcedureFinishedState(this.procedure, task));
        this.procedure.getEditor().turnCommandButtonsToFinishedState();
    }
    else{
        return task.execute();
    }
    return null;
};
//----

function ProcedureFinishedState(aProcedure, aTask){
    this.taskInExecution = null;
    this.procedure = aProcedure;
};

ProcedureFinishedState.prototype = new ProcedureState();

ProcedureFinishedState.prototype.getStateName = function(){
    return "Procedure Finished State";
};

ProcedureFinishedState.prototype.getNextTaskForBeExecuted = function(){
    return null;
};

ProcedureFinishedState.prototype.execute = function(){
    return "finished";
};

ProcedureFinishedState.prototype.isFinished = function(){
    return true;
};
/*************************************************/

function Task(){
    this.events = [];
    this.name = "Undefined Task";
    this.task_id;
    this.URL;
    this.state = null;
    this.taskAttributes = [];
    this.preconditions = [];
    this.repetitive = false;
    this.optional = false;
    this.automatic = false;
    this.procedure;
    this.useSetValuesAsConditionsForManualExecution = false;
    this.duiElement = null;
    this.duiStaticElement = true;
    this.duiElementStyle = null;
};

Task.prototype.initialize = function(){
    this.state = new UnstartedState(this);
    this.taskAttributes = [];
    this.repetitive = false;
    this.optional = false;
    this.automatic = false;
    this.preconditions =[];
};

Task.prototype.setProcedure = function(aProcedure){
    this.procedure = aProcedure;
};

Task.prototype.getProcedure = function(aProcedure){
    return this.procedure;
};

Task.prototype.isRepetitive = function(){
    return this.repetitive;
};

Task.prototype.setRepetitive = function(aBoolean){
    this.repetitive = aBoolean == true;
};

Task.prototype.isOptional = function(){
    return this.optional;
};

Task.prototype.setOptional = function(aBoolean){
    this.optional = aBoolean == true;
};

Task.prototype.isAutomatic = function(){
    return this.automatic;
};

Task.prototype.setAutomatic = function(aBoolean){
    this.automatic = aBoolean == true;
};

Task.prototype.setDuiElement = function(duiElement){
    this.duiElement = duiElement;
    this.refreshGUI();
};

Task.prototype.preconditionsSatisfied = function(){
    for (var i = 0;i < this.preconditions.length;i++)
        if (!(this.preconditions[i].isSatisfied()))
            return false;
    return true;
}

Task.prototype.addPrecondition = function(newPrecondition){
    this.preconditions.push(newPrecondition);
}

Task.prototype.removePrecondition = function(aPrecondition){
    var indexOf = this.preconditions.indexOf(aPrecondition);
    if (indexOf > -1){
        this.preconditions.splice(indexOf,1);
        return aPrecondition;
    }
    return null;
};

Task.prototype.addAttribute = function(anAttribute){
    this.taskAttributes.push(anAttribute);
    return anAttribute;
};

Task.prototype.removeAttribute = function(aAttribute){
    return;
};

Task.prototype.renderMenuTask = function(anEditor){
    alert("Task.renderMenuTask: Subclass responsibility");
};

Task.prototype.getName = function(){
    return this.name;
};

Task.prototype.setName = function(aName){
    this.name = aName;
};

Task.prototype.setState = function(aState){
    this.state = aState;
};

Task.prototype.getId = function(){
    return this.id;
};

Task.prototype.setId = function(anId){
    this.id = anId;
};

Task.prototype.getType = function(){
    return "Abstract Task";
};

Task.prototype.getDescription = function(){
    return this.getType() + " Task";
};

Task.prototype.clone = function(){
    var clonedTask = this.specificClone();
    if (this.getTaskAttributes().length){
        var clonedAttributes = this.cloneAttributes();
        clonedTask.taskAttributes = clonedAttributes;
    }
    return clonedTask;
};

Task.prototype.cloneAttributes = function(){
    var clonedAttributes = [];
    for (var i=0;i < this.taskAttributes.length;i++){
        var attribute = this.taskAttributes[i];
        var clonedAttribute = attribute.clone();
        clonedAttributes.push(clonedAttribute);
    }
    return clonedAttributes;
};

Task.prototype.buildAttributes = function(taskAttributes){
    for (var i=0;i < taskAttributes.length;i++){
        var name = taskAttributes[i]["name"];
        var type = taskAttributes[i]["type"];
        var value = taskAttributes[i]["value"];
        var example = taskAttributes[i]["example"];
        var newAttribute = new TaskAttribute(name, type, value, example);
        this.addAttribute(newAttribute);
    }
};

Task.prototype.updateValueAttributes = function(taskAttributes){
    for (var i=0;i < taskAttributes.length;i++){
        var attribute = this.getScriptAttributeByName(taskAttributes[i]["name"]);
        if (attribute)
            attribute.setValue(taskAttributes[i]["value"]);
    }
};


Task.prototype.updateDataFromExecution = function(taskAttributes){
    this.taskAttributes = [];
    //this.updateValueAttributes(taskAttributes);
    this.buildAttributes(taskAttributes);
};    

Task.prototype.getTaskAttributes = function(){
    return this.taskAttributes;
};

Task.prototype.refreshGUI = function(){
    this.procedure.getEditor().refreshTaskGUI(this);
};

var __flashHighlightTimer__;
var __flashHighlightTask__;
function __flashHighlightTimerFunction__(){
    __flashHighlightTask__.gui.style.color = "black";
    window.clearInterval(__flashHighlightTimer__);
};

Task.prototype.flashHighlight = function(){
    this.refreshGUI();
    this.gui.style.color = "white";
};

Task.prototype.isFinished = function(){
    return this.state.isFinished();
};

Task.prototype.isNotStarted = function(){
    return this.state.isNotStarted();
};

Task.prototype.wasExecutedAtLeastOnce = function(){
    return this.state.wasExecutedAtLeastOnce();
};

Task.prototype.forceNextState = function(){
    return this.state.forceNextState();
};

Task.prototype.isInExecution = function(){
    return this.state.isInExecution();
};

Task.prototype.dispatchTaskFinishEvent = function(){
    var evt = document.createEvent("Event");
    evt.initEvent("taskfinished", true, true);
    this.gui.dispatchEvent(evt);
};

Task.prototype.setNextState = function(){
    this.state.setNextState();
    if (this.isFinished()){        
        this.dispatchTaskFinishEvent();
    }        
    else
        this.execute();
};

Task.prototype.restartExecution = function(){
    try{
        this.getScript().removeManualExecutionData();
    }catch(e){;}
    this.state = new UnstartedState(this);
};

Task.prototype.execute = function(){
    this.state.execute();
};

Task.prototype.focusUI = function(){
    this.state.execute();
};

Task.prototype.stopTaskExecutionWithError = function(anException){
    this.state = new ErrorState(this.task,anException);
    this.refreshGUI();
};

Task.prototype.getStateDescription = function(){
    return this.state.getDescription();
};

function AugmenterDoesNotExistException(message) {
   this.message = message;
   this.name = "AugmenterDoesNotExistException";
};
AugmenterDoesNotExistException.prototype.toString = function(){
    return this.message;
};

Task.prototype.setScriptAttributesValues = function(){
    try{
        for (var i =0;i < this.taskAttributes.length;i++){
            var attribute = this.taskAttributes[i];
            this.getScript().setAttributeValue(attribute.getName(),attribute.getValue());
        }
    }
    catch(e){
        var augmenterDoesNotExist = new AugmenterDoesNotExistException("The target augmenter does not seem to be installed.")
        this.stopTaskExecutionWithError(augmenterDoesNotExist);
    }
};

Task.prototype.getScriptAttributeByName = function(aName){
    for (var i =0;i < this.taskAttributes.length;i++){
        var attribute = this.taskAttributes[i];
        if (attribute.getName() == aName)
            return attribute;
    }
    return null;
};

Task.prototype.getXML = function(){
    var xml = "\t\t" + this.getXMLHeader();
    if (this.taskAttributes.length){
        xml += "\t\t\t" + '<attributes usevaluesascondition="' + this.useSetValuesAsConditionsForManualExecution + '">\n';
        for (var i=0;i < this.taskAttributes.length;i++){
            xml += this.taskAttributes[i].getXML();
        }
        xml += "\t\t\t</attributes>\n";
    }
    if (this.preconditions.length){
        xml += "\t\t\t" +"<preconditions>\n";
        for (var i=0;i < this.preconditions.length;i++){
            xml += this.preconditions[i].getXML();
        }
        xml += "\t\t\t</preconditions>\n";
    }
    xml += "\t\t" + this.getXMLFooter();

    return xml;
};

Task.prototype.focusUI = function(){
    this.gui.focus();
    this.gui.focusableSubelement.focus();
    //this.gui.parent.scrollTop = this.gui.parent.scrollHeight;
    //this.gui.parent.parent.scrollTop = this.gui.parent.parent.scrollHeight;
};

//-------------
function PrimitiveTask(name,id,jsonTaskAttributes){
    this.name = (typeof(name) != "undefined")?name:"Undefined Primitive Task";
    this.id = id;
    this.state = new UnstartedState(this);
    this.initialize();
    if (typeof(jsonTaskAttributes) != "undefined"){
        this.buildAttributes(jsonTaskAttributes);
    }
};

PrimitiveTask.prototype = new Task();

PrimitiveTask.prototype.getScript = function(){
    return primitiveTasksScriptManager.getTaskScriptById(this.id);
};

PrimitiveTask.prototype.specificClone = function(){
    var cloned = new PrimitiveTask(this.name,this.id);
    cloned.taskAttributes = [];
    return cloned;
};

PrimitiveTask.prototype.getType = function(){
    return "Primitive";
};

PrimitiveTask.prototype.renderMenuTask = function(anEditor){
    anEditor.renderPrimitiveTaskMenuForEdition(this);
};

PrimitiveTask.prototype.getXMLHeader = function(){
    var xml = '<primitivetask name="' + this.getName() + '" id="' + this.getId() + '" repetitive="' + this.isRepetitive() + '" automatic="' + this.isAutomatic() + '" optional="' + this.isOptional() + '">\n';
    return xml;
};

PrimitiveTask.prototype.getXMLFooter = function(){
    var xml = '</primitivetask>\n';
    return xml;
};
//-------------
function AugmentationTask(name,id,jsonTaskAttributes){
    this.name = (typeof(name) != "undefined")?name:"Undefined Augmentation Task";
    this.id = id;
    this.state = new UnstartedState(this);
    this.initialize();
    if (typeof(jsonTaskAttributes) != "undefined"){
        this.buildAttributes(jsonTaskAttributes);
    }
};

AugmentationTask.prototype = new Task();

AugmentationTask.prototype.getScript = function(){
    return augmentationTasksScriptManager.getTaskScriptById(this.id);
};

AugmentationTask.prototype.getType = function(){
    return "Augmentation"
};

AugmentationTask.prototype.specificClone = function(){
    var cloned = new AugmentationTask(this.name,this.id);
    cloned.taskAttributes = [];
    return cloned;
};

AugmentationTask.prototype.renderMenuTask = function(anEditor){
    anEditor.renderAugmentationTaskMenuForEdition(this);
};

AugmentationTask.prototype.getXMLHeader = function(){
    var xml = '<augmentationtask name="' + this.getName() + '" id="' + this.getId() + '" repetitive="' + this.isRepetitive() + '" automatic="' + this.isAutomatic() + '" optional="' + this.isOptional() + '">\n';
    return xml;
};

AugmentationTask.prototype.getXMLFooter = function(){
    var xml = '</augmentationtask>\n';
    return xml;
};
/***************************/

//-------------
function TaskAttribute(name, type, value, example){
    this.name = (typeof(name) != "undefined")?name:"Undefined name";
    this.type = (typeof(type) != "undefined")?type:"Undefined type";
    this.value = (typeof(value) != "undefined")?value:"";
    this.valueExample = (typeof(example) != "undefined")?example:"Undefined value example";
};

TaskAttribute.prototype.isValueValid = function(aValue){
    return true;
};

TaskAttribute.prototype.setValue = function(aValue){
    this.value = aValue;
};

TaskAttribute.prototype.clone = function(){
    return new TaskAttribute(this.name, this.type, this.value, this.valueExample);
};

TaskAttribute.prototype.getName = function(){
    return this.name;
};

TaskAttribute.prototype.getType = function(){
    return this.type;
};

TaskAttribute.prototype.getValue = function(){
    return this.value;
};

TaskAttribute.prototype.getExample = function(){
    return this.valueExample;
};

TaskAttribute.prototype.setName = function(aName){
    this.name = aName;
};

TaskAttribute.prototype.setType = function(aType){
    this.type = aType;
};

TaskAttribute.prototype.setValue = function(aValue){
    this.value = aValue;
};

TaskAttribute.prototype.setExample = function(aValueExample){
    this.valueExample = aValueExample;
};

TaskAttribute.prototype.getXML = function(){
    var xml = '\t\t\t\t' + '<attribute id="' + this.getName() + '">\n';//Aca deberia ir el id del atributo y no el nombre
    xml += '\t\t\t\t\t' + '<name>' + this.getName() + '</name>\n';
    xml += '\t\t\t\t\t' + '<type>' + this.getType() + '</type>\n';
    xml += '\t\t\t\t\t' + '<value>' + this.getValue() + '</value>\n';
    xml += '\t\t\t\t\t' + '<example>' + this.getExample() + '</example>\n';
    xml += '\t\t\t\t' + "</attribute>\n"
    return xml;
};
//-------------

/***********************/

function TaskState(){
    this.task = null;
};

TaskState.prototype.initialize = function(aTask){
    this.task = aTask;
};

TaskState.prototype.isFinished = function(){
    return false;
};

TaskState.prototype.isNotStarted = function(){
    return false;
};

TaskState.prototype.wasExecutedAtLeastOnce = function(){
    return false;
};

TaskState.prototype.isInExecution = function(){
    return false;
};

TaskState.prototype.setNextState = function(){
    alert("subclass responsibility");
};

TaskState.prototype.execute = function(){
    if (this.mustBeExecuted()){
        if (this.task.preconditionsSatisfied()){
            if (!this.task.isAutomatic()){
                this.task.setScriptAttributesValues();
                this.startManualExecution();
            }
            else{
                this.task.setScriptAttributesValues();
                var result = this.executeTaskScript();
                if (!result){
                    this.startManualExecution();       
                }
            }
        }
        else{
            this.task.procedure.editor.waitingForPreconditions(this.task);
        }
    }
    return "Task Execution State: " + this.task.getStateDescription();
};

TaskState.prototype.getDescription = function(){
    return "subclass responsibility";
};

TaskState.prototype.mustBeExecuted = function(){
    return false;
};

TaskState.prototype.executeTaskScript = function(){
    alert("subclass responsibility") ;
};

TaskState.prototype.startManualExecution = function(){
    alert("subclass responsibility");
};

TaskState.prototype.forceNextState = function(){
    this.performTransition();
};
TaskState.prototype.removeManualExecutionDataAndContinue = function(eventName){
    this.task.getScript().removeManualExecutionData(eventName);
    this.performTransition();
};
TaskState.prototype.performTransition = function(){
    this.task.setNextState();
    this.task.refreshGUI();
    this.task.focusUI();
    this.task.execute();
};
TaskState.prototype.stoppedWithError = function(){
    return false;
};
//------
function UnstartedState(aTask){
    this.initialize(aTask);
};
UnstartedState.prototype = new TaskState();
UnstartedState.prototype.isFinished = function(){
    return false;
};
UnstartedState.prototype.isNotStarted = function(){
    return true;
};
UnstartedState.prototype.setNextState = function(){
    this.task.state = new StartedState(this.task);
};
UnstartedState.prototype.getDescription = function(){
    return "Not started";
};
UnstartedState.prototype.mustBeExecuted = function(){
    return true;
};
UnstartedState.prototype.startManualExecution = function(){
    this.performTransition();
};
UnstartedState.prototype.executeTaskScript = function(){
    this.performTransition();
};
UnstartedState.prototype.execute = function(){
    this.startManualExecution();
};

//------
function RecordingState(aTask){
    this.initialize(aTask);
};
RecordingState.prototype = new TaskState();
RecordingState.prototype.isFinished = function(){
    return false;
};
RecordingState.prototype.setNextState = function(){
    return;
};
RecordingState.prototype.getDescription = function(){
    return "Not started";
};
RecordingState.prototype.mustBeExecuted = function(){
    return true;
};
RecordingState.prototype.startManualExecution = function(){
    var script = this.task.getScript();
    script.setEventsManagersForManualExecution(this);
};

RecordingState.prototype.executeTaskScript = function(){
    this.performTransition();
};
RecordingState.prototype.removeManualExecutionDataAndContinue = function(eventName){
    var procedure = this.task.getProcedure();
    var clonedTask = this.task.clone();
    procedure.addTask(clonedTask);
    return;
};
//------
function StartedState(aTask){
    this.initialize(aTask);
};
StartedState.prototype = new TaskState();
StartedState.prototype.isFinished = function(){
    return false;
};
StartedState.prototype.isInExecution = function(){
    return true;
};
StartedState.prototype.setNextState = function(){
    if (this.task.isRepetitive())
        this.task.state = new InExecutionLoopState(this.task);
    else
        this.task.state = new FinishedState(this.task);
};
StartedState.prototype.getDescription = function(){
    return "Executing";
};
StartedState.prototype.mustBeExecuted = function(){
    return true;
};
StartedState.prototype.executeTaskScript = function(){
    var script = this.task.getScript();
    try{
        var result = script.tryAutomaticExecution();
        if (result){
            this.performTransition();
            return true;
        }
        else
            return false;
    }catch(e){
        this.task.state = new ErrorState(this.task,e);
        this.task.refreshGUI();
    };
    return false;
};
StartedState.prototype.startManualExecution = function(){
    var script = this.task.getScript();
    script.setEventsManagersForManualExecution(this);
};
StartedState.prototype.forceNextState = function(){
    this.task.state = new FinishedState(this.task);
    this.task.refreshGUI();
};
//------
function InExecutionLoopState(aTask){
    this.initialize(aTask);
};
InExecutionLoopState.prototype = new StartedState();
InExecutionLoopState.prototype.setNextState = function(){
    return;
};
InExecutionLoopState.prototype.wasExecutedAtLeastOnce = function(){
    return true;
};

//------
function ErrorState(aTask,e){
    this.initialize(aTask);
    this.errorMessage = e;
    //this.retryExecution();
};
ErrorState.prototype = new TaskState();
ErrorState.prototype.isFinished = function(){
    return false;
};
ErrorState.prototype.setNextState = function(){
    return this;
};
ErrorState.prototype.forceNextState = function(){
    this.task.state = new FinishedState(this.task);
    this.task.refreshGUI();
};
ErrorState.prototype.getDescription = function(){
    return "Error: " + this.errorMessage;
};
ErrorState.prototype.stoppedWithError = function(){
    return true;
};
ErrorState.prototype.retryExecution = function(){
    if ((typeof(CSA.mainWindow.__InErrorTask__) == "undefined") || CSA.mainWindow.__InErrorTask__ == null){
        CSA.mainWindow.__InErrorTask__ = this.task;
        CSA.mainWindow.__InErrorTaskInterval__ = CSA.mainWindow.setInterval(this.retryExecution,5000);
    }
    else{
        if(CSA.mainWindow.__InErrorTask__.isFinished()){
            CSA.mainWindow.__InErrorTask__ = null;
            CSA.mainWindow.clearInterval(CSA.mainWindow.__InErrorTaskInterval__);
        }
        else{
            CSA.mainWindow.__InErrorTask__.state = new StartedState(CSA.mainWindow.__InErrorTask__);
            CSA.mainWindow.__InErrorTask__.execute();
        }
    };
};

//------
function FinishedState(aTask){
    this.initialize(aTask);
};
FinishedState.prototype = new TaskState();
FinishedState.prototype.isFinished = function(){
    return true;
};
FinishedState.prototype.wasExecutedAtLeastOnce = function(){
    return true;
};
FinishedState.prototype.setNextState = function(){
    return this;
};
FinishedState.prototype.getDescription = function(){
    return "Finished";
};

/*************************************************/
//              ProcedureEditor
/*************************************************/
function myself(){
    if (((typeof(procedureEditorInstance) == "undefined") || (procedureEditorInstance) == null))
        return procedurePlayerInstance;
    return procedureEditorInstance;
};

function ProcedureEditor(aProcedure){
    this.procedure = (typeof(aProcedure) != "undefined")?aProcedure:(new Procedure("Undefined Procedure",this));
    this.procedure.setEditor(this);
    this.availableTasks = [];
    this.loadAvailableTasks();
};

ProcedureEditor.prototype.self = function(){
    return procedureEditorInstance;
};

ProcedureEditor.prototype.recordUserInteraction = function(aProcedure){
    var recordingButton = document.getElementById("recording-button");
    var stopRecordingButton = document.getElementById("stop-recording-button");
    recordingButton.setAttribute("hidden","true");
    stopRecordingButton.setAttribute("hidden","false");

    var tasks = this.getAvailableAllTasks();
    for (var i = tasks.length - 1; i >= 0; i--) {
        tasks[i].setProcedure(aProcedure);
        tasks[i].setState(new RecordingState(tasks[i]));
        tasks[i].execute();
    };
};

ProcedureEditor.prototype.stopRecordingUserInteraction = function(aProcedure){
    var recordingButton = document.getElementById("recording-button");
    var stopRecordingButton = document.getElementById("stop-recording-button");
    recordingButton.setAttribute("hidden","false");
    stopRecordingButton.setAttribute("hidden","true");
    var tasks = this.getAvailableAllTasks();
    for (var i = 0; i < tasks.length; i++){
        try{
            tasks[i].getScript().removeManualExecutionData();
        }catch(e){;}
    };
};

ProcedureEditor.prototype.getProcedure = function(){
    return this.procedure;
};

ProcedureEditor.prototype.setProcedure = function(aProcedure){
    this.procedure = aProcedure;
};

ProcedureEditor.prototype.addTask = function(aTask){
    this.procedure.addTask(aTask);
};

ProcedureEditor.prototype.getAvailableAllTasks = function(){
    var allTasks = [];
    var primitive = this.getAvailablePrimitiveTasks();
    
    for (var i = primitive.length - 1; i >= 0; i--) {
        allTasks.push(primitive[i]);
    };
    var augmentation = this.getAvailableAugmentationTasks();
    for (var i = augmentation.length - 1; i >= 0; i--) {
        allTasks.push(augmentation[i]);
    };
    return allTasks;
};

/* Este metodo deberia construirse a partir de los artefactos instalados localmente (Web augmentation tasks)
 * las tareas nativas contempladas. Las definiciones deben especificar: nombre, atributos, etc.
 */
ProcedureEditor.prototype.getAvailablePrimitiveTasks = function(){
    var primitiveTasks = [];

    try{
        var supportedTasks = primitiveTasksScriptManager.getSupportedTasks();
        for (taskScript in supportedTasks){
            var primitiveTask = supportedTasks[taskScript];
            primitiveTasks.push(new PrimitiveTask(primitiveTask.getName(),taskScript,primitiveTask.getScriptsAttributes()));
        }
    }
    catch(e){;}
    return primitiveTasks;
};

ProcedureEditor.prototype.getAvailableAugmentationTasks = function(){
    var augmentationTasks = [];

    try{
        var supportedTasks = augmentationTasksScriptManager.getSupportedTasks();
        for (taskScriptId in supportedTasks){
            var augmentationTaskScript = supportedTasks[taskScriptId];
            augmentationTasks.push(new AugmentationTask(augmentationTaskScript.getName(),taskScriptId,augmentationTaskScript.getScriptsAttributes()));
        }
    }
    catch(e){;}
    return augmentationTasks;
};

ProcedureEditor.prototype.setAvailableTasks = function(){
    var primitiveTasks = this.getAvailablePrimitiveTasks(); 
    for (var i = 0;i < primitiveTasks.length;i++)
        this.availableTasks.push(primitiveTasks[i]);
    
    var augmentationTasks = this.getAvailableAugmentationTasks(); 
    for (var i = 0;i < augmentationTasks.length;i++)
        this.availableTasks.push(augmentationTasks[i]);
    
};

ProcedureEditor.prototype.renderTasks = function(){
    var tasks = this.availableTasks;
    for (var i = 0;i < tasks.length;i++){
        var task = tasks[i];
        task.renderMenuTask(this);
    }
}

ProcedureEditor.prototype.loadAvailableTasks = function(){
    this.setAvailableTasks();
};

ProcedureEditor.prototype.addTaskMenuItemToMenu = function(aTask, aMenu){
    var newitem = document.createElement("menuitem");
    newitem.setAttribute("label",aTask.getName());
    newitem.setAttribute("id",aTask.getId());
    newitem.task = aTask;
    aMenu.appendChild(newitem);
};

ProcedureEditor.prototype.renderPrimitiveTaskMenuForEdition = function(task){
    var primitiveMenu = document.getElementById("primitive-tasks-menupopup");
    this.addTaskMenuItemToMenu(task,primitiveMenu);
};

ProcedureEditor.prototype.renderAugmentationTaskMenuForEdition = function(task){
    var augmentationMenu = document.getElementById("augmentation-tasks-menupopup");
    this.addTaskMenuItemToMenu(task,augmentationMenu);
};

ProcedureEditor.prototype.editTask = function(event){
    var self = myself();
    var aTask = event.target.task;
    var window_path = "chrome://cs-adaptation/content/scenarioRecorderSideBar/windows/taskEditor.xul"; 
    window.openDialog(window_path, "","chrome, dialog, modal, resizable=yes",aTask,self,CSA).focus();
    aTask.flashHighlight();
    __flashHighlightTask__ = aTask;
    __flashHighlightTimer__ = setInterval(__flashHighlightTimerFunction__, 1000);
};

ProcedureEditor.prototype.moveUpTask = function(event){
    var self = myself();//procedureEditorInstance;;
    var aTask = event.target.task;
    self.procedure.moveUpTask(aTask);
    self.renderProcedure();
};

ProcedureEditor.prototype.moveDownTask = function(event){
    var self = myself();//procedureEditorInstance;;
    var aTask = event.target.task;
    self.procedure.moveDownTask(aTask);
    self.renderProcedure();
};

ProcedureEditor.prototype.removeTaskFromProcedure = function(event){
    self = myself();//procedureEditorInstance;;
    var aTask = event.target.task;
    self.removeTask(aTask);
};

ProcedureEditor.prototype.removeTask = function(aTask){
    this.procedure.removeTask(aTask);
    this.renderProcedure();
};

ProcedureEditor.prototype.cleanProcedureViewer = function(){
    var container = document.getElementById("event-description-container");
    var children = container.children;
    while (children.length > 0){
        for (var i=0;i < children.length;i++){
            var child = children[i];
            container.removeChild(child);
        };
        children = container.children;
    };
};

ProcedureEditor.prototype.clone = function(){
    return new ProcedureEditor(this.getProcedure());
};

ProcedureEditor.prototype.class = function(){
    return "ProcedureEditor";
};

ProcedureEditor.prototype.renderProcedure = function(){
    this.cleanProcedureViewer();
    var tasks = this.procedure.getTasks();
//    for (var i = tasks.length - 1; i >= 0; i--) {
//        this.renderTask(tasks[i]);
//    };
    for (var i=0;i < tasks.length;i++)
        this.renderTask(tasks[i]);
};

ProcedureEditor.prototype.addNewTask = function(fromMenu){
    var selectedItem = fromMenu.selectedItem;
    var task = selectedItem.task
    this.procedure.addTask(task.clone());
};

ProcedureEditor.prototype.__renderTaskInVBox__ = function(aTask,task_vbox){
    task_vbox.setAttribute("style","background-color:#AABBCC;padding:3px;border-right:1px solid white;border-top:1px solid white;border-bottom:1px solid black;border-left:1px solid black;margin:3px;width:150px;margin-right;hight:auto;");
    var label_hbox = document.createElement("hbox");
    aTask.gui = task_vbox;
    var label_event = document.createElement("label");
    label_event.setAttribute("style","font-weight:bold;");
    var label_description = document.createElement("label");
    label_event.setAttribute("value",aTask.getName());    
    label_event.task = aTask;
    label_event.setAttribute("id", aTask.getId());
    label_hbox.appendChild(label_event);

    var edit_button = document.createElement("label");
    edit_button.setAttribute("style","background-color:#CCCCCC;padding:1px;");
    edit_button.setAttribute("value","Edit");
    edit_button.setAttribute("tooltiptext"," Click here to edit this task ");
    edit_button.task = aTask;
    edit_button.setAttribute("id", aTask.getId());
    edit_button.addEventListener("click",this.self().editTask,false);
    label_hbox.appendChild(edit_button);

    var remove_button = document.createElement("label");
    remove_button.setAttribute("style","background-color:#CCCCCC;padding:1px;");
    remove_button.setAttribute("value","Remove");
    remove_button.setAttribute("tooltiptext"," Click here to remove this task ");
    remove_button.task = aTask;
    remove_button.setAttribute("id", aTask.getId());
    remove_button.addEventListener("click",this.self().removeTaskFromProcedure,false);
    label_hbox.appendChild(remove_button);

    var up_button = document.createElement("label");
    up_button.setAttribute("style","background-color:#CCCCCC;padding:1px;");
    up_button.setAttribute("value","Move Up");
    up_button.setAttribute("tooltiptext"," Click here to move up this task ");
    up_button.task = aTask;
    up_button.setAttribute("id", aTask.getId());
    up_button.addEventListener("click",this.self().moveUpTask,false);
    label_hbox.appendChild(up_button);

    var down_button = document.createElement("label");
    down_button.setAttribute("style","background-color:#CCCCCC;padding:1px;");
    down_button.setAttribute("value","Move Down");
    down_button.setAttribute("tooltiptext"," Click here to move down this task ");
    down_button.task = aTask;
    down_button.addEventListener("click",this.self().moveDownTask,false);
    label_hbox.appendChild(down_button);

    var description_container = document.createElement("hbox");
    label_description.setAttribute("value",aTask.getDescription() + " with " + aTask.getTaskAttributes().length + " attributes");
    
    var attributes_names_str = "";
    for (var i = 0;i < aTask.getTaskAttributes().length;i++){
        attributes_names_str += aTask.getTaskAttributes()[i].getName();
        if (i < (aTask.getTaskAttributes().length-1))
            attributes_names_str += ", ";
    }
    description_container.setAttribute("style","border-top:1px solid white");

    description_container.setAttribute("tooltiptext"," This is the list of attributes for this task: " + attributes_names_str);
    description_container.appendChild(label_description);

    if(aTask.isRepetitive() == true){
        var repetitive_flag = document.createElement("label");
        repetitive_flag.setAttribute("style","color:#EEEEEE; background-color:#999999");
        repetitive_flag.setAttribute("value"," R ");
        repetitive_flag.setAttribute("tooltiptext"," This task has been marked as repetitive ");
        description_container.appendChild(repetitive_flag);
    }
    if(aTask.isOptional() == true){
        var optional_flag = document.createElement("label");
        optional_flag.setAttribute("style","color:#EEEEEE; background-color:#999999");
        optional_flag.setAttribute("value"," O ");
        optional_flag.setAttribute("tooltiptext"," This task has been marked as optional ");
        description_container.appendChild(optional_flag);
    }
    if(aTask.isAutomatic() == true){
        var automatic_flag = document.createElement("label");
        automatic_flag.setAttribute("style","color:#EEEEEE; background-color:#999999");
        automatic_flag.setAttribute("value"," A ");
        automatic_flag.setAttribute("tooltiptext"," This task has been marked as automatic ");
        description_container.appendChild(automatic_flag);
    }

    task_vbox.appendChild(label_hbox);
    task_vbox.label = label_event;    
    task_vbox.appendChild(description_container);
    task_vbox.description = label_description;
    task_vbox.setAttribute("id", aTask.getId());  
    task_vbox.focusableSubelement = label_event;

    if(aTask.duiElement != null){
        var dui_element_container = document.createElement("hbox");
        dui_element_container.setAttribute("style","border-top:1px solid white");
        task_vbox.appendChild(dui_element_container);
        dui_element_container.innerHTML = "<html:div id='" + aTask.getId() + "-dui-container" + "' style='background-color:#FFFFFF;width:auto;height:auto;'></html:div>";

        var dui_div = document.getElementById(aTask.getId()+'-dui-container');
        var parser = new DOMParser();
        var dom_DUI = parser.parseFromString(aTask.duiElement, "text/html");

        var anchors = dom_DUI.documentElement.getElementsByTagName("a");
        for (var i = anchors.length - 1; i >= 0; i--)
            anchors[i].setAttribute("target","_content");

        var forms = dom_DUI.documentElement.getElementsByTagName("form");
        for (var i = forms.length - 1; i >= 0; i--)
            forms[i].setAttribute("target","_content");
        dui_div.appendChild(dom_DUI.documentElement);          
    };
};

ProcedureEditor.prototype.refreshTaskGUI = function(aTask){
    var task_vbox = aTask.gui;
    var children = task_vbox.children;
    while (children.length > 0){
        for (var i=0;i < children.length;i++){
            var child = children[i];            
            task_vbox.removeChild(child);
        };
        children = task_vbox.children;
    };
    this.__renderTaskInVBox__(aTask, task_vbox);
    aTask.gui.focus();
};

ProcedureEditor.prototype.renderTask = function(aTask){
    var task_vbox = document.createElement("vbox");
    var container = document.getElementById("event-description-container");
    container.appendChild(task_vbox);    
    this.__renderTaskInVBox__(aTask, task_vbox);
    aTask.gui.focus();
};

ProcedureEditor.prototype.exportProcedure = function(){
    var xml = this.procedure.getXML();
    Components.utils.import("resource://gre/modules/NetUtil.jsm");
    Components.utils.import("resource://gre/modules/FileUtils.jsm");

    var file = Components.classes["@mozilla.org/file/directory_service;1"].
               getService(Components.interfaces.nsIProperties).
               get("TmpD", Components.interfaces.nsIFile);
    file.append("procedure"+ this.procedure.getName() +".xml");
    file.createUnique(Components.interfaces.nsIFile.NORMAL_FILE_TYPE, 0777);

    ostream = FileUtils.openSafeFileOutputStream(file);
    converter = Components.classes["@mozilla.org/intl/scriptableunicodeconverter"].createInstance(Components.interfaces.nsIScriptableUnicodeConverter);
    converter.charset = "UTF-8";

    var istream = converter.convertToInputStream(xml);
    NetUtil.asyncCopy(istream,ostream, function(status){
        if (!Components.isSuccessCode(status)){
            return;
        }
    });
    CSA.mainWindow.open("file://" + file.path);

};
/*************************************************/
//             Player
/*************************************************/

function ProcedurePlayer(aProcedure){
    this.procedure = (typeof(aProcedure) != "undefined")?aProcedure:(new Procedure("Undefined Procedure",this));
    this.procedure.setEditor(this);
    this.availableTasks = [];
    //this.loadAvailableTasks();
};

ProcedurePlayer.prototype = new ProcedureEditor();

ProcedurePlayer.prototype.self = function(){
    return procedurePlayerInstance;
};

ProcedurePlayer.prototype.toggleCommandButtons = function(){
    var start_button = document.getElementById("start-procedure-command");
    var pause_button = document.getElementById("pause-procedure-command");
    start_button.disabled = !start_button.disabled;
    pause_button.disabled = !pause_button.disabled;
};

ProcedurePlayer.prototype.waitingForPreconditions = function(){
    //if ((typeof(CSA.mainWindow.__procedureEditor__) == "undefined") || CSA.mainWindow.__procedureEditor__ == null){
    if ((typeof(CSA.mainWindow.__waitingForPreconditions__) == "undefined") || CSA.mainWindow.__waitingForPreconditions__ == null || CSA.mainWindow.__procedureEditor__ == null){
        CSA.mainWindow.__procedureEditor__ = myself();
        CSA.mainWindow.__waitingForPreconditions__ = CSA.mainWindow.setInterval(this.waitingForPreconditions,1000);
    }
    else{
        if(CSA.mainWindow.__procedureEditor__.procedure.state.taskInExecution.preconditionsSatisfied()){
            CSA.mainWindow.__waitingForPreconditions__ = null;
            CSA.mainWindow.clearInterval(CSA.mainWindow.__waitingForPreconditions__);
            CSA.mainWindow.__procedureEditor__.executeNextTask();
            CSA.mainWindow.__procedureEditor__ = null;
        }
        else{
            CSA.mainWindow.__procedureEditor__.renderProcedure();   
        }
    };
};

ProcedurePlayer.prototype.executeNextTask = function(anEvent){
    myself().procedure.execute();
};

ProcedurePlayer.prototype.removeDataFromPreviousExecution = function(){
    primitiveTasksScriptManager.removeDataFromPreviousExecution();
};

ProcedurePlayer.prototype.start = function(){
    this.removeDataFromPreviousExecution();
    this.turnCommandButtonsToExecutionState();
    document.addEventListener("taskfinished",myself().executeNextTask, false);
    this.procedure.execute();
};

ProcedurePlayer.prototype.pause = function(){
    this.turnCommandButtonsToPauseState();
    //this.procedure.execute();
};

ProcedurePlayer.prototype.restartExecution = function(){
    this.procedure.restartExecution();
    this.renderProcedure();
    this.turnCommandButtonsToStopState();
};

ProcedurePlayer.prototype.tryNewExecution = function(){
    myself().procedure.tryNewExecutionOfCurrentTask();
}

ProcedurePlayer.prototype.skipTask = function(){
    myself().procedure.skipTask();
    myself().procedure.execute();
};

ProcedurePlayer.prototype.turnCommandButtonsToFinishedState = function(){
    var playButton = document.getElementById("start-procedure-command");
    playButton.disabled = true;
    playButton.setAttribute("onclick",";");
    var pauseButton = document.getElementById("pause-procedure-command");
    pauseButton.disabled = true;
    pauseButton.setAttribute("onclick",";");
    var restartButton = document.getElementById("restart-procedure-command");
    restartButton.disabled = false;
    restartButton.setAttribute("onclick","procedurePlayerInstance.restartExecution();");

    var editButton = document.getElementById("edit-procedure-button");
    editButton.disabled = true;
    editButton.setAttribute("onclick","");
};

ProcedurePlayer.prototype.turnCommandButtonsToStopState = function(){
    var playButton = document.getElementById("start-procedure-command");
    playButton.disabled = false;
    playButton.setAttribute("onclick","procedurePlayerInstance.start();");
    var pauseButton = document.getElementById("pause-procedure-command");
    pauseButton.disabled = true;
    pauseButton.setAttribute("onclick",";");
    var restartButton = document.getElementById("restart-procedure-command");
    restartButton.disabled = true;
    restartButton.setAttribute("onclick",";");

    var editButton = document.getElementById("edit-procedure-button");
    editButton.disabled = false;
    editButton.setAttribute("onclick","CSA.startScenarioRecorder(procedureExample);");
};

ProcedurePlayer.prototype.turnCommandButtonsToExecutionState = function(){
    var playButton = document.getElementById("start-procedure-command");
    playButton.disabled = true;
    playButton.setAttribute("onclick",";");
    var pauseButton = document.getElementById("pause-procedure-command");
    pauseButton.disabled = false;
    pauseButton.setAttribute("onclick","procedurePlayerInstance.pause();");
    var restartButton = document.getElementById("restart-procedure-command");
    restartButton.disabled = true;
    restartButton.setAttribute("onclick",";");

    var editButton = document.getElementById("edit-procedure-button");
    editButton.disabled = true;
    editButton.setAttribute("onclick","");
};

ProcedurePlayer.prototype.turnCommandButtonsToPauseState = function(){
    var playButton = document.getElementById("start-procedure-command");
    playButton.disabled = false;
    playButton.setAttribute("onclick","procedurePlayerInstance.start();;");
    var pauseButton = document.getElementById("pause-procedure-command");
    pauseButton.disabled = true;
    pauseButton.setAttribute("onclick",";");
    var restartButton = document.getElementById("restart-procedure-command");
    restartButton.disabled = false;
    restartButton.setAttribute("onclick","procedurePlayerInstance.restartExecution();");

    var editButton = document.getElementById("edit-procedure-button");
    editButton.disabled = true;
    editButton.setAttribute("onclick","");
};

ProcedurePlayer.prototype.renderProcedure = function(){
    this.cleanProcedureViewer();
    var tasks = this.procedure.getTasks();
    for (var i = tasks.length - 1; i >= 0; i--) {
        this.renderTask(tasks[i]);
    };
    //for (var i=0;i < tasks.length;i++)
    //   this.renderTask(tasks[i]);
};

ProcedurePlayer.prototype.__renderTaskInVBox__ = function(aTask,task_vbox){
    if (aTask.isFinished())
        task_vbox.setAttribute("style","background-color:#CCDDEE;padding:3px;border-right:1px solid black;border-top:1px solid black;border-bottom:1px solid white;border-left:1px solid white;margin:3px;width:150px;margin-right");
    else
       task_vbox.setAttribute("style","background-color:#AABBCC;padding:3px;border-right:1px solid white;border-top:1px solid white;border-bottom:1px solid black;border-left:1px solid black;margin:3px;width:150px;margin-right");
    var label_hbox = document.createElement("hbox");
    aTask.gui = task_vbox;
    var label_event = document.createElement("label");
    label_event.setAttribute("style","font-weight:bold;");
    var label_description = document.createElement("label");
    label_event.setAttribute("value",aTask.getName());    
    label_event.task = aTask;
    label_event.setAttribute("id", aTask.getId());
    label_hbox.appendChild(label_event);
    if (aTask.state.stoppedWithError()){
        task_vbox.setAttribute("style","background-color:red;padding:3px;border-right:1px solid white;border-top:1px solid white;border-bottom:1px solid black;border-left:1px solid black;margin:3px;width:150px;margin-right");
        var skip_button = document.createElement("label");
        skip_button.setAttribute("style","background-color:pink;padding:1px;");
        skip_button.setAttribute("value","Try again");
        skip_button.setAttribute("tooltiptext"," Click here to edit this task ");
        skip_button.task = aTask;
        skip_button.setAttribute("id", aTask.getId());
        skip_button.addEventListener("click",myself().tryNewExecution,false);
        label_hbox.appendChild(skip_button);

        task_vbox.setAttribute("style","background-color:red;padding:3px;border-right:1px solid white;border-top:1px solid white;border-bottom:1px solid black;border-left:1px solid black;margin:3px;width:150px;margin-right");
        var skip_button = document.createElement("label");
        skip_button.setAttribute("style","background-color:pink;padding:1px;");
        skip_button.setAttribute("value","Skip anyway");
        skip_button.setAttribute("tooltiptext"," Click here to edit this task ");
        skip_button.task = aTask;
        skip_button.setAttribute("id", aTask.getId());
        skip_button.addEventListener("click",myself().skipTask,false);
        label_hbox.appendChild(skip_button);

    }
    else
    if (aTask.isInExecution()){
        if ((aTask.isOptional() && !aTask.wasExecutedAtLeastOnce()) || (!aTask.isAutomatic())){
            var skip_button = document.createElement("label");
            skip_button.setAttribute("style","background-color:#CCCCCC;padding:1px;");
            skip_button.setAttribute("value","Skip");
            skip_button.setAttribute("tooltiptext"," Click here to edit this task ");
            skip_button.task = aTask;
            skip_button.setAttribute("id", aTask.getId());
            skip_button.addEventListener("click",myself().skipTask,false);
            label_hbox.appendChild(skip_button);
        }

        if (aTask.isRepetitive() && aTask.wasExecutedAtLeastOnce()){
            var finish_button = document.createElement("label");
            finish_button.setAttribute("style","background-color:#CCCCCC;padding:1px;");
            finish_button.setAttribute("value","End loop");
            finish_button.setAttribute("tooltiptext"," Click here to finish with this repetitive task");
            finish_button.task = aTask;
            finish_button.setAttribute("id", aTask.getId());
            finish_button.addEventListener("click",myself().skipTask,false);
            label_hbox.appendChild(finish_button);
        }
    }

    var description_container = document.createElement("hbox");
    label_description.setAttribute("value",aTask.getDescription() + ", state: " + aTask.getStateDescription());
    var attributes_names_str = "";
    attributes_names_values_str = "";
    for (var i = 0;i < aTask.getTaskAttributes().length;i++){
        attributes_names_str += aTask.getTaskAttributes()[i].getName();
        attributes_names_values_str += "\n" + aTask.getTaskAttributes()[i].getName() + "=" + aTask.getTaskAttributes()[i].getValue();
        if (i < (aTask.getTaskAttributes().length-1)){
            attributes_names_str += ", ";
        }
    }
    description_container.setAttribute("style","border-top:1px solid white");
    if (!aTask.state.stoppedWithError())
        description_container.setAttribute("tooltiptext"," This is the list of attributes for this task: " + attributes_names_str);
    if (aTask.state.isFinished()){
        var textTooltip = "";
        description_container.setAttribute("tooltiptext"," This is the list of attributes for this task:" + attributes_names_values_str);
    }
    else
        description_container.setAttribute("tooltiptext", aTask.getStateDescription());
    description_container.appendChild(label_description);

    if(aTask.isRepetitive() == true){
        var repetitive_flag = document.createElement("label");
        repetitive_flag.setAttribute("style","color:#EEEEEE; background-color:#999999");
        repetitive_flag.setAttribute("value"," R ");
        repetitive_flag.setAttribute("tooltiptext"," This task has been marked as repetitive ");
        description_container.appendChild(repetitive_flag);
    }
    if(aTask.isOptional() == true){
        var optional_flag = document.createElement("label");
        optional_flag.setAttribute("style","color:#EEEEEE; background-color:#999999");
        optional_flag.setAttribute("value"," O ");
        optional_flag.setAttribute("tooltiptext"," This task has been marked as optional ");
        description_container.appendChild(optional_flag);
    }
    if(aTask.isAutomatic() == true){
        var automatic_flag = document.createElement("label");
        automatic_flag.setAttribute("style","color:#EEEEEE; background-color:#999999");
        automatic_flag.setAttribute("value"," A ");
        automatic_flag.setAttribute("tooltiptext"," This task has been marked as automatic ");
        description_container.appendChild(automatic_flag);
    }

    task_vbox.appendChild(label_hbox);
    task_vbox.label = label_event;    
    task_vbox.appendChild(description_container);
    task_vbox.description = label_description;
    task_vbox.focusableSubelement = label_event;

    if ((myself().procedure.state.taskInExecution == aTask) && !aTask.preconditionsSatisfied()){
        var preconditions_container = document.createElement("vbox");
        for (var i = 0;i < aTask.preconditions.length;i++){
            if (!aTask.preconditions[i].isSatisfied()){
                var preconditions_description = document.createElement("label");
                preconditions_description.setAttribute("value","-" + aTask.preconditions[i].getDescription());
                preconditions_description.setAttribute("style","font-style:italic;background-color:#CCDDEE");                
                preconditions_container.setAttribute("style","border-top:1px solid white");
                preconditions_container.appendChild(preconditions_description);
                task_vbox.appendChild(preconditions_container);
            }
        }
    }
    var current_style = task_vbox.getAttribute("style");
    if(aTask.isNotStarted())
        task_vbox.hidden = true;
    else
        task_vbox.hidden = false;

    if(aTask.duiElement != null){
        var dui_element_container = document.createElement("hbox");
        dui_element_container.setAttribute("style","border-top:1px solid white");
        task_vbox.appendChild(dui_element_container);
        dui_element_container.innerHTML = "<html:div id='" + aTask.getId() + "-dui-container" + "' style='background-color:#FFFFFF;width:auto;height:auto;'></html:div>";

        var dui_div = document.getElementById(aTask.getId()+'-dui-container');
        var parser = new DOMParser();
        var dom_DUI = parser.parseFromString(aTask.duiElement, "text/html");

        var anchors = dom_DUI.documentElement.getElementsByTagName("a");
        for (var i = anchors.length - 1; i >= 0; i--)
            anchors[i].setAttribute("target","_content");

        var forms = dom_DUI.documentElement.getElementsByTagName("form");
        for (var i = forms.length - 1; i >= 0; i--)
            forms[i].setAttribute("target","_content");
        dui_div.appendChild(dom_DUI.documentElement);          
    }
};



