//config.js

var __editor__ = null;

function onDialogAccept(){
	
};

function onDialogCancel(){
	
};

function moveAugmenterDown(id){
	var augmenter = getAugmenterFromId(id);
	artifactmanager.moveDown(augmenter);
	buildLists();
	//rebuildInstantiatedRefactoringList();
};

function reloadAugmenter(id){
	var augmenter = getAugmenterFromId(id);
	augmenter.reload(augmenter);
	buildLists();
	//rebuildInstantiatedRefactoringList();
};

function moveAugmenterUp(id){
	var augmenter = getAugmenterFromId(id);
	artifactmanager.moveUp(augmenter);
	buildLists();
	//rebuildInstantiatedRefactoringList();
};

function toggle(obj){
	if (obj.style.display == "none")
		obj.style.display = 'block';
	else
		obj.style.display = "none";
};

function toogleScriptView(target){
	toggle(document.getElementById("options-"+target));
	toggle(document.getElementById("short-description-"+target));
};

function addElementIntoGUI(id,element){
	//var id_element = "script" + id;
	var id_element = id;
	var script_container = document.createElement("vbox");
	script_container.setAttribute("id",id_element);
	script_container.setAttribute("class","script");
	script_container.setAttribute("onclick","toogleScriptView('"+id_element+"');");

	var script_title_container = document.createElement("hbox");		
	script_title_container.setAttribute("class","script-title");
	script_title_container.setAttribute("flex","1");
	script_container.appendChild(script_title_container);

	var script_title_container_name = document.createElement("hbox");
	script_title_container_name.textContent = element.name();
	script_title_container_name.setAttribute("class","script-title-options");
	script_title_container.appendChild(script_title_container_name);

	var script_title_container_options = document.createElement("hbox");
	script_title_container_options.setAttribute("class","script-title-options");
	script_title_container_options.setAttribute("flex","1");
	script_title_container.appendChild(script_title_container_options);

	var buttonDown = document.createElement("button");
	buttonDown.setAttribute("label","Move down");
	buttonDown.addEventListener("click",function(e){e.stopPropagation();moveAugmenterDown(id_element);},false);

	var buttonUp = document.createElement("button");
	buttonUp.setAttribute("label","Move up");
	buttonUp.addEventListener("click",function(e){e.stopPropagation();moveAugmenterUp(id_element);},false);
	
	script_title_container_options.appendChild(buttonDown);
	script_title_container_options.appendChild(buttonUp);

	var script_description_container = document.createElement("hbox");
	script_description_container.setAttribute("class","short-description");
	script_description_container.setAttribute("style","display:block;");
	script_description_container.setAttribute("id","short-description-"+id_element);
	script_description_container.textContent = element.description();
	script_container.appendChild(script_description_container);

	var script_options_container = document.createElement("vbox");
	script_options_container.setAttribute("class","full-options");
	script_options_container.setAttribute("style","display:none;");
	script_options_container.setAttribute("id","options-"+id_element);
	script_container.appendChild(script_options_container);

	var script_options_subcontainer = document.createElement("hbox");
	script_options_subcontainer.setAttribute("flex","1");
	script_options_container.appendChild(script_options_subcontainer);

	var script_options_grid = document.createElement("grid");
	script_options_grid.setAttribute("flex","1");
	script_options_grid.setAttribute("width","2000px");
	script_options_subcontainer.appendChild(script_options_grid);

	var script_options_columns = document.createElement("columns");		
	script_options_grid.appendChild(script_options_columns);

	var script_options_column = document.createElement("column");
	script_options_column.setAttribute("flex","1");
	script_options_columns.appendChild(script_options_column);


	var script_options_rows = document.createElement("rows");		
	script_options_grid.appendChild(script_options_rows);
				    
	var script_options_author_row = document.createElement("row");
	var script_options_author_vbox = document.createElement("vbox");
	script_options_author_vbox.setAttribute("class","script-author");
	script_options_author_vbox.textContent = element.author();
	script_options_author_row.appendChild(script_options_author_vbox);
	script_options_rows.appendChild(script_options_author_row);

	var script_options_description_row = document.createElement("row");
	var script_options_description_vbox = document.createElement("vbox");	
	script_options_description_vbox.setAttribute("class","script-description");
	script_options_description_vbox.textContent = element.description();
	script_options_description_row.appendChild(script_options_description_vbox);

	
	script_options_rows.appendChild(script_options_description_row);
	var script_options_operations_row = document.createElement("row");
	script_options_operations_row.setAttribute("class","script-operations-row-container");	
	var script_options_operations_flexbox = document.createElement("box");
	script_options_operations_flexbox.setAttribute("flex","1");
	script_options_operations_row.appendChild(script_options_operations_flexbox);

	var script_options_operations_hbox = document.createElement("hbox");
	script_options_operations_hbox.setAttribute("class","script-operations-container");
	var remove_button = document.createElement("button");
	remove_button.setAttribute("label","Remove");
	remove_button.setAttribute("class","remove-script");
	remove_button.setAttribute("onclick","removeAugmenter(" + id_element + ")");	
	script_options_operations_hbox.appendChild(remove_button);

	var edit_button = document.createElement("button");
	edit_button.setAttribute("label","Edit");
	edit_button.setAttribute("class","edit-script");
	edit_button.setAttribute("onclick","editAugmenter('" + id_element + "')");
	script_options_operations_hbox.appendChild(edit_button);

	//var reload_button = document.createElement("button");
	//reload_button.setAttribute("label","Reload");
	//reload_button.setAttribute("class","reload-script");
	//reload_button.setAttribute("onclick","reloadAugmenter('" + id_element + "')");
	//script_options_operations_hbox.appendChild(reload_button);

	var edit_activation = document.createElement("button");
	var label = (element.isActivated())?"Deactivate":"Activate";
	edit_activation.setAttribute("label",label);
	edit_activation.setAttribute("id","activation-button-"+id_element);
	edit_activation.setAttribute("class","edit-script-activation");
	edit_activation.setAttribute("onclick","toggleActivationAugmenter('" + id_element + "')");
	script_options_operations_hbox.appendChild(edit_activation);

	script_options_operations_row.appendChild(script_options_operations_hbox);
	script_options_rows.appendChild(script_options_operations_row);
	document.getElementById("instantiated-container-box").appendChild(script_container);
};

function addGenericElementIntoGUI(id,element){
	var id_element = "genericscript" + id;
	var script_container = document.createElement("vbox");
	script_container.setAttribute("id",id_element);
	script_container.setAttribute("class","script");
	script_container.setAttribute("onclick","toogleScriptView('"+id_element+"');");

	var script_title_container = document.createElement("hbox");		
	script_title_container.textContent = element.name();
	script_title_container.setAttribute("class","script-title");
	script_container.appendChild(script_title_container);

	var script_description_container = document.createElement("hbox");
	script_description_container.setAttribute("class","short-description");
	script_description_container.setAttribute("style","display:block;");
	script_description_container.setAttribute("id","short-description-"+id_element);
	script_description_container.textContent = element.description();
	script_container.appendChild(script_description_container);

	var script_options_container = document.createElement("vbox");
	script_options_container.setAttribute("class","full-options");
	script_options_container.setAttribute("style","display:none;");
	script_options_container.setAttribute("id","options-"+id_element);
	script_container.appendChild(script_options_container);

	var script_options_subcontainer = document.createElement("hbox");
	script_options_subcontainer.setAttribute("flex","1");
	script_options_container.appendChild(script_options_subcontainer);

	var script_options_grid = document.createElement("grid");
	script_options_grid.setAttribute("flex","1");
	script_options_grid.setAttribute("width","2000px");
	script_options_subcontainer.appendChild(script_options_grid);

	var script_options_columns = document.createElement("columns");		
	script_options_grid.appendChild(script_options_columns);

	var script_options_column = document.createElement("column");
	script_options_column.setAttribute("flex","1");
	script_options_columns.appendChild(script_options_column);

	var script_options_rows = document.createElement("rows");		
	script_options_grid.appendChild(script_options_rows);
	
	var script_options_author_row = document.createElement("row");
	var script_options_author_vbox = document.createElement("vbox");
	script_options_author_vbox.setAttribute("class","script-author");
	script_options_author_vbox.textContent = element.author();
	script_options_author_row.appendChild(script_options_author_vbox);
	script_options_rows.appendChild(script_options_author_row);

	var script_options_description_row = document.createElement("row");
	var script_options_description_vbox = document.createElement("vbox");	
	script_options_description_vbox.setAttribute("class","script-description");
	script_options_description_vbox.textContent = element.description();
	script_options_description_row.appendChild(script_options_description_vbox);
	
	script_options_rows.appendChild(script_options_description_row);
	var script_options_operations_row = document.createElement("row");
	script_options_operations_row.setAttribute("class","script-operations-row-container");	
	var script_options_operations_flexbox = document.createElement("box");
	script_options_operations_flexbox.setAttribute("flex","1");
	script_options_operations_row.appendChild(script_options_operations_flexbox);

	var script_options_operations_hbox = document.createElement("hbox");
	script_options_operations_hbox.setAttribute("class","script-operations-container");
	var remove_button = document.createElement("button");
	remove_button.setAttribute("label","Remove");
	remove_button.setAttribute("class","remove-script");
	remove_button.setAttribute("onclick","removeGenericAugmenter('" + id_element + "')");	
	script_options_operations_hbox.appendChild(remove_button);

	var edit_button = document.createElement("button");
	edit_button.setAttribute("label","Edit");
	edit_button.setAttribute("class","edit-script");
	edit_button.setAttribute("onclick","editAugmenter('" + id_element + "')");
	script_options_operations_hbox.appendChild(edit_button);

	script_options_operations_row.appendChild(script_options_operations_hbox);
	script_options_rows.appendChild(script_options_operations_row);
	document.getElementById("generic-container-box").appendChild(script_container);
};

function listInstantiatedRefactorings(){
	try{
		for (var i=0;i < augmenters.length;i++){
			addElementIntoGUI(i,augmenters[i]);		
		}
	}catch(e){;}
};

function rebuildInstantiatedRefactoringList(){
	var container = document.getElementById("instantiated-container-box");
	while (container.firstChild) {
 	   container.removeChild(container.firstChild);
	}
	augmenters = artifactmanager.getInstantiatedRefactorings();//accessibility augmenters
	listInstantiatedRefactorings();
};

function listGenericRefactorings(){
	try{
		for (var i=0;i < genericaugmenters.length;i++){
			addGenericElementIntoGUI(i,genericaugmenters[i]);		
		}
	}catch(e){;}
};

function openEditor(editor,file){
	try{
		var process = Components.classes["@mozilla.org/process/util;1"]
	              .createInstance(Components.interfaces.nsIProcess);
	    process.init(editor);
	    process.run(false,[file],1);	    
	}catch(e){
		;
	}    
};

function getAugmenterFromId(id_element){
	return augmenters[id_element];
};

function getGenericAugmenterFromId(id_element){
	return genericaugmenters[id_element];
};

function __removeAugmenter__(anAugmenter){
	var yes = confirm("Do you want to remove this refactoring?");
	if(yes)
		try{
			anAugmenter.delete();
			buildLists();
		}catch(e){
			alert("The file could not be removed.");
	    }
};

function removeGenericAugmenter(id_element){
	var array_position = id_element.split("genericscript")[1];
	__removeAugmenter__(getGenericAugmenterFromId(array_position));
};

function removeAugmenter(id_element){
	__removeAugmenter__(getAugmenterFromId(id_element));
};

function toggleActivationAugmenter(id_element){	
	var element = getAugmenterFromId(id_element);
	element.toggleActivation();	
	var label = (element.isActivated())?"Deactivate":"Activate";
	edit_activation = document.getElementById("activation-button-"+id_element);	
	edit_activation.setAttribute("label",label);
};

function editAugmenter(id_element){
	var augmenter = getAugmenterFromId(id_element);	
	if (__editor__ == null){
		const nsIFilePicker = Components.interfaces.nsIFilePicker;
		var filePicker = Components.classes["@mozilla.org/filepicker;1"]
	                 .createInstance(nsIFilePicker);
	    filePicker.init(window,"Choose a text editor",nsIFilePicker.modeOpen);
	    filePicker.appendFilters(nsIFilePicker.filterApps);
		var stop_asking_exec = true;
		var selectedFile;
		while(stop_asking_exec){
			selectedFile = filePicker.show();
			if (selectedFile == nsIFilePicker.returnOK && !filePicker.file.isExecutable()){
				alert("You must select an executable file.");
				continue;
			}
			stop_asking_exec = false;
		};		
		if (selectedFile == nsIFilePicker.returnOK || selectedFile == nsIFilePicker.returnReplace && selectedFile.isExecutable()) {
	  		var file = filePicker.file;  		
	  		var augmenter = getAugmenterFromId(id_element);  		
	  		try{
	  			openEditor(file,augmenter.getFilePath());  		
	  			__editor__ = file;
	  		}
	  		catch(e){return;}	  		
		}
	}
	else
		openEditor(__editor__,augmenter.getFilePath());
};

function buildLists(){
	var container = document.getElementById("instantiated-container-box");
	while (container.firstChild) {
 	   container.removeChild(container.firstChild);
	}
	container = document.getElementById("generic-container-box");
	while (container.firstChild) {
 	   container.removeChild(container.firstChild);
	}
	augmenters = artifactmanager.getInstantiatedRefactorings();//accessibility augmenters
	listInstantiatedRefactorings();
	genericaugmenters = artifactmanager.getGenericRefactorings();
	listGenericRefactorings();
}

function reloadAugmenters(){
	for (var i=0;i < augmenters.length;i++){
		augmenters[i].reload();
	}
};

/*
	gets all refactorings that match a given input string
	and loads them in the manager's window
*/
function searchByHostInput(){
	var results = [],
		searchInput = document.getElementById("url-input-search").value,
    	refactorings = artifactmanager.getInstantiatedRefactorings(),
		host;
	for(var i=0, j=refactorings.length;i<j;i++){
        host = refactorings[i].getMetadata()["host"];
        if(searchInput.match(host)){
            results.push(augmenters[i]);
		}
	}
	listSearchedInstantiatedRefactorings(results);
}

/*
	gets all refactorings that match the value of a selected option
	in a menu-list and loads them in the manager's window
*/
function searchByHostSelect(){
	var results = [],
		menuList = document.getElementById("url-select"),
		selection = menuList.selectedItem["value"],
    	refactorings = artifactmanager.getInstantiatedRefactorings(),
		host;
	for(var i=0, j=refactorings.length;i<j;i++){
        host = refactorings[i].getMetadata()["host"];
        if(selection.match(host)){
            results.push(augmenters[i]);
		}
	}
	listSearchedInstantiatedRefactorings(results);
}

/*
	loads the refactorings from results in the Instantiated tab
	of the manager's window  
*/
function listSearchedInstantiatedRefactorings(results){
	var container = document.getElementById("instantiated-container-box");
    selectInstantiatedsTab();
	while (container.firstChild) {
 	   container.removeChild(container.firstChild);
	}
	try{
		for (var i=0, j=results.length;i<j;i++){
			addElementIntoGUI(i,results[i]);		
		}
	}catch(e){;}
}

/*
	resets the search inputs and loads all the installed refactorings
	in the manager's window
*/
function cleanSearch(){
    buildLists();
    document.getElementById("url-input-search").value = "";
    selectInstantiatedsTab();
}

/*
	selects the instantiated refactoring's tab
*/
function selectInstantiatedsTab(){
    var tab = document.getElementById("instantiateds-tab"),
   	tabs = document.getElementById("tablist");
    tabs.selectedTab = tab;
}

/*
	fills a menu-list element with all the instantiated refactoring's host URLs
*/
function populateMenuList(){
    var refactorings = artifactmanager.getInstantiatedRefactorings(),
		urlArray = [],
		menuList = document.getElementById("url-select"),
		host,
		url;
    for(var i=0, j=refactorings.length;i<j;i++){
        host = refactorings[i].getMetadata()["host"];
		if(!isInArray(host,urlArray)){
			urlArray.push(host);
		}
    }
    for (var i=0,j=urlArray.length;i<j;i++) {
		url=urlArray[i];
		if(url !== undefined && url !== ''){
			menuList.insertItemAt(i,url,url);
		}
    }
	menuList.selectedIndex = 0;
}

function isInArray(element, array){
	return array.indexOf(element) >= 0;
}

/**
 closes the actual dialog window and opens a new browser tab
 which loads the repository's webpage 	
*/
function goToRepoWeb(){
    var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"]
                   .getService(Components.interfaces.nsIWindowMediator),
    	mainWindow = wm.getMostRecentWindow("navigator:browser"),
    	tab = mainWindow.gBrowser.addTab("http://vps-959090-x.dattaweb.com/refactorings/app.php/"),
    	dialog = document.getElementById("scripts-config");
    dialog.cancelDialog();
    mainWindow.gBrowser.selectedTab = tab;
}


/**
 * components must be an array instance with the components's script in installation 
 * order. Every script must be encoded in base64.
 *
	function installAccesibilityComponents(components){
		var installEvent = new CustomEvent('installAccesibilityComponents', 
			{ 'components': components });
		document.dispatchEvent(installEvent);
	}
*/