/***************************************
	TransformListInRadioButtonsRefactoring
 */

function TransformListInRadioButtonsRefactoring(name){
	this.name = name;
	this.selects_xpath = [];
};

TransformListInRadioButtonsRefactoring.prototype.adaptDocument = function(doc){
	for (var j=0;j < this.selects_xpath.length;j++){
		var xpath = this.selects_xpath[j];
		if (this.getSelectElement(doc,xpath) != null){
			var container = this.buildContainer(doc,xpath);
			this.renderRadioButtonGroup(doc,container,xpath);
		}
	}
};

TransformListInRadioButtonsRefactoring.prototype.addSelectXPath = function(aXpath){
	this.selects_xpath.push(aXpath);
};

TransformListInRadioButtonsRefactoring.prototype.getSelectElement = function(doc,aXpath){
	try{
		return doc.evaluate(aXpath , doc, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null).snapshotItem(0);
	}
	catch(e){
		return null;
	}
};

TransformListInRadioButtonsRefactoring.prototype.buildContainer = function(doc,xpath){
	var select = this.getSelectElement(doc,xpath);
	var container = doc.createElement("div");
	select.parentNode.replaceChild(container,select);
	var select_container = doc.createElement("div");
	select_container.appendChild(select);	
	select_container.setAttribute("style","display:none;");
	container.appendChild(select_container);
	return container;
};

TransformListInRadioButtonsRefactoring.prototype.renderRadioButtonGroup = function(doc,container,xpath){	
	var select = this.getSelectElement(doc,xpath);
	var name = select.getAttribute("name");
	var options = select.getElementsByTagName("option");
	for (var i = 0; i < options.length;i++){
		var new_option = doc.createElement("input");
		var onclick = "document.getElementsByName('" +  name + "')[0].selectedIndex = " + i +";";
		new_option.setAttribute("type","radio");
		new_option.setAttribute("name",name + "_button");
		new_option.setAttribute("value",i);
		new_option.setAttribute("onclick",onclick);
		var new_option_span = doc.createElement("span");
		new_option_span.innerHTML = options[i].innerHTML;				
		container.appendChild(new_option);
		container.appendChild(new_option_span);
		var br = doc.createElement("br");
		container.appendChild(br);
	}
};

