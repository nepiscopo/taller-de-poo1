/***************************************
	UnframeApplication
 */

function UnframeApplication(name, base_url, url_patterns){
	this.name = name;
	this.url = base_url;
	this.url_patterns = url_patterns;
	this.targetFrames = [];
	this.staticLinks = [];
	this.mergedFrameMock = [];
};

UnframeApplication.prototype.adaptDocument = function(doc){
	if (this.isLoadedMainFramedPage(doc.location.href)){
		this.renderMainPage(doc);
	}
	for(var i=0;i < this.targetFrames.length;i++)
		this.targetFrames[i].adaptDocument(doc);
}

UnframeApplication.prototype.renderMainPage = function(doc){	
	var body = doc.createElement("body");
	var title = doc.createElement("h2");
	title.innerHTML = "Opciones de " + this.name;
	var ul = doc.createElement("ul");
	for(var i=0;i < this.targetFrames.length;i++){
		var li = doc.createElement("li");
		var innerHTML = this.targetFrames[i].renderAnchor(doc);
		li.innerHTML = innerHTML;
		ul.appendChild(li);
	}
	for(var i=0;i < this.mergedFrameMock.length;i++){
		var li = doc.createElement("li");
		var innerHTML = this.mergedFrameMock[i].renderAnchor(doc);
		li.innerHTML = innerHTML;
		ul.appendChild(li);
	};	
	body.appendChild(title);
	body.appendChild(ul);
	var frame = doc.getElementsByTagName("frameset")[0];
	doc.body.parentNode.replaceChild(body,frame);
}


UnframeApplication.prototype.load = function (url){
	content.document.location.href = url;
};

UnframeApplication.prototype.reload = function (url){	
	if(url)
		this.load(url);
	else
		this.load(this.url);
};

UnframeApplication.prototype.isLoadedMainFramedPage = function(url){
	for (var i=0;i < this.url_patterns.length;i++){
		if (url.match(this.url_patterns[i]) != null){
			return true;
		}
	}
	return false;
};

UnframeApplication.prototype.isATargetFrameLoaded = function(url){
	for (var i=0;i < this.targetFrames.length;i++)
		if (this.targetFrames[i].isLoaded(url))
			return true;	
	return false;
};

UnframeApplication.prototype.isAppLoaded = function(url){
	if(typeof(url) != "string") return false;
	if (this.isLoadedMainFramedPage(url) || this.isATargetFrameLoaded(url))
		return true;
	return false;
};

UnframeApplication.prototype.addTargetFrame = function(aTargetFrame){
	this.targetFrames.push(aTargetFrame);
};

UnframeApplication.prototype.addStaticLink = function(aStaticLink){
	this.staticLinks.push(aStaticLink);
};

UnframeApplication.prototype.addMergedFrameMock = function(aMergedFrameMock){
	this.mergedFrameMock.push(aMergedFrameMock);
};


/*
 * Frames Elements 
 */

function TargetFrame(name,app,target_frame_xpath,target_url){
	this.name = name;
	//La aplicación de la cual se genera un menu principal 
	this.main = app;
	//La url de la aplicacion a la que este servicio refiere
	this.target_url = target_url;
	//La url de la aplicacion a la que este servicio refiere
	this.target = target_frame_xpath;	
};

TargetFrame.prototype.adaptDocument = function(doc){
	if (!this.isLoaded(doc.location.href)){
		return;
	}
	//var anchors = doc.getElementsByAttribute("target","*");
	var anchors = doc.getElementsByTagName("a");	
	for(var i=0;i < anchors.length;i++)
		anchors[i].removeAttribute("target");
};

TargetFrame.prototype.isLoaded = function(url){
	if((typeof(url) != "string") || (this.target_url == null)) return false;
	if (url.match(this.target_url) != null)
		return true;	
	return false
};

TargetFrame.prototype.renderAnchor = function(doc){
	//var onclick = 'event = document.createEvent("Event");event.initEvent("SplitedSectionSelected", true, false);this.dispatchEvent(event);';
	var elements = doc.evaluate(this.target, doc, null, XPathResult.ANY_TYPE, null);
	var element = elements.iterateNext();
	var url;
	if(element){
		url = element.getAttribute("src");
	}
	return " <a id='" + this.name + "'href='" + url + "' >" + this.name + "</a> ";
};


/*
 * MergedFrameMock
 */

function MergedFrameMock(name,app){
	this.name = name;
	//La aplicación de la cual ser genera un menu principal 
	this.main = app;
	this.target_elements = [];
};

MergedFrameMock.prototype.addTargetElement = function(name,url){
	//aDict es de la forma {"name":name_value,"url":target_url_value}
	aDict = {"name":name,"url":url};
	this.target_elements.push(aDict);
};

MergedFrameMock.prototype.renderAnchor = function(doc){
	var list = doc.createElement("ul");
	for(var i=0;i < this.target_elements.length;i++){
		var li = doc.createElement("li");
		li.innerHTML = "<a href='" + this.target_elements[i]['url'] + "' >" + this.target_elements[i]['name'] + "</a> ";
		list.appendChild(li);
	}
	return list.innerHTML = this.name + "<ul>" + list.innerHTML  + "</ul>";
};
