/***************************************
	ReorganizeIntoAListRefactoring
 */

function ReorganizeIntoAListRefactoring(name){
	this.name = name;
	this.container_xpath;
	this.elements_for_list = [];
};

ReorganizeIntoAListRefactoring.prototype.adaptDocument = function(doc){
	this.renderList(doc);
};

ReorganizeIntoAListRefactoring.prototype.setContainerNode = function(aXpath){
	this.container_xpath = aXpath;
};

ReorganizeIntoAListRefactoring.prototype.renderList = function(doc){	
	var dom_elements = doc.evaluate(this.container_xpath, doc, null, XPathResult.ANY_TYPE, null);
	//Se supone que es uno solo!
	var container_element = dom_elements.iterateNext();
	var list = doc.createElement("ul");
	container_element.appendChild(list);
	var elements_for_new_list = [];
	var elements_for_removing = [];
	for (var i = 0;i < this.elements_for_list.length;i++){
		var dom_target_elements = doc.evaluate(this.elements_for_list[i], doc, null, XPathResult.ANY_TYPE, null);
		var element = dom_target_elements.iterateNext();
		while (element) {
			var persistElement = element.cloneNode(true);
			elements_for_new_list.push(persistElement);
			elements_for_removing.push(element);
			element = dom_target_elements.iterateNext();			
		}
	}
	for (var i = 0;i < elements_for_new_list.length;i++){
		var list_element = doc.createElement("li");
		list_element.appendChild(elements_for_new_list[i]);
		list.appendChild(list_element);
		elements_for_new_list[i].setAttribute("class","");
	}
	for (var i = 0;i < elements_for_removing.length;i++)
		elements_for_removing[i].parentNode.removeChild(elements_for_removing[i]);
};

ReorganizeIntoAListRefactoring.prototype.addElementForList = function (xpath){
	this.elements_for_list.push(xpath);
};
