/***************************************
	DeleteBlankSpacesRefactoring
 */

function DeleteBlankSpacesRefactoring(name){
	this.name = name;
};

DeleteBlankSpacesRefactoring.prototype.adaptDocument = function(doc){
	this.removeBlankSpaces(doc);
};

DeleteBlankSpacesRefactoring.prototype.removeBlankSpaces = function(doc){	
	doc.body.innerHTML = doc.body.innerHTML.replace(/(&nbsp;)*/g,"");
};
