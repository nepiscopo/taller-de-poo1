function mainPageBuilderForMainFacade(services){
	var title = content.document.createElement("h2");
	title.innerHTML = "TestingMainPage";
	var ul = content.document.createElement("ul");
	for(var i=0;i < services.length;i++){
		var li = content.document.createElement("li");
		var innerHTML = "<br/>" + services[i].renderAnchor();
		li.innerHTML = innerHTML;
		ul.appendChild(li);
	}
	content.document.body.appendChild(title);
	content.document.body.appendChild(ul);
};

/***************************************
	MainFacade
 */

function MainFacadeRefactoring(name){
	this.name = name;
	this.services = [];
};

MainFacadeRefactoring.prototype.adaptDocument = function(doc){
	this.renderMainPage(doc);
}

MainFacadeRefactoring.prototype.renderMainPage = function(doc){
	var title = doc.createElement("h2");
	title.innerHTML = this.name() + " - Main Menu";
	var ul = doc.createElement("ul");
	for(var i=0;i < this.services.length;i++){
		var li = doc.createElement("li");
		var innerHTML = this.services[i].renderAnchor();
		li.innerHTML = innerHTML;
		ul.appendChild(li);
	}
			
	doc.body.appendChild(title);
	doc.body.appendChild(ul);	
}

MainFacadeRefactoring.prototype.buildMainPage = function(){
	content.document = mainPageBuilderForSplitedPage(this.services);
};

MainFacadeRefactoring.prototype.load = function (url){
	content.document.location.href = url;
};

MainFacadeRefactoring.prototype.reload = function (url){
	target_section = null;//Esta linea solo es temporal.
	if(url)
		this.load(url);
	else
		this.load(this.url);
};

MainFacadeRefactoring.prototype.getServiceByName = function(service_name){
	for(var i=0;i < this.services.length;i++){
		if (this.services[i].name == splited_section_name)
			return this.services[i];
	}
	return null;
};

MainFacadeRefactoring.prototype.renderAnchor = function(){
	onclick = 'event = document.createEvent("Event");event.initEvent("SplitedSectionSelected", true, false);this.dispatchEvent(event);';
	return " <a id='" + this.name + "_splited_home'href='#' onclick='" + onclick + "'>Inicio</a> ";
};

MainFacadeRefactoring.prototype.addServiceForMainFacade = function(aServiceForMainFacade){
	this.services.push(aServiceForMainFacade);
};

/***************************************
	SplitedSection
 */

function ServiceForMainFacade(name,app,target_url){
	this.name = name;
	//La aplicación de la cual se genera un menu principal 
	this.main = app;
	//La url de la aplicacion a la que este servicio refiere
	this.target_url;
};

ServiceForMainFacade.prototype.isLoaded = function(url){
	if (url.match(this.target_url) != null)
		return true;
};

ServiceForMainFacade.prototype.load = function(url){
	this.accesible_app.load(this.target_url);
};

ServiceForMainFacade.prototype.renderAnchor = function(){
	var onclick = 'event = document.createEvent("Event");event.initEvent("SplitedSectionSelected", true, false);this.dispatchEvent(event);';
	return " <a id='" + this.name + "'href='#' onclick='" + onclick + "'>" + this.name + "</a> ";
};
