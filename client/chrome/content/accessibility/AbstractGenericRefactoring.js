
function AbstractGenericRefactoring(){
	
};				

AbstractGenericRefactoring.prototype.adaptDocument = function(doc){
	alert("The method #adaptDocument should be redefined on subclasses of AbstractGenericRefactoring")
};

AbstractGenericRefactoring.prototype.isGeneric = function(){
	return true;
};

AbstractGenericRefactoring.prototype.isInstantiated = function(){
	return false;
};

AbstractGenericRefactoring.prototype.install = function(manager,file){
	manager.installGenericRefactoring(file);
};
