/***************************************
	ReorganizeIntoAListRefactoring
 */

function CodeRefactoring(name){
	this.name = name;
	this.container_xpath;
	this.innerHTML = null;
};

CodeRefactoring.prototype.adaptDocument = function(doc){
	this.renderCode(doc);
};

CodeRefactoring.prototype.setContainerNode = function(aXpath){
	this.container_xpath = aXpath;
};

CodeRefactoring.prototype.setNewCode = function(innerHTML){
	this.innerHTML = innerHTML;
};

CodeRefactoring.prototype.renderCode = function(doc){	
	var dom_elements = doc.evaluate(this.container_xpath, doc, null, XPathResult.ANY_TYPE, null);
	//Se supone que es uno solo!
	var container = dom_elements.iterateNext();
	container.innerHTML = this.innerHTML;
};
