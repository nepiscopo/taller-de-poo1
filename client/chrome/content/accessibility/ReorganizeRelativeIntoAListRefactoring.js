/***************************************
	ReorganizeIntoAListRefactoring
 */

function ReorganizeRelativeIntoAListRefactoring(name){
	this.name = name;
	this.container_xpath;
	this.elements_for_list = [];
	this.clean_container = true;
};

ReorganizeRelativeIntoAListRefactoring.prototype.adaptDocument = function(doc){
	this.renderList(doc);
};

ReorganizeRelativeIntoAListRefactoring.prototype.setContainerNode = function(aXpath){
	this.container_xpath = aXpath;
};

ReorganizeRelativeIntoAListRefactoring.prototype.renderList = function(doc){	
	var dom_elements = doc.evaluate(this.container_xpath, doc, null, XPathResult.ANY_TYPE, null);
	var container_element = dom_elements.iterateNext();
	var elements_for_removing = [];
	var containers = [];
	var lists = [];
	while (container_element){
		var list = doc.createElement("ul");		
		var elements_for_new_list = [];
		for (var i = 0;i < this.elements_for_list.length;i++){
			var dom_target_elements = doc.evaluate(this.elements_for_list[i], container_element, null, XPathResult.ANY_TYPE, null);
			var element = dom_target_elements.iterateNext();
			while (element) {
				var persistElement = element.cloneNode(true);
				elements_for_new_list.push(persistElement);
				elements_for_removing.push(element);
				element = dom_target_elements.iterateNext();			
			}
		}
		for (var i = 0;i < elements_for_new_list.length;i++){
			var list_element = doc.createElement("li");
			list_element.appendChild(elements_for_new_list[i]);
			list.appendChild(list_element);
			elements_for_new_list[i].setAttribute("class","");
		}

		containers.push(container_element);
		lists.push(list);
		container_element = dom_elements.iterateNext();
		
	}
	for (var i = 0;i < containers.length;i++){
		containers[i].appendChild(lists[i]);
		if (this.clean_container){
			var children = containers[i].children;
			for (var j = 0;j < children.length;j++){
				if (children[j] != lists[i])
					elements_for_removing.push(children[j]);
			}
		}
	}
	for (var i = 0;i < elements_for_removing.length;i++)
		elements_for_removing[i].parentNode.removeChild(elements_for_removing[i]);	
};

ReorganizeRelativeIntoAListRefactoring.prototype.addElementForList = function (xpath){
	this.elements_for_list.push(xpath);
};
