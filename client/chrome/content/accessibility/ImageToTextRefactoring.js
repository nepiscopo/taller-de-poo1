/***************************************
	ImageToTextRefactoring
 */

function ImageToTextRefactoring(name){
	this.name = name;	
};

ImageToTextRefactoring.prototype.adaptDocument = function(doc){
	this.transformImages(doc);
};

ImageToTextRefactoring.prototype.transformImages = function(doc){	
	var images = doc.getElementsByTagName("img");
	var images_for_removing = [];
	for (var i=0;i < images.length;i++){
		var img = images[i];
		if(img.getAttribute("title") != null){
			this.transformImageIntoText(doc,img,img.getAttribute("title"));
			images_for_removing.push(img);
			continue;
		}
		if(img.getAttribute("alt") != null){
			this.transformImageIntoText(doc,img,img.getAttribute("alt"));
			images_for_removing.push(img);
			continue;
		}		
	}	 
	for (var i = 0;i < images_for_removing.length;i++){
		var img = images_for_removing[i];
		img.parentNode.removeChild(img);
	}
};

ImageToTextRefactoring.prototype.transformImageIntoText = function(doc,img,text){
	if ((img.parentNode.tagName == "A") || (img.parentNode.parentNode.tagName == "A"))
		return;
	var txtNode = doc.createTextNode(text);
	img.parentNode.insertBefore(txtNode,img);
};
