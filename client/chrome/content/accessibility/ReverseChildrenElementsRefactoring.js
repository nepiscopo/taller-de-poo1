/***************************************
	ReorderElementChildrenRefactoring
 */

function ReverseChildrenElementsRefactoring(name){
	this.name = name;
	this.container = null;//Un xPath para obtener la tabla
	this.skipFirst = 0;
	this.skipLast = 0;
};

ReverseChildrenElementsRefactoring.prototype.adaptDocument = function(doc){
	try{
	 	//containers = doc.evaluate(this.container, doc, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null).snapshotItem(0);
	 	containers = doc.evaluate(this.container, doc, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
	}
	catch(e){
		return;//alert("exceiption: " + e);//container = doc.evaluate(this.container, doc, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null).snapshotItem(0);
	}
	for (var i = 0; i < containers.snapshotLength; i++) {
    	var container = containers.snapshotItem (i);
  		this.reverseChildren(doc,container);  		 		
	}	
};

ReverseChildrenElementsRefactoring.prototype.reverseChildren = function(doc,container){
	var original_children = container.children;
	var clone_children = [];
	for (var i=0;i < original_children.length;i++){	
		try{
			var clone_child = original_children[i].cloneNode(true);
			clone_children.push(clone_child);
		}catch(e){;}
	}
	while (container.hasChildNodes())
    	container.removeChild(container.firstChild);
    
    var first_children = clone_children.slice(0,this.skipFirst);
    var middle_children = clone_children.slice(this.skipFirst,clone_children.length - this.skipLast);
    var last_children = clone_children.slice(clone_children.length - this.skipLast,clone_children.length);
    
	for (var i=0;i < first_children.length;i++){
		container.appendChild(first_children[i]);
	}
	for (var i=middle_children.length - 1;i >= 0;i--){
		container.appendChild(middle_children[i]);
	}
	for (var i=0;i < last_children.length;i++){
		container.appendChild(last_children[i]);
	}
};

ReverseChildrenElementsRefactoring.prototype.setContainer = function(aXPath){
	this.container = aXPath;
};

ReverseChildrenElementsRefactoring.prototype.setSkipFirst = function(anInteger){
	this.skipFirst = anInteger;
};

ReverseChildrenElementsRefactoring.prototype.setSkipLast = function(anInteger){
	this.skipLast = anInteger;
};

