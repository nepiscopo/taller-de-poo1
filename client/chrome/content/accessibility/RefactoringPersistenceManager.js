function RefactoringPersistenceManager(){
	this.refactorings_data = {};
};

//Singleton
RefactoringPersistenceManager.__instance__ = null;
RefactoringPersistenceManager.getInstance = function (){
	if (RefactoringPersistenceManager.__instance__ == null)
			RefactoringPersistenceManager.__instance__ = new RefactoringPersistenceManager();
	return RefactoringPersistenceManager.__instance__;
};

RefactoringPersistenceManager.prototype.setRefactoringInformation = function (aRefactoring,key,value){
	var refactoring = aRefactoring.constructor + "-" + aRefactoring.name;
	var app = aRefactoring.url;
	if(typeof(this.refactorings_data[refactoring]) == "undefined")
		this.refactorings_data[refactoring] = {};
	if(typeof(this.refactorings_data[refactoring][app]) == "undefined")
		this.refactorings_data[refactoring][app] = {};
	this.refactorings_data[refactoring][app][key] = value;
};

RefactoringPersistenceManager.prototype.getRefactoringInformation = function (aRefactoring){
	var refactoring = aRefactoring.constructor + "-" + aRefactoring.name;
	var app = aRefactoring.url;
	if(typeof(this.refactorings_data[refactoring]) == "undefined"){
		this.refactorings_data[refactoring] = {};
	}
	if(typeof(this.refactorings_data[refactoring][app]) == "undefined")
		this.refactorings_data[refactoring][app] = {};
	return this.refactorings_data[refactoring][app];
}
