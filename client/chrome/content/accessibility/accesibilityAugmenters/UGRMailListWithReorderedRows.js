function getAccessibilityAugmenter(){
	return new UGRMailListWithReorderedRowsWrapper();
};

function UGRMailListWithReorderedRowsWrapper(){
};

UGRMailListWithReorderedRowsWrapper.prototype = new AbstractInstanceRefactoring();

UGRMailListWithReorderedRowsWrapper.prototype.setTargetURLs = function(){
	this.addTargetURL(/https:\/\/goliat[0-9]+.ugr.es\/src\/right_main.ph*/);
};

UGRMailListWithReorderedRowsWrapper.prototype.initRefactoringForPageLoaded = function(doc){
	try{
		var tr = doc.evaluate("/html/body/form/table/tbody/tr", doc, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null).snapshotItem(2);
		if (tr.getElementsByTagName("input").length>0){
			this.app = new ReorderTableRowsRefactoring("UGR WebMail");		               
			this.app.setRowsContainer("/html/body/form/table/tbody");
			this.app.addRowMovement(4,"bottom_shift");
		}
	}
	catch(e){
		;
	}
};
