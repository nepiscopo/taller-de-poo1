function getAccessibilityAugmenter(){
	return new GMailListSplitWrapper();
};

function GMailListSplitWrapper(){
	this.languages = {"es":{},"en":{}};
	this.languages["es"]["folders"] = "Carpetas";
	this.languages["es"]["more"] = "M&aacute;s aplicaciones";
	this.languages["es"]["search"] = "Buscar";
	this.languages["es"]["emails"] = "Correo";
	this.languages["es"]["logout"] = "Salir";
	this.languages["es"]["settings"] = "Configuraci&oacute;n";
	this.languages["es"]["compose"] = "Redactar";
	this.languages["es"]["help"] = "Ayuda";

	this.languages["en"]["folders"] = "Folders";
	this.languages["en"]["more"] = "More applications";
	this.languages["en"]["search"] = "Search";
	this.languages["en"]["emails"] = "E-Mails";
	this.languages["en"]["logout"] = "Logout";
	this.languages["en"]["settings"] = "Settings";
	this.languages["en"]["compose"] = "Compose";
	this.languages["en"]["help"] = "Help";
};

GMailListSplitWrapper.prototype = new AbstractInstanceRefactoring();

GMailListSplitWrapper.prototype.setTargetURLs = function(){
	//this.addTargetURL(/https:\/\/mail.google.com\/mail\/h\/[\w]*\//);	
	this.addTargetURL(/https*:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/&?(st=)?[0-9]*/);
};

GMailListSplitWrapper.prototype.initRefactoringForPageLoaded = function(doc,language){
		this.app = new AccessibleApplication("GMAIL WebMail");

		var folders = new SplitedSection(this.languages[language]["folders"],this.app);		
		folders.addElement("html/body/table[2]/tbody/tr/td[1]/table[1]");
		folders.addElement("html/body/table[2]/tbody/tr/td[1]/table[2]");
		folders.addElement("/html/body/link[2]");		
		folders.addElement("/html/body/style[1]");
		folders.addElementForRemoving("html/body/table/tbody/tr/td[1]/table[1]/tbody/tr[1]");
		
		var options = new SplitedSection(this.languages[language]["more"],this.app);
		//options.addElement(".//*[@id='guser']/ul");
		options.addElement(".//*[@id='gbar']/ul");

		var search = new SplitedSection(this.languages[language]["search"],this.app);
		search.addElement("/html/body/link[2]");		
		search.addElement("/html/body/style[1]");		
		search.addElement("/html/body/table[1]/tbody");
		//search.addElementForRemoving("html/body/table/tbody/tr[2]/tr/td/table/tbody/tr/td[1]/form/input[4]");
		search.addElementForRemoving("html/body/table/tbody/tr[2]/td/table/tbody/tr/td/input[3]");
									  
		var emails = new SplitedSection(this.languages[language]["emails"],this.app);
		emails.addElement("/html/body/link[2]");		
		emails.addElement("/html/body/style[1]");	
		if (content.document.location.href.match(/https:\/\/mail.google.com\/mail\/h\/[\w]*\/\?s=q[\w\W]*$/) != null){
			emails.addElement("/html/body/table[1]/tbody");
			emails.addElementForRemoving("html/body/table/tbody/tr[2]/td/table/tbody/tr/td/input[3]");
		}
		emails.addElementForRemoving("//h2[@class='hdn']");
		emails.addElement("/html/body/table[2]/tbody/tr/td[2]/table");
		emails.addRelatedSplitedSection(folders);

		var site = content.document.location.href.split("?")[0].split("#")[0];
		var logout = new StaticLink(this.languages[language]["logout"], site + "?logout");
		var settings = new StaticLink(this.languages[language]["settings"], site + "?v=prg");
		var compose = new StaticLink(this.languages[language]["compose"], site + "?v=b&pv=tl&cs=b");
		var help = new StaticLink(this.languages[language]["help"],"https://mail.google.com/support/?ctx=mail&hl=en");
		
		emails.addStaticLink(compose);
		emails.addStaticLink(logout);
		
		
		this.app.addStaticLink(compose);
		this.app.addStaticLink(logout);
		this.app.addStaticLink(settings);
		this.app.addStaticLink(help);
	
		this.app.addSplitedSection(emails);
		this.app.addSplitedSection(folders);
		this.app.addSplitedSection(search);
		this.app.addSplitedSection(options);
		this.app.setAsMain(emails);
		this.app.setAsFirstSplitedSection();
};
