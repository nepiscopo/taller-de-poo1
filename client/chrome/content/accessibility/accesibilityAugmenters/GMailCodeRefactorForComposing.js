function getAccessibilityAugmenter(){
	return new GMailCodeRefactoringForComposing();
};

function GMailCodeRefactoringForComposing(){
};

GMailCodeRefactoringForComposing.prototype = new AbstractInstanceRefactoring();

GMailCodeRefactoringForComposing.prototype.setTargetURLs = function(){
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?&v=b[\w\W]*&?(st=)?[0-9]*$/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?v=b[\w\W]*&?(st=)?[0-9]*$/);
};

GMailCodeRefactoringForComposing.prototype.initRefactoringForPageLoaded = function(){
		
		this.app = new CodeRefactoring("GMAIL WebMail");

		//var new_code = '<tr><td><table width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td nowrap="" width="1%" height="25"><form method="GET" name="sf" action="?"><input type="hidden" value="q" name="s"/><label for="searchinput">Buscar</label> <input value="" id="searchinput" name="q" title="Search" maxlength="2048" size="28"/>&nbsp;<input type="submit" value="Search Mail" name="nvp_site_mail"/>&nbsp;<input type="submit" value="Search the Web" name="nvp_site_web"/></form></td><td><font size="1">&nbsp;<a href="?v=as&amp;s=q&amp;q=asd&amp;pv=tl">Show&nbsp;search&nbsp;options</a></font><br><font size="1">&nbsp;<a href="?v=caf&amp;s=q&amp;q=asd&amp;pv=tl">Create&nbsp;a&nbsp;filter</a></font></td></tr></tbody></table></td></tr>';									  
		//this.app.setContainerNode("/html/body/table/tbody/tr[2]");
		//this.app.setNewCode(new_code);
};

GMailCodeRefactoringForComposing.prototype.adaptDocument = function(doc){	
	var to_element = doc.evaluate("html/body/table/tbody/tr[1]/td[1]/table[1]/tbody/tr[1]/td[2]/form/table[2]/tbody/tr[1]/td[1]",doc, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null).snapshotItem(0);
	to_element.innerHTML = "<label for='to'>" + to_element.innerHTML + "</label>";
	var cc_element  = doc.evaluate("html/body/table/tbody/tr[1]/td[1]/table[1]/tbody/tr[1]/td[2]/form/table[2]/tbody/tr[2]/td[1]",doc, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null).snapshotItem(0);
	cc_element.innerHTML = "<label for='cc'>" + cc_element.innerHTML + "</label>";
	var bcc_element  = doc.evaluate("html/body/table/tbody/tr[1]/td[1]/table[1]/tbody/tr[1]/td[2]/form/table[2]/tbody/tr[3]/td[1]",doc, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null).snapshotItem(0);
	bcc_element.innerHTML = "<label for='bcc'>" + bcc_element.innerHTML + "</label>";
	var subject_element  = doc.evaluate("html/body/table/tbody/tr[1]/td[1]/table[1]/tbody/tr[1]/td[2]/form/table[2]/tbody/tr[4]/td[1]",doc, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null).snapshotItem(0);
	var subject_input_element  = doc.evaluate("html/body/table/tbody/tr[1]/td[1]/table[1]/tbody/tr[1]/td[2]/form/table[2]/tbody/tr[4]/td[2]/input[1]",doc, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null).snapshotItem(0);
	subject_input_element.setAttribute("id","subject");
	subject_element.innerHTML = "<label for='subject'>" + subject_element.innerHTML + "</label>";
};

