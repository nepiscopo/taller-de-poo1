function getAccessibilityAugmenter(){
	return new UGRTransformListInRadioButtons();
};

function UGRTransformListInRadioButtons(){
	this.languages = {"es":{},"en":{}};
	this.languages["es"]["from"] = "Desde";
	this.languages["es"]["date"] = "Fecha";
	this.languages["es"]["state"] = "Estado";
	this.languages["es"]["subject"] = "Asunto";
	this.languages["es"]["actions"] = "Acciones";
	this.languages["es"]["markasread"] = "Marcar como leido";
	this.languages["es"]["markasunread"] = "Marcar como no leido";
	this.languages["es"]["delete"] = "Eliminar";

	this.languages["en"]["from"] = "From";
	this.languages["en"]["date"] = "Date";
	this.languages["en"]["state"] = "State";
	this.languages["en"]["subject"] = "Subject";
	this.languages["en"]["actions"] = "Actions";
	this.languages["en"]["markasread"] = "Mark as read";
	this.languages["en"]["markasunread"] = "Mark as unread";
	this.languages["en"]["delete"] = "Delete";
};

UGRTransformListInRadioButtons.prototype = new AbstractInstanceRefactoring();

UGRTransformListInRadioButtons.prototype.setTargetURLs = function(){
	this.addTargetURL(/http[s]+:\/\/goliat[0-9]+.ugr.es\/src\/right_main.ph*/);
};				
	
UGRTransformListInRadioButtons.prototype.initRefactoringForPageLoaded = function(doc,language){
	this.app = new TransformListInRadioButtonsRefactoring("UGR WebMail");
	this.app.addSelectXPath(".//select[@name='targetMailbox']");
	this.app.addSelectXPath(".//select[@name='targetMailbox2']");
};

