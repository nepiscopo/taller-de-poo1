function getAccessibilityAugmenter(){
	return new UGRWebMailWithoutImagesRefactoring();
};

function UGRWebMailWithoutImagesRefactoring(){
};

UGRWebMailWithoutImagesRefactoring.prototype = new AbstractInstanceRefactoring();

UGRWebMailWithoutImagesRefactoring.prototype.setTargetURLs = function(){
	this.addTargetURL(/http[s]+:\/\/goliat[0-9]+.ugr.es\/src\/*/);
};

UGRWebMailWithoutImagesRefactoring.prototype.initRefactoringForPageLoaded = function(){

		this.app = new ImageToTextRefactoring("Correo Electronico UGR", 
											  "https://goliat9.ugr.es/src/webmail.*",
											  [/http[s]+:\/\/goliat[0-9]+.ugr.es\/src\/*/]);
};