function getAccessibilityAugmenter(){
	return new GMailRemoveRedundantOperations();
};

function GMailRemoveRedundantOperations(){

};

GMailRemoveRedundantOperations.prototype = new AbstractInstanceRefactoring();

GMailRemoveRedundantOperations.prototype.setTargetURLs = function(){
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?&v=b[\w\W]*&?(st=)?[0-9]*$/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?v=b[\w\W]*&?(st=)?[0-9]*$/);
};

GMailRemoveRedundantOperations.prototype.initRefactoringForPageLoaded = function(doc){
	
		this.app = new RemoveRedundantOperationRefactoring("UGR WebMail");

		this.app.addRedundantOperation("html/body/table/tbody/tr/td/table[1]/tbody/tr/td[2]/form/table[1]/tbody/tr/td/input");
};
