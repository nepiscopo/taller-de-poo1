function getAccessibilityAugmenter(){
	return new UGRAddressListWithReorderedColumnsWrapper();
};

function UGRAddressListWithReorderedColumnsWrapper(){
};

UGRAddressListWithReorderedColumnsWrapper.prototype = new AbstractInstanceRefactoring();

UGRAddressListWithReorderedColumnsWrapper.prototype.setTargetURLs = function(){
	this.addTargetURL(/https:\/\/goliat[0-9]+.ugr.es\/src\/addressbook.ph*/);
};

UGRAddressListWithReorderedColumnsWrapper.prototype.initRefactoringForPageLoaded = function(){
		this.app = new ReorderTableColumnsRefactoring("UGR WebMail");
		this.app.setColumnContainers("/html/body/form[1]/table[2]/tbody[1]/tr[position()>1]");
		this.app.addColumnMovement(0,"right_shift");
};
