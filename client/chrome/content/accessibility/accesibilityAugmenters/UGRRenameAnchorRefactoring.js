function getAccessibilityAugmenter(){
	return new UGRRenameAnchorRefactoring();
};

function UGRRenameAnchorRefactoring(){
};	
	
UGRRenameAnchorRefactoring.prototype = new AbstractInstanceRefactoring();

UGRRenameAnchorRefactoring.prototype.setTargetURLs = function(){
	this.addTargetURL(/http[s]+:\/\/goliat[0-9]+.ugr.es\/src\/right_main.ph*/);						
};

UGRRenameAnchorRefactoring.prototype.initRefactoringForPageLoaded = function(doc){
	this.app = new RenameAnchorRefactoring("Correo Electr&oacute;nico UGR");
	this.app.addAnchorForRenameByHref("/src/folders.php","Administrar carpetas");
};
