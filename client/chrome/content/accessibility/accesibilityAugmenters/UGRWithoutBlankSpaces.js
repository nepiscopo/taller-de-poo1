function getAccessibilityAugmenter(){
	return new UGRWebMailWithoutBlankSpacesRefactoring();
};

function UGRWebMailWithoutBlankSpacesRefactoring(){
};	
	
UGRWebMailWithoutBlankSpacesRefactoring.prototype = new AbstractInstanceRefactoring();

UGRWebMailWithoutBlankSpacesRefactoring.prototype.setTargetURLs = function(){
	this.addTargetURL(/http[s]+:\/\/goliat[0-9]+.ugr.es\/src\/*/);
};

UGRWebMailWithoutBlankSpacesRefactoring.prototype.initRefactoringForPageLoaded = function(doc){
	
		this.app = new DeleteBlankSpacesRefactoring("Correo Electronico UGR");
								  
};
