function getAccessibilityAugmenter(){
	return new UGRMailSummarizer();
};

function UGRMailSummarizer(){	
};
	
UGRMailSummarizer.prototype = new AbstractInstanceRefactoring();

UGRMailSummarizer.prototype.setTargetURLs = function(){
	this.addTargetURL(/http[s]+:\/\/goliat[0-9]+.ugr.es\/src\/read_body.*/);
};

UGRMailSummarizer.prototype.initRefactoringForPageLoaded = function(){	
		this.app = new InformationResumeRefactoring.InformationResumeRefactoring("Email de UGR", 
											  "https://goliat9.ugr.es/src/read_body.php*",
											  [/http[s]+:\/\/goliat[0-9]+.ugr.es\/src\/read_body.*/]);											  

		this.app.setOriginalNode("/html/body/span/div/div[2]/dl[2]");
		this.app.addToSummary("./dt[2]");//xPath relativo al "Original Node"
		this.app.addToSummary("./dd[3]");//xPath relativo al "Original Node"
};
