function getAccessibilityAugmenter(){
	return new GMailRemoveRedundantOperations();
};

function GMailRemoveRedundantOperations(){
};

GMailRemoveRedundantOperations.prototype = new AbstractInstanceRefactoring();

GMailRemoveRedundantOperations.prototype.setTargetURLs = function(){
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/*&?(st=)?[0-9]*/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/*/);
};

GMailRemoveRedundantOperations.prototype.initRefactoringForPageLoaded = function(doc){
		this.app = new RemoveRedundantOperationRefactoring("UGR WebMail");
		this.app.addRedundantOperation("html/body/table[2]/tbody/tr/td[2]/table[1]/tbody/tr/td[2]/form/table[1]/tbody/tr/td[1]/select[1]");
		this.app.addRedundantOperation("html/body/table[2]/tbody/tr/td[2]/table[1]/tbody/tr/td[2]/form/table[1]/tbody/tr/td[1]/input[@name='nvp_tbu_go']");
		this.app.addRedundantOperation("html/body/table[2]/tbody/tr/td[2]/table[1]/tbody/tr/td[2]/form/table[1]/tbody/tr/td[1]/input[@name='nvp_a_arch']");
		this.app.addRedundantOperation("html/body/table[2]/tbody/tr/td[2]/table[1]/tbody/tr/td[2]/form/table[1]/tbody/tr/td[1]/input[@name='nvp_a_sp']");
		this.app.addRedundantOperation("html/body/table[2]/tbody/tr/td[2]/table[1]/tbody/tr/td[2]/form/table[1]/tbody/tr/td[1]/input[@name='nvp_a_tr']");
		this.app.addRedundantOperation("html/body/table[2]/tbody/tr/td[2]/table[1]/tbody/tr/td[2]/form/table[1]/tbody/tr/td[1]/input[@name='nvp_a_ib']");
		this.app.addRedundantOperation("html/body/table[2]/tbody/tr/td[2]/table[1]/tbody/tr/td[2]/form/table[1]/tbody/tr/td[1]/input[@name='nvp_bu_rl']");
		this.app.addRedundantOperation("html/body/table[2]/tbody/tr/td[2]/table[1]/tbody/tr/td[2]/form/table[1]/tbody/tr/td[1]/input[@name='nvp_a_dl']");
		this.app.addRedundantOperation("html/body/table[2]/tbody/tr/td[2]/table[1]/tbody/tr/td[2]/form/table[1]/tbody/tr/td[1]/input[@name='nvp_a_us']");
		this.app.addRedundantOperation("html/body/table[2]/tbody/tr/td[2]/table[1]/tbody/tr/td[2]/form/table[1]/tbody/tr/td[1]/input[@name='nvp_a_dd']");
		this.app.addRedundantOperation("html/body/table[2]/tbody/tr/td[2]/table[1]/tbody/tr/td[2]/form/table[1]/tbody/tr/td[1]/input[@name='nvp_bu_rs']");
		
		

};
