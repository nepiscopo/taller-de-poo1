function getAccessibilityAugmenter(){
	return new UGRMailListWithReorderedColumnsWrapper();
};

function UGRMailListWithReorderedColumnsWrapper(){
};

UGRMailListWithReorderedColumnsWrapper.prototype = new AbstractInstanceRefactoring();

UGRMailListWithReorderedColumnsWrapper.prototype.setTargetURLs = function(){
	this.addTargetURL(/https:\/\/goliat[0-9]+.ugr.es\/src\/right_main.ph*/);
};

UGRMailListWithReorderedColumnsWrapper.prototype.initRefactoringForPageLoaded = function(){
		this.app = new ReorderTableColumnsRefactoring("UGR WebMail");

		this.app.setColumnContainers("html/body/form/table/tbody/tr[5]/td/table/tbody/tr/td/table/tbody/tr");
		//this.app.addColumnMovement(3,"right_shift");
		this.app.addColumnMovement(0,"right_shift");
};
