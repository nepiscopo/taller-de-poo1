function getAccessibilityAugmenter(){
	return new UGRDistributeMenuAddresses();
};

function UGRDistributeMenuAddresses(){
	this.languages = {"es":{},"en":{}};
	this.languages["es"]["alias"] = "Alias";
	this.languages["es"]["name"] = "Nombre";
	this.languages["es"]["address"] = "Email";
	this.languages["es"]["info"] = "Info";
	this.languages["es"]["actions"] = "Acciones";
	this.languages["es"]["sendemail"] = "Enviar";
	this.languages["es"]["edit"] = "Editar";
	this.languages["es"]["delete"] = "Eliminar";

	this.languages["en"]["alias"] = "Nickname";
	this.languages["en"]["name"] = "Name";
	this.languages["en"]["address"] = "Address";
	this.languages["en"]["info"] = "Info";
	this.languages["en"]["actions"] = "Actions";
	this.languages["en"]["sendemail"] = "Send an e-mail";
	this.languages["en"]["edit"] = "Edit";
	this.languages["en"]["delete"] = "Delete";
};

UGRDistributeMenuAddresses.prototype = new AbstractInstanceRefactoring();

UGRDistributeMenuAddresses.prototype.setTargetURLs = function(){
	this.addTargetURL(/http[s]+:\/\/goliat[0-9]+.ugr.es\/src\/addressbook.php*/);
};
	
UGRDistributeMenuAddresses.prototype.initRefactoringForPageLoaded = function(doc,language){
	this.app = new DistributeMenu("UGR WebMail");
	//this.app.setItemXpath("/html/body/form/table/tbody/tr[5]/td/table/tbody/tr/td/table/tbody/tr");
	this.app.setItemXpath("/html/body/form[1]/table[2]/tbody[1]/tr");
	this.app.setCheckBoxRelativePath("./td[5]/small/input");
		
	var headers = [this.languages[language]["alias"],this.languages[language]["name"],this.languages[language]["address"],this.languages[language]["info"],"",this.languages[language]["actions"]];
	this.app.replaceFirstByHeader("./th",headers);
	
	var send_email = new DistributedOperation(this.languages[language]["sendemail"],this.app);
	send_email.setAction(".//input[@name='emailaddr']");
	var send_email_img = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAFo9M/3AAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGuSURBVHjaYvz//z8DCDCByR9z/wMEECNMhOX760X//3y7wwAQQFCR6xDhH9cZWED0r6eHGT68usLw9+M3BoAAguuBAabrx6f+h2i5/r8kx/s/y5u3XxiubetneP/yLsPnVz8g9olIcjAoKSqCtQAEEIYZ6ABs6+Etnf+Fme4yaDmZwyWOLl/GsGg71EgQkNNTZfj16QMc6zlZE2cFQAARVMDEQACwQKjjQGMEkIQ/MPx4cYyBQ6KYEapAiOHXqy0MbHwQRSBHvr9xnUFSAm4CA8PHhw8Z+OUh7OdXTjAI80kg3HBtWy9QUh5ugaSOBcPFy9eQfXHz/7e7Mxm+fPoDjiK4Qoc5jAS9CRBgBBUQGQyQ+CJVs61POSMLsoC5GQsDm5gVqqofzzE0Hl0+FZwQgAagxhSbmA/D54fbgaE2Hxjux4CaQSH1Hox/fboP1vj55QUGfQNtTC+AwKfnNxieXH3BoGKizvDn81OGO2duMvx8foHhGxMvgypQk6m3F8PprdvgiRnDgD9MHMA06wa07SEDC6800CCQqDqK80GGvL12HbsBlblNJMcCxdEIALlLvT0cl3AsAAAAAElFTkSuQmCC";
	send_email.setImg(send_email_img);

	var edit = new DistributedOperation(this.languages[language]["edit"],this.app);
	edit.setAction(".//input[@name='editaddr']");
	var edit_img = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH1gMaDhES2knn6wAAAZtJREFUOMvFk79LW1EUxz/J9WnjEiGCoHMxb3Ko4KKTi7NQUPHXaNyCYKeCIKKEiJqhgg4pKkTx5Q/oVK1VsohaatISB6Voh5rEJEOIL77rEF98QasZBA8cuPdwv9/7Ped+L7x22MzF0vLiYTKVbKkEpCiKNuYdf19W9Pmnb2SFMeObkiauylxIWayl01dP3u501lFz8JG5XrsEBkoENlupG2Kx2KNgVVWZ7xMMLXznbGeTLW1h9YEC8+BjEfS4GA7s8md7lR+xfzeAqFhBJNB+D44nSEbDAmitSEHQ42JwdofTrysc/U6QimoArd6Qsf+sgkignUH/N+Jfgvw6z5KKalw3j/Bh4tM+YLP/b1iqquJ2uzmzt3G8t0f8Ik3qeJOeyQiGo7HkIbu1BSkNrPvPo/Wc/E2woWlc/gzTMRLmjestuq6bFpBWBUahUCgj8IYMOhtO6Ooe5p1nC1nbRDabRdcLAKLMSEWLVuN0VpcIcrkcmUyGfD6PEAJFUe6yCsAoIxBCrPv80/3W1yi2VUzrkB0Oxxqgv8hvvAX0qNjurJSGMwAAAABJRU5ErkJggg%3D%3D";
	edit.setImg(edit_img);	

	var delete_operation = new DistributedOperation(this.languages[language]["delete"],this.app);
	delete_operation.setAction(".//input[@name='deladdr']");
	var delete_img = "data:image/gif,GIF89a%14%00%14%00%C4%17%00RXR%B9%C9%E9%C3%D4%E8%E6%EE%F8342%CC%D2%D6%98%B6%DF%87%88%87%DB%E7%F3%C3%C7%C0%A3%A4%A1%D9%DC%DA%9D%A6%BCq%8C%95%F4%F3%F5%D7%DE%EBCGH%99%A6%DB%8A%9C%AB%B1%BA%E2Vgu%B7%C0%CCfy%86%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00!%F9%04%01%00%00%17%00%2C%00%00%00%00%14%00%14%00%00%05v%E0%25%8Edi%9Eh%AA%AE%A2%D4X%14%ABPV%E3%C2%14%00%1D(%E0%0C%88%87%200%99D%22%84%C5%09%40%B3%BD(9%08%A1rJP~B%A2!%02%91PO%D7%C6%83%C1%98X%B8_%B0%C32%96%98%D1%A9%2B%9B%E1%3EC%D2%26y%FB%7D%8F%AF%F7vx%25zt%7C%82%24ace%81~%03Y%13%5B%7D(%1257P%00%16%87%24%05%15%9D%9E%9D%05%2C%A2%A3%25!%00%3B";
	delete_operation.setImg(delete_img);
};



