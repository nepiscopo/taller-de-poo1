function getAccessibilityAugmenter(){
	return new UGRMailRemoveMenu();
};

function UGRMailRemoveMenu(){
};

UGRMailRemoveMenu.prototype = new AbstractInstanceRefactoring();

UGRMailRemoveMenu.prototype.setTargetURLs = function(){
	this.addTargetURL(/https:\/\/goliat[0-9]+.ugr.es\/src\/right_main.ph*/);
	this.addTargetURL(/https:\/\/goliat[0-9]+.ugr.es\/src\/*/);
};

UGRMailRemoveMenu.prototype.initRefactoringForPageLoaded = function(doc){
	
		this.app = new RemoveRedundantOperationRefactoring("UGR WebMail");

		this.app.addRedundantOperation("html/body/table/tbody/tr[3]/td[1]");
		this.app.addRedundantOperation("html/body/table/tbody/tr[2]/td[3]");
		this.app.addRedundantOperation("html/body/table/tbody/tr[2]/td[2]");
	
		var mark_ar_read_buttons = doc.evaluate(".//input[@name='markRead']",doc, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
		if (mark_ar_read_buttons.snapshotLength > 1)
			this.app.addRedundantOperation("html/body/form/table/tbody/tr[3]/td/table/tbody/tr/td/table/tbody");
			
};
