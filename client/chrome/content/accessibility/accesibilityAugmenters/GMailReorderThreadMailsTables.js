function getAccessibilityAugmenter(){
	return new GMailReorderThreadMailsWrapper2();
};

function GMailReorderThreadMailsWrapper2(){
};

GMailReorderThreadMailsWrapper2.prototype = new AbstractInstanceRefactoring();

GMailReorderThreadMailsWrapper2.prototype.setTargetURLs = function(){
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/[?]&v=c*&?(st=)?[0-9]*/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/[?]v=c*&?(st=)?[0-9]*/);
};

GMailReorderThreadMailsWrapper2.prototype.initRefactoringForPageLoaded = function(){
	this.app = new ReverseChildrenElementsRefactoring("UGR WebMail");
	//this.app.setContainer("/html/body/table/tbody/tr/td/table/tbody/tr/td[2]");

	this.app.setContainer("/html/body/table/tbody/tr/td/table/tbody/tr/td[2]");
						   
	this.app.setSkipFirst(3);
	this.app.setSkipLast(1);
};
