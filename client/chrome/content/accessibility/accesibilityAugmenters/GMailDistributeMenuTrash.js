function getAccessibilityAugmenter(){
	return new GMailDistributeMenu();
};

function GMailDistributeMenu(){
	this.languages = {"es":{},"en":{}};
	this.languages["es"]["move"] = "Mover a bandeja de entrada";
	this.languages["es"]["delete"] = "Eliminar definitivamente";

	this.languages["en"]["move"] = "Move to Inbox";
	this.languages["en"]["delete"] = "Delete forever";
};

GMailDistributeMenu.prototype = new AbstractInstanceRefactoring();

GMailDistributeMenu.prototype.setTargetURLs = function(){
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?s=t&?(st=)?[0-9]*$/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?&s=t&?(st=)?[0-9]*$/);
};
	
GMailDistributeMenu.prototype.initRefactoringForPageLoaded = function(doc,language){
		this.app = new DistributeMenu("GMAIL WebMail");

		this.app.setItemXpath(".//table[@class='th']/tbody/tr");
		this.app.setCheckBoxRelativePath("./td[4]/input");

		var inbox_operation = new DistributedOperation(this.languages[language]["move"],this.app);
		inbox_operation.setAction(".//input[@name='nvp_a_ib']");
		var inbox_img = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAkRJREFUeNrslb9rFEEUx9/szm72YBMvGCIBk9xpcecqwmFxphBSpU9hkTqQv8T/IF3+gRQRGzurA/FIBAt/FCFyeJWKiTnlfu6P2fG92dtANnMXYpJC8MEwy86+z/f9mJllUkq4TjPgmu3fF+C1dSt9vsEA1nC+e0lmA7u6jfNvJRAPe4zwB7ceLq8WnjxduQy9+Xrn1fcPtY+IfZMVKOcXvKKMY/iyvw+Au8uyLOCcjwVGUQRhGCKAQbFcBmJ8fV8rnwgMd+kMMy3PnS0u9rtd6HY6YNs2+rBzI04FgiAA8iUGGJYnRTiDy0dGhCkIKb2p26U7jBl26/AQfN9XDuRIgyC6ka7Tt+RDvsQgFjGJbVCJRAz33dnCQozl+dVqQb/fVw40UsiokX5HPuRLDGIRk9gcH8hK0/P3FqUQ0G63odfrqaio/q7rguM42vIMBgPoYDnTjCwsKzGIJeovS6oHIunBpONO36QGVyqVU5C93V0wTVMr0MWaP15aOvWOGMRC7mQikGRg5gsFLSSu15NdoltD2Cg/5KqoeJTsUx5gqgHWMWtUXwLpjIQ72Nis2bkcIJcPBZITreqoibRarY7tgc7HwPOD3IwARhphY7M253ljz8FRs3lWAHt2IjBIAnACjCbUCHw7OLj4fYEHFLkqbe5HqgdOOCLdvzK8HpCbCGy+Bdh4BBMBlkiOaOZFjTaFL2Bi6x0KNI5l/tMPeBFsPrOv8j/w+Sc8bxzDFN1mlEpuOLMr4gsctOd99v+nf579EWAAJKNpvBpkyY0AAAAASUVORK5CYII%3D";
		//inbox_operation.setImg(inbox_img);
		
		var delete_operation = new DistributedOperation(this.languages[language]["delete"],this.app);
		delete_operation.setAction(".//input[@name='nvp_a_dl']");
		var delete_img = "data:image/gif,GIF89a%14%00%14%00%C4%17%00RXR%B9%C9%E9%C3%D4%E8%E6%EE%F8342%CC%D2%D6%98%B6%DF%87%88%87%DB%E7%F3%C3%C7%C0%A3%A4%A1%D9%DC%DA%9D%A6%BCq%8C%95%F4%F3%F5%D7%DE%EBCGH%99%A6%DB%8A%9C%AB%B1%BA%E2Vgu%B7%C0%CCfy%86%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00!%F9%04%01%00%00%17%00%2C%00%00%00%00%14%00%14%00%00%05v%E0%25%8Edi%9Eh%AA%AE%A2%D4X%14%ABPV%E3%C2%14%00%1D(%E0%0C%88%87%200%99D%22%84%C5%09%40%B3%BD(9%08%A1rJP~B%A2!%02%91PO%D7%C6%83%C1%98X%B8_%B0%C32%96%98%D1%A9%2B%9B%E1%3EC%D2%26y%FB%7D%8F%AF%F7vx%25zt%7C%82%24ace%81~%03Y%13%5B%7D(%1257P%00%16%87%24%05%15%9D%9E%9D%05%2C%A2%A3%25!%00%3B";
		//delete_operation.setImg(delete_img);
};				
