function getAccessibilityAugmenter(){
	return new UGRWebMailMainFacadeRefactoring();
};

function UGRWebMailMainFacadeRefactoring(){
		this.app = new MainFacadeRefactoring("Correo Electronico UGR", 
											  "https://goliat9.ugr.es/src/webmail.*",
											  [/http[s]+:\/\/goliat[0-9]+.ugr.es\/src\/webmail.*/]);

		var home = new ServiceForMainFacade("Inicio",this.app,"https://goliat9.ugr.es/src/left_main.php");											  
		var folders = new ServiceForMainFacade("Lista de Carpetas",this.app,"https://goliat9.ugr.es/src/left_main.php");		
		var emails = new ServiceForMainFacade("Lista de Emails",this.app,"https://goliat9.ugr.es/src/right_main.php");		
		
		this.app.addServiceForMainFacade(folders);
		this.app.addServiceForMainFacade(emails);
};

UGRWebMailMainFacadeRefactoring.prototype.getApp = function(){
	return this.app;
};

UGRWebMailMainFacadeRefactoring.prototype.adaptDocument = function(doc){
	alert("UGR MAIN FACADE USED");
	this.app.adaptDocument(doc);
}