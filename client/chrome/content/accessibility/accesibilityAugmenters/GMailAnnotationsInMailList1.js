function getAccessibilityAugmenter(){
	return new GMailAnnotationMailListMenu();
};

function GMailAnnotationMailListMenu(){
	this.languages = {"es":{},"en":{}};
	
	this.languages["es"]["from"] = "Remitente: ";
	this.languages["es"]["subject"] = "Asunto: ";
	this.languages["es"]["date"] = "Fecha/hora: ";

	this.languages["en"]["from"] = "From: ";
	this.languages["en"]["subject"] = "Subject: ";
	this.languages["en"]["date"] = "Date/Time: ";
};

GMailAnnotationMailListMenu.prototype = new AbstractInstanceRefactoring();

GMailAnnotationMailListMenu.prototype.setTargetURLs = function(){
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?&?(st=)?[0-9]*$/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?f=[\w\W]*&?(st=)?[0-9]*/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?gausr=[\w\W]*&?(st=)?[0-9]*/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?auth=[\w\W]*&?(st=)?[0-9]*/);
	//this.addTargetURL(/https:\/\/mail.google.com\/mail\/h\/[\w]*\/\#/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?shva=[\w\W]*&?(st=)?[0-9]*/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/&?(st=)?[0-9]*$/);		
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?pli=[\w\W]*&?(st=)?[0-9]*/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?#&?(st=)?[0-9]*$/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?st=[\w\W]*&?(st=)?[0-9]*/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?zy=[\w\W]*&?(st=)?[0-9]*/);

	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?s=a&?(st=)?[0-9]*$/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?&s=a&?(st=)?[0-9]*$/);	

	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?s=d&?(st=)?[0-9]*$/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?&s=d&?(st=)?[0-9]*$/);	

	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?s=l[\w\W]*&?(st=)?[0-9]*$/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?&s=l[\w\W]*&?(st=)?[0-9]*$/);	

	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?s=q[\w\W]*&?(st=)?[0-9]*$/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?&s=q[\w\W]*&?(st=)?[0-9]*$/);	

	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?s=m&?(st=)?[0-9]*$/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?&s=m&?(st=)?[0-9]*$/);	

	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?s=m&?(st=)?[0-9]*$/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?&s=m&?(st=)?[0-9]*$/);

	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?s=r&?(st=)?[0-9]*$/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?&s=r&?(st=)?[0-9]*$/);

	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?s=t&?(st=)?[0-9]*$/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?&s=t&?(st=)?[0-9]*$/);	
};			

GMailAnnotationMailListMenu.prototype.initRefactoringForPageLoaded = function(doc,language){

		this.app = new ContextInformationRefactoring("GMAIL mail list with labels");
							  
		var from_label = new ContextAnnotationRefactoring("/html/body/table/tbody/tr/td/table/tbody/tr/td[2]/form/table[2]/tbody/tr/td[2]",this.languages[language]["from"],this.app);
		var subject_label = new ContextAnnotationRefactoring("/html/body/table/tbody/tr/td/table/tbody/tr/td[2]/form/table[2]/tbody/tr/td[3]",this.languages[language]["subject"],this.app);		
		var date_label = new ContextAnnotationRefactoring("/html/body/table/tbody/tr/td/table/tbody/tr/td[2]/form/table[2]/tbody/tr/td[4]",this.languages[language]["date"],this.app);

};
