function getAccessibilityAugmenter(){
	return new UGRComposeWithReorderedRowsWrapper();
};

function UGRComposeWithReorderedRowsWrapper(){
};

UGRComposeWithReorderedRowsWrapper.prototype = new AbstractInstanceRefactoring();

UGRComposeWithReorderedRowsWrapper.prototype.setTargetURLs = function(){
	this.addTargetURL(/https:\/\/goliat[0-9]+.ugr.es\/src\/compose.ph*/);
};

UGRComposeWithReorderedRowsWrapper.prototype.initRefactoringForPageLoaded = function(){
		this.app = new ReorderTableRowsRefactoring("UGR WebMail");		               
		this.app.setRowsContainer("/html/body/form/table/tbody");		
		this.app.addRowMovement(5,"bottom_shift");
};
