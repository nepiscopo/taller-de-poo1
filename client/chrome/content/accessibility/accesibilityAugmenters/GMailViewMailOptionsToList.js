function getAccessibilityAugmenter(){
	return new GMailViewEmailRemoveRedundantOperation();
};

function GMailViewEmailRemoveRedundantOperation(){
};

GMailViewEmailRemoveRedundantOperation.prototype = new AbstractInstanceRefactoring();

GMailViewEmailRemoveRedundantOperation.prototype.setTargetURLs = function(){
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/[?]&v=c*&?(st=)?[0-9]*/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/[?]v=c*&?(st=)?[0-9]*/);
};

GMailViewEmailRemoveRedundantOperation.prototype.initRefactoringForPageLoaded = function(doc){
			
		this.app = new ReorganizeRelativeIntoAListRefactoring("GMAIL WebMail");

		this.app.setContainerNode("//div[@class='r']");
		this.app.addElementForList("./font/a[1]");
		this.app.addElementForList("./font/a[2]");
		this.app.addElementForList("./font/a[3]");
		this.app.addElementForList("./font/a[4]");
};				
