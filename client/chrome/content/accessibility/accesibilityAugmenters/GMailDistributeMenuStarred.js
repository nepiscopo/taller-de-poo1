function getAccessibilityAugmenter(){
	return new GMailDistributeMenuStarred();
};

function GMailDistributeMenuStarred(){
	this.languages = {"es":{},"en":{}};
	this.languages["es"]["star"] = "Sacar estrella";
	this.languages["es"]["spam"] = "Marcar como spam";

	this.languages["en"]["star"] = "Remove star";
	this.languages["en"]["spam"] = "Mark as Spam";
};

GMailDistributeMenuStarred.prototype = new AbstractInstanceRefactoring();

GMailDistributeMenuStarred.prototype.setTargetURLs = function(){
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?s=r&?(st=)?[0-9]*$/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/\?&s=r&?(st=)?[0-9]*$/);
};

GMailDistributeMenuStarred.prototype.initRefactoringForPageLoaded = function(doc,language){
		this.app = new DistributeMenu("GMAIL WebMail");

		this.app.setItemXpath(".//table[@class='th']/tbody/tr");
		this.app.setCheckBoxRelativePath("./td[4]/input");

		var star_img = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAYAAADEtGw7AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABFpJREFUeNqkVWtoW2UYfs7JSXKSkzS3Nk27NptNLEXXzenK5pxWGF6YCMOCwkRQK/pH6w83xMHYj7mJDMULU0QQLfpDZTLGHOpQOufcaJXp1tnLujal17S5tufkJDmXzzeNtpb+aNUDX76Tl/d7zvM+7+XjGGNYy5OPXyJPDo7qbdxa/IW1OJn5FJuf7IQFBsTAJsYJjlXBubUwLg59xjRXCJyhAIYEZ3jXqsD8ag7M0Nj8fC+kQBSiL4pC+uKapFsVOD97FYIYINEE8A4/mJmCNhdj/xs4N9YJd/39gJYB9AxcdW2QYyfXljxmFJkhTxMbg1T/Sz7eSswmaAuAF0USWi7b3TXITd2M+EcfMLm/H6auw1ZZCX9rK1yNEfj8IXR1necWkpf47XUGLUbAWfpKKaWW0g/9L8Db8DRs7koqDR053Y7MlSRuHD2Cwmwcdp+f3HjoOQVGXkXtI4+iom0nqirry4wt1iDpWAF3dBPFniS2NjpgL++aSmyT0JgIecTAjZf3Q2qIoHrL7VB+v0w+PITaGljrwpj+6gtYKGJ7+5NlYN+tT3GpKx+y7OD38FQFgUKcDrgJXKJdXFgquwkz7x+HKNrRdOgwxPUbMHbwFRT7rqH2hZcgbW0B0zWkCFzcvmNoMXn+5nZo+SrIyXRJBUDto5IYoDVI74MwZhQIo0NwO91Qzn5HCghYt/8AwkePLYBOneuCHhuFV3LA7P45utR5HM8FNj/D4t2vgak9cHNfkwz/SLP8MexWA1Yfj8x7R2BmR1F94A0IVVWY7b6AeMde+GvD4IISzMTs8nLjLFbOF92LzIxMOSRJdCwuXrBQ5zEY2UEIdQzSth2L5zzhDQjecxsMipLlspQz28o6LlWJVSiAM9PLgQMy+KIJPZ5C6HAnXLvaMPHtCUwcfxG2UC1q3zoB/pa7UOztgzXaqK4YQnPjFyC5XKQr6WAs2S3505Ae3oPkm4eg9l5DWmFIv/08XMVpJEvjacseZK+OIBBtgr6z9YcVQyh2po2Fa0ij1Pklo5O+YWlCrvog1GMnkT71JYyAFw4XBSxYYSoq1LQOl6cC3nc+TY7XRO5bxrgwc50Jlnnw+Z/KEjhoVdyJecUJWakBm/sG0r7HEWzeCOXU59ASCUq0AcHpRXD3Vtja9w3HffXP5qYmLy8DzsZ+hLeCWJgUhX8jNcV6DA+kYYQ7EL37QRgFFemBM1AaZYTefRXcXIFq14BOLV/w34EkL92rpZNjMIyleVxQVRY/14F1kRCMxEUMDJuYyG1GXcsTEB0uFLUilTd1lc2OfC6D8V8+QbjiD0Qifljdzbg0VI3tu5/jVtwgqiyjoBm4/utZjOYfgFDTApfkwcjYOIrFIkzTLCfRYqHuE+FpegyTcgrT/T2o405A4x8q3zamyXie5xYZlwzjfT0wSAZPKIqSOafQcKGJV/JhJvu7jxbAed4Cp9Ox4KcmRmhQBRCsb+D+1dX0X54/BRgA3yfmdeJGXmAAAAAASUVORK5CYII%3D";											  
		var star_operation2 = new DistributedOperation(this.languages[language]["star"],this.app);
		star_operation2.setAction(".//input[@name='nvp_bu_rs']");
		//star_operation2.setImg(star_img);
		
		var spam_img = "data:image/png,%89PNG%0D%0A%1A%0A%00%00%00%0DIHDR%00%00%00%10%00%00%00%10%08%06%00%00%00%1F%F3%FFa%00%00%00%01sRGB%00%AE%CE%1C%E9%00%00%00%04gAMA%00%00%B1%8F%0B%FCa%05%00%00%00%20cHRM%00%00z%26%00%00%80%84%00%00%FA%00%00%00%80%E8%00%00u0%00%00%EA%60%00%00%3A%98%00%00%17p%9C%BAQ%3C%00%00%00%18tEXtSoftware%00Paint.NET%20v3.350%EE%B3%9F%00%00%02%40IDAT8O%95%92MH%D3a%1C%C7G%87%0EED%B7%AE%5D%EB%10%81%A7%3A%88%DB%FF%BF%B7%BFN%96%CE9%5BnHSzQ%D1f%84%D3%B5J%3B%A4v%C8%EC%85%3A%04AB%5E%12%14d%98%92H%9E%0Cl%EF%FB%EF%B5%E6Ls%CEMq%ED%ED%F9%F6X%97%1D%B6%B1%1E%F8%C2%8F%2F%BF%CF%E79%3C%8F%40Ppl55%E7%9D%ACPY%D8U%3C%DB%AB%ABO%DB%25%C2%88%9D%15%5E%A9%18*%5Ct%C8%85%D3NN%04%87%5C%D4%FA%DF%02G%3D%23%F7)Y%12n%90%C0%A9%10%19%0F%05.%85%B0%D1%AFbNV%24s50%2BI%BD%02%3F%B5%1C%E8%FC%F6%10r%5E%16%19%5C%8D%8C%D5%AE%3A%7B%B4%AC%C4%D1%2C%BA%E0k%91%90%BCQ%8Bd%97%1A%EE%26%26rO%208%E2Q1u%DEf1%5CM%CCTY%81G%C3%0Cm%DET%82%3CjG~%F4%06%7Cz%19%5C%1AF%C6k%99%AAu%DA%87%0Cup%B7%B0c%25%25%DE%AB%ECR%D2%A2%03%5E%DF%06%994ag%A4%03%9EV%F1%82%5B%CB%9C%89%DEQ%235%D1%05%8FNL%7C%3A%F6RQ%89%B7M%BC%95z%D5%09%CC%DE%07V%C6%90%5B%7B%06%FF%AD%3A%E2%D5%89%EA7G%AF%81X%87%F1kX%07%BE%8D%FDPT%C0%B7%8B3%E9%8F%FD%80m%1C%88%BD%07%C9%CCakr%00%BCA%3C%9B%98%7F%08%F0%2F%91%99%B3%80%EE%AD%17%17%5C%97%C4%D3%8B%16%20%F1%0E%C8%2F%01%24%88%7D%9B%15%FE%EE%DA%5Cv%EF3%F0%7B%0A%C4%F3%14%81%1E%EE%A0%A8%C0%D7)YN%CE%0FPx%86%C2%01%00qdb%3C%B6%A7'%40%C8%06%ED%17%40bo%10%EC%E5%A2E%05%FEni%CF%C6s%3D%85%AD%14%8E%D2%EC%22%9F%DDF.Ea%C4h%FF%05%E9%F08%02%BD%D2OE%05%113w%2C%D0'%F3%EF%AF%BD%A0%40%84f%8F%DE%FC%2F%7F%05X%C6%CEL%1F%02Fi%7F%C9%A7%0C%DE%95%9E%0B%0D%D6~%8F%CF%3FA.%C1S(I%B3%83%FC%C1*v%17%87%106q_%5D%7D%17O%94%FDPAs%F5%A9%A0I%FE%20%3C%C89~%3CVg%23%23%9AL%C8%CC%7D%0B%0F%C8%FAW%3B%AA%8E%97%82%FF%00%A6%EBsk%1D%8C%14%02%00%00%00%00IEND%AEB%60%82";
		var spam_operation2 = new DistributedOperation(this.languages[language]["spam"],this.app);
		spam_operation2.setAction(".//input[@name='nvp_a_sp']");		
		//spam_operation2.setImg(spam_img);
};
