function getAccessibilityAugmenter(){
	return new UGRWebMailOptionsToListRefactoring();
};

function UGRWebMailOptionsToListRefactoring(){
};

UGRWebMailOptionsToListRefactoring.prototype = new AbstractInstanceRefactoring();

UGRWebMailOptionsToListRefactoring.prototype.setTargetURLs = function(){
	this.addTargetURL(/http[s]+:\/\/goliat[0-9]+.ugr.es\/src\/right_main.ph*/);
};

UGRWebMailOptionsToListRefactoring.prototype.initRefactoringForPageLoaded = function(){
		this.app = new ReorganizeIntoAListRefactoring("Opciones UGR", 
											  "https://goliat9.ugr.es/src/right_main.*",
											  [/http[s]+:\/\/goliat[0-9]+.ugr.es\/src\/right_main.ph*/]);											  

		this.app.setContainerNode("html/body/table/tbody");
		this.app.addElementForList("html/body/table/tbody/tr[3]/td[1]/a");							 		 
		this.app.addElementForList("html/body/table/tbody/tr[2]/td[3]/b/a");
};
