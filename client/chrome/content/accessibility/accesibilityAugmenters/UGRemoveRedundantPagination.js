function getAccessibilityAugmenter(){
	return new UGRMailRemoveMenu();
};

function UGRMailRemoveMenu(){
};

UGRMailRemoveMenu.prototype = new AbstractInstanceRefactoring();

UGRMailRemoveMenu.prototype.setTargetURLs = function(){
	this.addTargetURL(/https:\/\/goliat[0-9]+.ugr.es\/src\/right_main.ph*/);
};

UGRMailRemoveMenu.prototype.initRefactoringForPageLoaded = function(){	
		this.app = new RemoveRedundantOperationRefactoring("UGR WebMail", 
											  "https://goliat*.ugr.es/src/right_main.ph*",
											  [/https:\/\/goliat[0-9]+.ugr.es\/src\/right_main.ph*/]);

};

UGRMailRemoveMenu.prototype.adaptDocument = function(doc){
	var xpath_container = "html/body/form/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td";
	var dom_elements = doc.evaluate(xpath_container, doc, null, XPathResult.ANY_TYPE, null);
	var element = dom_elements.iterateNext();	
	var anchors = element.getElementsByTagName("a");
	var anchors_for_removing = [];
	for(var i=0;i < anchors.length;i++)
		if((anchors[i].innerHTML.match("[0-9]*") != "") || (anchors[i].innerHTML == "Cambiar todos"))
			anchors_for_removing.push(anchors[i]);
	for(var i=0;i < anchors_for_removing.length;i++)
		anchors_for_removing[i].parentNode.removeChild(anchors_for_removing[i]);
};
