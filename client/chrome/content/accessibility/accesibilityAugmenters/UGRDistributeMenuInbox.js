function getAccessibilityAugmenter(){
	return new UGRDistributeMenuInbox();
};

function UGRDistributeMenuInbox(){
	this.languages = {"es":{},"en":{}};
	this.languages["es"]["from"] = "Desde";
	this.languages["es"]["date"] = "Fecha";
	this.languages["es"]["state"] = "Estado";
	this.languages["es"]["subject"] = "Asunto";
	this.languages["es"]["actions"] = "Acciones";
	this.languages["es"]["markasread"] = "Marcar como leido";
	this.languages["es"]["markasunread"] = "Marcar como no leido";
	this.languages["es"]["delete"] = "Eliminar";

	this.languages["en"]["from"] = "From";
	this.languages["en"]["date"] = "Date";
	this.languages["en"]["state"] = "State";
	this.languages["en"]["subject"] = "Subject";
	this.languages["en"]["actions"] = "Actions";
	this.languages["en"]["markasread"] = "Mark as read";
	this.languages["en"]["markasunread"] = "Mark as unread";
	this.languages["en"]["delete"] = "Delete";
};

UGRDistributeMenuInbox.prototype = new AbstractInstanceRefactoring();

UGRDistributeMenuInbox.prototype.setTargetURLs = function(){
	this.addTargetURL(/http[s]+:\/\/goliat[0-9]+.ugr.es\/src\/right_main.php*/);
};				
	
UGRDistributeMenuInbox.prototype.initRefactoringForPageLoaded = function(doc,language){
	this.app = new DistributeMenu("UGR WebMail");
	this.app.setItemXpath("/html/body/form/table/tbody/tr[4]/td/table/tbody/tr/td/table/tbody/tr");
	this.app.setCheckBoxRelativePath("./td[5]/input");
	
	var headers = [this.languages[language]["from"],this.languages[language]["date"],this.languages[language]["state"],this.languages[language]["subject"],"",this.languages[language]["actions"]];
	this.app.replaceFirstByHeader("./td",headers);
	
	var mark_as_read = new DistributedOperation(this.languages[language]["markasread"],this.app);
	mark_as_read.setAction(".//input[@name='markRead']");
	var mark_as_read_img = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAFo9M/3AAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIUSURBVHjaYvz//z8DCDCBiNbeff8BAogRJsIyde75/z9+vGcACCAGkMiUOeeA1Pv/1S1r/4OVHP/A8P/a/R8Mx4/dZwAIILgeFABS/v/7WrAM45ylx/7/+M7B8OHDe4bv7z8wML14/p1B0duQ4bumEcOLF+8ZAAIIbAYjIyNYK8g2Ds4fDB/ef2coyXEGC4IVzF12/H+y130GBpC7vj9n+PDiBVhDz1YvBhYQA2RH71IJhujsKLDEOU4GhnVTjjNw/HjOgN2ZSAAggAgqYEBWAPISyKEocjAFYM+D8bX//59P+Q9TALHiwzKIKpAv3l8HBsoPMFfAcg4jOCKOMwBdzwH0yVpNhtZtQQwCAhwM+zh6EI7c9oLhvybHDwZOoMT9DwwMoJi4//wHQ4uXACM4HBje/2B4IckBlGBgAIXRc6CqHyCOlyXhcAAIMMLhQAAwwU0CRigMg9IT0OtI+DoQb/tf07ruP7I6eGDCXAFJe9f+I8IUioGJ6//7peDw/X+vGpw2YXpZkGMJGNBA1nMgvg8R/PEDiiGpCCL2gYGTQ4AhuxhsCCPcAFCKCk42ZNh2/QfD8/PnGZy8DBEe5WBgOA8M9m1rrzN4BWsySDhxMDxfug01EHun7gMzgqKtwPEFAqA4g8YSAzAjgKMIlC1ebNsGspFhzpxkJBf8ACcGhmVzjzF8/wHVCdQETTUMCHU/qBuNAI4BSpFqmp+3AAAAAElFTkSuQmCC";
	mark_as_read.setImg(mark_as_read_img);
	
	var mark_as_unread = new DistributedOperation(this.languages[language]["markasunread"],this.app);
	mark_as_unread.setAction(".//input[@name='markUnread']");
	var mark_as_unread_img = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAFo9M/3AAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGuSURBVHjaYvz//z8DCDCByR9z/wMEECNMhOX760X//3y7wwAQQFCR6xDhH9cZWED0r6eHGT68usLw9+M3BoAAguuBAabrx6f+h2i5/r8kx/s/y5u3XxiubetneP/yLsPnVz8g9olIcjAoKSqCtQAEEIYZ6ABs6+Etnf+Fme4yaDmZwyWOLl/GsGg71EgQkNNTZfj16QMc6zlZE2cFQAARVMDEQACwQKjjQGMEkIQ/MPx4cYyBQ6KYEapAiOHXqy0MbHwQRSBHvr9xnUFSAm4CA8PHhw8Z+OUh7OdXTjAI80kg3HBtWy9QUh5ugaSOBcPFy9eQfXHz/7e7Mxm+fPoDjiK4Qoc5jAS9CRBgBBUQGQyQ+CJVs61POSMLsoC5GQsDm5gVqqofzzE0Hl0+FZwQgAagxhSbmA/D54fbgaE2Hxjux4CaQSH1Hox/fboP1vj55QUGfQNtTC+AwKfnNxieXH3BoGKizvDn81OGO2duMvx8foHhGxMvgypQk6m3F8PprdvgiRnDgD9MHMA06wa07SEDC6800CCQqDqK80GGvL12HbsBlblNJMcCxdEIALlLvT0cl3AsAAAAAElFTkSuQmCC";
	mark_as_unread.setImg(mark_as_unread_img);
	var delete_operation = new DistributedOperation(this.languages[language]["delete"],this.app);
	delete_operation.setAction(".//input[@name='delete']");
	var delete_img = "data:image/gif,GIF89a%14%00%14%00%C4%17%00RXR%B9%C9%E9%C3%D4%E8%E6%EE%F8342%CC%D2%D6%98%B6%DF%87%88%87%DB%E7%F3%C3%C7%C0%A3%A4%A1%D9%DC%DA%9D%A6%BCq%8C%95%F4%F3%F5%D7%DE%EBCGH%99%A6%DB%8A%9C%AB%B1%BA%E2Vgu%B7%C0%CCfy%86%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00!%F9%04%01%00%00%17%00%2C%00%00%00%00%14%00%14%00%00%05v%E0%25%8Edi%9Eh%AA%AE%A2%D4X%14%ABPV%E3%C2%14%00%1D(%E0%0C%88%87%200%99D%22%84%C5%09%40%B3%BD(9%08%A1rJP~B%A2!%02%91PO%D7%C6%83%C1%98X%B8_%B0%C32%96%98%D1%A9%2B%9B%E1%3EC%D2%26y%FB%7D%8F%AF%F7vx%25zt%7C%82%24ace%81~%03Y%13%5B%7D(%1257P%00%16%87%24%05%15%9D%9E%9D%05%2C%A2%A3%25!%00%3B";
	delete_operation.setImg(delete_img);
};

