function getAccessibilityAugmenter(){
	return new UGRMailMainWithoutFramesWrapper();
};

function UGRMailMainWithoutFramesWrapper(){
};


UGRMailMainWithoutFramesWrapper.prototype = new AbstractInstanceRefactoring();

UGRMailMainWithoutFramesWrapper.prototype.setTargetURLs = function(){
	this.addTargetURL(/https:\/\/goliat[0-9]+.ugr.es\/src\/webmail.php*/);
	//this.addTargetURL(/https:\/\/goliat[0-9]+.ugr.es\/src\//src/left_main.php*/);
};

UGRMailMainWithoutFramesWrapper.prototype.initRefactoringForPageLoaded = function(doc){
	
		this.app = new UnframeApplication("Correo Electr&oacute;nico UGR", 
											  "https://goliat*.ugr.es/src/webmail.php",
											  [/https:\/\/goliat[0-9]+.ugr.es\/src\/webmail.php*/]);

		
		var site = "https://" + doc.location.host;
		
		var folders = new TargetFrame("Otras Carpetas",this.app,".//*[@id='fs1']/frame[1]",site + "/src/left_main.php*");
		var emails = new TargetFrame("Carpeta de Entrada",this.app,".//*[@id='fs1']/frame[2]",null);
		var options = new MergedFrameMock("Otras Opciones",this.app);
		options.addTargetElement("Componer Email",site + "/src/compose.php");
		options.addTargetElement("Direcciones",site + "/src/addressbook.php");
		options.addTargetElement("Gesti&oacute;n de Carpetas",site + "/src/folders.php");
		options.addTargetElement("Configuraci&oacute;n",site + "/src/options.php");
		options.addTargetElement("Buscar",site + "/src/search.php");
		options.addTargetElement("Desconectar",site + "/src/signout.php");		
		
		this.app.addTargetFrame(emails);
		this.app.addTargetFrame(folders);		
		this.app.addMergedFrameMock(options);
};
