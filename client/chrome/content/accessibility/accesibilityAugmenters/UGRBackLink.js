function getAccessibilityAugmenter(){
	return new UGRWithBackLink();
};

function UGRWithBackLink(){
};	
	
UGRWithBackLink.prototype = new AbstractInstanceRefactoring();

UGRWithBackLink.prototype.setTargetURLs = function(){
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\//);
	this.addTargetURL(/http[s]+:\/\/goliat[0-9]+.ugr.es\/src\/compose.php/);
	this.addTargetURL(/http[s]+:\/\/goliat[0-9]+.ugr.es\/src\/addressbook.php/);
	this.addTargetURL(/http[s]+:\/\/goliat[0-9]+.ugr.es\/src\/options.php/);
	this.addTargetURL(/http[s]+:\/\/goliat[0-9]+.ugr.es\/src\/search.php/);
	this.addTargetURL(/http[s]+:\/\/goliat[0-9]+.ugr.es\/src\/folders.php/);	
	this.addTargetURL(/http[s]+:\/\/goliat[0-9]+.ugr.es\/src\/help.php/);
	this.addTargetURL(/http[s]+:\/\/goliat[0-9]+.ugr.es\/src\/read_body.php/);
};

UGRWithBackLink.prototype.initRefactoringForPageLoaded = function(doc){
	this.app = new BackLinkRefactoring("Correo Electronico UGR",null);
};
