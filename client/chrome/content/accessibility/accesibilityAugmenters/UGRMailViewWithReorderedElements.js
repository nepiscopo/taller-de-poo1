function getAccessibilityAugmenter(){
	return new UGRComposeWithReorderedRowsWrapper();
};

function UGRComposeWithReorderedRowsWrapper(){
};

UGRComposeWithReorderedRowsWrapper.prototype = new AbstractInstanceRefactoring();

UGRComposeWithReorderedRowsWrapper.prototype.setTargetURLs = function(){
	this.addTargetURL(/https:\/\/goliat[0-9]+.ugr.es\/src\/read_body.ph*/);
};

UGRComposeWithReorderedRowsWrapper.prototype.initRefactoringForPageLoaded = function(){
		this.app = new ReorderElementChildrenRefactoring("UGR WebMail");
		this.app.setContainer("/html/body");
		this.app.addMovement(4,"bottom_shift");
};
