function getAccessibilityAugmenter(){
	return new GMailOptionsToListRefactoring();
};

function GMailOptionsToListRefactoring(){
};

GMailOptionsToListRefactoring.prototype = new AbstractInstanceRefactoring();

GMailOptionsToListRefactoring.prototype.setTargetURLs = function(){
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/*(st=)*(0-9)*/);
};

GMailOptionsToListRefactoring.prototype.initRefactoringForPageLoaded = function(doc){	

		this.app = new ReorganizeIntoAListRefactoring("GMAIL WebMail");

		this.app.setContainerNode("//*[@id='gbar']");
		this.app.addElementForList(".//*[@id='gbar']/nobr/b");
		this.app.addElementForList(".//*[@id='gbar']/nobr/a[1]");							 		 
		this.app.addElementForList(".//*[@id='gbar']/nobr/a[2]");
		this.app.addElementForList(".//*[@id='gbar']/nobr/a[3]");
		this.app.addElementForList(".//*[@id='gbar']/nobr/a[4]");
		this.app.addElementForList(".//*[@id='gbar']/nobr/a[5]");
		this.app.addElementForList(".//*[@id='gbar']/nobr/a[6]");

};				
