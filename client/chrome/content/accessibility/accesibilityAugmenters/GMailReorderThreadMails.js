function getAccessibilityAugmenter(){
	return new GMailReorderThreadMailsWrapper();
};

function GMailReorderThreadMailsWrapper(){
};

GMailReorderThreadMailsWrapper.prototype = new AbstractInstanceRefactoring();

GMailReorderThreadMailsWrapper.prototype.setTargetURLs = function(){
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/[?]&v=c*&?(st=)?[0-9]*/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/[?]v=c*&?(st=)?[0-9]*/);
};

GMailReorderThreadMailsWrapper.prototype.initRefactoringForPageLoaded = function(){
	this.app = new ReverseChildrenElementsRefactoring("UGR WebMail");
	//this.app.setContainer("/html/body/table/tbody/tr/td/table/tbody/tr/td[2]/table[4]/tbody/tr/td");	
	this.app.setContainer("/html/body/table/tbody/tr/td/table/tbody/tr/td[2]/table[position()>3][position()< last()]/tbody/tr/td");	
	//this.app.setContainer("/html/body/table[2]/tbody/tr/td[2]/table/tbody/tr/td[2]");
	//this.app.setContainer("/html/body/table/tbody/tr/td/table/tbody/tr/td[2]");
	
};
