function getAccessibilityAugmenter(){
	return new GMailMailViewWithReorderedRows();
};

function GMailMailViewWithReorderedRows(){
};

GMailMailViewWithReorderedRows.prototype = new AbstractInstanceRefactoring();

GMailMailViewWithReorderedRows.prototype.setTargetURLs = function(){
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/[?]&v=c*&?(st=)?[0-9]*/);
	this.addTargetURL(/https:\/\/mail.google.com\/mail[\w|\W|0-9|\/]*\/h\/[\w]*\/[?]v=c*&?(st=)?[0-9]*/);
};

GMailMailViewWithReorderedRows.prototype.initRefactoringForPageLoaded = function(doc){
	
		this.app = new ReorderTableRowsRefactoring("Gmail WebMail");

};

GMailMailViewWithReorderedRows.prototype.adaptDocument = function(doc){		
	var response_sections = doc.getElementsByClassName('r');
	var duplicated = null;
	for(var i = 0;i < response_sections.length;i++){
		var last_response_section = response_sections[i];
		var tr_container = last_response_section.parentNode.parentNode;
		var cloned = tr_container.cloneNode(true);
	
		for(var j = 0;j < tr_container.parentNode.children.length;j++){			
			var child = tr_container.parentNode.children[j];
			var item_anchors = child.getElementsByTagName("a");
			if((tr_container == child) || (item_anchors.length == 0))
				continue;
			if(item_anchors.length >= 5 && item_anchors[0].parentNode.nodeName == "FONT"){
				if(item_anchors[0].parentNode.parentNode.nodeName == "TD")
					duplicated = child;
			}
		}
		if(duplicated != null){
			tr_container.parentNode.removeChild(duplicated);
			duplicated = null;
		}
		tr_container.parentNode.appendChild(cloned);
		tr_container.parentNode.removeChild(tr_container);
		duplicated = null;
	}
};
