function getAccessibilityAugmenter(){
	return new UGRMailListSplitWrapper();
};

function UGRMailListSplitWrapper(){
	this.languages = {"es":{},"en":{}};
	this.languages["es"]["other_options"] = "Otras Opciones";
	this.languages["es"]["emails"] = "Ver Mensajes";
	this.languages["es"]["folders"] = "Carpetas";
	this.languages["es"]["compose"] = "Componer";
	this.languages["es"]["logout"] = "Cerrar Sesi&oacute;n";	
	this.languages["en"]["other_options"] = "Other Options";
	this.languages["en"]["emails"] = "E-Mails";
	this.languages["en"]["folders"] = "Folders";
	this.languages["en"]["compose"] = "Compose";
	this.languages["en"]["logout"] = "Logout";
};

UGRMailListSplitWrapper.prototype = new AbstractInstanceRefactoring();

UGRMailListSplitWrapper.prototype.setTargetURLs = function(){
	this.addTargetURL(/http[s]+:\/\/goliat[0-9]+.ugr.es\/src\/right_main.php/);
};

UGRMailListSplitWrapper.prototype.initRefactoringForPageLoaded = function(doc,language){
		this.app = new AccessibleApplication("UGR WebMail");
		
		var options = new SplitedSection(this.languages[language]["other_options"],this.app);
		options.addElement("html/body/table/tbody");
		options.addElementForRemoving("html/body/tbody/tr");
		options.addElement("/html/body/table/tbody/tr[2]/td[3]/b/a");
				
		var emails = new SplitedSection(this.languages[language]["emails"],this.app);
		emails.addElement("html/body/table/tbody/tr[2]/td[2]");		
		emails.addElement("html/body/form");
		//emails.addElementForRemoving("html/body/form/table/tbody/tr[7]/td/table/tbody/tr/td/table/tbody");
		emails.addRelatedSplitedSection(options);

		var site = "https://" + content.document.location.host;
		var home = new StaticLink(this.languages[language]["folders"],site + "/src/left_main.php");
		var compose = new StaticLink(this.languages[language]["compose"],site + "/src/compose.php");
		var logout = new StaticLink(this.languages[language]["logout"],site + "/src/signout.php");
		
		this.app.addStaticLink(home);
		this.app.addSplitedSection(emails);		
		this.app.addSplitedSection(options);
		this.app.setAsMain(emails);
		this.app.addStaticLink(compose);
		this.app.addStaticLink(logout);
};

