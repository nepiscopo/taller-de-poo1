function getAccessibilityAugmenter(){
	return new UGRWithoutTargetAttribute();
};

function UGRWithoutTargetAttribute(){
};	
	
UGRWithoutTargetAttribute.prototype = new AbstractInstanceRefactoring();

UGRWithoutTargetAttribute.prototype.setTargetURLs = function(){
	this.addTargetURL(/http[s]+:\/\/goliat[0-9]+.ugr.es\/src\/left_main.ph*/);						
};

UGRWithoutTargetAttribute.prototype.initRefactoringForPageLoaded = function(doc){
		this.app = new AnchorsWithoutTargetAttribute("Correo Electr&oacute;nico UGR");
};
