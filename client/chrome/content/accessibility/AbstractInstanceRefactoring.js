
function AbstractInstanceRefactoring(){
	this.url_expressions = [];
	this.excluded_url_expressions = [];
	this.abstract_refactoring = null;
	this.language = {};
};				

AbstractInstanceRefactoring.prototype.addTargetURL = function(url){
	this.url_expressions.push(url);
};

AbstractInstanceRefactoring.prototype.addExcludedURL = function(url){
	this.excluded_url_expressions.push(url);
};

AbstractInstanceRefactoring.prototype.isPageATarget = function(url){
	var isURLaTarget = false;
	if(typeof(url) != "string"){
		return false;
	}
	for (var i=0;i < this.url_expressions.length;i++){
		if (url.match(this.url_expressions[i]) != null){
			isURLaTarget = true;
		}
	}
	if (isURLaTarget){
		for (var i=0;i < this.excluded_url_expressions.length;i++){
			if (url.match(this.excluded_url_expressions[i]) != null){
				isURLaTarget = false;
			}
	}	
	}
	return isURLaTarget;
};	

AbstractInstanceRefactoring.prototype.initRefactoringForPageLoaded = function(doc){
	return;
};

AbstractInstanceRefactoring.prototype.initialize = function(doc){
	alert("#initialize must be a redefined in the subclass");
};
	
AbstractInstanceRefactoring.prototype.getAbstractRefactoring = function(){
	return this.abstract_refactoring;
};

AbstractInstanceRefactoring.prototype.adaptDocument = function(doc){
	try{
		this.abstract_refactoring.adaptDocument(doc);
	}catch(e){;}
};

AbstractInstanceRefactoring.prototype.setTargetURLs = function(doc){
	alert("#setTargetURLs must be redefined in the subclass");
};

AbstractInstanceRefactoring.prototype.isGeneric = function(){
	return false;
};

AbstractInstanceRefactoring.prototype.isInstantiated = function(){
	return true;
};

AbstractInstanceRefactoring.prototype.install = function(manager,file){
	manager.installInstantiatedRefactoring(file);
};


