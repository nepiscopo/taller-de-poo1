/***************************************
	BackLinkRefactoring
 */

function BackLinkRefactoring(name,container){
	this.name = name;
	this.container = container;
	this.use_history = false;
	this.add_link_on_top = false;
	this.add_link_on_bottom = true;
	
	this.languages = {"es":{},"en":{}};
	this.languages["es"]["back"] = "Volver";
	this.languages["en"]["back"] = "Back";

	this.current_language = this.languages["es"];
};

BackLinkRefactoring.prototype.setLanguage = function(language){
	try{
		this.current_language = this.languages[language];
	}
	catch(e){;}
};

BackLinkRefactoring.prototype.adaptDocument = function(doc){
	if (!this.container)
		var container = doc.createElement("div");
	var anchor = doc.createElement("a");	
	anchor.setAttribute("href","javascript:history.go(-1);");
	anchor.innerHTML = this.current_language["back"]; 
	container.insertBefore(anchor,container.firstChild);
	if(this.add_link_on_top)
		doc.body.insertBefore(container,doc.body.firstChild);
	if(this.add_link_on_bottom)
		doc.body.appendChild(container);	
};
