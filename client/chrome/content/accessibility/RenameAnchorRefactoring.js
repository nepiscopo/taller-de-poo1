/***************************************
	RenameAnchorRefactoring
 */

function RenameAnchorRefactoring(name){
	this.name = name;
	this.anchors_for_rename_with_xpath = [];
	this.anchors_for_rename_with_href = [];
};

RenameAnchorRefactoring.prototype.adaptDocument = function(doc){
	this.renameAnchorsByXPath(doc);
	this.renameAnchorsByHref(doc);
};

RenameAnchorRefactoring.prototype.renameAnchor = function(anchor,label){
	anchor.innerHTML = label;
};

RenameAnchorRefactoring.prototype.renameAnchorsByXPath = function(doc){
	for (var i = 0; i < this.anchors_for_rename_with_xpath.length;i++){
		var anchor = doc.evaluate(this.anchors_for_rename_with_xpath[i]["anchor"], doc, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null).snapshotItem(0);
		try{
			this.renameAnchor(anchor,this.anchors_for_rename_with_xpath[i]["label"]);
		}
		catch(e){
			continue;
		}
	}
};

RenameAnchorRefactoring.prototype.renameAnchorsByHref = function(doc){
	var anchors = doc.getElementsByTagName("a");
	for (var i = 0; i < this.anchors_for_rename_with_href.length;i++){
		var target_href = this.anchors_for_rename_with_href[i]["anchor"];
		for (var j = 0; j < anchors.length;j++){
			var is = anchors[j].getAttribute("href") == target_href
			if (anchors[j].getAttribute("href") == target_href){
				try{
					this.renameAnchor(anchors[j],this.anchors_for_rename_with_href[i]["label"]);
				}
				catch(e){
					continue;
				}
			}
		}
	}
};

RenameAnchorRefactoring.prototype.addAnchorForRenameByXpath = function(anchor_xpath,label){
	var target_anchor = {"anchor":anchor_xpath,"label":label};
	this.anchors_for_rename_with_xpath.push(target_anchor);
};

RenameAnchorRefactoring.prototype.addAnchorForRenameByHref = function(anchor_href,label){
	var target_anchor = {"anchor":anchor_href,"label":label};
	this.anchors_for_rename_with_href.push(target_anchor);
};
