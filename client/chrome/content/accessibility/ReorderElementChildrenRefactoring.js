/***************************************
	ReorderElementChildrenRefactoring
 */

function ReorderElementChildrenRefactoring(name){
	this.name = name;
	this.container = null;//Un xPath para obtener la tabla
	this.movements = [];
};

ReorderElementChildrenRefactoring.prototype.adaptDocument = function(doc){
	var table;
	try{
		//var table = doc.evaluate(this.row_container, doc, null, XPathResult.ANY_TYPE, null).snashopItem(0);		
		 container = doc.evaluate(this.container, doc, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null).snapshotItem(0);
	}
	catch(e){alert(e);return;}
	this.moveChildren(doc,container);
};

ReorderElementChildrenRefactoring.prototype.moveChildren = function(doc,container){		
	for (var i=0;i < this.movements.length;i++){		
		var movement = this.movements[i];
		var target_child;
		try{
			var children = container.children;
			target_child = children[movement["position"]];
		}catch(e){;}
		
		try{
			container.removeChild(target_child);
		}catch(e){;}		
		var new_child = target_child.cloneNode(true);
		//Aca vendria un insertBefore cuando el refactoring soporte la especificacion del corriemiento
		container.appendChild(target_child);
	}
};

ReorderElementChildrenRefactoring.prototype.setContainer = function(aXPath){
	this.container = aXPath;
};

ReorderElementChildrenRefactoring.prototype.addMovement = function(number_of_row,shift){
	var movement = {"position":number_of_row,"shift":shift};
	this.movements.push(movement);
};

