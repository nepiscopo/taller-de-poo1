/***************************************
	InformationResumeRefactoring
 */

function ContextInformationRefactoring(name){
	this.name = name;
	this.contextual_annotations = [];
};

ContextInformationRefactoring.prototype.adaptDocument = function(doc){
	for (var i = 0;i < this.contextual_annotations.length;i++)
		this.contextual_annotations[i].decorate(doc);
};

ContextInformationRefactoring.prototype.addContextualAnnotation = function (aContextAnnotationInstance){
	this.contextual_annotations.push(aContextAnnotationInstance);
};


/****
Anotaciones
*/

function ContextAnnotationRefactoring(xpath,label,contect_annotation_manager){
	this.label = label;
	this.xpath_element = xpath;	
	contect_annotation_manager.addContextualAnnotation(this);
};

ContextAnnotationRefactoring.prototype.addAnnotation = function (doc,target){
	var new_label = doc.createElement("spam");
	new_label.innerHTML = this.label;
	if (typeof(target.firstChild) != "undefined"){
		target.insertBefore(new_label,target.firstChild);
	}
	else{
		target.innerHTML = new_label.innerHTML + " " + target.innerHTML;
	}
};

ContextAnnotationRefactoring.prototype.decorate = function (doc){
	var elements = doc.evaluate(this.xpath_element, doc, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
    for (var i = 0; i < elements.snapshotLength; i++) {
        var element = elements.snapshotItem(i);
        if(element.parentNode.children.length > 2)
        	this.addAnnotation(doc,element);
    };	
};
