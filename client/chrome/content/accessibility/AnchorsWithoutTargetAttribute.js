

function AnchorsWithoutTargetAttribute(name){
	this.name = name;
};

AnchorsWithoutTargetAttribute.prototype.adaptDocument = function(doc){
	this.removeTargetAttributes(doc);
};

AnchorsWithoutTargetAttribute.prototype.removeTargetAttributes = function(doc){
	anchors = doc.getElementsByTagName("a");
	for (var i=0;i  < anchors.length;i++){
		var anchor = anchors[i];
		try{
			anchor.removeAttribute('target');
		}
		catch(e){
			continue;
		}
	}	
};

