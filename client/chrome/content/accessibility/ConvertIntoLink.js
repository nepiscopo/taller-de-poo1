
function ConvertIntoLink(name){	
	this.name = name;
	this.xpath_elements;
	this.href_target;
};

ConvertIntoLink.prototype.adaptDocument = function(doc){	
	var elements = doc.evaluate(this.xpath_elements, doc, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);	
    for (var i = 0; i < elements.snapshotLength; i++){
        var element = elements.snapshotItem(i);
        this.decorate(doc,element);
    };
};

ConvertIntoLink.prototype.decorate = function (doc,element){	
	var anchor = doc.createElement("a");	
	anchor.setAttribute("href", this.href_target);	
	
	anchor.innerHTML = element.textContent;
	element.innerHTML = "";
	element.appendChild(anchor);	
};

ConvertIntoLink.prototype.setElement = function(anXpath, href){
	this.xpath_elements = anXpath;
	this.href_target = href;
}