/***************************************
	ReorderRowsRefactoring
 */

function ReorderTableRowsRefactoring(name){
	this.name = name;
	this.row_container = null;//Un xPath para obtener la tabla
	this.movements = [];
};

ReorderTableRowsRefactoring.prototype.adaptDocument = function(doc){
	var table;
	try{
		//var table = doc.evaluate(this.row_container, doc, null, XPathResult.ANY_TYPE, null).snashopItem(0);		
		 table = doc.evaluate(this.row_container, doc, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null).snapshotItem(0);
	}
	catch(e){;return;}
	this.moveRows(doc,table);
};

ReorderTableRowsRefactoring.prototype.moveRows = function(doc,table){
	for (var i=0;i < this.movements.length;i++){
		var movement = this.movements[i];
		var target_row;
		try{
			var rows = table.getElementsByTagName("tr");
			target_row = rows[movement["row"]];
			table.removeChild(target_row);
		}catch(e){;}
		
		var new_row = target_row.cloneNode(true);
		//Aca vendria un insertBefore cuando el refactoring soporte la especificacion del corriemiento
		table.appendChild(new_row);
	}
};

ReorderTableRowsRefactoring.prototype.setRowsContainer = function(aXPath){
	this.row_container = aXPath;
};

ReorderTableRowsRefactoring.prototype.addRowMovement = function(number_of_row,shift){
	var movement = {"row":number_of_row,"shift":shift};
	this.movements.push(movement);
};
