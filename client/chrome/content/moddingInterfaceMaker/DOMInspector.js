/*
 * CSNDOM Inspector
 */

function CSADOMInspector(){
	this.dom_element_for_concept = null;
}; 	

CSADOMInspector.prototype.getCSAInstance = function (){
	return CSA;
}

CSADOMInspector.prototype.addNewElementToModdingInterface = function(){
	var element_xpath = getXPathForElement(this.dom_element_for_concept,content.document);	
	this.getCSAInstance().addConcept(element_xpath);
};

CSADOMInspector.prototype.setStyleHighlighting = function(event){
	event.target.normal_style = event.target.getAttribute("style");
	event.target.setAttribute("style",event.target.getAttribute("style") + " border:1px solid #336699;background-color:#FFFFCC;");
};

CSADOMInspector.prototype.backStyleHighlighting = function(event){
	var style = event.target.normal_style;
	event.target.setAttribute("style",style);
	event.target.setAttribute("style",event.target.getAttribute("style").split(" border:1px solid #336699;background-color:#FFFFCC;")[1]);
};

function my_func(event){
	alert(event.target);
};

CSADOMInspector.prototype.selectElement = function(event){
	//Como es un listener de un evento, no tiene el ambito de la instancia de CSNDOMInspector.
	//DOMInspector es la instancia creada en el overlay principal. 
	var self = DOMInspector;
	if (event.button != 2)
		return 0;
	var menu_option_add_to_modding = document.getElementById('context-item-add-element-to-mi');
	if ((event.target.getAttribute("class") == "") || (event.target.getAttribute("class") == null)){
		alert("Este componente no puede ser utilizado en una interfaz de modding");
		menu_option_add_to_modding.setAttribute("disabled","true");
	}
	else{		
		menu_option_add_to_modding.setAttribute("disabled","false");
		self.dom_element_for_concept = event.target;						
		self.disableDomHighlighting();
	}
};

CSADOMInspector.prototype.recursiveEnableDomHighlighting = function(element){
	element.addEventListener("mousedown", this.selectElement,true);
	element.addEventListener("mouseover", this.setStyleHighlighting,true);		
	element.addEventListener("mouseout", this.backStyleHighlighting,true);
	if (!((element.getAttribute("class") == "") || (element.getAttribute("class") == null))){
		element.addEventListener("mousedown", this.selectElement,true);
		element.addEventListener("mouseover", this.setStyleHighlighting,true);		
		element.addEventListener("mouseout", this.backStyleHighlighting,true);		
	}
	for (var i = 0; i < element.childNodes.length; i++){
		var child = element.childNodes[i];
		if (!((child.getAttribute("class") == "") || (child.getAttribute("class") == null))){
			child.addEventListener("mousedown", this.selectElement,true);			
			child.addEventListener("mouseover", this.setStyleHighlighting,true);			
			child.addEventListener("mouseout", this.backStyleHighlighting,true);			
		}
		//child.parentNode.insertBefore(a,child);
		child.addEventListener("mousedown", this.selectElement,true);		
		child.addEventListener("mouseover", this.setStyleHighlighting,true);			
		child.addEventListener("mouseout", this.backStyleHighlighting,true);		
		this.recursiveEnableDomHighlighting(child);
	}
};

CSADOMInspector.prototype.enableDomHighlighting = function(){
	this.getCSAInstance().toggleHighlightingMenu();	
	var body = content.document.documentElement.getElementsByTagName("body")[0];
	this.recursiveEnableDomHighlighting(body);
};

CSADOMInspector.prototype.recursiveDisableDomHighlighting = function(element){	
	element.removeEventListener("mouseover",this.setStyleHighlighting,true);	
	element.removeEventListener("mouseout",this.backStyleHighlighting,true);
	element.removeEventListener("mousedown", this.selectElement,true);
	for (var i = 0; i < element.childNodes.length; i++){
		var child = element.childNodes[i];
		child.removeEventListener("mousedown", this.selectElement,true);
		child.removeEventListener("mouseover",this.setStyleHighlighting,true);
		child.removeEventListener("mouseout",this.backStyleHighlighting,true);
		this.recursiveDisableDomHighlighting(child);
	}
};

CSADOMInspector.prototype.disableDomHighlighting = function(){
	var csa = this.getCSAInstance();
	csa.toggleHighlightingMenu();
	var body = content.document.documentElement.getElementsByTagName("body")[0];	
	this.recursiveDisableDomHighlighting(body);
};
