function test_csn_adaptation(){
	var test_modding = CSN.getModdingInterface(content.document.location);
	var concept = test_modding.getConceptByName("Result");
	var concepts_list = content.document.getElementsByClassName(concept.getClassName());
	for (var i=0; i < concepts_list.length;i++){
		var concept_node = concepts_list[i];
		var user_property = concept.getPropertyByName("user");
		var user_xpath = user_property["xpath"];
		var user_node = myEvaluateXPath(concept_node,user_xpath);
		text = content.document.createElement("p");
		text.setAttribute("style","color:red;font-weight:bold;")
		var oText = document.createTextNode("Usuario: ");
  		text.appendChild(oText);
		concept_node.insertBefore(text,user_node);
	}
};

//LO SIGUIENTE DEBE BORRARSE; ERA SOLO UNA PRUEBA DE CONCEPTOS
function test_csn_adaptation(){
	var test_modding = CSN.getModdingInterface(content.document.location.host);
	var concept = test_modding.getConceptByName("UserComment");
	var concepts_list = content.document.getElementsByClassName(concept.getClassName());
	for (var i=0; i < concepts_list.length;i++){
		var concept_node = concepts_list[i];
		var user_property = concept.getPropertyByName("user");
		var user_xpath = user_property["xpath"];
		var user_node = myEvaluateXPath(concept_node,user_xpath);
		text = content.document.createElement("p");
		text.setAttribute("style","color:red;font-weight:bold;")
		var oText = document.createTextNode("Usuario: ");
  		text.appendChild(oText);
		concept_node.insertBefore(text,user_node);
	}
};

function printingInformation(){	
	var apps = CSN.getApplications();
	for (app in apps){
		var adaptation = apps[app].getSourceAdaptations()[0];
		alert(adaptation.applyAdaptation);
		adaptation.applyAdaptation();
	}	
};

function testCopyingModel(){	
	var googleModel = CSA.getModdingInterface("www.google.com.ar");
	var yahooModel = CSA.createModdingInterface();
	var yahoo = yahooModel.getApplication();
	
	var concepts = googleModel.getConcepts();
  	for(var c in concepts){
  		var concept = concepts[c];
	  	var conceptWrapper = new ConceptDOMWrapper(concept,yahoo,"res");
	  	var tmp = yahooModel.addWrappedConcept(conceptWrapper);  		
  		var properties = concept.getProperties();  		
	  	for (var j=0; j < properties.length; j++){	  		
	  		var property = properties[j];	
	  		var propertyWrapper = new PropertyDOMWrapper(property,yahoo,"data-xpath");
	  		property.basicAddDOMElement(propertyWrapper,yahoo);	  		
	  	}
  	}
	alert("El modelo ha sido copiado");
};

function testAdaptation(){
	var model = CSA.getModdingInterface(content.document.location.host);	
	var application = model.getApplication();
	var concepts = model.getConcepts();
  	for(var c in concepts){
  		var concept = concepts[c];
  		var wrapper = concept.getDOMWrapperForApplication(application);  		
  		var dom_elements = concept.getDOMInstancesForApp(application);
  		for(var i = 0; i < dom_elements.length ;i++){
  			dom_elements[i].setAttribute("style","background-color:#DDDDDD;");	
  			var properties = concept.getProperties();  		
	  		for (var j=0; j < properties.length; j++)  			
	  			var property = properties[j];	  		
  		}
  	}
};
