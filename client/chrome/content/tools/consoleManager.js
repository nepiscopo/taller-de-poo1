function ConsoleManager(){
	var aConsoleService = Components.classes["@mozilla.org/consoleservice;1"].
	     getService(Components.interfaces.nsIConsoleService);
	this.console = aConsoleService;
};

ConsoleManager.prototype.showMessage = function(msg){	
	this.console.logStringMessage(msg);
};
