
function SimpleStorage(){
	try{
		this.file = Components.classes["@mozilla.org/file/directory_service;1"]
	                    .getService(Components.interfaces.nsIProperties)
	                    .get("ProfD", Components.interfaces.nsIFile);	                    
		this.file.append("csa_db.sqlite");		
		this.storageService = Components.classes["@mozilla.org/storage/service;1"]
	                     .getService(Components.interfaces.mozIStorageService);	
		this.mDBConn = this.storageService.openDatabase(this.file);
		this.create_schema();
	}
	catch(e){
		//alert("creation:" +e);
		return false;
	}
};

SimpleStorage.prototype.openDBConn = function(){
	this.mDBConn = this.storageService.openDatabase(this.file);
};

SimpleStorage.prototype.closeDBConn = function(){
	this.mDBConn.asyncClose();
};

SimpleStorage.prototype.create_schema = function(){
	try{
		if (!this.mDBConn.tableExists("JSON_STORAGE"))
			this.mDBConn.executeSimpleSQL("CREATE TABLE JSON_STORAGE (key varchar(256) PRIMARY KEY,value varchar(1024))");
	}
	catch (e){
		//alert("creation schema:" +e);
		return false;
	}
	return true;
};

SimpleStorage.prototype.getValue = function(key, callback){
	var returnValue = null;
	var statement = null;
	try{
		if (this.mDBConn.tableExists("JSON_STORAGE")){			
			statement = this.mDBConn.createStatement("SELECT value FROM JSON_STORAGE WHERE key= :key");
			statement.params.key = key;
			statement.executeAsync({
			  handleResult: function(aResultSet) {
			    	for (var row = aResultSet.getNextRow(); row; row = aResultSet.getNextRow()) {
			      		var value = row.getResultByName("value");
			      		returnValue = value;
			      		callback(returnValue);
		    	    }
			  },

			  handleError: function(aError) {
			    //alert("Error: " + aError.message);
			  },

			  handleCompletion: function(aReason) {
			    ;
			  }
			});
			statement.finalize();
		}
	}
	catch (e){
		return returnValue;
	}
	return returnValue;
};


SimpleStorage.prototype.setValue = function(key, value){
	this.openDBConn();
	var statement = "";

	try{		
		if (this.mDBConn.tableExists("JSON_STORAGE")){
			//if(this.hasKey(key))
			//	statement = this.mDBConn.createStatement("UPDATE JSON_STORAGE SET value= :value WHERE key= :key;");
			//else
			//	statement = this.mDBConn.createStatement("INSERT INTO JSON_STORAGE (key,value) VALUES (:key,:value)");
			statement = this.mDBConn.createStatement("INSERT OR REPLACE INTO JSON_STORAGE (key,value) VALUES (:key,:value)");
			statement.params.value = value;
			statement.params.key = key;

			statement.executeAsync({
			  handleResult: function(aResultSet) {
			    	;
			  },

			  handleError: function(aError) {
			    //alert("Error INSERT/UPDATE: " + aError.message);
			    return false;
			  },

			  handleCompletion: function(aReason) {
			    ;//alert("Error INSERT/UPDATE 2222: " + aError.message);;
			  }
			});
			statement.finalize();

		}
	}
	catch (e){
		//alert(e);
		return null;
	}
	return true;
};

SimpleStorage.prototype.hasKey = function(key){
	return this.getValue(key) != null;
};
