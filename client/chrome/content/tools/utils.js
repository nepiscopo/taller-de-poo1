  
function myEvaluateXPath(doc, aNode, aExpr) {
  var element = doc.evaluate(aExpr, aNode, null, XPathResult.ANY_TYPE, null).iterateNext();
  return element;
  
//function myEvaluateXPath(aNode, aExpr) {
//Vieja version que no utilizaba el document para obtener el elemento.  
  var xpe = new XPathEvaluator();
  var nsResolver = xpe.createNSResolver(aNode.ownerDocument == null ?
    aNode.documentElement : aNode.ownerDocument.documentElement);
  var result = xpe.evaluate(aExpr, aNode, nsResolver, 0, null);
  var found = [];
  while (res = result.iterateNext()){
  	alert(res);
	return res;//ESTA LINEA ESTA INSERTADA A LA FUERZA PARA PROBAR
  	found.push(res);
  }  
  return found;
};


function getXPathForElement(elt){
	var path = "";
    for (; elt && elt.nodeType == 1; elt = elt.parentNode){
   		idx = getElementIdx(elt);
		xname = elt.tagName;
		if (idx > 1) xname += "[" + idx + "]";
			path = "/" + xname + path;
    }
    return path;	
}

function getElementIdx(elt){
    var count = 1;
    for (var sib = elt.previousSibling; sib ; sib = sib.previousSibling){
		if(sib.nodeType == 1 && sib.tagName == elt.tagName)	count++
    }    
    return count;
}


/*VIEJA FUNCION PARA OBTENER EL XPATH.
 *Deprecated 23/08/2010 
 
function getXPathForElement2(el, xmlFile) {
	
	var xpath = '';
	var pos, tempitem2;
	
	while(el !== xmlFile.documentElement) {		
		pos = 0;
		tempitem2 = el;
		while(tempitem2) {
			if (tempitem2.nodeType === 1 && tempitem2.nodeName === el.nodeName) // If it is ELEMENT_NODE of the same name
				pos += 1;
			tempitem2 = tempitem2.previousSibling;
		}
		xpath = "*[name()='"+el.nodeName+"' and namespace-uri()='"+(el.namespaceURI===null?'':el.namespaceURI)+"']["+pos+']'+'/'+xpath;
		el = el.parentNode;
	}
	xpath = '/*'+"[name()='"+xmlFile.documentElement.nodeName+"' and namespace-uri()='"+(el.namespaceURI===null?'':el.namespaceURI)+"']"+'/'+xpath;
	xpath = xpath.replace(/\/$/, '');
	return xpath;
};
*/

