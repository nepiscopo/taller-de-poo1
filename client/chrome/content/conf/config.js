//config.js
var __editor__ = null;

function onDialogAccept(){
	
};

function onDialogCancel(){
	
};

function toggle(obj){
	if (obj.style.display == "none")
		obj.style.display = 'block';
	else
		obj.style.display = "none";
}

function toogleScriptView(target){
	toggle(document.getElementById("options-"+target));
	toggle(document.getElementById("short-description-"+target));
};

function addElementIntoGUI(id,element){
	var id_element = "script" + id;
	var script_container = document.createElement("vbox");
	script_container.setAttribute("id",id_element);
	script_container.setAttribute("class","script");
	script_container.setAttribute("onclick","toogleScriptView('"+id_element+"');");

	var script_title_container = document.createElement("hbox");		
	script_title_container.textContent = element.name();
	script_title_container.setAttribute("class","script-title");
	script_container.appendChild(script_title_container);

	var script_description_container = document.createElement("hbox");
	script_description_container.setAttribute("class","short-description");
	script_description_container.setAttribute("style","display:block;");
	script_description_container.setAttribute("id","short-description-"+id_element);
	script_description_container.textContent = element.description();
	script_container.appendChild(script_description_container);

	var script_options_container = document.createElement("vbox");
	script_options_container.setAttribute("class","full-options");
	script_options_container.setAttribute("style","display:none;");
	script_options_container.setAttribute("id","options-"+id_element);
	script_container.appendChild(script_options_container);

	var script_options_subcontainer = document.createElement("hbox");
	script_options_subcontainer.setAttribute("flex","1");
	script_options_container.appendChild(script_options_subcontainer);

	var script_options_grid = document.createElement("grid");
	script_options_grid.setAttribute("flex","1");
	script_options_grid.setAttribute("width","2000px");
	script_options_subcontainer.appendChild(script_options_grid);

	var script_options_columns = document.createElement("columns");		
	script_options_grid.appendChild(script_options_columns);

	var script_options_column = document.createElement("column");
	script_options_column.setAttribute("flex","1");
	script_options_columns.appendChild(script_options_column);


	var script_options_rows = document.createElement("rows");		
	script_options_grid.appendChild(script_options_rows);
				    
	var script_options_author_row = document.createElement("row");
	var script_options_author_vbox = document.createElement("vbox");
	script_options_author_vbox.setAttribute("class","script-author");
	script_options_author_vbox.textContent = element.author();
	script_options_author_row.appendChild(script_options_author_vbox);
	script_options_rows.appendChild(script_options_author_row);

	var script_options_description_row = document.createElement("row");
	var script_options_description_vbox = document.createElement("vbox");	
	script_options_description_vbox.setAttribute("class","script-description");
	script_options_description_vbox.textContent = element.description();
	script_options_description_row.appendChild(script_options_description_vbox);

	
	script_options_rows.appendChild(script_options_description_row);
	var script_options_operations_row = document.createElement("row");
	script_options_operations_row.setAttribute("class","script-operations-row-container");	
	var script_options_operations_flexbox = document.createElement("box");
	script_options_operations_flexbox.setAttribute("flex","1");
	script_options_operations_row.appendChild(script_options_operations_flexbox);

	var script_options_operations_hbox = document.createElement("hbox");
	script_options_operations_hbox.setAttribute("class","script-operations-container");
	var remove_button = document.createElement("button");
	remove_button.setAttribute("label","Remove");
	remove_button.setAttribute("class","remove-script");
	remove_button.setAttribute("onclick","removeAugmenter('" + id_element + "')");	
	script_options_operations_hbox.appendChild(remove_button);

	var edit_button = document.createElement("button");
	edit_button.setAttribute("label","Edit");
	edit_button.setAttribute("class","edit-script");
	edit_button.setAttribute("onclick","editAugmenter('" + id_element + "')");		
	script_options_operations_hbox.appendChild(edit_button);

	var edit_activation = document.createElement("button");
	var label = (element.isActivated())?"Deactivate":"Activate";
	edit_activation.setAttribute("label",label);
	edit_activation.setAttribute("id","activation-button-"+id_element);
	edit_activation.setAttribute("class","edit-script-activation");
	edit_activation.setAttribute("onclick","toggleActivationAugmenter('" + id_element + "')");
	script_options_operations_hbox.appendChild(edit_activation);

	script_options_operations_row.appendChild(script_options_operations_hbox);
	script_options_rows.appendChild(script_options_operations_row);
	document.getElementById("container-box").appendChild(script_container);
	
};

function listAugmenters(){
	try{
		for (var i=0;i < augmenters.length;i++){
			addElementIntoGUI(i,augmenters[i]);		
		}
	}catch(e){;}
};

function openEditor(editor,file){
	try{
		var process = Components.classes["@mozilla.org/process/util;1"]
	              .createInstance(Components.interfaces.nsIProcess);
	    process.init(editor);
	    process.run(false,[file],1);	    
	}catch(e){
		;
	}    
};

function getAugmenterFromId(id_element){
	var id_augmenter = id_element.split("script")[1];
	return augmenters[id_augmenter];
};

function removeAugmenter(id_element){
	try{
		getAugmenterFromId(id_element).delete();
		augmenters = artifactmanager.getAugmenters();
		var container = document.getElementById(id_element);
		container.parentNode.removeChild(container);
	}catch(e){
		alert("The file could not be removed.");
    }
};

function toggleActivationAugmenter(id_element){	
	var element = getAugmenterFromId(id_element);
	element.toggleActivation();	
	var label = (element.isActivated())?"Deactivate":"Activate";	
	edit_activation = document.getElementById("activation-button-"+id_element);	
	edit_activation.setAttribute("label",label);	
};

function editAugmenter(id_element){
	var augmenter = getAugmenterFromId(id_element);	
	if (__editor__ == null){
		const nsIFilePicker = Components.interfaces.nsIFilePicker;
		var filePicker = Components.classes["@mozilla.org/filepicker;1"]
	                 .createInstance(nsIFilePicker);
	    filePicker.init(window,"Choose a text editor",nsIFilePicker.modeOpen);
	    filePicker.appendFilters(nsIFilePicker.filterApps);
		var stop_asking_exec = true;
		var selectedFile;
		while(stop_asking_exec){
			selectedFile = filePicker.show();
			if (selectedFile == nsIFilePicker.returnOK && !filePicker.file.isExecutable()){
				alert("You must select a executable file.");
				continue;
			}
			stop_asking_exec = false;
		};		
		if (selectedFile == nsIFilePicker.returnOK || selectedFile == nsIFilePicker.returnReplace && selectedFile.isExecutable()) {
	  		var file = filePicker.file;  		
	  		var augmenter = getAugmenterFromId(id_element);  		
	  		try{
	  			openEditor(file,augmenter.getFilePath());  		
	  			__editor__ = file;
	  		}
	  		catch(e){return;}	  		
		}
	}
	else
		openEditor(__editor__,augmenter.getFilePath());
};

function reloadAugmenters(){
	for (var i=0;i < augmenters.length;i++){
		augmenters[i].reload();
	}
};
