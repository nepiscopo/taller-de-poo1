
function exportOWL(){
    var modding_interface = CSA.getModdingInterface(content.document.location);
    if (typeof(modding_interface) == "undefined")	return;
    var str = modding_interface.getOWL("algo","http://"+content.document.location.host+"#");
    file = writeFile("owl.xml",str);
    window.open("file://" + file.path);
};

function exportXSLT(){
    var modding_interface = CSA.getModdingInterface(content.document.location);
    if (typeof(modding_interface) == "undefined")	return;
    var str = modding_interface.getXSLT("algo","http://"+content.document.location.host+"#");
    file = writeFile("transformation.xsl",str);
    window.open("file://" + file.path);
};

function setCSNSource(create_anchor){  			
	var tree = document.getElementById("main-tree");
	var selected_item = tree.view.getItemAtIndex(tree.currentIndex);
    var element = {"type":selected_item.modding_interface_type, "element":selected_item.modding_interface_element, "onwer":selected_item.modding_interface_owner};
    
	CSA.setAppSourceForNewPlugin(content.document.location,element,create_anchor);
};

function doElementSelectAction(){  			
	var tree = document.getElementById("main-tree");  			
	var selected_element = tree.childNodes[tree.currentIndex-1];
	selected_element.do_action();
};

function highlightingProperty(){
	var my_concept = this.concept;
	alert(my_concept);
	var xpath = this.xpath;
	alert(xpath);
};

function outHighlightingProperty(event){
	var my_concept = event.target.concept;
	var xpath = event.target.xpath;
};

function init(){
	mainWindow = window.QueryInterface(Components.interfaces.nsIInterfaceRequestor)
           .getInterface(Components.interfaces.nsIWebNavigation)
           .QueryInterface(Components.interfaces.nsIDocShellTreeItem)
           .rootTreeItem
           .QueryInterface(Components.interfaces.nsIInterfaceRequestor)
           .getInterface(Components.interfaces.nsIDOMWindow);
    CSA = mainWindow.CSA;
    var modding_interface = CSA.getModdingInterface(content.document.location.host);            
    if (typeof(modding_interface) == "undefined")	return;
    var concepts_container = document.getElementById("concepts-container");
    var mi_concepts = modding_interface.getConcepts();
	for (var name_class in mi_concepts){				
		concept = mi_concepts[name_class];
        concept_treeitem = document.createElement("treeitem");
        concept_treeitem.setAttribute("container","true");
        concept_treeitem.setAttribute("open","false");
        concept_treeitem.setAttribute("contextmenu","contextual-menu-for-tree");
		concept_treeitem.modding_interface_type = "concept";
        concept_treeitem.modding_interface_element = concept;
        concept_treeitem.modding_interface_owner = modding_interface;
        
        concept_treerow = document.createElement("treerow");
        concept_treecell = document.createElement("treecell");
        
        var application = modding_interface.getApplication();
		var wrapper = concept.getDOMWrapperForApplication(application);
		
		if (!wrapper){			
			alert("El concepto " + concept.getConceptName() + " aun no esta definido para esta aplicación.");
			continue;
		}		
        concept_treecell.setAttribute("label",concept.getConceptName() + " [" + wrapper.getWrapperDOMInfo() + "]");
        //concept_treecell.setAttribute("label",concept.getConceptName());
        concept_treerow.appendChild(concept_treecell);
        concept_treeitem.appendChild(concept_treerow);
    
        concept_treechildren = document.createElement("treechildren");
        
        var properties = concept.getProperties();
		if (properties.length > 0){
            properties_treeitem = document.createElement("treeitem");
            properties_treeitem.setAttribute("container","true");
            properties_treeitem.setAttribute("open","true"); 
            properties_treerow = document.createElement("treerow");
            properties_treecell = document.createElement("treecell");
            properties_treecell.setAttribute("label","Properties");
            properties_treerow.appendChild(properties_treecell);
            properties_treeitem.appendChild(properties_treerow);
            concept_treechildren.appendChild(properties_treeitem);
            concept_treeitem.appendChild(concept_treechildren);
            property_treechildren = document.createElement("treechildren");
            
            for (var i=0; i < properties.length; i++){		            
	            property_treeitem = document.createElement("treeitem");
	           	property_treeitem.modding_interface_type = "property";
        		property_treeitem.modding_interface_element = properties[i];
        		property_treeitem.modding_interface_owner = concept;
	            
	            property_treeitem.setAttribute("container","true");
	            property_treeitem.setAttribute("open","true");
	            property_treerow = document.createElement("treerow");
	            var propertyWrapper = properties[i].getDOMWrapperForApplication(application);

	            property_treecell = document.createElement("treecell");            
	            property_treecell.setAttribute("label",properties[i].getName() + "[ " + propertyWrapper.getWrapperDOMInfo() + " ]");
	            property_treerow.appendChild(property_treecell);
	            property_treeitem.appendChild(property_treerow);
	            property_treechildren.appendChild(property_treeitem);		            
        	}
        	properties_treeitem.appendChild(property_treechildren);
  			concept_treechildren.appendChild(properties_treeitem);
		}
		
		var conceptual_events = concept.getConceptualEvents();
		if (conceptual_events.length > 0){
            conceptual_events_treeitem = document.createElement("treeitem");
            conceptual_events_treeitem.setAttribute("container","true");
            conceptual_events_treeitem.setAttribute("open","true");           
            conceptual_events_treerow = document.createElement("treerow");
            conceptual_events_treecell = document.createElement("treecell");
            conceptual_events_treecell.setAttribute("label","Events");
            conceptual_events_treerow.appendChild(conceptual_events_treecell);
            conceptual_events_treeitem.appendChild(conceptual_events_treerow);
            concept_treechildren.appendChild(conceptual_events_treeitem);
            concept_treeitem.appendChild(concept_treechildren);	            
            conceptual_event_treechildren = document.createElement("treechildren");
        	for (var i=0; i < conceptual_events.length; i++){
	            conceptual_event_treeitem = document.createElement("treeitem");
	            conceptual_event_treeitem.setAttribute("container","true");
	            conceptual_event_treeitem.setAttribute("open","true");            
	            conceptual_event_treerow = document.createElement("treerow");
	            conceptual_event_treecell = document.createElement("treecell");
	            conceptual_event_treecell.setAttribute("label",conceptual_events[i]["name"]);
	            conceptual_event_treerow.appendChild(conceptual_event_treecell);
	            conceptual_event_treeitem.appendChild(conceptual_event_treerow);
	            conceptual_event_treechildren.appendChild(conceptual_event_treeitem);		            
        	}
        	conceptual_events_treeitem.appendChild(conceptual_event_treechildren);
  			concept_treechildren.appendChild(conceptual_events_treeitem);	  			
		}	  			
        concepts_container.appendChild(concept_treeitem);
	}
};