INSERT INTO `tpoo1`.`RefactoringState` (
`id` ,
`name` ,
`discr`
)
VALUES (
'1', 'Testing', 'test'
), (
'2', 'Production', 'prod'
), (
'3', 'Development', 'dev'
), (
'4', 'Unavailable', 'una'
);
