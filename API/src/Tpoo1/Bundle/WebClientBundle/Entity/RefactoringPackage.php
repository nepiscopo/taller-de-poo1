<?php

namespace Tpoo1\Bundle\WebClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RefactoringPackage
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class RefactoringPackage extends AccessibilityComponent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="InstantiatedRefactoring", cascade={"persist", "remove" })
     * @ORM\JoinTable(name="PackageRefactorings",
     *      joinColumns={@ORM\JoinColumn(name="package_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="refactoring_id", referencedColumnName="id", unique=true)}
     *      )
     **/
    protected $refactorings;

    /**
     * @var string
     * 
     * @ORM\Column(name="name", type="string")
     */
    protected $name;

    /**
     * @var string
     * 
     * @ORM\Column(name="description", type="text")
     */
    protected $description;

    /**
     * @var string
     * 
     * @ORM\Column(name="version", type="string")
     */
    protected $version;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var \Tpoo1\Bundle\WebClientBundle\Entity\RefactoringState
     */
    protected $state;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->refactorings = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     * @return RefactoringPackage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return RefactoringPackage
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set version
     *
     * @param string $version
     * @return RefactoringPackage
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string 
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Add refactorings
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\InstantiatedRefactoring $refactorings
     * @return RefactoringPackage
     */
    public function addRefactoring(\Tpoo1\Bundle\WebClientBundle\Entity\InstantiatedRefactoring $refactorings)
    {
        $this->refactorings[] = $refactorings;

        return $this;
    }

    /**
     * Remove refactorings
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\InstantiatedRefactoring $refactorings
     */
    public function removeRefactoring(\Tpoo1\Bundle\WebClientBundle\Entity\InstantiatedRefactoring $refactorings)
    {
        $this->refactorings->removeElement($refactorings);
    }

    /**
     * Get refactorings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRefactorings()
    {
        return $this->refactorings;
    }

    /**
     * Set state
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\RefactoringState $state
     * @return RefactoringPackage
     */
    public function setState(\Tpoo1\Bundle\WebClientBundle\Entity\RefactoringState $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return \Tpoo1\Bundle\WebClientBundle\Entity\RefactoringState 
     */
    public function getState()
    {
        return $this->state;
    }
    /**
     * @var \Tpoo1\Bundle\WebClientBundle\Entity\User
     */
    protected $author;

    /**
     * @var \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent
     */
    protected $previous;


    /**
     * Set author
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\User $author
     * @return RefactoringPackage
     */
    public function setAuthor(\Tpoo1\Bundle\WebClientBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \Tpoo1\Bundle\WebClientBundle\Entity\User 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set previous
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $previous
     * @return RefactoringPackage
     */
    public function setPrevious(\Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $previous = null)
    {
        $this->previous = $previous;

        return $this;
    }

    /**
     * Get previous
     *
     * @return \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent 
     */
    public function getPrevious()
    {
        return $this->previous;
    }
    /**
     * @var \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent
     */
    protected $next;


    /**
     * Set next
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $next
     * @return RefactoringPackage
     */
    public function setNext(\Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $next = null)
    {
        $this->next = $next;

        return $this;
    }

    /**
     * Get next
     *
     * @return \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent 
     */
    public function getNext()
    {
        return $this->next;
    }
    
    // TODO
    public function __toString()
    {
      return '';
    }
}
