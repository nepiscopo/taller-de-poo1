<?php

namespace Tpoo1\Bundle\WebClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Refactoring
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"gr" = "GenericRefactoring", "ir" = "InstantiatedRefactoring"})
 */
class Refactoring extends AccessibilityComponent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * 
     * @ORM\Column(name="name", type="string")
     */
    protected $name;

    /**
     * @var string
     * 
     * @ORM\Column(name="description", type="text")
     */
    protected $description;

    /**
     * @var string
     * 
     * @ORM\Column(name="namespace", type="string")
     */
    protected $namespace;

    /**
     * @var string
     * 
     * @ORM\Column(name="version", type="string")
     */
    protected $version;

    /**
     * @var string
     * 
     * @ORM\Column(name="url", type="string")
     */
    protected $url;

    /**
     * @var string
     * 
     * @ORM\Column(name="script", type="text")
     */
    protected $script;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var \Tpoo1\Bundle\WebClientBundle\Entity\RefactoringState
     */
    protected $state;


    /**
     * Set name
     *
     * @param string $name
     * @return Refactoring
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Refactoring
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set namespace
     *
     * @param string $namespace
     * @return Refactoring
     */
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;

        return $this;
    }

    /**
     * Get namespace
     *
     * @return string 
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * Set version
     *
     * @param string $version
     * @return Refactoring
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string 
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Refactoring
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set state
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\RefactoringState $state
     * @return Refactoring
     */
    public function setState(\Tpoo1\Bundle\WebClientBundle\Entity\RefactoringState $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return \Tpoo1\Bundle\WebClientBundle\Entity\RefactoringState 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set script
     *
     * @param string $script
     * @return Refactoring
     */
    public function setScript($script)
    {
        $this->script = $script;

        return $this;
    }

    /**
     * Get script
     *
     * @return string 
     */
    public function getScript()
    {
        return $this->script;
    }
    /**
     * @var \Tpoo1\Bundle\WebClientBundle\Entity\User
     */
    protected $author;

    /**
     * @var \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent
     */
    protected $previous;


    /**
     * Set author
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\User $author
     * @return Refactoring
     */
    public function setAuthor(\Tpoo1\Bundle\WebClientBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \Tpoo1\Bundle\WebClientBundle\Entity\User 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set previous
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $previous
     * @return Refactoring
     */
    public function setPrevious(\Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $previous = null)
    {
        $this->previous = $previous;

        return $this;
    }

    /**
     * Get previous
     *
     * @return \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent 
     */
    public function getPrevious()
    {
        return $this->previous;
    }
    
    /**
     * @var \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent
     */
    protected $next;


    /**
     * Set next
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $next
     * @return Refactoring
     */
    public function setNext(\Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $next = null)
    {
        $this->next = $next;

        return $this;
    }

    /**
     * Get next
     *
     * @return \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent 
     */
    public function getNext()
    {
        return $this->next;
    }
}
