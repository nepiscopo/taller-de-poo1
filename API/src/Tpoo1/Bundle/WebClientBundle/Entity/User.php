<?php

namespace Tpoo1\Bundle\WebClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class User extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="AccessibilityComponent")
     * @ORM\JoinTable(name="UserComponents",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="component_id", referencedColumnName="id", unique=true)}
     *      )
     **/
    protected $installedComponents;

    /**
     * @ORM\OneToMany(targetEntity="AccessibilityComponent", mappedBy="author")
     **/
    protected $developedComponents;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add installedComponents
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $installedComponents
     * @return User
     */
    public function addInstalledComponent(\Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $installedComponents)
    {
        $this->installedComponents[] = $installedComponents;

        return $this;
    }

    /**
     * Remove installedComponents
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $installedComponents
     */
    public function removeInstalledComponent(\Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $installedComponents)
    {
        $this->installedComponents->removeElement($installedComponents);
    }

    /**
     * Get installedComponents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInstalledComponents()
    {
        return $this->installedComponents;
    }

    /**
     * Add developedComponents
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $developedComponents
     * @return User
     */
    public function addDevelopedComponent(\Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $developedComponents)
    {
        $this->developedComponents[] = $developedComponents;

        return $this;
    }

    /**
     * Remove developedComponents
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $developedComponents
     */
    public function removeDevelopedComponent(\Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $developedComponents)
    {
        $this->developedComponents->removeElement($developedComponents);
    }

    /**
     * Get developedComponents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDevelopedComponents()
    {
        return $this->developedComponents;
    }

    public function isGranted($role)
    {
        return in_array($role, $this->getRoles());
    }
}
