<?php

namespace Tpoo1\Bundle\WebClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InstantiatedRefactoring
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class InstantiatedRefactoring extends Refactoring
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="GenericRefactoring")
     * @ORM\JoinColumn(name="dependency_id", referencedColumnName="id")
     **/
    protected $dependency;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $namespace;

    /**
     * @var string
     */
    protected $version;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var \Tpoo1\Bundle\WebClientBundle\Entity\RefactoringState
     */
    protected $state;


    /**
     * Set name
     *
     * @param string $name
     * @return InstantiatedRefactoring
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return InstantiatedRefactoring
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set namespace
     *
     * @param string $namespace
     * @return InstantiatedRefactoring
     */
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;

        return $this;
    }

    /**
     * Get namespace
     *
     * @return string 
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * Set version
     *
     * @param string $version
     * @return InstantiatedRefactoring
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string 
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return InstantiatedRefactoring
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set dependency
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\GenericRefactoring $dependency
     * @return InstantiatedRefactoring
     */
    public function setDependency(\Tpoo1\Bundle\WebClientBundle\Entity\GenericRefactoring $dependency = null)
    {
        $this->dependency = $dependency;

        return $this;
    }

    /**
     * Get dependency
     *
     * @return \Tpoo1\Bundle\WebClientBundle\Entity\GenericRefactoring 
     */
    public function getDependency()
    {
        return $this->dependency;
    }

    /**
     * Set state
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\RefactoringState $state
     * @return InstantiatedRefactoring
     */
    public function setState(\Tpoo1\Bundle\WebClientBundle\Entity\RefactoringState $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return \Tpoo1\Bundle\WebClientBundle\Entity\RefactoringState 
     */
    public function getState()
    {
        return $this->state;
    }
    /**
     * @var string
     */
    protected $script;


    /**
     * Set script
     *
     * @param string $script
     * @return InstantiatedRefactoring
     */
    public function setScript($script)
    {
        $this->script = $script;

        return $this;
    }

    /**
     * Get script
     *
     * @return string 
     */
    public function getScript()
    {
        return $this->script;
    }
    /**
     * @var \Tpoo1\Bundle\WebClientBundle\Entity\User
     */
    protected $author;

    /**
     * @var \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent
     */
    protected $previous;


    /**
     * Set author
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\User $author
     * @return InstantiatedRefactoring
     */
    public function setAuthor(\Tpoo1\Bundle\WebClientBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \Tpoo1\Bundle\WebClientBundle\Entity\User 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set previous
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $previous
     * @return InstantiatedRefactoring
     */
    public function setPrevious(\Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $previous = null)
    {
        $this->previous = $previous;

        return $this;
    }

    /**
     * Get previous
     *
     * @return \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent 
     */
    public function getPrevious()
    {
        return $this->previous;
    }
    /**
     * @var \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent
     */
    protected $next;


    /**
     * Set next
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $next
     * @return InstantiatedRefactoring
     */
    public function setNext(\Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $next = null)
    {
        $this->next = $next;

        return $this;
    }

    /**
     * Get next
     *
     * @return \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent 
     */
    public function getNext()
    {
        return $this->next;
    }
}
