<?php

namespace Tpoo1\Bundle\WebClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccessibilityComponent
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"rp" = "RefactoringPackage", "r" = "Refactoring"})
 */
class AccessibilityComponent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="RefactoringState")
     * @ORM\JoinColumn(name="state_id", referencedColumnName="id")
     **/
    protected $state;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="developedComponents")
     * @ORM\JoinColumn(name="author", referencedColumnName="id", nullable=true)
     **/
    protected $author;

    /**
     * @ORM\OneToOne(targetEntity="AccessibilityComponent")
     * @ORM\JoinColumn(name="previous_version_id", referencedColumnName="id")
     **/
    protected $previous;

    /**
     * @ORM\OneToOne(targetEntity="AccessibilityComponent")
     * @ORM\JoinColumn(name="last_version_id", referencedColumnName="id")
     **/
    protected $next;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set state
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\RefactoringState $state
     * @return AccessibilityComponent
     */
    public function setState(\Tpoo1\Bundle\WebClientBundle\Entity\RefactoringState $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return \Tpoo1\Bundle\WebClientBundle\Entity\RefactoringState 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set author
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\User $author
     * @return AccessibilityComponent
     */
    public function setAuthor(\Tpoo1\Bundle\WebClientBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \Tpoo1\Bundle\WebClientBundle\Entity\User 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set previous
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $previous
     * @return AccessibilityComponent
     */
    public function setPrevious(\Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $previous = null)
    {
        $this->previous = $previous;

        return $this;
    }

    /**
     * Get previous
     *
     * @return \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent 
     */
    public function getPrevious()
    {
        return $this->previous;
    }

    /**
     * Set next
     *
     * @param \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $next
     * @return AccessibilityComponent
     */
    public function setNext(\Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent $next = null)
    {
        $this->next = $next;

        return $this;
    }

    /**
     * Get next
     *
     * @return \Tpoo1\Bundle\WebClientBundle\Entity\AccessibilityComponent 
     */
    public function getNext()
    {
        return $this->next;
    }
    
    public function __toString()
    {
      return '';
    }
}
