<?php

namespace Tpoo1\Bundle\WebClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction()
    {
    	if($this->get('security.context')->isGranted('ROLE_DEVELOPER'))
            return $this->redirect($this->generateUrl('tpoo1_web_client_developer_index'));
    	else if($this->get('security.context')->isGranted('ROLE_CONSUMER'))
    		return $this->redirect($this->generateUrl('tpoo1_web_client_consumer_index'));
    	else if($this->get('security.context')->isGranted('ROLE_USER'))
    		return $this->redirect($this->generateUrl('tpoo1_web_client_select_role'));
    	else
    		return $this->redirect($this->generateUrl('fos_user_security_login'));
    }

    public function selectRoleAction(){
    	return $this->render('Tpoo1WebClientBundle:Default:select_role.html.twig');
    }

    public function selectRoleConsumerAction(){
    	$user = $this->getUser();
    	$user->addRole('ROLE_CONSUMER');
    	$em = $this->getDoctrine()->getManager();
    	$em->persist($user);
    	$em->flush();
        return $this->redirect($this->generateUrl('fos_user_security_logout'));
    }

    public function selectRoleDeveloperAction(){
    	$user = $this->getUser();
    	$user->addRole('ROLE_DEVELOPER');
    	$em = $this->getDoctrine()->getManager();
    	$em->persist($user);
    	$em->flush();
        return $this->redirect($this->generateUrl('fos_user_security_logout'));
    }

    public function componentSearchAction()
    {
        $searchValue = $this->get('request')->request->get('query');
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT r
             FROM Tpoo1WebClientBundle:InstantiatedRefactoring r
             WHERE 
                (r.name LIKE :query 
                OR r.namespace LIKE :query 
                OR r.description LIKE :query 
                OR r.url LIKE :query)'
                .(($this->get('security.context')->isGranted('ROLE_DEVELOPER'))? ' AND r.state BETWEEN 1 AND 3' : ' AND r.state=2 ').
                'AND r.next IS NULL
                ORDER BY r.id DESC'
        )->setParameter('query', "%".$searchValue."%");
        $components = $query->getResult();

        $query = $em->createQuery(
            'SELECT p
             FROM Tpoo1WebClientBundle:RefactoringPackage p
             WHERE 
                (p.name LIKE :query 
                OR p.description LIKE :query)'
                .(($this->get('security.context')->isGranted('ROLE_DEVELOPER'))? ' AND p.state BETWEEN 1 AND 3' : ' AND p.state=2 ').
                'AND p.next IS NULL
                ORDER BY p.id DESC'
        )->setParameter('query', "%".$searchValue."%");

        $moreComponents = $query->getResult();
        $components = array_merge($components,$moreComponents);
        shuffle($components);

        return $this->render('Tpoo1WebClientBundle:Default:search_result.html.twig',
            array(
                "components" => $components
            ));
    }
    
    // 2014-10-19 Lucio Di Giacomo Noack <daniffig@gmail.com>
    // FIXME: Esto lo tenemos que corregir cuando solucionemos otros problemas con Doctrine, por ejemplo, el tema de las dependencias.

    public function componentDetailAction($id){
        $em = $this->getDoctrine()->getManager();
        
        $component = $em->getRepository("Tpoo1WebClientBundle:AccessibilityComponent")
            ->find($id);
            
            if (!$component) {
                $component = $em->getRepository("Tpoo1WebClientBundle:Refactoring")
                  ->find($id);  
                $previous_version = $em->getRepository("Tpoo1WebClientBundle:Refactoring")
                  ->findOneBy(array('next' => $component->getId()));
            }
            else
            {       
              $previous_version = $em->getRepository("Tpoo1WebClientBundle:AccessibilityComponent")
                ->findOneBy(array('next' => $component->getId()));
            }
            
        return $this->render('Tpoo1WebClientBundle:Default:refactoring_detail.html.twig',
            array(
                "component" => $component,
                "previous_version" => $previous_version
            ));
    }
    
    public function componentScriptAction($id, $name) {
    
        $em = $this->getDoctrine()->getManager();
      
        $component = $em->getRepository("Tpoo1WebClientBundle:AccessibilityComponent")
            ->find($id);
            
            if (!$component) {
                $component = $em->getRepository("Tpoo1WebClientBundle:Refactoring")
                  ->find($id);  
                $previous_version = $em->getRepository("Tpoo1WebClientBundle:Refactoring")
                  ->findOneBy(array('next' => $component->getId()));
            }
            else
            {       
              $previous_version = $em->getRepository("Tpoo1WebClientBundle:AccessibilityComponent")
                ->findOneBy(array('next' => $component->getId()));
            }
            
        return new Response(
            $component->getScript(),
            200,
            array('content-type' => 'text/javascript')
        );

        //$response->send();
    }

    
}
