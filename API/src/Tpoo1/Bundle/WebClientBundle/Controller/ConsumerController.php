<?php

namespace Tpoo1\Bundle\WebClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ConsumerController extends Controller
{
    public function getIndexComponents($size)
    {
        $em = $this->getDoctrine()->getManager();   

        $criteria = array("state"=>2, "next"=>NULL);
        $instantiated = $em->getRepository("Tpoo1WebClientBundle:InstantiatedRefactoring")->findBy(
            $criteria, array("id"=>"DESC"), $size);
        $packages = $em->getRepository("Tpoo1WebClientBundle:RefactoringPackage")->findBy(
            $criteria, array("id"=>"DESC"), $size);
        $components = array_merge($instantiated,$packages);
        shuffle($components);
        return $components;
    }

    public function indexAction()
    {
        $maxResults = 5;
                
        $components = $this->getIndexComponents($maxResults);
        
        return $this->render('Tpoo1WebClientBundle:Consumer:index.html.twig', array(
               "components" => $components
            ));    }

    public function searchRefactoringAction()
    {
        return $this->render('Tpoo1WebClientBundle:Consumer:searchRefactoring.html.twig', array(
                // ...
            ));    }

    public function viewRefactoringAction()
    {
        return $this->render('Tpoo1WebClientBundle:Consumer:viewRefactoring.html.twig', array(
                // ...
            ));    }

}
