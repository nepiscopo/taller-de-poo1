<?php

namespace Tpoo1\Bundle\WebClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;

use Tpoo1\Bundle\WebClientBundle\Entity\InstantiatedRefactoring;
use Tpoo1\Bundle\WebClientBundle\Entity\GenericRefactoring;
use Tpoo1\Bundle\WebClientBundle\Entity\Refactoring;
use Tpoo1\Bundle\WebClientBundle\Entity\RefactoringPackage;
use Tpoo1\Bundle\WebClientBundle\Entity\RefactoringState;

class DeveloperController extends Controller
{
    public function getIndexComponents($size)
    {
        $em = $this->getDoctrine()->getManager();   

        $criteria = array("state"=>array(1,2,3), "next"=>NULL);

        return $em->getRepository("Tpoo1WebClientBundle:InstantiatedRefactoring")->findBy(
            $criteria, array("id"=>"DESC"), $size);
    }

    public function indexAction()
    {
        $maxResults = 10;
                
        $components = $this->getIndexComponents($maxResults);
        
        return $this->render('Tpoo1WebClientBundle:Consumer:index.html.twig', array(
               "components" => $components
            ));    }

    public function refactoringFormAction()
    {
        return $this->render('Tpoo1WebClientBundle:Developer:refactoringForm.html.twig');    
    }

    public function refactoringPackageFormAction()
    {
        return $this->render('Tpoo1WebClientBundle:Developer:refactoringPackageForm.html.twig');    
    }

    public function refactoringPackageNewVersionFormAction($id)
    {
        return $this->render('Tpoo1WebClientBundle:Developer:refactoringPackageForm.html.twig',
            array("id"=>$id));    
    }

    private function isJavascript($name){
        return preg_match("/(.*).\js/", $name, $array);
    }
    
    // 2014-09-19 Lucio Di Giacomo Noack <daniffig@gmail.com>
    // Método que devuelve la última versión de un refactoring para un determinado namespace. FIXME: esto deberíamos volcarlo luego en algún repositorio.
    private function getLastRefactoringByNamespace($namespace)
    {
        $em = $this->getDoctrine()->getManager();
                
        $query = $em->createQuery("SELECT r FROM Tpoo1WebClientBundle:Refactoring r WHERE r.namespace LIKE :namespace ORDER BY r.id DESC")->setParameter('namespace', '%' . $namespace . '%');
        
        if (count($results = $query->getResult()))
        {
          return $results[0];
        }
    }

    private function validateMetadataInstantiated($metadata){
        return (isset($metadata->name) && isset($metadata->description) &&
            isset($metadata->id) && isset($metadata->version) &&
            isset($metadata->host) && isset($metadata->dependency));
    }

    private function validateMetadataGeneric($metadata){
        return (isset($metadata->name) && isset($metadata->description) &&
            isset($metadata->id) && isset($metadata->version));
    }

    private function getMetadataFromInstantiatedRefactoring($file){
        $data = array();
        try{
            $contents = file_get_contents($file);

            $json = explode("metadata = {",$contents);
            
            if(isset($json[1]))
                $json = explode("};",$json[1]);
            else
                $json = explode("};",$json[0]);

            $json = json_decode("{".$json[0]."}");
            if($this->validateMetadataInstantiated($json)){
                if($json->type == "instantiated"){
                    $data = array(
                        "name"              => $json->name,
                        "description"       => $json->description,
                        "namespace"         => $json->id,
                        "version"           => $json->version,
                        "url"               => $json->host,
                        "dependency"        => $json->dependency,
                        "script"            => $contents
                    );

                    $anotherRefactoring = $this->getLastRefactoringByNamespace($data["namespace"]);
                    if(($anotherRefactoring)&&($anotherRefactoring->getAuthor())&&($anotherRefactoring->getAuthor()->getId()!=$this->getUser()->getId())){
                        $this->get('session')->getFlashBag()->add('error','The namespace defined in metadata already exists.');
                        return false;
                    }
                    return $data;
                }else{
                    $this->get('session')->getFlashBag()->add('error','Uploaded file is not an instantiated refactoring.');
                    return false;
                }
            }else{
                $this->get('session')->getFlashBag()->add('error','Some fields from metadata are missing.');
                return false;
            }
        }catch(Exception $e){
            $this->get('session')->getFlashBag()->add('error','Errors during the upload.');
            return false;
        }
    }

    public function hasDependency($metadata)
    {
        return ($metadata["dependency"]!="")&&($metadata["dependency"]!=null);
    }

    public function getDependency($dependency)
    {
        // 2014-10-19 Lucio Di Giacomo <daniffig@gmail.com>
        // De la forma que estaba implementado originalmente, no encuentra nunca nada, lo que nos trae varios problemas.
        $em = $this->getDoctrine()->getManager();
                
        $query = $em->createQuery("SELECT r FROM Tpoo1WebClientBundle:Refactoring r WHERE r.namespace LIKE :namespace")->setParameter('namespace', '%' . $dependency . '%');
        
        if (count($results = $query->getResult()) > 0)
        {
          return $results[0];
        }
    }

    private function createInstantiatedRefactoring($metadata){
        $em = $this->getDoctrine()->getManager();
        $iRefactoring = new InstantiatedRefactoring();
        $iRefactoring->setName($metadata["name"]);
        $iRefactoring->setDescription($metadata["description"]);
        $iRefactoring->setNamespace($metadata["namespace"]);
        $iRefactoring->setVersion($metadata["version"]);
        $iRefactoring->setUrl($metadata["url"]);
        $iRefactoring->setScript($metadata["script"]);
        $iRefactoring->setAuthor($this->getUser());
        $aState = $em->getRepository("Tpoo1WebClientBundle:Development")
            ->findOneByName('Development');
        $iRefactoring->setState($aState);

        $previousVersion = $this->getLastRefactoringByNamespace($metadata["namespace"]);
        if($previousVersion){
            $iRefactoring->setPrevious($previousVersion);              
        }

        if($generic = $this->getDependency($metadata["dependency"])){
            $iRefactoring->setDependency($generic);
            $em->persist($iRefactoring);
            
            $em->flush();

            if($previousVersion){
                $previousVersion->setNext($iRefactoring);
                $em->persist($previousVersion);
            }

            $em->flush();

            $developer = $em->getRepository("Tpoo1WebClientBundle:User")
                ->find($this->getUser()->getId());
            $developer->addDevelopedComponent($iRefactoring);
            $em->persist($developer);

            $em->flush();
            
            return $iRefactoring;
        }else{
            $this->get('session')->getFlashBag()->add('error','The dependency doesn\'t exist in the system.');
        }
    }

    public function uploadRefactoringAction(Request $request)
    {
        foreach($request->files as $uploadedFile) {
            if($this->isJavascript($uploadedFile->getClientOriginalName())){
                if($metadata = $this->getMetadataFromInstantiatedRefactoring($uploadedFile->getPathname())){                
                    $this->createInstantiatedRefactoring($metadata);
                }
            }else{
                $this->get('session')->getFlashBag()->add('error','You uploaded a non javascript file. Be sure it has the ".js" extension.');
            }
        }
        return $this->redirect($this->generateUrl('tpoo1_web_client_developer_developed_refactorings'));
    }

    public function uploadRefactoringPackageAction(Request $request)
    {
        if ($request->isMethod('POST')) {

            $name = $this->get('request')->request->get('name');
            $version = $this->get('request')->request->get('version');
            $description = $this->get('request')->request->get('description');
            $fileOrder = $this->get('request')->request->get('file-order');            
            
            $em = $this->getDoctrine()->getManager();

            $package = new RefactoringPackage();
            $package->setName($name);
            $package->setDescription($description);
            $package->setVersion($version);
            $package->setAuthor($this->getUser());
            $aState = $em->getRepository("Tpoo1WebClientBundle:Development")
                ->findOneByName('Development');
            $package->setState($aState);

            if($this->get('request')->request->get('previous')){
                $previousVersion = $em->getRepository("Tpoo1WebClientBundle:RefactoringPackage")
                    ->find($this->get('request')->request->get('previous'));
                if($previousVersion->getAuthor()->getId()==$this->getUser()->getId())
                    $package->setPrevious($previousVersion);
            }

            // FIXME Esto seguramente se pueda hacer de una manera mucho más eficiente.
            foreach ($fileOrder as $fileInput)
            {            
              foreach($request->files->get("file") as $uploadedFile) {
                  if ($fileInput == $uploadedFile->getClientOriginalName())
                  {              
                    if($this->isJavascript($uploadedFile->getClientOriginalName())){
                        // FIXME Esto tal vez deberíamos encararlo de otra manera, ya que si ocurre un error con algún refactoring no queremos guardar todos los demás.
                        if($metadata = $this->getMetadataFromInstantiatedRefactoring($uploadedFile->getPathname())){                
                            $refactoring = $this->createInstantiatedRefactoring($metadata);
                        }else{
                            //is a generic refactoring
                            $metadata = $this->getMetadataFromGenericRefactoring($uploadedFile->getPathname());
                            $refactoring = $this->createGenericRefactoring($metadata);
                        }
                        
                        if ($refactoring)
                        {
                          $package->addRefactoring($refactoring);
                        }
                    }
                    else
                    {
                        $this->get('session')->getFlashBag()->add('error','You uploaded a non javascript file. Be sure it has the ".js" extension.');
                    }
                  }
              }
            }
            
            $em->persist($package);
            $em->flush();
            
            $developer = $em->getRepository("Tpoo1WebClientBundle:User")
                ->find($this->getUser()->getId());
            $developer->addDevelopedComponent($package);
            
            $em->persist($developer);            
            $em->flush();
        }
        return $this->redirect($this->generateUrl('tpoo1_web_client_developer_developed_refactorings'));
    }

    public function genericRefactoringFormAction()
    {
        return $this->render('Tpoo1WebClientBundle:Developer:genericRefactoringForm.html.twig');    
    }

    private function getMetadataFromGenericRefactoring($file){
        $data = array();
        try{
            $contents = file_get_contents($file);

            $json = explode("metadata = {",$contents);
            
            if(isset($json[1]))
                $json = explode("};",$json[1]);
            else
                $json = explode("};",$json[0]);

            $json = json_decode("{".$json[0]."}");
            if($this->validateMetadataGeneric($json)){
                if($json->type == "generic"){
                    $data = array(
                        "name"              => $json->name,
                        "description"       => $json->description,
                        "namespace"         => $json->id,
                        "version"           => $json->version,
                        "script"            => $contents
                    );

                    $anotherRefactoring = $this->getLastRefactoringByNamespace($data["namespace"]);
                    if(($anotherRefactoring)&&($anotherRefactoring->getAuthor())&&($anotherRefactoring->getAuthor()->getId()!=$this->getUser()->getId())){
                        $this->get('session')->getFlashBag()->add('error','The namespace defined in metadata already exists.');
                        return false;
                    }
                }else{
                    $this->get('session')->getFlashBag()->add('error','Uploaded file is not a generic refactoring.');
                    return false;
                }
                return $data;
            }else{
                $this->get('session')->getFlashBag()->add('error','Some fields from metadata are missing.');
                return false;
            }
        }catch(Exception $e){
            $this->get('session')->getFlashBag()->add('error','Errors during the upload.');
            return false;
        }
    }

    private function createGenericRefactoring($metadata){
        $em = $this->getDoctrine()->getManager();
        $gRefactoring = new GenericRefactoring();
        $gRefactoring->setName($metadata["name"]);
        $gRefactoring->setDescription($metadata["description"]);
        $gRefactoring->setNamespace($metadata["namespace"]);
        $gRefactoring->setVersion($metadata["version"]);
        $gRefactoring->setScript($metadata["script"]);
        $gRefactoring->setAuthor($this->getUser());
        // ... se rompe en esta línea.
        $aState = $em->getRepository("Tpoo1WebClientBundle:Development")
            ->findOneByName('Development');
        $gRefactoring->setState($aState);

        $previousVersion = $this->getLastRefactoringByNamespace($metadata['namespace']);
        
        if($previousVersion){
            $gRefactoring->setPrevious($previousVersion);              
        }

        $em->persist($gRefactoring);
        $em->flush();
        
        if($previousVersion){
            $previousVersion->setNext($gRefactoring);
            $em->persist($previousVersion);
        }
        $developer = $em->getRepository("Tpoo1WebClientBundle:User")
            ->find($this->getUser()->getId());
        $developer->addDevelopedComponent($gRefactoring);
        $em->persist($developer);

        $em->flush();
        
        return $gRefactoring;
    }

    public function uploadGenericRefactoringAction(Request $request)
    {
        foreach($request->files as $uploadedFile) {
            if($this->isJavascript($uploadedFile->getClientOriginalName())){
                if($metadata = $this->getMetadataFromGenericRefactoring($uploadedFile->getPathname())){                
                    $this->createGenericRefactoring($metadata);  
                }
            }else{
                $this->get('session')->getFlashBag()->add('error','You uploaded a non javascript file. Be sure it has the ".js" extension.');
            }
        }
        return $this->redirect($this->generateUrl('tpoo1_web_client_developer_developed_refactorings'));
    }

    public function developedRefactoringsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $grefactorings = $em->getRepository("Tpoo1WebClientBundle:GenericRefactoring")
                            ->findBy(array("author" => $this->getUser()->getId(), "next" => NULL));
        $irefactorings = $em->getRepository("Tpoo1WebClientBundle:InstantiatedRefactoring")
                            ->findBy(array("author" => $this->getUser()->getId(), "next" => NULL));
        $packages = $em->getRepository("Tpoo1WebClientBundle:RefactoringPackage")
                            ->findBy(array("author" => $this->getUser()->getId(), "next" => NULL));
        
        return $this->render('Tpoo1WebClientBundle:Developer:developedRefactorings.html.twig', 
            array(
                "grefactorings"    => $grefactorings,
                "irefactorings"    => $irefactorings,
                "packages"        => $packages
            )
        );    
    }

    public function editRefactoringAction()
    {
        //new version
        return $this->render('Tpoo1WebClientBundle:Developer:editRefactoring.html.twig', array(
                // ...
            ));    }

    public function updateRefactoringAction()
    {
        return $this->render('Tpoo1WebClientBundle:Developer:updateRefactoring.html.twig', array(
                // ...
            ));    }

    public function setStateAction($id, $stateId)
    {
        $em = $this->getDoctrine()->getManager();
        $state = $em->getRepository("Tpoo1WebClientBundle:RefactoringState")->find($stateId);
        $component = $em->getRepository("Tpoo1WebClientBundle:AccessibilityComponent")->find($id);
        if(!$component){
            $component = $em->getRepository("Tpoo1WebClientBundle:Refactoring")->find($id);
            if(($component)&&($component->getAuthor()->getId() == $this->getUser()->getId())){
                $component->setState($state);
                $em->persist($component);
                $em->flush();
            }
            return $this->redirect($this->generateUrl('tpoo1_web_client_developer_developed_refactorings'));
        }
    }

}
