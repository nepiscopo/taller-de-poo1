jQuery('input#file').on('change', function() {
  jQuery('select#file-order').html(null);
  
  for (var i = 0; i < this.files.length; i++) {  
    jQuery('select#file-order').append(new Option(this.files[i].name, this.files[i].name));   
  }
});

jQuery('button#file-order-up').on('click', function () {
  jQuery('select#file-order option:selected').each(function (key, value) {  
    if (jQuery(value).prev().length == 0) {
      return false;
    }
    else {    
      jQuery(value).insertBefore(jQuery(value).prev());
    }
  });
});

jQuery('button#file-order-down').on('click', function () {    
  jQuery(jQuery('select#file-order option:selected').get().reverse()).each(function (key, value) {  
    if (jQuery(value).next().length == 0) {
      return false;
    }
    else {    
      jQuery(value).insertAfter(jQuery(value).next());
    }
  });
});

jQuery('button.file-order-button').on('click', function (event) {
  event.preventDefault();
});

jQuery('form#refactoring_package_form').on('submit', function (event) {
  jQuery('select#file-order option').each(function (key, value) {
    jQuery(value).prop('selected', true);
  });
});
