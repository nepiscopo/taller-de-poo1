/*
function installComponent(event, url, dependencyUrl) {
	
	if(dependencyUrl!="")
		installComponent(event, dependencyUrl, "");	  
	event.preventDefault();
	var object = jQuery('<object data="'+url+'"></object>');
	jQuery(object).attr('data', url);
	jQuery(object).css('display', 'none');
	jQuery('body').append(object);
	
}
*/

/**
 * components must be an array instance with the components's script in installation 
 * order. Every script must be encoded in base64.
 **/
function installComponent(components){
	var installEvent = new CustomEvent('installAccessibilityComponents', 
		{ 'components': components });
	window.dispatchEvent(installEvent);
}