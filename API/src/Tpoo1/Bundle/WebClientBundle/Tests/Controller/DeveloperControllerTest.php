<?php

namespace Tpoo1\Bundle\WebClientBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DeveloperControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/index');
    }

    public function testRefactoringform()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/add-refactoring');
    }

    public function testUploadrefactoring()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/upload-refactoring');
    }

    public function testDevelopedrefactorings()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'developed-refactorings');
    }

    public function testEditrefactoring()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/edit-refactoring/:id');
    }

    public function testUpdaterefactoring()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/update-refactoring/:id');
    }

}
