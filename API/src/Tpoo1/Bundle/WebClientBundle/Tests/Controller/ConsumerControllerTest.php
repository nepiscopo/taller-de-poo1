<?php

namespace Tpoo1\Bundle\WebClientBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ConsumerControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/index');
    }

    public function testSearchrefactoring()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/search-refactoring/:term');
    }

    public function testViewrefactoring()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/view-refactoring/:id');
    }

}
