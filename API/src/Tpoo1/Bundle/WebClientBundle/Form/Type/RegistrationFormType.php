<?php

namespace Tpoo1\Bundle\WebClientBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationFormType extends BaseType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    parent::buildForm($builder, $options);
  }

  public function getParent()
  {
    return 'fos_user_registration';
  }

  public function getName()
  {
    return 'webclient_user_registration';
  }
}
