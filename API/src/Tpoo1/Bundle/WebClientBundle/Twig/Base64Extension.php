<?php

namespace Tpoo1\Bundle\WebClientBundle\Twig;

class Base64Extension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('base64', array($this, 'base64Filter')),
        );
    }

    public function base64Filter($text)
    {
        return base64_encode($text);
    }

    public function getName()
    {
        return 'base64_extension';
    }
}