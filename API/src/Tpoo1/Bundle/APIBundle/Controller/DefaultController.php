<?php

namespace Tpoo1\Bundle\APIBundle\Controller;

use Tpoo1\Bundle\WebClientBundle\Entity\Refactoring;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('Tpoo1APIBundle:Default:index.html.twig', array('name' => $name));
    }

    public function getRefactoringUpdateAction($namespace, $actualVersion)
    {
    	$namespace = base64_decode($namespace);
    	$actualVersion = base64_decode($actualVersion);
    	$manager = $this->getDoctrine()->getManager();
    	$criteria = array("state"=>2, "next"=>NULL, "namespace" => $namespace);
		$lastVersion = $manager->getRepository("Tpoo1WebClientBundle:GenericRefactoring")
    		->findOneBy($criteria);
    	if($lastVersion==NULL)
    		$lastVersion = $manager->getRepository("Tpoo1WebClientBundle:InstantiatedRefactoring")
    		->findOneBy($criteria);	
    	if($lastVersion==NULL){
            $response = new Response(json_encode(array(
                "response" => "Accesibility component doesn't exist."
            )));
        }else{
            $response = new Response(json_encode(array(
        		"response" => "Up to date"
        	)));
        	if(($lastVersion!=NULL)&&($lastVersion->getVersion()!=$actualVersion)){
        		$response = new Response(json_encode(array(
        			"response" => "Success",
        			"name" => $lastVersion->getName(),
        			"version" => $lastVersion->getVersion(),
        			"description" => $lastVersion->getDescription(),
        			"script" => base64_encode($lastVersion->getScript()),
        			"encriptationType" => "base64"
        		)));
        	}
    		$response->headers->set('Content-Type', 'application/json');
        }
        return $response;
    }
}
